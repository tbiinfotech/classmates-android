package com.brst.classmates.service;

import android.util.Log;

import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.HashMap;


/**
 * Created by brst-pc89 on 7/17/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.d("sdsddd", "Refreshed token: " + refreshedToken);

        addDataToSharedPreference(refreshedToken);
    }

    private void addDataToSharedPreference(String refreshedToken) {

        String key=SharedPreference.getInstance().getData(getApplicationContext(),Constant.KEY_NAME);

        if(!key.equals("null")) {

            HashMap hashMap=new HashMap<>();
            hashMap.put("token",refreshedToken);
            FirebaseDatabase.getInstance().getReference("registered_user").child(key).updateChildren(hashMap);
        }
        else
        {
            Log.d("sdsddd", "Refreshed token: " );
        }
    }
}
