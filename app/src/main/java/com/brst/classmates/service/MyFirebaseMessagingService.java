package com.brst.classmates.service;

import android.app.Fragment;
import android.app.NotificationManager;
import android.app.Notification;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.brst.classmates.R;
import com.brst.classmates.activity.MainActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.FirebaseData;
import com.brst.classmates.fragments.MessageBoardFragment;
import com.brst.classmates.fragments.OneToOneChatFragment;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.brst.classmates.activity.WelcomeActivity.context;

/**
 * Created by brst-pc89 on 7/17/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String groupID, groupName, message, senderName, userList, courseKey, owner, image, chat;
    Uri notificationSound;

    JSONObject object;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d("notification", "From: " + remoteMessage.getFrom());
        Log.d("notification", "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.d("notification", "Notification Message Body: " + remoteMessage.getData());
    /*    try {
            object = new JSONObject(remoteMessage.getData().get("body"));
            if (object.has("msg_key")) {

                String msg_key = object.getString("msg_key");
                FirebaseData firebaseData = new FirebaseData();
                firebaseData.setDeliver(true);

                HashMap hashMap = new HashMap();
                hashMap.put("deliver", true);
                FirebaseDatabase.getInstance().getReference("group").child(courseKey).child(groupID).child("chat").child(msg_key).updateChildren(hashMap);
            }
        } catch (JSONException e) {

        }*/
        if (!MessageBoardFragment.active && !OneToOneChatFragment.active) {
            Log.d("notification", "wewerr-----");
            //   RemoteMessage.Notification notification=remoteMessage.getNotification();
            RemoteMessage.Notification notification = remoteMessage.getNotification();

            notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

            try {

                //Log.d("fgfgsd",notification.getBody());
                String abc = remoteMessage.getData().get("body");

                object = new JSONObject(remoteMessage.getData().get("body"));

                if (object.has(Constant.GROUP_kEY)) {
                    groupID = object.getString(Constant.GROUP_kEY);
                }
                if (object.has(Constant.GROUP_NAME)) {
                    groupName = object.getString(Constant.GROUP_NAME);
                }
                if (object.has(Constant.MESSAGE)) {
                    message = object.getString(Constant.MESSAGE);
                }
                if (object.has(Constant.SENDER_NAME)) {
                    senderName = object.getString(Constant.SENDER_NAME);
                }
                if (object.has(Constant.USER_LIST)) {
                    userList = object.getString(Constant.USER_LIST);
                }
                if (object.has(Constant.COURSE_KEY)) {
                    courseKey = object.getString(Constant.COURSE_KEY);
                }
                if (object.has(Constant.OWNER)) {
                    owner = object.getString(Constant.OWNER);
                }
                if (object.has(Constant.IMAGE_NAME)) {
                    image = object.getString(Constant.IMAGE_NAME);
                }
                if (object.has(Constant.CHAT)) {
                    chat = object.getString(Constant.CHAT);
                }


                Log.e("Values", "" + groupID + "  " + groupName + "  " + message + "  " + senderName);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("tyValues", "" + groupID + "  " + groupName + "  " + message + "  " + senderName);
            Intent intent = new Intent(context, WelcomeActivity.class);
            Bundle extras = new Bundle();
            //     if (!object.has("chat")) {
            extras.putString("notification", "notification");
            //   }

            if (groupName != null && !groupName.isEmpty()) {
                extras.putString(Constant.GROUP_NAME, groupName);
            }
            if (message != null && !message.isEmpty()) {
                extras.putString(Constant.TEXT_NAME, message);
            }
            if (userList != null && !userList.isEmpty()) {
                extras.putString(Constant.USER_LIST, userList);
            }
            if (groupID != null && !groupID.isEmpty()) {
                extras.putString(Constant.GROUP_kEY, groupID);
            }
            if (courseKey != null && !courseKey.isEmpty()) {
                extras.putString(Constant.COURSE_KEY, courseKey);
            }
            if (owner != null && !owner.isEmpty()) {
                extras.putString(Constant.OWNER, owner);
            }
            if (image != null && !image.isEmpty()) {
                extras.putString(Constant.IMAGE_NAME, image);
            }

            if (chat != null && !chat.isEmpty()) {
                extras.putString(Constant.CHAT, chat);
            }

            if (senderName != null && !senderName.isEmpty()) {
                extras.putString(Constant.SENDER_NAME, senderName);
            }


            intent.putExtras(extras);

            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT
            );

            //  if (!MessageBoardFragment.active ) {
            showSmallNotification(mBuilder, notification.getTitle(), notification.getBody(), resultPendingIntent);
            //  }
        }
    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder, String title, String message, PendingIntent resultPendingIntent) {

        NotificationCompat.BigTextStyle inboxStyle = new NotificationCompat.BigTextStyle();

        inboxStyle.bigText(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_logo)
                //  notification=mBuilder.setTicker(title)
                .setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(notificationSound)
                .setStyle(inboxStyle)
                //    .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_app_logo_new))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }
}

