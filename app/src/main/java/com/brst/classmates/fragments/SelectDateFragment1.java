package com.brst.classmates.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.brst.classmates.interfaces.DateTimeInterface;

import java.util.Calendar;

/**
 * Created by brst-pc89 on 7/24/17.
 */

public class SelectDateFragment1 extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    static DateTimeInterface dateTimeInterfacee;

    public static SelectDateFragment getInstance(DateTimeInterface dateTimeInterface) {
        dateTimeInterfacee = dateTimeInterface;
        return new SelectDateFragment();
    }

    public SelectDateFragment1() {

    }

    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        String month = MONTHS[mm];
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        dateTimeInterfacee.setOnDate(year, month , dayOfMonth);

    }


}
