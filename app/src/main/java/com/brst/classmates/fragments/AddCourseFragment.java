package com.brst.classmates.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.ConnectivityReceiver;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import static com.brst.classmates.classses.ReplaceFragment.popBackStack;
import static com.brst.classmates.classses.ShowDialog.networkDialog;
import static com.seatgeek.placesautocomplete.Constants.LOG_TAG;

public class AddCourseFragment extends Fragment implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    ImageView cou_bck_iv;

    View view = null;

    TextView add_tv;
    String userKey, coursename, credit, spinner_val, universityName, courseKey,collegeKey;

    EditText coursename_et, credit_et;
    Spinner term_spinner;
    boolean visible = false;

    HashMap<String, String> hashmap;

    SearchStudentFragment searchStudentFragment = new SearchStudentFragment();

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, reference;


    public AddCourseFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_add_course, container, false);
        }

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(Constant.UNIVERSITY_NAME)) {

            universityName = bundle.getString(Constant.UNIVERSITY_NAME);

        }

        bindViews(view);

        setListener();

        userKey = SharedPreference.getInstance().getData(getContext(), Constant.KEY_NAME);
        collegeKey = StoreData.getCollegeKeyFromSharedPre(getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("course").child(userKey);

        return view;
    }

    private void bindViews(View view) {

        cou_bck_iv = (ImageView) view.findViewById(R.id.cou_bck_iv);

        add_tv = (TextView) view.findViewById(R.id.add_tv);

        coursename_et = (EditText) view.findViewById(R.id.coursename_et);
        credit_et = (EditText) view.findViewById(R.id.credit_et);

        term_spinner = (Spinner) view.findViewById(R.id.term_spinner);
    }

    private void setListener() {
        cou_bck_iv.setOnClickListener(this);
        add_tv.setOnClickListener(this);


    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.GONE);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.cou_bck_iv:

                hideSoftKeyboard(getContext());

                popBackStack(((WelcomeActivity) getActivity()));


                break;

            case R.id.add_tv:

                hideSoftKeyboard(getContext());

                getFieldData(v);


        }

    }

    private void getFieldData(View v) {

        coursename = coursename_et.getText().toString();
        credit = credit_et.getText().toString();
        spinner_val = term_spinner.getSelectedItem().toString();


        checkFieldEmpty(v);

    }

    private void checkFieldEmpty(View v) {

        if (coursename.isEmpty()) {
            ShowDialog.showSnackbarError(v, "Please enter course name", getContext());
        } else {

            if (!NetworkCheck.isNetworkAvailable(getContext())) {

                ShowDialog.networkDialog(getContext());
            } else {

                ShowDialog.showDialog(getContext());
                new AsyncCaller().execute();


            }
        }
    }

    private void checkUserCourseExist() {

        ShowDialog.showDialog(getContext());


        hashmap = new HashMap<>();
        hashmap.put("course", coursename);
        hashmap.put("credit", credit);
        hashmap.put("term", spinner_val);
        hashmap.put(Constant.KEY_NAME, StoreData.getUserKeyFromSharedPre(getContext()));

        Query query = databaseReference.orderByChild("course").equalTo(coursename);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    Toast.makeText(getActivity(), "existuser", Toast.LENGTH_LONG).show();

                } else {
                    popBackStack(((WelcomeActivity) getActivity()));
                    addCourseToUser();

                }
                ShowDialog.hideDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
    }

    private void addCourseToUser() {

        HashMap map = new HashMap();
        map.put(courseKey, Constant.COURSE_NAME);


        databaseReference.updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {
                    //  Toast.makeText(getContext(), "success", Toast.LENGTH_SHORT).show();


                } else {
                    //  Toast.makeText(getContext(), "fail", Toast.LENGTH_SHORT).show();
                }

                if (visible == true) {

                    Toast.makeText(getContext(), "Subject added successfully", Toast.LENGTH_SHORT).show();

                    popBackStack(((WelcomeActivity) getActivity()));

                }


                ShowDialog.hideDialog();

            }
        });

    }


    private void checkCourseUniversityExist() {

     //   reference = firebaseDatabase.getReference("university").child(universityName);
        reference = firebaseDatabase.getReference("subjects").child(collegeKey);
       // Query query = reference.orderByChild("course").equalTo(coursename);
        Query query = reference.orderByChild("subject").equalTo(coursename);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {

                    Toast.makeText(getContext(), "This course already exist", Toast.LENGTH_SHORT).show();

                    visible = false;

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                        courseKey = snapshot.getKey();

                        addCourseToUser();
                    }


                } else {

                    visible = true;
                    addCourseToUniversity();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
    }

    private void addCourseToUniversity() {

        hashmap = new HashMap<>();
        hashmap.put("subject", coursename);
        hashmap.put(Constant.CREDIT_NAME, credit);
        hashmap.put(Constant.TERM_NAME, spinner_val);
        hashmap.put(Constant.KEY_NAME, StoreData.getUserKeyFromSharedPre(getContext()));


        courseKey = reference.push().getKey();
        reference.child(courseKey).setValue(hashmap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {


                    addCourseToUser();
                }

            }
        });

    }

    public static void hideSoftKeyboard(Context context) {
        InputMethodManager inputManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);


        View v = ((Activity) context).getCurrentFocus();

        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }

    public static void hideSoftKeyboard(Activity activity) {


        View v = activity.getCurrentFocus();

        if (v == null)
            return;
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        if (!isConnected) {

            ShowDialog.networkDialog(getContext());
            if (ShowDialog.dialog != null) {
                if (ShowDialog.dialog.isShowing()) {
                    ShowDialog.hideDialog();
                }
            }
        } else if (isConnected) {
            if (ShowDialog.network_dialog != null) {
                if (ShowDialog.network_dialog.isShowing()) {
                    ShowDialog.network_dialog.dismiss();
                }
            }

        }

    }

    public static boolean isInternetAccessible(Context context) {

        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Couldn't check internet connection", e);
        }

        return false;


    }

    private class AsyncCaller extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            boolean networkAccess = isInternetAccessible(getContext());
            return networkAccess;
        }

        @Override
        protected void onPostExecute(Boolean networkAccess) {
            super.onPostExecute(networkAccess);

            if (!networkAccess) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {

                        networkDialog(getContext());
                        ShowDialog.hideDialog();
                    }
                }
            }
            else
            {
                checkCourseUniversityExist();
            }
        }
    }

}
