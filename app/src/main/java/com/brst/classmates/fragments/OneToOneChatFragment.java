package com.brst.classmates.fragments;


import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.MesssageBoardadaptor;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.FirebaseData;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.iceteck.silicompressorr.SiliCompressor;

import com.netcompss.ffmpeg4android.CommandValidationException;
import com.netcompss.ffmpeg4android.GeneralUtils;
import com.netcompss.loader.LoadJNI;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.activity.WelcomeActivity.context;

/**
 * A simple {@link Fragment} subclass.
 */
public class OneToOneChatFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener {

    View view;

    String picture_Path, videoPath;
    File destination_path;
    Uri videoUri;
    Uri imageuri, imageUploadUri, downloadImageUri, downloadVideoUri, downloadVideoThumb;

    String image, name, chatKey, loginUserKey, senderName, loginUserImage, to, from, sender, receiver, senderProfile, receiverProfile;

    Bitmap videoThumb;

    ImageView profileUser_iv, back_iv, emoji_iv, send_iv, selectimage_iv;
    TextView headerUser_tv;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, groupReference;
    FirebaseStorage firebaseStorage;
    StorageReference storageReference;

    EmojiconEditText emojiconEditText;
    private EmojIconActions emojIcon;
    public static ArrayList<FirebaseData> chatList = new ArrayList<>();
    public ArrayList<String> tokenList;

    public static final ArrayList<String> compressVideo = new ArrayList<>();
    List<byte[]> byteArrayList = new ArrayList();

    Boolean compress = true;

    MesssageBoardadaptor messsageBoardadaptor;
    RecyclerView message_rv;

    View contentRoot;
    String displayName = null, collegeName, universityName, countryName, degreeName, yearName;

    DataSnapshot chatSnapshot;
    int SELECT_VIDEO = 135, SELECT_FILE = 450;

    private final int STOP_TRANSCODING_MSG = -1;
    private final int FINISHED_TRANSCODING_MSG = 0;
    LoadJNI vk = null;

    static String tempraryAudioName = "testrecord" + SystemClock.currentThreadTimeMillis() + ".mp4";
    String workFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
    public static String tempraryDirectory = Environment.getExternalStorageDirectory() + "/classmate";
    static String tempraryVideoPath = tempraryDirectory + "/" + tempraryAudioName;

    ByteArrayOutputStream baos;
    byte[] byteArray;

    int i = 0;

    LinearLayout header_ll;

    Bundle bundle;

    public static Boolean active = false;

    public OneToOneChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_one_to_one_chat, container, false);

            bindViews();

            setAdaptor();

            loginUserKey = StoreData.getUserKeyFromSharedPre(getContext());
            senderName = StoreData.getUserFullNameFromSharedPre(getContext());
            loginUserImage = StoreData.getProfileFromSharedPre(getContext());

            bundle = getArguments();

            if (bundle != null) {

                image = bundle.getString("imagePath");
                name = bundle.getString("userName");
                chatKey = bundle.getString("chatKey");
                to = bundle.getString("to");
                from = bundle.getString("from");
                sender = bundle.getString("sender");
                receiver = bundle.getString("receiver");
                senderProfile = bundle.getString(Constant.PROFILE_PICTURE);
                collegeName = bundle.getString(Constant.COLLEGE_NAME);
                universityName = bundle.getString(Constant.UNIVERSITY_NAME);
                countryName = bundle.getString(Constant.COUNTRY_NAME);
                degreeName = bundle.getString(Constant.DEGREE_NAME);
                yearName = bundle.getString(Constant.YEAR_NAME);
                receiverProfile = bundle.getString("RECEIVER_PROFILE");
                Glide.with(getContext()).load(image).apply(new RequestOptions().circleCrop().placeholder(R.mipmap.user)).into(profileUser_iv);
                headerUser_tv.setText(name);

            }

            firebaseStorage = FirebaseStorage.getInstance();
            storageReference = firebaseStorage.getReference();
            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference(Constant.INVITED_USER).child(chatKey).child("OneToOneChat");
            groupReference = firebaseDatabase.getReference(Constant.CREATEGROUP_NAME).push().child(chatKey);
            //  databaseReference = firebaseDatabase.getReference(Constant.CREATEGROUP_NAME).push().child(chatKey);
            getSenderReceiverId();

            getChats();

        }


        return view;
    }

    private void setAdaptor() {


        messsageBoardadaptor = new MesssageBoardadaptor(chatList, getContext());

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        message_rv.setLayoutManager(layoutManager);
        message_rv.setAdapter(messsageBoardadaptor);

    }

    private void getSenderReceiverId() {

        tokenList = new ArrayList<>();

        firebaseDatabase.getReference(Constant.INVITED_USER).child(chatKey)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        // Log.d("iddata",dataSnapshot.child("from").getValue()+"---");
                        tokenList.add(dataSnapshot.child("from").getValue().toString());
                        tokenList.add(dataSnapshot.child("to").getValue().toString());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void getChats() {

        chatList.clear();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                chatList = new ArrayList<FirebaseData>();
                chatSnapshot = dataSnapshot;
                Log.d("data", dataSnapshot.getChildrenCount() + "");

                if (dataSnapshot.getValue() != null) {

                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                    while (iterator.hasNext()) {
                        DataSnapshot dataSnapshot1 = iterator.next();

                        //  HashMap hashMap = (HashMap) dataSnapshot1.getValue();

                        FirebaseData firebaseData = new FirebaseData();

                        firebaseData = dataSnapshot1.getValue(FirebaseData.class);
                        chatList.add(firebaseData);
                    }

                    messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /*--------------------------click listener on dialog for capture image and select from gallery-------------------------*/

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            requestAppPermissions(permission, R.string.permission, 54);
        }
    };


    View.OnClickListener cameraClickLictener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
            requestAppPermissions(permission, R.string.permission, 55);

        }
    };

    /*-------------------------------------------capture image and store in sd card-----------------------------*/
    private void capture_image() {
        picture_Path = "";
        destination_path = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = "image";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picture_Path = String.valueOf(new File(f, System.currentTimeMillis() + ".jpg"));
        destination_path = new File(picture_Path);

        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination_path);
        } else {
            imageuri = Uri.fromFile(destination_path);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, Constant.CAMERA_CODE);
    }


    private void bindViews() {

        contentRoot = view.findViewById(R.id.contentRoot);
        profileUser_iv = (ImageView) view.findViewById(R.id.profileUser_iv);
        back_iv = (ImageView) view.findViewById(R.id.back_iv);
        headerUser_tv = (TextView) view.findViewById(R.id.headerUser_tv);

        message_rv = (RecyclerView) view.findViewById(R.id.message_rv);

        emoji_iv = (ImageView) view.findViewById(R.id.emoji_iv);
        send_iv = (ImageView) view.findViewById(R.id.send_iv);
        selectimage_iv = (ImageView) view.findViewById(R.id.selectimage_iv);
        emojiconEditText = (EmojiconEditText) view.findViewById(R.id.emojicon_edit_text);

        header_ll = (LinearLayout) view.findViewById(R.id.header_ll);

        header_ll.setOnClickListener(this);

        emojIcon = new EmojIconActions(getContext(), contentRoot, emojiconEditText, emoji_iv);
        emojIcon.ShowEmojIcon();


        emojiconEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    send_iv.setVisibility(View.VISIBLE);
                    selectimage_iv.setVisibility(View.GONE);

                } else {
                    send_iv.setVisibility(View.GONE);
                    selectimage_iv.setVisibility(View.VISIBLE);
                }


            }

        });

        back_iv.setOnClickListener(this);
        send_iv.setOnClickListener(this);
        selectimage_iv.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back_iv:

                int count = getFragmentManager().getBackStackEntryCount();
                Log.d("cxc", count + "");
                AddCourseFragment.hideSoftKeyboard(getContext());
                // ((WelcomeActivity) getActivity()).finish();

                if (count == 1) {
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), new MenuFragment(), "Menu_Fragment");
                } else {
                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                }

                break;

            case R.id.send_iv:

                sendChat();


                break;

            case R.id.selectimage_iv:

                ShowDialog.cameraDialog(getContext(), onClickListener, cameraClickLictener);

                break;

            case R.id.header_ll:

                if (bundle.containsKey(Constant.COLLEGE_NAME)) {
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), new ChatProfile(), "chat_profile", bundle);
                }
                else
                {
                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                }

                break;
        }
    }

    private void sendChat() {

        String key = databaseReference.push().getKey();
        String msg = emojiconEditText.getText().toString();
    /*    final HashMap hashMap = new HashMap();
        hashMap.put(Constant.TEXT_NAME, msg);
        hashMap.put(Constant.USERID_NAME, loginUserKey);
        hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
        hashMap.put(Constant.PROFILE_PICTURE, image);*/

        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setText(msg);
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(loginUserImage);
        firebaseData.setName(name);

        databaseReference.child(key).setValue(firebaseData);
        // groupReference.child("chat").setValue(firebaseData);

        emojiconEditText.setText("");

        String message = "{\n" +
                "  \"groupKey\": \"" + chatKey + "\",\n" +
                "  \"Message\": \"" + msg + "\",\n" +
                "  \"chat\": \"" + "OneTOOne" + "\",\n" +
                "  \"image\": \"" + loginUserImage + "\",\n" +
                "  \"SenderName\": \"" + senderName + "\"\n" +

                "}";

        HashMap map = new HashMap();
        map.put("title", senderName);
        map.put("body", message);


        getToken(map);
    }

    private void getToken(final HashMap map) {

        String tokenKey = "";

        for (int i = 0; i < tokenList.size(); i++) {
            if (!tokenList.get(i).equals(loginUserKey)) {
                tokenKey = tokenList.get(i);
            }
        }

        if (!tokenKey.equals("") && !tokenList.isEmpty()) {
            FirebaseDatabase.getInstance().getReference("registered_user").child(tokenKey).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String notificetion = dataSnapshot.child(Constant.NOTIFICATION_KEY).getValue().toString();

                    if (notificetion.equals("true")) {
                        String token = dataSnapshot.child("token").getValue().toString();

                        sendNotification(token, map);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    public void sendNotification(String fcmID, HashMap<String, String> map) {


        String body123 = "{\n\t\"to\": \" " + fcmID + "\",\n\t\"notification\" :" + convert(map) + ",\n\t\"data\" :" + convert1(map) + "\n}";

        Log.e("DataToSend", "" + body123);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");


        RequestBody body = RequestBody.create(mediaType, body123);
        Request request = new Request.Builder()
                .url("https://fcm.googleapis.com/fcm/send")
                .post(body)
                .addHeader("authorization", "key=" + Constant.SERVER_KEY)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")

                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    Log.e("ResponseBALLI", response.body().string());
                    Log.d("sa", "sa");
                    Log.i("MESSAGE", response.message());
                }
            }


        });


    }

    public String convert(HashMap<String, String> map) {
        JSONObject obj = null;
        try {


            obj = new JSONObject();
            String message = map.get("body");

            JSONObject meJson = new JSONObject(message);

            //   obj.put("body",  meJson.optString("Message") +" from "+ meJson.optString("groupName") +" by "+ meJson.optString("SenderName") );
            obj.put("body", meJson.optString("Message"));
            obj.put("title", senderName);
            obj.put("icon", R.mipmap.ic_logo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj.toString();
    }


    public String convert1(HashMap<String, String> map) {
        JSONObject obj = null;
        try {

            obj = new JSONObject(map);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj.toString();
    }


    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 54) {
           /* Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image*//*");
            startActivityForResult(galleryIntent, Constant.GALLERY_CODE);
*/
            imageVideoActionSheet();
        }

        if (requestCode == 55) {
            capture_image();

        }

    }

    private void imageVideoActionSheet() {

        final String[] stringItems = {"Image", "Video", "File"};

        final ActionSheetDialog dialog = new ActionSheetDialog(getContext(), stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                    Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, Constant.GALLERY_CODE);


                } else if (position == 1) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_VIDEO);

                } else {
                    // Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    // intent.setType("*/*");
                   /* intent.addCategory(Intent.CATEGORY_OPENABLE);

                    try {
                        startActivityForResult(
                                Intent.createChooser(intent, "Select a File to Upload"),
                                SELECT_FILE);
                    } catch (android.content.ActivityNotFoundException ex) {

                        Toast.makeText(getContext(), "Please install a File Manager.", Toast.LENGTH_SHORT).show();
                    }*/
                    //   ReplaceFragment.replace((WelcomeActivity) getActivity(), new DocumentFragment(), "document_fragment");

                    DocumentFragment documentFragment = new DocumentFragment();
                    documentFragment.setTargetFragment(OneToOneChatFragment.this, SELECT_FILE);
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), documentFragment, "InviteStudent_Fragment");

                }

                dialog.dismiss();
            }
        });
    }

    /*------------------------------------------get image after capture and pic from gallery-----------------------------*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.GALLERY_CODE && resultCode == RESULT_OK) {

            try {

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
                //  Bitmap bitmap = decodeUri(data.getData());

                imageUploadUri = getImageUri(getContext(), bitmap);

                imageUpload();

            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (requestCode == Constant.CAMERA_CODE && resultCode == RESULT_OK) {

            try {


                //  Bitmap bitmap = decodeUri(imageuri);
                //  imageUploadUri = getImageUri(getContext(), bitmap);
                imageUploadUri = imageuri;
                imageUpload();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == SELECT_VIDEO && resultCode == RESULT_OK) {
            videoPath = getPath(data.getData());
            // Log.d("videoSize", videoPath.length() + "----" + videoPath);
            videoUri = data.getData();
            videoThumb = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Images.Thumbnails.MINI_KIND);

            baos = new ByteArrayOutputStream();
            videoThumb.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byteArray = baos.toByteArray();

            File myFile = new File(videoUri.toString());
            String path = getPath(videoUri);

            if (videoUri.toString().startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = getActivity().getContentResolver().query(videoUri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    }
                } finally {
                    cursor.close();
                }
            } else if (videoUri.toString().startsWith("file://")) {
                displayName = myFile.getName();
            }

            displayName = displayName.replace(" ", "");

            tempraryVideoPath = tempraryDirectory + "/" + displayName;

            File file1 = new File(videoPath);

            double fileSizeInBytes = file1.length();

            double fileSizeInKB = fileSizeInBytes / 1024;

            double fileSizeInMB = fileSizeInKB / 1024;

            if (fileSizeInMB <= 10) {

                copyFile(path, displayName);

                FirebaseData firebaseData = new FirebaseData();
                firebaseData.setVideo(tempraryVideoPath);
                firebaseData.setUserid(loginUserKey);
                firebaseData.setTime(SystemTime.getTime());
                firebaseData.setProfile_picture(image);
                firebaseData.setVideoBitmap(videoThumb);
                firebaseData.setFileName(displayName);
                firebaseData.setOriginalPath(videoPath);
                firebaseData.setName(name);

                chatList.add(firebaseData);
                messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);

                uploadVideo(videoPath, displayName, byteArray, "");

            } else if (fileSizeInMB > 100) {
                Toast.makeText(getContext(), "Sorry, media size too large.", Toast.LENGTH_SHORT).show();
            } else {

                copyFile(path, "1" + displayName);

                File f = new File(tempraryDirectory);
                if (f.mkdirs() || f.isDirectory()) {

                    File file = new File(tempraryVideoPath);
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    runTranscoding(displayName, byteArray, videoPath);
                }
            }

            // copyFile(path, displayName);

            // uploadVideo(displayName);


        } else if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
            //try {

            String mime = "";
            Log.d("path", data.getStringExtra(Constant.FILE_NAME) + "");
            String path = data.getStringExtra(Constant.FILE_NAME);
            String fileName[] = path.split("/");
            displayName = fileName[fileName.length - 1];
            Log.d("name", displayName);
            String type = path.substring(path.length() - 3);
             /*   if (filePath.startsWith("file://")) {
                    String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.parse(filePath).getPath());
                    Log.d("file", extension + "");
                    if (extension != null) {
                        mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                        Log.d("type",mime+"-----");
                    }
                } else {
                    ContentResolver cr = getContext().getContentResolver();
                    mime = cr.getType(Uri.parse(filePath));
                    Log.d("type","-----"+mime);
                }*/
            if (type.equals("pdf")) {
                mime = "application/pdf";
            } else if (type.equals("doc")) {
                mime = "application/msword";
            } else {
                mime = "text/plain";
            }
            //      if (mime.equals("text/plain") || mime.equals("application/msword") || mime.equals("application/pdf")) {
            if (mime.equals("text/plain") || mime.equals("application/msword") || mime.equals("application/pdf")) {
                // imageUploadUri = data.getData();
                // File myFile = new File(imageUploadUri.toString());
                File myFile = new File(path);

              /*      if (filePath.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = getActivity().getContentResolver().query(Uri.parse(filePath), null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                            }
                        } finally {
                            cursor.close();
                        }
                    } else {
                        displayName = myFile.getName();
                    }*/
                 /*   String path = "";
                    if (filePath.startsWith("file://")) {
                        path = imageUploadUri.getPath();
                    } else {
                        path = getPath(imageUploadUri);
                    }*/
                copyFile(path, displayName);
                uploadFile(mime, displayName, myFile, path);
            }
           /* } catch (Exception e) {
                Log.d("type", e.getMessage());
            }*/
        }
    }

    private void uploadFile(final String mime, final String displayName, File filePath, String path) {

/*
        HashMap hashMap = new HashMap();
        hashMap.put("file", imageUploadUri.toString());
        hashMap.put(Constant.USERID_NAME, loginUserKey);
        hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
        hashMap.put(Constant.PROFILE_PICTURE, image);
        hashMap.put("type", mime);
        if (displayName != null) {
            hashMap.put("fileName", displayName);
        }*/

        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setFile(path);
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(loginUserImage);
        firebaseData.setType(mime);
        firebaseData.setName(name);

        if (displayName != null) {
            firebaseData.setFileName(displayName);
        }
        chatList.add(firebaseData);
        messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);

        //  messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);
        storageReference.child("file" + "_" + System.currentTimeMillis())
                .putFile(Uri.fromFile(filePath))
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();
                        //   storeImageDatabase();
                        storeFileDatabase(mime, displayName);
                    }
                });


    }


    private void storeFileDatabase(String mime, String displayName) {

        String key = databaseReference.push().getKey();
        /*HashMap hashMap = new HashMap();
        hashMap.put("file", downloadImageUri.toString());
        hashMap.put(Constant.USERID_NAME, loginUserKey);
        hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
        hashMap.put(Constant.PROFILE_PICTURE, image);
        hashMap.put("type", mime);
        hashMap.put("fileName", displayName);*/

        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setFile(downloadImageUri.toString());
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(loginUserImage);
        firebaseData.setFileName(displayName);
        firebaseData.setType(mime);
        firebaseData.setName(name);


        databaseReference.child(key).setValue(firebaseData);
        //  groupReference.child("chat").setValue(firebaseData);

        String message = "{\n" +
                "  \"groupKey\": \"" + chatKey + "\",\n" +
                "  \"Message\": \"" + "File" + "\",\n" +
                "  \"chat\": \"" + "OneTOOne" + "\",\n" +
                "  \"image\": \"" + loginUserImage + "\",\n" +
                "  \"SenderName\": \"" + senderName + "\"\n" +

                "}";


        HashMap map = new HashMap();
        map.put("title", senderName);
        map.put("body", message);

        // getStoreToken(map);

        getToken(map);
        // ShowDialog.hideDialog();

    }

    public void copyFile(String inputPath, String displayName) {

        String tempraryFilePath = tempraryDirectory + "/" + displayName;

        Log.e("path", tempraryFilePath);
        Log.e("inputPath", inputPath);

        if (new File(inputPath).exists()) {
            Log.e("path", inputPath);

            InputStream in = null;
            OutputStream out = null;
            try {                //create output directory if it doesn't exist
                File dir = new File(tempraryDirectory);
                if (!dir.exists()) {
                    Log.e("path", dir + "");
                    dir.mkdirs();
                }
                in = new FileInputStream(inputPath);
                // hiddenVideoPath=hiddenVideoDirectory+"/"+System.currentTimeMillis()+".mp4";
                Log.e("TAG", "hiddenVideoPath " + tempraryFilePath);
                out = new FileOutputStream(tempraryFilePath);
                byte[] buffer = new byte[1024];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                in.close();
                in = null;
                // write the output file
                out.flush();
                out.close();
                out = null;
                // delete the original file                // new File(inputPath + inputFile).delete();
                //   deleteRawData(tempraryDirectory);
            } catch (FileNotFoundException fnfe1) {
                Log.e("tag", fnfe1.getMessage());
            } catch (Exception e) {
                Log.e("tag", e.getMessage());
            }
        } else {
            Log.e("Balli", "temprary video is not available");
            // Toast.makeText(context, "No video is availabe to download", Toast.LENGTH_LONG).show();
        }
    }

    private void uploadVideo(final String videoPath, final String displayName, final byte[] byteArray, final String path) {

        if (videoPath != null) {

            File file = new File(videoPath);
            Uri uri = Uri.fromFile(file);
          /*  FirebaseData firebaseData = new FirebaseData();
            firebaseData.setVideo(videoUri.toString());
            firebaseData.setUserid(loginUserKey);
            firebaseData.setTime(SystemTime.getTime());
            firebaseData.setProfile_picture(image);
            firebaseData.setVideoBitmap(videoThumb);
            firebaseData.setFileName(displayName);
            chatList.add(firebaseData);
            messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);*/


            // Create file metadata including the content type
            StorageMetadata metadata = new StorageMetadata.Builder()
                    .setContentType("video/mp4")
                    .build();

            StorageReference reference = storageReference.child("video" + "_" + SystemClock.currentThreadTimeMillis());
            reference.putFile(uri, metadata).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    downloadVideoUri = taskSnapshot.getDownloadUrl();
                    Log.d("path", downloadVideoUri + "");

                    storeThumbnails(displayName, videoPath, byteArray, path);

                }

            });
        }
    }


    public class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
        }


        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            try {

                filePath = SiliCompressor.with(mContext).compressVideo(paths[0], paths[1]);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);

            Log.d("videoSize", compressedFilePath.length() + "");

        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private void imageUpload() {


    /*    HashMap hashMap = new HashMap();
        hashMap.put(Constant.IMAGE_NAME, imageUploadUri.toString());
        hashMap.put(Constant.USERID_NAME, loginUserKey);
        hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
        hashMap.put(Constant.PROFILE_PICTURE, image);
*/
        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setImage(imageUploadUri.toString());
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(loginUserImage);
        firebaseData.setName(name);

        chatList.add(firebaseData);

        messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);
        storageReference.child("image" + "_" + System.currentTimeMillis())
                .putFile(imageUploadUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();
                        storeImageDatabase();
                    }
                });

    }

    private void storeImageDatabase() {

        String key = databaseReference.push().getKey();
      /*  HashMap hashMap = new HashMap();

        hashMap.put(Constant.IMAGE_NAME, downloadImageUri.toString());
        hashMap.put(Constant.USERID_NAME, loginUserKey);
        hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
        hashMap.put(Constant.PROFILE_PICTURE, image);*/

        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setImage(downloadImageUri.toString());
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(loginUserImage);
        firebaseData.setName(name);


        databaseReference.child(key).setValue(firebaseData);
        //   groupReference.child("chat").setValue(firebaseData);

        String message = "{\n" +
                "  \"groupKey\": \"" + chatKey + "\",\n" +
                "  \"Message\": \"" + "Photo" + "\",\n" +
                "  \"chat\": \"" + "OneTOOne" + "\",\n" +
                "  \"image\": \"" + loginUserImage + "\",\n" +
                "  \"SenderName\": \"" + senderName + "\"\n" +

                "}";

        HashMap map = new HashMap();
        map.put("title", senderName);
        map.put("body", message);


        getToken(map);


    }

    private void storeThumbnails(final String displayName, final String videoPath, byte[] byteArray, final String path) {
        if (byteArray != null) {
            StorageReference ref = storageReference.child("images/" + byteArray);
            ref.putBytes(byteArray).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    downloadVideoThumb = taskSnapshot.getDownloadUrl();

                    storeVideoDatabase(displayName, videoPath, path);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {


                }
            });
        }
    }

    private void storeVideoDatabase(String displayName, String videoPath, String path) {

        String key = databaseReference.push().getKey();
    /*    HashMap hashMap = new HashMap();
        hashMap.put(Constant.VIDEO_NAME, downloadVideoUri.toString());
        hashMap.put(Constant.USERID_NAME, loginUserKey);
        hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
        hashMap.put(Constant.PROFILE_PICTURE, image);
        hashMap.put(Constant.VIDEO_THUMB, downloadVideoThumb.toString());*/

        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setVideo(downloadVideoUri.toString());
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(loginUserImage);
        firebaseData.setVideothumb(downloadVideoThumb.toString());
        firebaseData.setFileName(displayName);
        firebaseData.setName(name);


        if (path != null && !path.isEmpty()) {
            firebaseData.setOriginalPath(path);
        } else {
            firebaseData.setOriginalPath(videoPath);
        }

        databaseReference.child(key).setValue(firebaseData);
        //   groupReference.child("chat").setValue(firebaseData);


        String message = "{\n" +
                "  \"groupKey\": \"" + chatKey + "\",\n" +
                "  \"Message\": \"" + "Video" + "\",\n" +
                "  \"chat\": \"" + "OneTOOne" + "\",\n" +
                "  \"image\": \"" + loginUserImage + "\",\n" +
                "  \"SenderName\": \"" + senderName + "\"\n" +

                "}";

        HashMap map = new HashMap();
        map.put("title", senderName);
        map.put("body", message);

        getToken(map);

        String videoPathFile = tempraryDirectory + "/" + displayName;
        String videoPath1 = tempraryDirectory + "/" + "1" + displayName;
        Log.d("video", videoPath);
        File file = new File(videoPathFile);
        file.delete();
        if (file.exists()) {
            try {
                file.getCanonicalFile().delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file.exists()) {
                getContext().deleteFile(file.getName());
            }
        }
        File file1 = new File(videoPath1);
        file1.delete();
        if (file1.exists()) {
            try {
                file1.getCanonicalFile().delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file1.exists()) {
                getContext().deleteFile(file1.getName());
            }
        }


    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, Constant.TITLE_NAME, null);
        return Uri.parse(path);
    }


    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(selectedImage), null, o2);
    }

    private void runTranscoding(String displayName, byte[] byteArray, String videoPath) {
        //  ShowDialog.showDialog(getContext());
        //  initProgressDialog(context);
        //   ((MainActivity) context).isVideoCreated = false;

        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setVideo(videoPath);
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(loginUserImage);
        firebaseData.setVideoBitmap(videoThumb);
        firebaseData.setFileName(displayName);
        firebaseData.setOriginalPath(videoPath);
        firebaseData.setName(name);
        chatList.add(firebaseData);
        messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);

        compressVideo.add(displayName);
        byteArrayList.add(byteArray);

        compressVideo(videoPath);

      /*  new Thread() {
            public void run() {
                Log.e("TAG", "Worker started");
                try {
                    //    sleep(5000);
                    // runTranscodingUsingLoader();
                    // handler.sendEmptyMessage(FINISHED_TRANSCODING_MSG);
                } catch (Exception e) {
                    ShowDialog.hideDialog();
                    Log.e("TAG", "threadmessage " + e.getMessage());
                }
            }
        }.start(); */       //Log.e(TAG, "runTranscodingThread.getState()  " + runTranscodingThread.getState());    }

    }


    private void compressVideo(final String videoPath) {

        //  Boolean compress = SharedPreference.getInstance().getIsFirstTime(getContext(), "compress");

        if (compress) {

            new Thread() {
                public void run() {
                    Log.e("TAG", "Worker started");
                    try {

                        //    sleep(5000);
                        runTranscodingUsingLoader(compressVideo.get(i), videoPath);
                        handler.sendEmptyMessage(FINISHED_TRANSCODING_MSG);
                    } catch (Exception e) {
                        ShowDialog.hideDialog();
                        Log.e("TAG", "threadmessage " + e.getMessage());
                    }
                }

            }.start();
        }
    }

    private void runTranscodingUsingLoader(String displayName, String videoPath) {

        String tempraryVideoPath = tempraryDirectory + "/" + displayName;
        compress = false;

        String videoPath1 = tempraryDirectory + "/" + "1" + displayName;

        Log.e("TAG", "runTranscodingUsingLoader started...");
        vk = new LoadJNI();
        //  String commandStr = "ffmpeg -y -r 10/1 -i " + tempraryDirectory + "/sharan%d.jpg -i " + tempraryAudioPath + " -strict experimental -ar 44100 -ac 2 -ab 256k -b 2097152 -ar 22050 -vcodec mpeg4 -b 2097152 -s 1280x720 " + tempraryDirectory + "/" + tempraryVideoName;
        //   String commandStr = "ffmpeg -y -i " + videoPath1 + " -strict experimental -s 1024x720 -r 55 -vcodec mpeg4 -b 300k  -ab 48000 -ac 2 -ar 22050 " + tempraryVideoPath;
        //  String commandStr = "ffmpeg -y -i " + videoPath + " -vcodec h264 -b:v 1000k -acodec mp2 " + tempraryVideoPath;
        // String commandStr = "ffmpeg -i " + videoPath + " -vcodec libx264 -crf 20 " + tempraryVideoPath;
        String commandStr = "ffmpeg -y -i " + videoPath1 + " -strict experimental -r 30 -ab 48000 -ac 2 -ar 48000 -vcodec mpeg4 -b 4097152 " + tempraryVideoPath;

        Log.e("TAG", commandStr);
        try {
            Log.e("TAG", "vk.going to run.");
            // running regular command with validation
            vk.run(GeneralUtils.utilConvertToComplex(commandStr), workFolder, context.getApplicationContext());
            Log.e("TAG", "vk.run finished.");

            uploadVideo(tempraryVideoPath, displayName, byteArrayList.get(i), videoPath);

            i++;

            compress = true;
            compressVideo(videoPath);
            //   ShowDialog.hideDialog();
        } catch (CommandValidationException e) {
            Log.e("TAG", "vk run exeption.", e);
            ShowDialog.hideDialog();
        } catch (Throwable e) {
            Log.e("TAG", "vk run exeption.", e);
            ShowDialog.hideDialog();
        }
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e("TAG", "Handler got message");
            //            if (progressDialog != null)
            //            {//                progressDialog.dismiss();
            // stopping the transcoding native
            if (msg.what == STOP_TRANSCODING_MSG) {
                Log.e("TAG", "Got cancel message, calling fexit");
                vk.fExit(context.getApplicationContext());
            }//            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }
}



