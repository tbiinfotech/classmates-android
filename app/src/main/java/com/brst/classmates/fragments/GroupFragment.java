package com.brst.classmates.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.CropImageActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.GroupAdaptor;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.FirebaseData;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.classses.NetworkCheck.isNetworkAvailable;
import static com.brst.classmates.classses.ShowDialog.dialog;
import static com.brst.classmates.classses.ShowDialog.networkDialog;
import static com.brst.classmates.classses.ShowDialog.showSnackbarError;
import static com.seatgeek.placesautocomplete.Constants.LOG_TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener/*, ClickRecyclerInterface*/ {


    RecyclerViewEmptySupport groupmessage_rv;

    /*String name[] = {"group1", "group1", "group1"}, groupKey;*/

    ImageView addcourse_iv, back_msg_iv;

    List<HashMap<String, String>> userList = new ArrayList<>();
    List<HashMap<String, String>> msgList = new ArrayList<HashMap<String, String>>();
    List<String> msg = new ArrayList<>();
    List<String> lasttime = new ArrayList<>();
    List<String> groupKeValue = new ArrayList<>();

    HashMap<String, String> hashMap = new HashMap<>();

    TextView group_tv, users_tv, emptyview;

    Query query;

    FirebaseDatabase firebaseDatabase;

    Uri imageuri, imageUploadUri, downloadImageUri;

    GroupFragment fragmentGroup;

    String picture_Path, collegeName, groupKey;
    File destination_path;

    Fragment fragment, fragmentVisible;

    EditText editText;

    String loginUserKey;

    public static String userKey, university, userFullName;

    DatabaseReference databaseReference, groupContactReference;

    List<HashMap<String, String>> groupName = new ArrayList<>();
    List<HashMap<String, String>> collegeUserList = new ArrayList<>();

    HashMap<String, String> hashMap1;


    GroupAdaptor groupAdaptor;


    public GroupFragment() {
        // Required empty public constructor
    }

    View view;

    boolean isShowing = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        if (view == null) {
            view = inflater.inflate(R.layout.fragment_group, container, false);

            userKey = SharedPreference.getInstance().getData(getContext(), Constant.KEY_NAME);
            university = SharedPreference.getInstance().getData(getContext(), Constant.UNIVERSITY_NAME);
            userFullName = StoreData.getUserFullNameFromSharedPre(getContext());
            collegeName = StoreData.getCollegeFromSharedPre(getContext());

            firebaseDatabase = FirebaseDatabase.getInstance();

            fragmentVisible = getFragmentManager().findFragmentByTag("visibleSearch_fragment");
            bindViews(view);

            setAdaptor();

            ChecNetwork();

            fragment = getFragmentManager().findFragmentByTag("Group_Fragment");

            groupContactReference = firebaseDatabase.getReference(Constant.CREATEGROUP_NAME);

            loginUserKey = StoreData.getUserKeyFromSharedPre(getContext());

            // fetchGroupName();
            setListener();
        }

        return view;
    }

    private void ChecNetwork() {

        if (isNetworkAvailable(getContext())) {

            fetchGroupName();

        } else

        {

            networkDialog(getContext());

            fetchGroupName();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        isShowing = false;

    }

    int a = 0;

    private void fetchGroupName() {


    /*    if (NetworkCheck.isNetworkAvailable(getContext())) {

            if (fragmentVisible == null) {
                ShowDialog.showDialog(getContext());
            }
        }*/
        //  }

        query = firebaseDatabase.getReference(Constant.CREATEGROUP_NAME);

        firebaseDatabase.getReference(Constant.CREATEGROUP_NAME).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.getValue() != null) {

                    getUsers();


                } else {

                    getIndividualChat();

                    groupAdaptor.addData(groupName);
                    groupAdaptor.notifyDataSetChanged();
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });


    }


    private void getUsers() {


        query.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                groupName = new ArrayList<>();

                final Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();

                while (iterator.hasNext()) {

                    DataSnapshot dataSnapshot1 = iterator.next();

                    String courseKey = dataSnapshot1.getKey();
                    Iterator<DataSnapshot> iterator1 = dataSnapshot1.getChildren().iterator();
                    while (iterator1.hasNext()) {


                        String groupIcon = "";
                        HashMap hashMap = new HashMap();
                        DataSnapshot child = iterator1.next();
                        // groupKey = dataSnapshotChild.getKey();

                        // }

                        // }

               /* for (DataSnapshot course : dataSnapshot.getChildren()) {

                    String groupIcon = "";
                    String courseKey = course.getKey();


                    for (DataSnapshot child : course.getChildren()) {
*/
                        groupKey = child.getKey();

                        String usersName = child.child(Constant.USER_NAME).getValue() != null ? child.child(Constant.USER_NAME).getValue().toString() : "";
                        String group = child.child(Constant.GROUP_NAME).getValue() != null ? child.child(Constant.GROUP_NAME).getValue().toString() : "";
                        String owner = child.child(Constant.OWNER).getValue() != null ? child.child(Constant.OWNER).getValue().toString() : "";
                        String mode = child.child("mode").getValue() != null ? child.child("mode").getValue().toString() : "";
                        if (child.child(Constant.GROUP_ICON).exists()) {
                            groupIcon = child.child(Constant.GROUP_ICON).getValue() != null ? child.child(Constant.GROUP_ICON).getValue().toString() : "";
                        }

                        hashMap1 = new HashMap();


                        if (usersName.contains(userKey)) {


                            hashMap1.put(Constant.GROUP_kEY, groupKey);
                            hashMap1.put(Constant.GROUP_NAME, group);
                            hashMap1.put(Constant.COURSE_KEY, courseKey);
                            hashMap1.put(Constant.OWNER, owner);
                            hashMap1.put("mode", mode);
                            if (groupIcon != null) {
                                hashMap1.put(Constant.GROUP_ICON, groupIcon);
                            }
                            hashMap1.put(Constant.USER_NAME, usersName);
                            //    groupName.add(hashMap1);

                            // }

                            Iterator<DataSnapshot> iterator2 = child.child(Constant.CHAT).getChildren().iterator();

                            while (iterator2.hasNext()) {


                                hashMap = new HashMap();
                                DataSnapshot dataSnapshot2 = iterator2.next();

                                if (dataSnapshot2.child(Constant.TEXT_NAME).exists()) {
                                    String lastmsg = dataSnapshot2.child(Constant.TEXT_NAME).getValue().toString();

                                    hashMap.put(Constant.TEXT_NAME, lastmsg);

                                } else if (dataSnapshot2.child(Constant.IMAGE_NAME).exists()) {


                                    hashMap.put(Constant.IMAGE_NAME, Constant.IMAGE_NAME);

                                } else if (dataSnapshot2.child(Constant.VIDEO_NAME).exists()) {

                                    hashMap.put(Constant.VIDEO_NAME, Constant.VIDEO_NAME);
                                } else if (dataSnapshot2.child(Constant.FILE_NAME).exists()) {

                                    hashMap.put(Constant.FILE_NAME, Constant.FILE_NAME);
                                }

                                if (dataSnapshot2.child(Constant.TIME_NAME).exists()) {
                                    String time = dataSnapshot2.child(Constant.TIME_NAME).getValue().toString();

                                    String localTime = SystemTime.getInstance().getLocalTime(time);
                                    String date = SystemTime.getInstance().getDate(time);

                                    hashMap.put(Constant.TIME_NAME, localTime);
                                    hashMap.put("date", date);

                                }

                            }


                            if (hashMap.containsKey(Constant.TEXT_NAME)) {
                                String text = hashMap.get(Constant.TEXT_NAME).toString();
                                hashMap1.put(Constant.TEXT_NAME, text);
                            }
                            if (hashMap.containsKey(Constant.IMAGE_NAME)) {
                                String image = hashMap.get(Constant.IMAGE_NAME).toString();
                                hashMap1.put(Constant.IMAGE_NAME, image);
                            }
                            if (hashMap.containsKey(Constant.VIDEO_NAME)) {
                                String video = hashMap.get(Constant.VIDEO_NAME).toString();
                                hashMap1.put(Constant.VIDEO_NAME, video);
                            }
                            if (hashMap.containsKey(Constant.FILE_NAME)) {
                                String file = hashMap.get(Constant.FILE_NAME).toString();
                                hashMap1.put(Constant.FILE_NAME, file);
                            }
                            if (hashMap.containsKey(Constant.TIME_NAME)) {
                                String time = hashMap.get(Constant.TIME_NAME).toString();
                                hashMap1.put(Constant.TIME_NAME, time);
                            }
                            if (hashMap.containsKey("date")) {
                                hashMap1.put("date", hashMap.get("date").toString());
                            }



                          /*  final Query lastQuery = FirebaseDatabase.getInstance().getReference(Constant.CHAT).child(groupKey).limitToLast(1);

                            lastQuery.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {


                                    if (dataSnapshot.getValue() != null) {

                                    *//*    Query lastQuery = FirebaseDatabase.getInstance().getReference(Constant.CHAT).child(groupKey).limitToLast(1);
                                        lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
*//*
                                        HashMap hashMap1 = new HashMap();
                                        // Iterator iterator2 = dataSnapshot.getChildren().iterator();
                                        // while (iterator2.hasNext()) {
                                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                            // DataSnapshot dataSnapshot11 = iterator2.next();
                                            if (snapshot.child(Constant.TEXT_NAME).exists()) {
                                                String lastmsg = snapshot.child(Constant.TEXT_NAME).getValue().toString();
                                                // holder.msg_tv.setVisibility(View.VISIBLE);
                                                // holder.msg_tv.setText(lastmsg);
                                                Log.d("key", lastmsg);
                                                hashMap1.put("text", lastmsg);

                                            } else if (snapshot.child(Constant.IMAGE_NAME).exists()) {

                                                //  holder.msg_tv.setText(Constant.IMAGE_NAME);
                                                // holder.msg_tv.setVisibility(View.VISIBLE);
                                                Log.d("key", "ddd");
                                                hashMap1.put("image", "image");

                                            } else if (snapshot.child("video").exists()) {
                                                // holder.msg_tv.setText("video");
                                                // holder.msg_tv.setVisibility(View.VISIBLE);
                                                Log.d("key", "ddd");
                                                hashMap1.put("video", "video");
                                            } else if (snapshot.child("file").exists()) {
                                                //  holder.msg_tv.setText("file");
                                                // holder.msg_tv.setVisibility(View.VISIBLE);
                                                Log.d("key", "ddd");
                                                hashMap1.put("file", "file");
                                            }

                                            if (snapshot.child(Constant.TIME_NAME).exists()) {
                                                String time = snapshot.child(Constant.TIME_NAME).getValue().toString();

                                                String localTime = SystemTime.getInstance().getLocalTime(time);
                                                Log.d("data", localTime);
                                                hashMap1.put("time", localTime);
                                                // holder.time_iv.setVisibility(View.VISIBLE);

                                                //   holder.time_iv.setText(localTime);

                                            }
                                            Log.d("key", "-----" + hashMap1);
                                            msgList.add(hashMap1);
                                            Log.d("key", groupName + "fgdmnb");
                                            groupAdaptor.addData(groupName);
                                            groupAdaptor.notifyDataSetChanged();

                                        }


                                    }

                                           *//* @Override
                                            public void onCancelled(DatabaseError databaseError) {


                                            }
                                        });*//*

                                    //  }
                                    else {

                                        Log.d("key", "-----" + hashMap1);
                                        groupName.add(hashMap1);
                                        Log.d("key", groupName + "mnbfffg");
                                        groupAdaptor.addData(groupName);
                                        groupAdaptor.notifyDataSetChanged();
                                    }


                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });*/
                            groupName.add(hashMap1);


                        }


                       /* groupAdaptor.addData(groupName);
                        groupAdaptor.notifyDataSetChanged();*/
                    }


                }

                getIndividualChat();
                FirebaseDatabase.getInstance().getReference(Constant.INVITED_USER).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Iterator<DataSnapshot> iterator1 = dataSnapshot.getChildren().iterator();
                        while (iterator1.hasNext()) {

                            DataSnapshot dataSnapshot1 = iterator1.next();

                            for (int i = 0; i < groupName.size(); i++) {
                                if (groupName.get(i).get(Constant.GROUP_kEY).equals(dataSnapshot1.getKey())) {
                                    groupName.remove(i);

                                }
                            }

                            if (dataSnapshot1.child("OneToOneChat").getValue() != null) {
                                String to = dataSnapshot1.child("to").getValue().toString();
                                String from = dataSnapshot1.child("from").getValue().toString();

                                if (to.equals(loginUserKey)) {

                                    hashMap1 = new HashMap<String, String>();

                                    if (dataSnapshot1.child("SENDER").exists()) {
                                        String chatUserName = dataSnapshot1.child("SENDER").getValue().toString();
                                        hashMap1.put(Constant.GROUP_NAME, chatUserName);
                                    }
                                    if (dataSnapshot1.child("profile_picture").exists()) {
                                        String chatUserProfile = dataSnapshot1.child("profile_picture").getValue().toString();
                                        hashMap1.put(Constant.PROFILE_PICTURE, chatUserProfile);
                                    }
                                    if (dataSnapshot1.child("Sendercollege_name").exists()) {
                                        String clooegeName = dataSnapshot1.child("Sendercollege_name").getValue().toString();
                                        hashMap1.put(Constant.COLLEGE_NAME, clooegeName);
                                    }
                                    if (dataSnapshot1.child("Sendercountry_name").exists()) {
                                        String countryName = dataSnapshot1.child("Sendercountry_name").getValue().toString();
                                        hashMap1.put(Constant.COUNTRY_NAME, countryName);
                                    }

                                    if (dataSnapshot1.child("Senderuniversity_name").exists()) {
                                        String universityName = dataSnapshot1.child("Senderuniversity_name").getValue().toString();
                                        hashMap1.put(Constant.UNIVERSITY_NAME, universityName);
                                    }

                                    if (dataSnapshot1.child("Senderdegree").exists()) {
                                        String degreeName = dataSnapshot1.child("Senderdegree").getValue().toString();
                                        hashMap1.put(Constant.DEGREE_NAME, degreeName);
                                    }

                                    if (dataSnapshot1.child("Senderyear").exists()) {
                                        String yearName = dataSnapshot1.child("Senderyear").getValue().toString();
                                        hashMap1.put(Constant.YEAR_NAME, yearName);
                                    }
                                    hashMap1.put("mode", "individual");
                                    hashMap1.put(Constant.GROUP_kEY, dataSnapshot1.getKey());

                                    Iterator<DataSnapshot> iterator2 = dataSnapshot1.child("OneToOneChat").getChildren().iterator();
                                    while (iterator2.hasNext()) {

                                        hashMap = new HashMap();
                                        DataSnapshot dataSnapshot2 = iterator2.next();

                                        if (dataSnapshot2.child(Constant.TEXT_NAME).exists()) {
                                            String lastmsg = dataSnapshot2.child(Constant.TEXT_NAME).getValue().toString();

                                            hashMap.put(Constant.TEXT_NAME, lastmsg);

                                        } else if (dataSnapshot2.child(Constant.IMAGE_NAME).exists()) {


                                            hashMap.put(Constant.IMAGE_NAME, Constant.IMAGE_NAME);

                                        } else if (dataSnapshot2.child(Constant.VIDEO_NAME).exists()) {

                                            hashMap.put(Constant.VIDEO_NAME, Constant.VIDEO_NAME);
                                        } else if (dataSnapshot2.child(Constant.FILE_NAME).exists()) {

                                            hashMap.put(Constant.FILE_NAME, Constant.FILE_NAME);
                                        }

                                        if (dataSnapshot2.child(Constant.TIME_NAME).exists()) {
                                            String time = dataSnapshot2.child(Constant.TIME_NAME).getValue().toString();

                                            String localTime = SystemTime.getInstance().getLocalTime(time);

                                            String date = SystemTime.getInstance().getDate(time);

                                            hashMap.put(Constant.TIME_NAME, localTime);
                                            hashMap.put("date", date);

                                        }

                                    }


                                    if (hashMap.containsKey(Constant.TEXT_NAME)) {
                                        String text = hashMap.get(Constant.TEXT_NAME).toString();
                                        hashMap1.put(Constant.TEXT_NAME, text);
                                    }
                                    if (hashMap.containsKey(Constant.IMAGE_NAME)) {
                                        String image = hashMap.get(Constant.IMAGE_NAME).toString();
                                        hashMap1.put(Constant.IMAGE_NAME, image);
                                    }
                                    if (hashMap.containsKey(Constant.VIDEO_NAME)) {
                                        String video = hashMap.get(Constant.VIDEO_NAME).toString();
                                        hashMap1.put(Constant.VIDEO_NAME, video);
                                    }
                                    if (hashMap.containsKey(Constant.FILE_NAME)) {
                                        String file = hashMap.get(Constant.FILE_NAME).toString();
                                        hashMap1.put(Constant.FILE_NAME, file);
                                    }
                                    if (hashMap.containsKey(Constant.TIME_NAME)) {
                                        String time = hashMap.get(Constant.TIME_NAME).toString();
                                        hashMap1.put(Constant.TIME_NAME, time);
                                    }
                                    if (hashMap.containsKey("date")) {
                                        hashMap1.put("date", hashMap.get("date").toString());
                                    }

                                    groupName.add(hashMap1);

                                } else if (from.equals(loginUserKey)) {

                                    hashMap1 = new HashMap<String, String>();

                                    if (dataSnapshot1.child("RECEIVER").exists()) {
                                        String chatUserName = dataSnapshot1.child("RECEIVER").getValue().toString();
                                        hashMap1.put(Constant.GROUP_NAME, chatUserName);
                                    }
                                    if (dataSnapshot1.child("RECEIVER_PROFILE").exists()) {
                                        String chatUserProfile = dataSnapshot1.child("RECEIVER_PROFILE").getValue().toString();
                                        hashMap1.put(Constant.PROFILE_PICTURE, chatUserProfile);
                                    }

                                    if (dataSnapshot1.child("college_name").exists()) {
                                        String clooegeName = dataSnapshot1.child("college_name").getValue().toString();
                                        hashMap1.put(Constant.COLLEGE_NAME, clooegeName);
                                    }
                                    if (dataSnapshot1.child("country_name").exists()) {
                                        String countryName = dataSnapshot1.child("country_name").getValue().toString();
                                        hashMap1.put(Constant.COUNTRY_NAME, countryName);
                                    }

                                    if (dataSnapshot1.child("university_name").exists()) {
                                        String universityName = dataSnapshot1.child("university_name").getValue().toString();
                                        hashMap1.put(Constant.UNIVERSITY_NAME, universityName);
                                    }

                                    if (dataSnapshot1.child("degree").exists()) {
                                        String degreeName = dataSnapshot1.child("degree").getValue().toString();
                                        hashMap1.put(Constant.DEGREE_NAME, degreeName);
                                    }

                                    if (dataSnapshot1.child("year").exists()) {
                                        String yearName = dataSnapshot1.child("year").getValue().toString();
                                        hashMap1.put(Constant.YEAR_NAME, yearName);
                                    }
                                    hashMap1.put("mode", "individual");
                                    hashMap1.put(Constant.GROUP_kEY, dataSnapshot1.getKey());

                                    Iterator<DataSnapshot> iterator2 = dataSnapshot1.child("OneToOneChat").getChildren().iterator();
                                    while (iterator2.hasNext()) {

                                        hashMap = new HashMap();
                                        DataSnapshot dataSnapshot2 = iterator2.next();

                                        if (dataSnapshot2.child(Constant.TEXT_NAME).exists()) {
                                            String lastmsg = dataSnapshot2.child(Constant.TEXT_NAME).getValue().toString();

                                            hashMap.put(Constant.TEXT_NAME, lastmsg);

                                        } else if (dataSnapshot2.child(Constant.IMAGE_NAME).exists()) {


                                            hashMap.put(Constant.IMAGE_NAME, Constant.IMAGE_NAME);

                                        } else if (dataSnapshot2.child(Constant.VIDEO_NAME).exists()) {

                                            hashMap.put(Constant.VIDEO_NAME, Constant.VIDEO_NAME);
                                        } else if (dataSnapshot2.child(Constant.FILE_NAME).exists()) {

                                            hashMap.put(Constant.FILE_NAME, Constant.FILE_NAME);
                                        }

                                        if (dataSnapshot2.child(Constant.TIME_NAME).exists()) {
                                            String time = dataSnapshot2.child(Constant.TIME_NAME).getValue().toString();

                                            String localTime = SystemTime.getInstance().getLocalTime(time);

                                            String date = SystemTime.getInstance().getDate(time);

                                            hashMap.put(Constant.TIME_NAME, localTime);
                                            hashMap.put("date", date);

                                        }

                                    }


                                    if (hashMap.containsKey(Constant.TEXT_NAME)) {
                                        String text = hashMap.get(Constant.TEXT_NAME).toString();
                                        hashMap1.put(Constant.TEXT_NAME, text);
                                    }
                                    if (hashMap.containsKey(Constant.IMAGE_NAME)) {
                                        String image = hashMap.get(Constant.IMAGE_NAME).toString();
                                        hashMap1.put(Constant.IMAGE_NAME, image);
                                    }
                                    if (hashMap.containsKey(Constant.VIDEO_NAME)) {
                                        String video = hashMap.get(Constant.VIDEO_NAME).toString();
                                        hashMap1.put(Constant.VIDEO_NAME, video);
                                    }
                                    if (hashMap.containsKey(Constant.FILE_NAME)) {
                                        String file = hashMap.get(Constant.FILE_NAME).toString();
                                        hashMap1.put(Constant.FILE_NAME, file);
                                    }
                                    if (hashMap.containsKey(Constant.TIME_NAME)) {
                                        String time = hashMap.get(Constant.TIME_NAME).toString();
                                        hashMap1.put(Constant.TIME_NAME, time);
                                    }
                                    if (hashMap.containsKey("date")) {
                                        hashMap1.put("date", hashMap.get("date").toString());
                                    }
                                    groupName.add(hashMap1);

                                }

                                // groupName.add(hashMap1);

                            }


                        }
                        //  Log.d("data", groupName + "----");
                        sortList();
                       /* groupAdaptor.addData(groupName);
                        groupAdaptor.notifyDataSetChanged();*/
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                //  Log.d("key",groupName+"mnb");
             /*   groupAdaptor.addData(groupName);
                groupAdaptor.notifyDataSetChanged();*/

                if (fragment != null && fragment.isVisible()) {
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                if (fragment != null && fragment.isVisible()) {
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }
                }


            }
        });

    }

    private void getIndividualChat() {

        FirebaseDatabase.getInstance().getReference(Constant.INVITED_USER).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterator<DataSnapshot> iterator1 = dataSnapshot.getChildren().iterator();
                while (iterator1.hasNext()) {

                    DataSnapshot dataSnapshot1 = iterator1.next();

                    for (int i = 0; i < groupName.size(); i++) {
                        if (groupName.get(i).get(Constant.GROUP_kEY).equals(dataSnapshot1.getKey())) {
                            groupName.remove(i);

                        }
                    }

                    if (dataSnapshot1.child("OneToOneChat").getValue() != null) {
                        String to = dataSnapshot1.child("to").getValue().toString();
                        String from = dataSnapshot1.child("from").getValue().toString();

                        if (to.equals(loginUserKey)) {

                            hashMap1 = new HashMap<String, String>();

                            if (dataSnapshot1.child("SENDER").exists()) {
                                String chatUserName = dataSnapshot1.child("SENDER").getValue().toString();
                                hashMap1.put(Constant.GROUP_NAME, chatUserName);
                            }
                            if (dataSnapshot1.child("profile_picture").exists()) {
                                String chatUserProfile = dataSnapshot1.child("profile_picture").getValue().toString();
                                hashMap1.put(Constant.PROFILE_PICTURE, chatUserProfile);
                            }
                            if (dataSnapshot1.child("Sendercollege_name").exists())
                            {
                                String clooegeName = dataSnapshot1.child("Sendercollege_name").getValue().toString();
                                hashMap1.put(Constant.COLLEGE_NAME, clooegeName);
                            }
                            if (dataSnapshot1.child("Sendercountry_name").exists())
                            {
                                String countryName = dataSnapshot1.child("Sendercountry_name").getValue().toString();
                                hashMap1.put(Constant.COUNTRY_NAME, countryName);
                            }

                            if (dataSnapshot1.child("Senderuniversity_name").exists())
                            {
                                String universityName = dataSnapshot1.child("Senderuniversity_name").getValue().toString();
                                hashMap1.put(Constant.UNIVERSITY_NAME, universityName);
                            }

                            if (dataSnapshot1.child("Senderdegree").exists())
                            {
                                String degreeName = dataSnapshot1.child("Senderdegree").getValue().toString();
                                hashMap1.put(Constant.DEGREE_NAME, degreeName);
                            }

                            if (dataSnapshot1.child("Senderyear").exists())
                            {
                                String yearName = dataSnapshot1.child("Senderyear").getValue().toString();
                                hashMap1.put(Constant.YEAR_NAME, yearName);
                            }
                            hashMap1.put("mode", "individual");
                            hashMap1.put(Constant.GROUP_kEY, dataSnapshot1.getKey());

                            Iterator<DataSnapshot> iterator2 = dataSnapshot1.child("OneToOneChat").getChildren().iterator();
                            while (iterator2.hasNext()) {

                                hashMap = new HashMap();
                                DataSnapshot dataSnapshot2 = iterator2.next();

                                if (dataSnapshot2.child(Constant.TEXT_NAME).exists()) {
                                    String lastmsg = dataSnapshot2.child(Constant.TEXT_NAME).getValue().toString();

                                    hashMap.put(Constant.TEXT_NAME, lastmsg);

                                } else if (dataSnapshot2.child(Constant.IMAGE_NAME).exists()) {


                                    hashMap.put(Constant.IMAGE_NAME, Constant.IMAGE_NAME);

                                } else if (dataSnapshot2.child(Constant.VIDEO_NAME).exists()) {

                                    hashMap.put(Constant.VIDEO_NAME, Constant.VIDEO_NAME);
                                } else if (dataSnapshot2.child(Constant.FILE_NAME).exists()) {

                                    hashMap.put(Constant.FILE_NAME, Constant.FILE_NAME);
                                }

                                if (dataSnapshot2.child(Constant.TIME_NAME).exists()) {
                                    String time = dataSnapshot2.child(Constant.TIME_NAME).getValue().toString();

                                    String localTime = SystemTime.getInstance().getLocalTime(time);

                                    String date = SystemTime.getInstance().getDate(time);

                                    hashMap.put(Constant.TIME_NAME, localTime);
                                    hashMap.put("date", date);

                                }

                            }


                            if (hashMap.containsKey(Constant.TEXT_NAME)) {
                                String text = hashMap.get(Constant.TEXT_NAME).toString();
                                hashMap1.put(Constant.TEXT_NAME, text);
                            }
                            if (hashMap.containsKey(Constant.IMAGE_NAME)) {
                                String image = hashMap.get(Constant.IMAGE_NAME).toString();
                                hashMap1.put(Constant.IMAGE_NAME, image);
                            }
                            if (hashMap.containsKey(Constant.VIDEO_NAME)) {
                                String video = hashMap.get(Constant.VIDEO_NAME).toString();
                                hashMap1.put(Constant.VIDEO_NAME, video);
                            }
                            if (hashMap.containsKey(Constant.FILE_NAME)) {
                                String file = hashMap.get(Constant.FILE_NAME).toString();
                                hashMap1.put(Constant.FILE_NAME, file);
                            }
                            if (hashMap.containsKey(Constant.TIME_NAME)) {
                                String time = hashMap.get(Constant.TIME_NAME).toString();
                                hashMap1.put(Constant.TIME_NAME, time);
                            }
                            if (hashMap.containsKey("date")) {
                                hashMap1.put("date", hashMap.get("date").toString());
                            }

                            groupName.add(hashMap1);

                        } else if (from.equals(loginUserKey)) {

                            hashMap1 = new HashMap<String, String>();

                            if (dataSnapshot1.child("RECEIVER").exists()) {
                                String chatUserName = dataSnapshot1.child("RECEIVER").getValue().toString();
                                hashMap1.put(Constant.GROUP_NAME, chatUserName);
                            }
                            if (dataSnapshot1.child("RECEIVER_PROFILE").exists()) {
                                String chatUserProfile = dataSnapshot1.child("RECEIVER_PROFILE").getValue().toString();
                                hashMap1.put(Constant.PROFILE_PICTURE, chatUserProfile);
                            }

                            if (dataSnapshot1.child("college_name").exists())
                            {
                                String clooegeName = dataSnapshot1.child("college_name").getValue().toString();
                                hashMap1.put(Constant.COLLEGE_NAME, clooegeName);
                            }
                            if (dataSnapshot1.child("country_name").exists())
                            {
                                String countryName = dataSnapshot1.child("country_name").getValue().toString();
                                hashMap1.put(Constant.COUNTRY_NAME, countryName);
                            }

                            if (dataSnapshot1.child("university_name").exists())
                            {
                                String universityName = dataSnapshot1.child("university_name").getValue().toString();
                                hashMap1.put(Constant.UNIVERSITY_NAME, universityName);
                            }

                            if (dataSnapshot1.child("degree").exists())
                            {
                                String degreeName = dataSnapshot1.child("degree").getValue().toString();
                                hashMap1.put(Constant.DEGREE_NAME, degreeName);
                            }

                            if (dataSnapshot1.child("year").exists())
                            {
                                String yearName = dataSnapshot1.child("year").getValue().toString();
                                hashMap1.put(Constant.YEAR_NAME, yearName);
                            }
                            hashMap1.put("mode", "individual");
                            hashMap1.put(Constant.GROUP_kEY, dataSnapshot1.getKey());

                            Iterator<DataSnapshot> iterator2 = dataSnapshot1.child("OneToOneChat").getChildren().iterator();
                            while (iterator2.hasNext()) {

                                hashMap = new HashMap();
                                DataSnapshot dataSnapshot2 = iterator2.next();

                                if (dataSnapshot2.child(Constant.TEXT_NAME).exists()) {
                                    String lastmsg = dataSnapshot2.child(Constant.TEXT_NAME).getValue().toString();

                                    hashMap.put(Constant.TEXT_NAME, lastmsg);

                                } else if (dataSnapshot2.child(Constant.IMAGE_NAME).exists()) {


                                    hashMap.put(Constant.IMAGE_NAME, Constant.IMAGE_NAME);

                                } else if (dataSnapshot2.child(Constant.VIDEO_NAME).exists()) {

                                    hashMap.put(Constant.VIDEO_NAME, Constant.VIDEO_NAME);
                                } else if (dataSnapshot2.child(Constant.FILE_NAME).exists()) {

                                    hashMap.put(Constant.FILE_NAME, Constant.FILE_NAME);
                                }

                                if (dataSnapshot2.child(Constant.TIME_NAME).exists()) {
                                    String time = dataSnapshot2.child(Constant.TIME_NAME).getValue().toString();

                                    String localTime = SystemTime.getInstance().getLocalTime(time);

                                    String date = SystemTime.getInstance().getDate(time);

                                    hashMap.put(Constant.TIME_NAME, localTime);
                                    hashMap.put("date", date);

                                }

                            }


                            if (hashMap.containsKey(Constant.TEXT_NAME)) {
                                String text = hashMap.get(Constant.TEXT_NAME).toString();
                                hashMap1.put(Constant.TEXT_NAME, text);
                            }
                            if (hashMap.containsKey(Constant.IMAGE_NAME)) {
                                String image = hashMap.get(Constant.IMAGE_NAME).toString();
                                hashMap1.put(Constant.IMAGE_NAME, image);
                            }
                            if (hashMap.containsKey(Constant.VIDEO_NAME)) {
                                String video = hashMap.get(Constant.VIDEO_NAME).toString();
                                hashMap1.put(Constant.VIDEO_NAME, video);
                            }
                            if (hashMap.containsKey(Constant.FILE_NAME)) {
                                String file = hashMap.get(Constant.FILE_NAME).toString();
                                hashMap1.put(Constant.FILE_NAME, file);
                            }
                            if (hashMap.containsKey(Constant.TIME_NAME)) {
                                String time = hashMap.get(Constant.TIME_NAME).toString();
                                hashMap1.put(Constant.TIME_NAME, time);
                            }
                            if (hashMap.containsKey("date")) {
                                hashMap1.put("date", hashMap.get("date").toString());
                            }
                            groupName.add(hashMap1);

                        }

                        // groupName.add(hashMap1);

                    }


                }
                //  Log.d("data", groupName + "----");
                sortList();
                       /* groupAdaptor.addData(groupName);
                        groupAdaptor.notifyDataSetChanged();*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void sortList() {


        Collections.sort(groupName, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                if (s1.containsKey("date")) {
                    firstValue = s1.get("date").toString();
                }
                if (s2.containsKey("date")) {
                    secondValue = s2.get("date").toString();
                }

                return secondValue.compareToIgnoreCase(firstValue);
            }
        });

        groupAdaptor.addData(groupName);
        groupAdaptor.notifyDataSetChanged();
    }


    @Override
    public void setMenuVisibility(boolean menuVisible) {


        if (menuVisible) {
            isShowing = true;
        }

        super.setMenuVisibility(menuVisible);
    }

    private void bindViews(View view) {


        fragmentGroup = new GroupFragment();
        groupmessage_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.groupmessage_rv);
        addcourse_iv = (ImageView) view.findViewById(R.id.addcourse_iv);
        back_msg_iv = (ImageView) view.findViewById(R.id.back_msg_iv);

        group_tv = (TextView) view.findViewById(R.id.group_tv);
        users_tv = (TextView) view.findViewById(R.id.users_tv);
        emptyview = (TextView) view.findViewById(R.id.result);

        users_tv.setVisibility(View.GONE);
        back_msg_iv.setVisibility(View.GONE);

        groupmessage_rv.addOnItemTouchListener(new GroupAdaptor.RecyclerTouchListener(getContext(), groupmessage_rv, new ClickRecyclerInterface() {
            @Override
            public void onRecyClick(View view, int position) {

                Log.d("position", position + "");
                if (position == -1) {
                    return;
                } else {
                    List<HashMap<String, String>> studentList = ((GroupAdaptor) groupAdaptor).getUserList();

                    HashMap<String, String> hashMap = studentList.get(position);
                    if (hashMap.get("mode").equals("individual")) {
                        OneToOneChatFragment oneToOneChatFragment = new OneToOneChatFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("userName", view.getTag().toString());
                        bundle.putString("chatKey", hashMap.get(Constant.GROUP_kEY));
                        if (hashMap.containsKey(Constant.PROFILE_PICTURE)) {
                            bundle.putString("imagePath", hashMap.get(Constant.PROFILE_PICTURE));
                        }
                        if (hashMap.containsKey(Constant.COLLEGE_NAME)) {
                            bundle.putString(Constant.COLLEGE_NAME, hashMap.get(Constant.COLLEGE_NAME));
                        }
                        if (hashMap.containsKey(Constant.UNIVERSITY_NAME)) {
                            bundle.putString(Constant.UNIVERSITY_NAME, hashMap.get(Constant.UNIVERSITY_NAME));
                        }
                        if (hashMap.containsKey(Constant.COUNTRY_NAME)) {
                            bundle.putString(Constant.COUNTRY_NAME, hashMap.get(Constant.COUNTRY_NAME));
                        }
                        if (hashMap.containsKey(Constant.DEGREE_NAME)) {
                            bundle.putString(Constant.DEGREE_NAME, hashMap.get(Constant.DEGREE_NAME));
                        }
                        if (hashMap.containsKey(Constant.YEAR_NAME)) {
                            bundle.putString(Constant.YEAR_NAME, hashMap.get(Constant.YEAR_NAME));
                        }

                        oneToOneChatFragment.setArguments(bundle);

                        ReplaceFragment.replace((WelcomeActivity) getActivity(), oneToOneChatFragment, "OneToOne_Fragment");
                    } else {
                        MessageBoardFragment messageBoardFragment = new MessageBoardFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(Constant.GROUP_NAME, view.getTag().toString());
                        bundle.putString(Constant.GROUP_kEY, hashMap.get(Constant.GROUP_kEY));
                        bundle.putString(Constant.USER_LIST, hashMap.get(Constant.USER_NAME));
                        bundle.putString(Constant.COURSE_KEY, hashMap.get(Constant.COURSE_KEY));
                        bundle.putString(Constant.OWNER, hashMap.get(Constant.OWNER));
                        if (hashMap.containsKey(Constant.COLLEGE_NAME)) {
                            bundle.putString(Constant.COLLEGE_NAME, hashMap.get(Constant.COLLEGE_NAME));
                        }
                        if (hashMap.containsKey(Constant.UNIVERSITY_NAME)) {
                            bundle.putString(Constant.UNIVERSITY_NAME, hashMap.get(Constant.UNIVERSITY_NAME));
                        }
                        if (hashMap.containsKey(Constant.COUNTRY_NAME)) {
                            bundle.putString(Constant.COUNTRY_NAME, hashMap.get(Constant.COUNTRY_NAME));
                        }
                        if (hashMap.containsKey(Constant.DEGREE_NAME)) {
                            bundle.putString(Constant.DEGREE_NAME, hashMap.get(Constant.DEGREE_NAME));
                        }
                        if (hashMap.containsKey(Constant.YEAR_NAME)) {
                            bundle.putString(Constant.YEAR_NAME, hashMap.get(Constant.YEAR_NAME));
                        }
                        bundle.putString("mode", hashMap.get("mode"));
                        //   Log.d("position", position + "---" + hashMap.get(Constant.GROUP_kEY) + "------" + hashMap.get(Constant.COURSE_KEY));

                        messageBoardFragment.setArguments(bundle);

                        ReplaceFragment.replace((WelcomeActivity) getActivity(), messageBoardFragment, "Message_Fragment");
                    }
                }

            }


            @Override
            public void onLongClick(View view, int position) {

                String uni = "";
                if (position == -1) {
                    return;
                } else {

                    List<HashMap<String, String>> studentList = ((GroupAdaptor) groupAdaptor).getUserList();
                    HashMap<String, String> hashMap = studentList.get(position);
                    if (hashMap.get("mode").equals("individual")) {

                        final String groupKey = hashMap.get(Constant.GROUP_kEY);

                    } else {
                        final String groupKey = hashMap.get(Constant.GROUP_kEY);
                        final String owner = hashMap.get(Constant.OWNER);
                        Log.d("position", position + "------" + groupKey + "-----" + owner);
                        if (owner.equals(userKey)) {

                            ShowDialog.getInstance().showDeleteDialog(getContext(), groupKey, uni, 1);
                        }

                    }
                }
            }


        }));
    }

    private void setAdaptor() {

        groupAdaptor = new GroupAdaptor(getContext(), groupName);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        groupmessage_rv.setLayoutManager(mLayoutManager);
        groupmessage_rv.setAdapter(groupAdaptor);
        groupmessage_rv.setEmptyView(emptyview);


    }

    private void setListener() {

        addcourse_iv.setOnClickListener(this);
        back_msg_iv.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();


        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.GONE);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.addcourse_iv:

                choose_group_option();

                break;

            case R.id.back_msg_iv:

                Fragment fragment = getFragmentManager().findFragmentByTag("group_fragment");
                if (fragment != null && fragment.isVisible()) {
                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), new MenuFragment(), "Menus_fragment");

                } else {

                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                }

                break;

        }
    }

    public void choose_group_option() {
        final Dialog dialog_search = new Dialog(getActivity());
        dialog_search.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_search.setContentView(R.layout.layout_group_option);

        WindowManager.LayoutParams lp = dialog_search.getWindow().getAttributes();
        lp.dimAmount = 0.8f;
        dialog_search.getWindow().setAttributes(lp);

        dialog_search.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        /*TextView college = (TextView) dialog_search.findViewById(R.id.college);
        TextView contacts = (TextView) dialog_search.findViewById(R.id.contacts);
        TextView course = (TextView) dialog_search.findViewById(R.id.course);*/
        TextView chat = (TextView) dialog_search.findViewById(R.id.chat);
        TextView group = (TextView) dialog_search.findViewById(R.id.group);
        //  TextView courseMates = (TextView) dialog_search.findViewById(R.id.courseMates);

      /*  courseMates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog_search.dismiss();

            }
        });*/

       /* course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                CourseFragment courseFragment = new CourseFragment();
                //  ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                ReplaceFragment.replace((WelcomeActivity) getActivity(), courseFragment, "CourseGroup_Fragment");

                dialog_search.dismiss();
            }
        });

        contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                createGroup("Network");

                dialog_search.dismiss();


            }
        });

        college.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getCollegeStudent();
                createGroup("College");
                dialog_search.dismiss();


            }
        });
*/

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog_search.dismiss();
                Bundle bundle = new Bundle();
                bundle.putString("network", "contact");
                ReplaceFragment.replace((WelcomeActivity) getActivity(), new Network_Invite_Fragment(), "netCont_fragmant", bundle);
            }
        });

        group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog_search.dismiss();
                createGroup("Network");
            }
        });
        dialog_search.show();
    }

    private void getCollegeStudent(final String groupname) {

        FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).orderByChild(Constant.COLLEGE_NAME)
                .equalTo(collegeName).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                collegeUserList = new ArrayList<>();

                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                if (dataSnapshot.getChildrenCount() > 1) {
                    while (iterator.hasNext()) {
                        DataSnapshot dataSnapshot1 = iterator.next();
                        String fullName = "";
                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        String status = dataSnapshot1.child(Constant.STATUS_NAME).exists() ? dataSnapshot1.child(Constant.STATUS_NAME).getValue().toString() : "";
                        if (status.equals("active")) {
                            String firstName = dataSnapshot1.child(Constant.FIRST_NAME).exists() ? dataSnapshot1.child(Constant.FIRST_NAME).getValue().toString() : "";
                            String lastName = dataSnapshot1.child(Constant.LAST_NAME).exists() ? dataSnapshot1.child(Constant.LAST_NAME).getValue().toString() : "";
                            String key = dataSnapshot1.getKey();
                            fullName = firstName + " " + lastName;

                            hashMap.put(key, fullName);

                            collegeUserList.add(hashMap);


                        }
                    }

                    HashMap map1 = new HashMap();
                    map1.put(Constant.GROUP_NAME, groupname);
                    map1.put("mode", editText.getTag().toString());
                    map1.put(Constant.OWNER, userKey);
                    if (downloadImageUri != null && !downloadImageUri.equals(Uri.EMPTY)) {

                        map1.put(Constant.GROUP_ICON, downloadImageUri.toString());
                    }

                    //      groupContactReference = groupContactReference.child(groupContactReference.push().push().getKey());

                    groupContactReference = FirebaseDatabase.getInstance().getReference(Constant.CREATEGROUP_NAME).push().push();

                    groupContactReference.updateChildren(map1);
                    HashMap hashMap = new HashMap<>();

                    hashMap.put(userKey, userFullName);
                    groupContactReference.child("users").updateChildren(hashMap);

                    addUsersToGroup(0);

                } else {
                    Toast.makeText(getContext(), "Student not found.,", Toast.LENGTH_SHORT).show();
                    ShowDialog.hideDialog();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addUsersToGroup(int i) {

        hashMap = new HashMap();
        if (i < collegeUserList.size()) {
          /*  registeredUserKey = checkList.get(i).get(Constant.STUDENT_KEY).toString();
            String firstname = checkList.get(i).get(Constant.FIRST_NAME).toString();
            String lastName = checkList.get(i).get(Constant.LAST_NAME).toString();
            userFullName = firstname + " " + lastName;
            mapStudent.put(registeredUserKey, userFullName);
*/

            HashMap hashMap = collegeUserList.get(i);
            groupContactReference.child("users").updateChildren(hashMap);

            i++;
            addUsersToGroup(i);

        }
        if (i == collegeUserList.size()) {

            ShowDialog.hideDialog();

        }


    }

    private void createGroup(String key) {


        // groupList = new ArrayList<>();

        editText = ShowDialog.getInstance().groupDialog(getContext(), onClickListener, getOnClickListener);

        editText.setTag(key);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 19) {

                    showSnackbarError(editText, "You can't add more than 20 characters.", getContext());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


    }

    View.OnClickListener getOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.cameraDialog(getContext(), galleryClick, cameraClick);
        }
    };

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Context context = ShowDialog.getInstance().group_dialog.getContext();

            InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);


            final String groupname = editText.getText().toString();
            Log.d("groupName", groupname);
            if (groupname.equals(" ") || groupname.isEmpty()) {

                ShowDialog.showSnackbarError(v, "Please enter group name.", getContext());

            } /*else if (!isNetworkAvailable(getContext())) {
                ShowDialog.networkDialog(getContext());
            }*/ else {

               /* ShowDialog.showDialog(getContext());
                AsyncCaller asyncCaller=new AsyncCaller(groupname);
                asyncCaller.execute();*/

                addGroup(groupname);
            }


        }
    };

    View.OnClickListener cameraClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            capture_image();

        }
    };

    View.OnClickListener galleryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();

            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            requestAppPermissions(permission, R.string.permission, 54);

        }
    };


    private void capture_image() {
        picture_Path = "";
        destination_path = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = "image";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picture_Path = String.valueOf(new File(f, System.currentTimeMillis() + ".jpg"));
        destination_path = new File(picture_Path);
        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination_path);
        } else {
            imageuri = Uri.fromFile(destination_path);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, Constant.CAMERA_CODE);
    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 54) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, Constant.GALLERY_CODE);
        }
    }

    public static boolean isInternetAccessible(Context context) {

        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Couldn't check internet connection", e);
        }

        return false;


    }

    private class AsyncCaller extends AsyncTask<Void, Void, Boolean> {

        String groupname;

        public AsyncCaller(String groupname) {

            this.groupname = groupname;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            boolean networkAccess = isInternetAccessible(getContext());
            return networkAccess;
        }

        @Override
        protected void onPostExecute(Boolean networkAccess) {
            super.onPostExecute(networkAccess);


            if (!networkAccess) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {

                        networkDialog(getContext());
                        ShowDialog.hideDialog();
                    }
                }
            } else {

               /* if (ShowDialog.dialog.isShowing()) {
                    ShowDialog.hideDialog();
                }*/
                if (imageUploadUri != null && !imageUploadUri.equals(Uri.EMPTY)) {


                    imageUpload(groupname);

                } else {

                    //addGroup(groupname);

                    // createCollegeGroup(groupname);
                    getCollegeStudent(groupname);

                }
            }
        }


    }


    private void imageUpload(final String groupname) {

        //  ShowDialog.showDialog(getContext());

        FirebaseStorage.getInstance().getReference().child(Constant.IMAGE_NAME + "_" + imageUploadUri.getLastPathSegment())
                .putFile(imageUploadUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                        ShowDialog.hideDialog();

                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();


                        //  ShowDialog.hideDialog();


//                        addGroup(groupname);

                        getCollegeStudent(groupname);


                    }
                });

    }

    private void addGroup(final String groupname) {

        ShowDialog.getInstance().group_dialog.dismiss();
        Bundle bundle = new Bundle();
        if (editText.getTag().equals("College")) {

            ShowDialog.showDialog(getContext());
            AsyncCaller asyncCaller = new AsyncCaller(groupname);
            asyncCaller.execute();

        } else {
            bundle.putString(Constant.GROUP_NAME, groupname);
            bundle.putString("mode", editText.getTag().toString());

            if (imageUploadUri != null && !imageUploadUri.equals(Uri.EMPTY)) {


                bundle.putString(Constant.PROFILE_PICTURE, imageUploadUri.toString());
            }
            SearchStudentFragment searchStudentFragment = new SearchStudentFragment();
            searchStudentFragment.setArguments(bundle);


            ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
            ReplaceFragment.replace((WelcomeActivity) getActivity(), searchStudentFragment, "SearchStudent_Fragment");

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.GALLERY_CODE && resultCode == RESULT_OK) {


            Uri image = data.getData();

            Intent intent = new Intent(getContext(), CropImageActivity.class);
            intent.putExtra("imageuri", image.toString());
            startActivityForResult(intent, Constant.CROP_IMAGE);


        } else if (requestCode == Constant.CAMERA_CODE && resultCode == RESULT_OK) {

            Intent intent = new Intent(getContext(), CropImageActivity.class);
            intent.putExtra("imageuri", imageuri.toString());
            startActivityForResult(intent, Constant.CROP_IMAGE);

        } else if (requestCode == Constant.CROP_IMAGE && resultCode == RESULT_OK) {

            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getActivity().openFileInput("myImage"));
                //  bitmap=decodeUri()
                imageUploadUri = getImageUri(getContext(), bitmap);

                setProfileImage();

            } catch (FileNotFoundException e) {
                e.printStackTrace();

            }

        }


    }

  /*--------------------------get the uri from bitmap----------------------------*/

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, Constant.TITLE_NAME, null);
        return Uri.parse(path);
    }

    /*-------------------------------------set image into imageciew------------------------------*/

    private void setProfileImage() {

        Glide.with(GroupFragment.this).load(imageUploadUri).apply(RequestOptions.circleCropTransform()).into(ShowDialog.getInstance().group_iv);

    }

}