package com.brst.classmates.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.ReplaceFragment;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


public class Event_Invite_Fragment extends Fragment implements View.OnClickListener {

    View view = null;

    TextView event_tv, invite_tv;
    FrameLayout eventlist_fl, invitelist_fl;

    private FirebaseStorage firebaseStorage;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    StorageReference storageReference;


    public Event_Invite_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_event__invite_, container, false);


            event_tv = (TextView) view.findViewById(R.id.event__tv);
            invite_tv = (TextView) view.findViewById(R.id.invite_tv);
            eventlist_fl = (FrameLayout) view.findViewById(R.id.eventlist_fl);
            invitelist_fl = (FrameLayout) view.findViewById(R.id.invitelist_fl);

            event_tv.setOnClickListener(this);
            invite_tv.setOnClickListener(this);


        }

        Bundle bundle=getArguments();
        if(bundle != null)
        {
            if(bundle.containsKey("event"))
            {
                Log.d("dsa","dasd");
                event_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventborder));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.invite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                event_tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                eventlist_fl.setVisibility(View.GONE);
                invitelist_fl.setVisibility(View.VISIBLE);
            }
        }
      //  firebaseDatabase = FirebaseDatabase.getInstance();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_tv.setText("Events");
        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setOnClickListener(this);
        //((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.event__tv:

                event_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventleft));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.inviteright));
                event_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                eventlist_fl.setVisibility(View.VISIBLE);
                invitelist_fl.setVisibility(View.GONE);

                break;
            case R.id.header_iv:


                EventFragment eventFragment=new EventFragment();
                ReplaceFragment.replace((WelcomeActivity) getActivity(),eventFragment,"Event_Fragment");


                break;

            case R.id.invite_tv:

                event_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventborder));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.invite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                event_tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                eventlist_fl.setVisibility(View.GONE);
                invitelist_fl.setVisibility(View.VISIBLE);

        }

    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else {
            EventFragment eventFragment=new EventFragment();
            ReplaceFragment.replace((WelcomeActivity) getActivity(),eventFragment,"Event_Fragment");
        }
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }



}
