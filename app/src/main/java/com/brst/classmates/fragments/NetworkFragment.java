package com.brst.classmates.fragments;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.ContactAdapter;
import com.brst.classmates.adaptors.NetworkAdapter;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NetworkFragment extends Fragment implements TextWatcher, View.OnClickListener {
    String selected_option = "name";
    RecyclerViewEmptySupport contact_rv;
  public static  EditText searchContactUser_et;
    TextView empty_tv, invite_tv;
    List<HashMap<String, String>> courseSearchtList = new ArrayList<>();
    List<String> courseKey = new ArrayList<>();
    List<String> courseKey_invited = new ArrayList<>();
    ArrayList<HashMap<String, String>> searchContactList;
    ArrayList<HashMap<String, String>> contactsList;
    ImageView iv_search;
    NetworkAdapter contactAdaptor;
    private FirebaseStorage firebaseStorage;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    StorageReference storageReference;
    List<HashMap<String, String>> dataList = new ArrayList<>();
    List<HashMap<String, String>> dataList1 = new ArrayList<>();
    List<HashMap<String, String>> dataList_invited = new ArrayList<>();
    List<String> groupList;
    View view = null;

    public NetworkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_contact, container, false);
        }

        bindViews();
        getInvitedUser();
        getCourseList();
        try{
        }
        catch(Exception e){}
        return view;
    }

    private void getCourseList() {


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("registered_user");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                dataList = new ArrayList<HashMap<String, String>>();
                courseKey = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                    try {
                        HashMap hashMap1 = new HashMap();
                        hashMap1.put(Constant.FIRST_NAME, hashMap.get(Constant.FIRST_NAME).toString());
                        hashMap1.put(Constant.COLLEGE_NAME, hashMap.get("specialization").toString());
                        hashMap1.put(Constant.UNIVERSITY_NAME, hashMap.get(Constant.COLLEGE_NAME).toString());
                        hashMap1.put(Constant.COUNTRY_NAME, hashMap.get(Constant.COUNTRY_NAME).toString());
                        hashMap1.put(Constant.DEGREE_NAME, hashMap.get(Constant.DEGREE_NAME).toString());
                        hashMap1.put(Constant.FIRST_NAME, hashMap.get(Constant.FIRST_NAME).toString());
                        hashMap1.put(Constant.LAST_NAME, hashMap.get(Constant.LAST_NAME).toString());

                        hashMap1.put(Constant.YEAR_NAME, hashMap.get(Constant.YEAR_NAME).toString());
                        hashMap1.put("KEY", dataSnapshot1.getKey());


                        try {
                            hashMap1.put(Constant.PROFILE_PICTURE, hashMap.get(Constant.PROFILE_PICTURE).toString());
                        } catch (Exception e) {
                        }


                        dataList.add(hashMap1);

                        // dataList.add(hashMap.get(Constant.KEY_NAME).toString());

                        courseKey.add(dataSnapshot1.getKey());
                        // hashMap1.put(Constant.KEY_NAME, hashMap.get(dataSnapshot1.getKey()));


                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                }
                try {
                    for (int i = 0; i < dataList.size(); i++) {
                        for (int j = 0; j < dataList_invited.size(); j++) {
                            if (dataList.get(i).get("KEY").equals(dataList_invited.get(j).get("to"))) {
                                dataList.remove(i);
                                courseKey.remove(i);
                            }

                            if ((dataList.get(i).get("KEY").equals(dataList_invited.get(j).get("from")))) {
                                dataList.remove(i);
                                courseKey.remove(i);

                            }

                        }
                    }
                }
                catch (Exception e){}
               /* contactAdaptor = new NetworkAdapter(dataList, getContext(), courseKey, selected_option);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                contact_rv.setLayoutManager(mLayoutManager);
                contact_rv.setAdapter(contactAdaptor);
                contact_rv.setEmptyView(empty_tv);*/


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }

    private void bindViews() {

        contact_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.contact_rv);
        searchContactUser_et = (EditText) view.findViewById(R.id.searchContactUser_et);
        empty_tv = (TextView) view.findViewById(R.id.empty_tv);
        invite_tv = (TextView) view.findViewById(R.id.invite_tv);
        iv_search = (ImageView) view.findViewById(R.id.iv_search);
        searchContactUser_et.addTextChangedListener(this);
        iv_search.setVisibility(View.VISIBLE);
        invite_tv.setOnClickListener(this);
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choose_search_option();
            }
        });
        searchContactUser_et.setText("");

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {


        String s2;
        //  studenSearchtList.clear();

        searchContactList = new ArrayList<>();
        // courseSearchKey.clear();
        String s1 = s.toString().toLowerCase();

        s1 = s1.replace(" ", "");

        try {
            if (s1.length() == 0) {
                try {
                    contactAdaptor.notifyDataSetChanged();

                    contactAdaptor = new NetworkAdapter(dataList1, getContext(), courseKey, selected_option);

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                    contact_rv.setLayoutManager(mLayoutManager);
                    contact_rv.setAdapter(contactAdaptor);
                    contact_rv.setEmptyView(empty_tv);
                } catch (Exception e) {
                }
            }

            else {

                for (int i = 0; i < dataList.size(); i++) {

                    String firstName = dataList.get(i).get(Constant.FIRST_NAME).toLowerCase().replace(" ", "");
                    String collegeName = dataList.get(i).get(Constant.COLLEGE_NAME).toLowerCase().replace(" ", "");
                    String location = dataList.get(i).get(Constant.COUNTRY_NAME).toLowerCase().replace(" ", "");
                    String university = dataList.get(i).get(Constant.UNIVERSITY_NAME).toLowerCase().replace(" ", "");
                    String last_name = dataList.get(i).get(Constant.LAST_NAME).toLowerCase().replace(" ", "");
                    //String fullName = firstName + "" + lastName.replace(" ", "");

                    //|| firstName.contains(s1) || location.contains(s1) || university.contains(s1)
                    if (selected_option.equals("college")) {
                        if (collegeName.contains(s1)) {
                            searchContactList.add(dataList.get(i));
                            // courseSearchKey.add(courseKey.get(i));

                        }
                    } else if (selected_option.equals("name")) {
                        if (firstName.contains(s1)) {
                            searchContactList.add(dataList.get(i));
                            // courseSearchKey.add(courseKey.get(i));

                        }
                    } else if (selected_option.equals("location")) {
                        if (location.contains(s1)) {
                            searchContactList.add(dataList.get(i));
                            // courseSearchKey.add(courseKey.get(i));

                        }
                    } else if (selected_option.equals("university")) {
                        if (university.contains(s1)) {
                            searchContactList.add(dataList.get(i));
                            // courseSearchKey.add(courseKey.get(i));

                        }
                    }

                }
       /* contactAdaptor.addData(searchContactList);
        contactAdaptor.notifyDataSetChanged();*/

                contactAdaptor = new NetworkAdapter(searchContactList, getContext(), courseKey, selected_option);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                contact_rv.setLayoutManager(mLayoutManager);
                contact_rv.setAdapter(contactAdaptor);
                contact_rv.setEmptyView(empty_tv);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.invite_tv:

                List<String> checkList = new ArrayList<>();
                List<HashMap<String, String>> contactList = ((NetworkAdapter) contactAdaptor).getSelectedContactList();

                for (int i = 0; i < contactList.size(); i++) {
                    if (contactList.get(i).get("boolean").equals(Constant.TRUE_NAME)) {


                        checkList.add(contactList.get(i).get("ContactNumber"));

                    }

                }

                if (checkList.isEmpty()) {
                    ShowDialog.showSnackbarError(view, "Please select Contacts from list", getContext());
                } else {

                    Log.d("sds", checkList + "");
                    String sms = "Follow the link: https://play.google.com/store/apps/details?id=com.brst.classmates";
                    for (int i = 0; i < checkList.size(); i++) {

                        try {
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(checkList.get(i), null, sms, null, null);
                            Toast.makeText(getContext(), "SMS Sent to: " + checkList.get(i),
                                    Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(getContext(),
                                    "SMS faild, please try again later!",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }

                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                    break;
                }


        }
    }


    public ArrayList<HashMap<String, String>> sortList(final ArrayList<HashMap<String, String>> contactList) {

        Collections.sort(contactList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                firstValue = s1.get("ContactName").toString();
                secondValue = s2.get("ContactName").toString();

                return firstValue.compareToIgnoreCase(secondValue);
            }
        });

        return contactList;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.GONE);
      //  ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_iv.setImageResource(R.mipmap.add);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);
        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        ((WelcomeActivity) getActivity()).header_tv.setText("");

    }


    public void choose_search_option() {
        final Dialog dialog_search = new Dialog(getActivity());
        final TextView tv_college, tv_university, tv_location, name;

        dialog_search.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_search.setContentView(R.layout.layout_search_option);

        WindowManager.LayoutParams lp = dialog_search.getWindow().getAttributes();
        lp.dimAmount = 0.8f;
        dialog_search.getWindow().setAttributes(lp);

        dialog_search.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        tv_college = (TextView) dialog_search.findViewById(R.id.college);
        tv_university = (TextView) dialog_search.findViewById(R.id.university);

        tv_location = (TextView) dialog_search.findViewById(R.id.location);
        name = (TextView) dialog_search.findViewById(R.id.name);

        tv_college.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_option = "college";
                searchContactUser_et.setText("");
                searchContactUser_et.setHint("Search by specialization name.");
                contactAdaptor = new NetworkAdapter(dataList1, getContext(), courseKey, selected_option);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                contact_rv.setLayoutManager(mLayoutManager);
                contact_rv.setAdapter(contactAdaptor);
                contact_rv.setEmptyView(empty_tv);


                dialog_search.dismiss();

            }
        });

        tv_university.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchContactUser_et.setText("");
                selected_option = "university";
                searchContactUser_et.setHint("Search by college name.");
                contactAdaptor = new NetworkAdapter(dataList1, getContext(), courseKey, selected_option);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                contact_rv.setLayoutManager(mLayoutManager);
                contact_rv.setAdapter(contactAdaptor);
                contact_rv.setEmptyView(empty_tv);

                dialog_search.dismiss();
            }
        });

        tv_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchContactUser_et.setText("");
                selected_option = "location";
                contactAdaptor = new NetworkAdapter(dataList1, getContext(), courseKey, selected_option);
                searchContactUser_et.setHint("Search by location.");

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                contact_rv.setLayoutManager(mLayoutManager);
                contact_rv.setAdapter(contactAdaptor);
                contact_rv.setEmptyView(empty_tv);


                contactAdaptor = new NetworkAdapter(dataList1, getContext(), courseKey, selected_option);
                searchContactUser_et.setHint("Search by location.");
                dialog_search.dismiss();


            }
        });

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchContactUser_et.setText("");
                selected_option = "name";
                searchContactUser_et.setHint("Search by name.");
                contactAdaptor = new NetworkAdapter(dataList1, getContext(), courseKey, selected_option);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                contact_rv.setLayoutManager(mLayoutManager);
                contact_rv.setAdapter(contactAdaptor);
                contact_rv.setEmptyView(empty_tv);

                dialog_search.dismiss();


            }
        });

        dialog_search.show();
    }


    private void getInvitedUser() {


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Constant.INVITED_USER);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                dataList = new ArrayList<HashMap<String, String>>();
                courseKey = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                    Log.d("asda", dataSnapshot1.getKey() + "");
                    HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                    try {
                        HashMap hashMap1 = new HashMap();

                        hashMap1.put("to", hashMap.get("to").toString());
                        hashMap1.put("request_status", hashMap.get("request_status").toString());
                        hashMap1.put("SENDER", hashMap.get("SENDER").toString());
                        hashMap1.put("from", hashMap.get("from").toString());
                        hashMap1.put("RECEIVER", hashMap.get("RECEIVER").toString());

                        hashMap1.put(Constant.COLLEGE_NAME, hashMap.get(Constant.COLLEGE_NAME).toString());
                        hashMap1.put(Constant.UNIVERSITY_NAME, hashMap.get(Constant.UNIVERSITY_NAME).toString());
                        hashMap1.put(Constant.COUNTRY_NAME, hashMap.get(Constant.COUNTRY_NAME).toString());
                        hashMap1.put(Constant.LAST_NAME, hashMap.get(Constant.LAST_NAME).toString());
                        hashMap1.put(Constant.YEAR_NAME, hashMap.get(Constant.YEAR_NAME).toString());
                        hashMap1.put(Constant.DEGREE_NAME, hashMap.get(Constant.DEGREE_NAME).toString());

                        try {
                            try {
                                hashMap1.put("profile_picture", hashMap.get("profile_picture").toString());
                            } catch (Exception e) {
                            }
                            try {
                                hashMap1.put("RECEIVER_PROFILE", hashMap.get("RECEIVER_PROFILE").toString());

                            } catch (Exception e) {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (hashMap.get("to").toString().equals(StoreData.getUserKeyFromSharedPre(getActivity()))) {

                            dataList_invited.add(hashMap1);


                            courseKey_invited.add(dataSnapshot1.getKey());
                        } else if (hashMap.get("from").toString().equals(StoreData.getUserKeyFromSharedPre(getActivity()))) {
                            dataList_invited.add(hashMap1);


                            courseKey_invited.add(dataSnapshot1.getKey());
                        }


                    } catch (Exception e) {

                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }


}

