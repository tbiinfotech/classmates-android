package com.brst.classmates.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.StudentAdaptor;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.activity.WelcomeActivity.context;
import static com.brst.classmates.classses.NetworkCheck.isNetworkAvailable;
import static com.brst.classmates.classses.ShowDialog.networkDialog;


public class SearchStudentFragment extends Fragment implements View.OnClickListener, TextWatcher {
    String mode = "", key = "";

    RecyclerViewEmptySupport student_rv;

    TextView invite_tv, empty_tv;

    EditText searchUser_et;
    int count = 0;

    String registeredUserKey, courseKey, userLoginkey,
            groupKey = "", groupName, userFullName, userLoginname, courseKeyValue, groupUserKey = "";


    View view = null;

    String title, location, time, date, price = "", desc = "", studentKeyList = "";

    List<String> listStudent;

    String imageArray[];

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, groupReference, courseReference, eventdatabaseReference, groupContactReference;

    StudentAdaptor studentAdaptor;
    HashMap map1, hashMap;

    List<HashMap<String, String>> studentList = new ArrayList<>();

    List<HashMap<String, String>> studenSearchtList = new ArrayList<>();
    List<HashMap<String, String>> collegeUserList = new ArrayList<>();

    Bundle bundle;
    HashMap map, ownerMap;
    HashMap mapStudent;

    Fragment fragment, fragment1;

    List<String> groupUser;
    List<HashMap> checkList;

    String groupProfile, collegeName;
    Uri groupProfileUri, downloadImageUri;

    public SearchStudentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_search_student, container, false);
        }
        userLoginkey = SharedPreference.getInstance().getData(getContext(), Constant.KEY_NAME);
        userLoginname = SharedPreference.getInstance().getData(getContext(), Constant.FULL_NAME);
        collegeName = StoreData.getCollegeFromSharedPre(getContext());

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Constant.REGISTERED_NAME);
        eventdatabaseReference = firebaseDatabase.getReference(Constant.EVENT).child("Event");


        fragment = getFragmentManager().findFragmentByTag("SearchStudent_Fragment");
        fragment1 = getFragmentManager().findFragmentByTag("InviteStudent_Fragment");

        bundle = getArguments();
        if (bundle != null) {
            mode = bundle.getString("mode", "");
            if (mode.equals("Event") || mode.equals("Meeting")) {

                //    key = bundle.getString("key", "");
                title = bundle.getString("title");
                location = bundle.getString("location");
                date = bundle.getString("date");
                time = bundle.getString("time");
                imageArray = bundle.getStringArray("imageArray");
                if (bundle.containsKey("price")) {
                    price = bundle.getString("price");
                }
                if (bundle.containsKey("desc")) {
                    desc = bundle.getString("desc");
                }
                if (bundle.containsKey("key")) {
                    key = bundle.getString("key");
                }
                if (bundle.containsKey("studentList")) {

                    studentKeyList = bundle.getString("studentList");

                    listStudent = new ArrayList<String>(Arrays.asList(studentKeyList.split(",")));
                }

            } else if (mode.equals("Network") || mode.equals("College")) {

                groupName = bundle.getString(Constant.GROUP_NAME);

                map1 = new HashMap();
                map1.put(Constant.GROUP_NAME, groupName);
                map1.put("mode", mode);

                ownerMap = new HashMap();

                ownerMap.put(Constant.OWNER, userLoginkey);
                if (bundle.containsKey(Constant.PROFILE_PICTURE)) {
                    groupProfile = bundle.getString(Constant.PROFILE_PICTURE);
                    groupProfileUri = Uri.parse(groupProfile);
                    //  ownerMap.put(Constant.GROUP_ICON, groupProfile);

                }

                if (bundle.containsKey(Constant.KEY_NAME) && bundle.containsKey(Constant.GROUP_kEY)) {
                    groupUserKey = bundle.getString(Constant.KEY_NAME);
                    groupKey = bundle.getString(Constant.GROUP_kEY);
                    courseKey = bundle.getString(Constant.COURSE_KEY);
                    groupContactReference = firebaseDatabase.getReference(Constant.CREATEGROUP_NAME).child(courseKey);
                } else {
                    groupContactReference = firebaseDatabase.getReference(Constant.CREATEGROUP_NAME).push();
                }

            } else {

                groupName = bundle.getString(Constant.GROUP_NAME);
                courseKeyValue = bundle.getString(Constant.COURSE_KEY);


                if (bundle.containsKey(Constant.KEY_NAME) && bundle.containsKey(Constant.GROUP_kEY)) {

                    groupUserKey = bundle.getString(Constant.KEY_NAME);
                    groupKey = bundle.getString(Constant.GROUP_kEY);

                }

                map1 = new HashMap();
                map1.put(Constant.GROUP_NAME, groupName);
                map1.put("mode", mode);

                ownerMap = new HashMap();

                ownerMap.put(Constant.OWNER, userLoginkey);
                if (bundle.containsKey(Constant.PROFILE_PICTURE)) {
                    groupProfile = bundle.getString(Constant.PROFILE_PICTURE);
                    ownerMap.put(Constant.GROUP_ICON, groupProfile);

                }


                groupReference = firebaseDatabase.getReference(Constant.CREATEGROUP_NAME).child(courseKeyValue);
                courseReference = firebaseDatabase.getReference(Constant.COURSE_NAME);


            }

        }


        bindViews(view);

        setAdaptor();

        setListener();

        networkCheck();

        return view;


    }

    private void networkCheck() {


        if (!isNetworkAvailable(getContext())) {

            networkDialog(getContext());

            if (mode.equals("Network")) {

                getNetworkContacts();

            } else if (mode.equals("College")) {
                getCollegeStudent();
            } else {
                getStudentList();
            }

        } else {

            if (mode.equals("Network")) {

                getNetworkContacts();

            } else if (mode.equals("College")) {

                getCollegeStudent();
            }
            else {
                getStudentList();
            }
        }
    }


    private void getCollegeStudent() {

        if (groupUserKey != null && !groupUserKey.isEmpty()) {

            groupUser = new ArrayList<String>(Arrays.asList(groupUserKey.split(",")));
            groupUser.add(userLoginkey);

            FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).orderByChild("specialization")
                    .equalTo(collegeName).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    collegeUserList = new ArrayList<HashMap<String, String>>();
                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();

                    while (iterator.hasNext()) {
                        DataSnapshot dataSnapshot1 = iterator.next();

                        if (!groupUser.contains(dataSnapshot1.getKey())) {
                            String status = dataSnapshot1.child(Constant.STATUS_NAME).exists() ? dataSnapshot1.child(Constant.STATUS_NAME).getValue().toString() : "";

                            if (status.equals("active")) {

                                HashMap<String, String> hashMap = (HashMap) dataSnapshot1.getValue();
                                hashMap.put(Constant.CHECKED_NAME, Constant.FALSE_NAME);
                                hashMap.put(Constant.STUDENT_KEY, dataSnapshot1.getKey());
                                collegeUserList.add(hashMap);
                            }
                        }
                    }

                    //createGroup("College");
                    studentAdaptor.addData(collegeUserList);
                    studentAdaptor.notifyDataSetChanged();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void getNetworkContacts() {


        if (groupUserKey.isEmpty() && studentKeyList.isEmpty())

        {

           /* if (NetworkCheck.isNetworkAvailable(getContext())) {
                ShowDialog.showDialog(getContext());
            }*/
            FirebaseDatabase.getInstance().getReference(Constant.INVITED_USER).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();

                    while (iterator.hasNext()) {
                        DataSnapshot dataSnapshot1 = iterator.next();

                        if (dataSnapshot1.child("request_status").getValue().equals("Accepted")) {
                            if (dataSnapshot1.child("from").getValue().equals(userLoginkey)) {
                                getStudentDetail(dataSnapshot1.child("to").getValue().toString());
                            } else if (dataSnapshot1.child("to").getValue().equals(userLoginkey)) {
                                getStudentDetail(dataSnapshot1.child("from").getValue().toString());
                            }
                        }
                    }

                }


                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {

            groupUser = new ArrayList<String>(Arrays.asList(groupUserKey.split(",")));
            groupUser.add(userLoginkey);

            // ShowDialog.showDialog(getContext());

            FirebaseDatabase.getInstance().getReference(Constant.INVITED_USER).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();

                    while (iterator.hasNext()) {
                        DataSnapshot dataSnapshot1 = iterator.next();
                        //   if (!groupUser.contains(dataSnapshot1.child("from").getValue()) && !groupUser.contains(dataSnapshot1.child("to").getValue())) {

                        if (dataSnapshot1.child("request_status").getValue().equals("Accepted")) {
                            if (dataSnapshot1.child("from").getValue().equals(userLoginkey)) {

                                if (!groupUser.contains(dataSnapshot1.child("to").getValue())) {

                                    getStudentDetail(dataSnapshot1.child("to").getValue().toString());
                                }
                            } else if (dataSnapshot1.child("to").getValue().equals(userLoginkey)) {

                                if (!groupUser.contains(dataSnapshot1.child("from").getValue())) {

                                    getStudentDetail(dataSnapshot1.child("from").getValue().toString());
                                }
                            }
                        }

                    }
                }

                //  }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void getStudentDetail(String key) {

        databaseReference.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d("datasnapshot", dataSnapshot.getValue() + "");

                if (!dataSnapshot.getKey().equals(userLoginkey)) {

                    if (dataSnapshot.child("status").exists()) {

                        if (dataSnapshot.child("status").getValue().equals("active")) {

                            HashMap<String, String> hashMap = (HashMap) dataSnapshot.getValue();
                            hashMap.put(Constant.CHECKED_NAME, Constant.FALSE_NAME);
                            hashMap.put(Constant.STUDENT_KEY, dataSnapshot.getKey());
                            studentList.add(hashMap);
                        }
                    }

                    studentAdaptor.addData(studentList);
                    studentAdaptor.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setAdaptor() {

        studentAdaptor = new StudentAdaptor(studentList, getContext());
        RecyclerViewEmptySupport.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        student_rv.setLayoutManager(mLayoutManager);
        student_rv.setAdapter(studentAdaptor);
        student_rv.setEmptyView(empty_tv);
    }

    private void bindViews(View view) {

        student_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.student_rv);

        invite_tv = (TextView) view.findViewById(R.id.invite_tv);
        empty_tv = (TextView) view.findViewById(R.id.empty_tv);

        searchUser_et = (EditText) view.findViewById(R.id.searchUser_et);

        searchUser_et.addTextChangedListener(this);


    }


    private void setListener() {

        invite_tv.setOnClickListener(this);
    }

    private void getStudentList() {

        if (groupUserKey.isEmpty() && studentKeyList.isEmpty()) {

            if (NetworkCheck.isNetworkAvailable(getContext())) {
                ShowDialog.showDialog(getContext());
            }

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    studentList = new ArrayList<HashMap<String, String>>();

                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                        if (!dataSnapshot1.getKey().equals(userLoginkey)) {

                            if (dataSnapshot1.child("status").exists()) {

                                if (dataSnapshot1.child("status").getValue().toString().equals("active")) {

                                    HashMap<String, String> hashMap = (HashMap) dataSnapshot1.getValue();
                                    hashMap.put(Constant.CHECKED_NAME, Constant.FALSE_NAME);
                                    hashMap.put(Constant.STUDENT_KEY, dataSnapshot1.getKey());
                                    studentList.add(hashMap);
                                }
                            }

                        }

                    }

                    studentAdaptor.addData(studentList);
                    studentAdaptor.notifyDataSetChanged();

                    //  try {
                    if (fragment != null && fragment.isVisible() || fragment1 != null && fragment1.isVisible()) {

                        //    if (NetworkCheck.isNetworkAvailable(getContext())) {
                        if (ShowDialog.dialog != null) {
                            if (ShowDialog.dialog.isShowing()) {
                                ShowDialog.hideDialog();
                            }
                        }
                    }
                    //  } catch (Exception e) {

                    //  }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                    //    if (NetworkCheck.isNetworkAvailable(getContext())) {
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }


                }
            });


        } else if (studentKeyList.isEmpty()) {

            groupUser = new ArrayList<String>(Arrays.asList(groupUserKey.split(",")));
            groupUser.add(userLoginkey);

            ShowDialog.showDialog(getContext());

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                        if (!groupUser.contains(dataSnapshot1.getKey())) {

                            if (dataSnapshot1.child("status").exists()) {
                                if (dataSnapshot1.child("status").getValue().toString().equals("active")) {

                                    HashMap<String, String> hashMap = (HashMap) dataSnapshot1.getValue();
                                    hashMap.put(Constant.CHECKED_NAME, Constant.FALSE_NAME);
                                    hashMap.put(Constant.STUDENT_KEY, dataSnapshot1.getKey());

                                    studentList.add(hashMap);
                                }

                            }
                        }

                    }

                    studentAdaptor.addData(studentList);
                    studentAdaptor.notifyDataSetChanged();

                    ShowDialog.hideDialog();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    ShowDialog.hideDialog();


                }
            });
        } else {

            ShowDialog.showDialog(getContext());

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                        if (!listStudent.contains(dataSnapshot1.getKey())) {

                            if (dataSnapshot1.child("status").exists()) {
                                if (dataSnapshot1.child("status").getValue().toString().equals("active")) {

                                    HashMap<String, String> hashMap = (HashMap) dataSnapshot1.getValue();
                                    hashMap.put(Constant.CHECKED_NAME, Constant.FALSE_NAME);
                                    hashMap.put(Constant.STUDENT_KEY, dataSnapshot1.getKey());

                                    studentList.add(hashMap);
                                }
                            }
                        }

                    }

                    studentAdaptor.addData(studentList);
                    studentAdaptor.notifyDataSetChanged();

                    ShowDialog.hideDialog();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    ShowDialog.hideDialog();


                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);

        Fragment fragment = getFragmentManager().findFragmentByTag("InviteStudent_Fragment");
        Fragment fragment1 = getFragmentManager().findFragmentByTag("SearchStudent_Fragment");
        ((WelcomeActivity) getActivity()).header_tv.setText("Invite");

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.invite_tv:

                if (!isNetworkAvailable(getContext())) {
                    ShowDialog.networkDialog(getContext());
                } else {


                    if (mode != null && !mode.isEmpty() && mode.equals("Event") || mode.equals("Meeting")) {


                        getInviteListForEvent(mode);
                    } else if (mode.equals("Network") || mode.equals("College")) {

                        getNetWorkContactList();

                    } else {

                        getInviteList();
                    }
                }

                break;
        }

    }

    private void getNetWorkContactList() {

        checkList = new ArrayList<>();
        List<HashMap<String, String>> studentList = ((StudentAdaptor) studentAdaptor).getStudentist();

        for (int i = 0; i < studentList.size(); i++) {
            if (studentList.get(i).get(Constant.CHECKED_NAME).equals(Constant.TRUE_NAME)) {
                checkList.add(studentList.get(i));
            }

        }

        if (checkList.isEmpty()) {
            ShowDialog.showSnackbarError(view, "Please select student from list", getContext());
        } else {
            ShowDialog.showDialog(getContext());

            if (groupProfileUri != null && !groupProfileUri.equals(Uri.EMPTY)) {


                imageUpload();

            } else {

                addContactGroup();
            }
        }

    }

    private void imageUpload() {

        // ShowDialog.showDialog(getContext());

        FirebaseStorage.getInstance().getReference().child(Constant.IMAGE_NAME + "_" + groupProfileUri + System.currentTimeMillis())
                .putFile(groupProfileUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                        ShowDialog.hideDialog();

                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();

                        //  ShowDialog.hideDialog();

                        addContactGroup();


                    }
                });

    }

    int i = 0;

    private void addContactGroup() {

        if (groupKey.isEmpty()) {

            groupContactReference = groupContactReference.child(groupContactReference.push().getKey());
            if (downloadImageUri != null && !downloadImageUri.equals(Uri.EMPTY)) {

                map1.put(Constant.GROUP_ICON, downloadImageUri.toString());
            }
            groupContactReference.updateChildren(map1);

            groupContactReference.updateChildren(ownerMap);
            HashMap hashMap = new HashMap<>();

            hashMap.put(userLoginkey, userLoginname);

            groupContactReference.child("users").updateChildren(hashMap);

            addUsersToGroup(i);


        } else {

            groupContactReference = groupContactReference.child(groupKey);

            addUsersToGroup(i);
        }


    }

    private void addUsersToGroup(int i) {

        mapStudent = new HashMap();
        if (i < checkList.size()) {
            registeredUserKey = checkList.get(i).get(Constant.STUDENT_KEY).toString();
            String firstname = checkList.get(i).get(Constant.FIRST_NAME).toString();
            String lastName = checkList.get(i).get(Constant.LAST_NAME).toString();
            userFullName = firstname + " " + lastName;
            mapStudent.put(registeredUserKey, userFullName);

            groupContactReference.child("users").updateChildren(mapStudent);

            i++;
            addUsersToGroup(i);

        }
        if (i == checkList.size()) {

            ShowDialog.hideDialog();
            if (fragment != null && fragment.isVisible()) {
                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                ReplaceFragment.replace((WelcomeActivity) getActivity(), new GroupFragment(), "visibleSearch_fragment");
            }
        }


    }


    private void addCourseToRegisteredUser(String registeredUserKey, HashMap hashMap) {


        courseReference.child(registeredUserKey).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {

                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }

                } else {
                    Toast.makeText(getContext(), "fail", Toast.LENGTH_LONG).show();
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }
                }
            }
        });
    }


    private void addCourseToLoginUser(HashMap<String, Object> hashMap, final List<HashMap> checkList) {

        courseReference.child(userLoginkey).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {


                if (task.isSuccessful()) {
                    getEmail(checkList);

                    replace();
                }
            }
        });

    }

    private void getInviteList() {

        List<HashMap> checkList = new ArrayList<>();
        List<HashMap<String, String>> studentList = ((StudentAdaptor) studentAdaptor).getStudentist();

        for (int i = 0; i < studentList.size(); i++) {
            if (studentList.get(i).get(Constant.CHECKED_NAME).equals(Constant.TRUE_NAME)) {
                checkList.add(studentList.get(i));
            }

        }

        if (checkList.isEmpty()) {
            ShowDialog.showSnackbarError(view, "Please select student from list", getContext());
        } else {
            ShowDialog.showDialog(getContext());

            if (groupKey.isEmpty()) {

                groupReference = groupReference.child(groupReference.push().getKey());

                groupReference.updateChildren(map1);

                groupReference.updateChildren(ownerMap);
                HashMap hashMap = new HashMap<>();

                hashMap.put(userLoginkey, userLoginname);

                groupReference.child("users").updateChildren(hashMap);


                final HashMap map = new HashMap();

                map.put(courseKeyValue, Constant.COURSE_NAME);

                addCourseToLoginUser(map, checkList);

            } else {

                groupReference = groupReference.child(groupKey);


                //  ShowDialog.showDialog(getContext());

                final HashMap map = new HashMap();

                map.put(courseKeyValue, Constant.COURSE_NAME);

                addCourseToLoginUser(map, checkList);
            }


        }

    }

    private void getInviteListForEvent(final String mode) {

        List<HashMap> checkList = new ArrayList<>();
        List<HashMap<String, String>> studentList = ((StudentAdaptor) studentAdaptor).getStudentist();

        for (int i = 0; i < studentList.size(); i++) {
            if (studentList.get(i).get(Constant.CHECKED_NAME).equals(Constant.TRUE_NAME)) {


                checkList.add(studentList.get(i));

            }

        }

        if (checkList.isEmpty()) {
            ShowDialog.showSnackbarError(view, "Please select student from list", getContext());
        } else if (mode.equals("Event") || mode.equals("Meeting")) {
            List<String> newList = new ArrayList<>();

            for (int i = 0; i < checkList.size(); i++) {
                newList.add(checkList.get(i).get(Constant.STUDENT_KEY).toString());
            }

            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.putExtra(Constant.CHECK_LIST, (Serializable) newList);
            getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
            ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
        }

    }


    private void getEmail(List<HashMap> checkList) {


        for (int ii = 0; ii < checkList.size(); ii++) {


            final String email = checkList.get(ii).get("email").toString();

            getRegisteredUserKey(email);


        }
    }

    private void replace() {

        if (ShowDialog.dialog != null) {
            if (ShowDialog.dialog.isShowing()) {
                ShowDialog.hideDialog();
            }
        }
        if (fragment != null && fragment.isVisible()) {
            ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
            ReplaceFragment.replace((WelcomeActivity) getActivity(), new GroupFragment(), "groupSearch_fragment");
        }

    }

    private void getRegisteredUserKey(String email) {

        databaseReference.orderByChild("email").equalTo(email).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String firstName = "", lastName = "";
                mapStudent = new HashMap();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    registeredUserKey = snapshot.getKey();
                    if (snapshot.child("first_name").exists()) {
                        firstName = snapshot.child("first_name").getValue().toString();
                    }
                    if (snapshot.child("first_name").exists()) {
                        lastName = snapshot.child("last_name").getValue().toString();
                    }

                    userFullName = firstName + " " + lastName;

                    mapStudent.put(registeredUserKey, userFullName);
                }


                groupReference.child("users").updateChildren(mapStudent);

                map = new HashMap<>();
                map.put(courseKeyValue, Constant.COURSE_NAME);
                addCourseToRegisteredUser(registeredUserKey, map);
/*
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }*/


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


    }

    @Override
    public void afterTextChanged(Editable s) {


        String s2;
        studenSearchtList.clear();
        // courseSearchKey.clear();
        String s1 = s.toString().toLowerCase();

        s1 = s1.replace(" ", "");

        for (int i = 0; i < studentList.size(); i++) {

            String firstName = studentList.get(i).get(Constant.FIRST_NAME).toLowerCase().replace(" ", "");
            String lastName = studentList.get(i).get(Constant.LAST_NAME).toLowerCase().replace(" ", "");

            String fullName = firstName + "" + lastName.replace(" ", "");

            if (fullName.contains(s1)) {
                studenSearchtList.add(studentList.get(i));
                // courseSearchKey.add(courseKey.get(i));

            }

        }
        studentAdaptor.addData(studenSearchtList);
        studentAdaptor.notifyDataSetChanged();


    }
}
