package com.brst.classmates.fragments;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.adaptors.InviteEventAdaptor;
import com.brst.classmates.classses.AlarmReceiver;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.OnClickEvent;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import static android.content.Context.ALARM_SERVICE;


public class InviteListFragment extends Fragment implements OnClickEvent, View.OnClickListener {

    RecyclerViewEmptySupport invite_rv;


    EventFragment eventFragment = new EventFragment();

    Dialog dialog;

    TextView done_tv, cancel_tv, delete_tv,result;
    InviteEventAdaptor inviteEventAdaptor;
    View view = null;

    DatabaseReference eventRef, meetingRef;

    FirebaseDatabase firebaseDatabase;
    private ArrayList<HashMap> eventList;

    String user_key = "";


    public InviteListFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_invite_list, container, false);
        }
        user_key = StoreData.getUserKeyFromSharedPre(getActivity());
        invite_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.invite_rv);
        result = (TextView) view.findViewById(R.id.result);

        eventList = new ArrayList<HashMap>();
        inviteEventAdaptor = new InviteEventAdaptor(eventList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        invite_rv.setLayoutManager(mLayoutManager);
        invite_rv.setAdapter(inviteEventAdaptor);
        invite_rv.setEmptyView(result);

        inviteEventAdaptor.setClickListener(this);
        initFireBaseDataBase();
        return view;
    }


    private void initFireBaseDataBase() {


        firebaseDatabase = FirebaseDatabase.getInstance();

        eventRef = firebaseDatabase.getReference(Constant.EVENT);

        meetingRef = firebaseDatabase.getReference(Constant.EVENT);


        if (!NetworkCheck.isNetworkAvailable(getContext())) {
          //  ShowDialog.networkDialog(getContext());

            getEventFromEvent();

        } else {
            getEventFromEvent();
        }

    }

    private void getEventFromEvent() {

     //   ShowDialog.showDialog(getContext());


        eventRef.child("Event").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    eventList = new ArrayList<HashMap>();

                    Iterator<DataSnapshot> iteratedValue = dataSnapshot.getChildren().iterator();

                    while (iteratedValue.hasNext()) {
                        DataSnapshot dataSnapshot1 = iteratedValue.next();
                        String student_key = dataSnapshot1.getKey();
                        Log.d("student_key ", "= " + student_key + "--" + dataSnapshot1.child("admin").getValue());

                        String admin = (String) dataSnapshot1.child("admin").getValue();

                        DataSnapshot dataSnapshot2 = dataSnapshot1.child(Constant.INVITED_STUDENTS);

                        if (dataSnapshot2.child(user_key).exists()) {

                            String userEventKey = dataSnapshot2.child(user_key).getKey();
                            String userEventValue = dataSnapshot2.child(user_key).getValue().toString();

                            if (!userEventKey.equals(admin) && userEventValue.equals("Waiting")) {
                                HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                                Log.d("hashmap_is", "=" + hashMap);

                                hashMap.put("event_key", student_key);
                                hashMap.put("status", userEventValue);

                                eventList.add(hashMap);
                            }

                        }

                    }


                    // getEventFromMeeting();

                    Collections.reverse(eventList);

                    inviteEventAdaptor.addList(eventList);

                    //   ShowDialog.hideDialog();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


    private void getEventFromMeeting() {

        meetingRef.child("Meeting").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterator<DataSnapshot> iteratedValue = dataSnapshot.getChildren().iterator();

                while (iteratedValue.hasNext()) {
                    DataSnapshot dataSnapshot1 = iteratedValue.next();
                    String student_key = dataSnapshot1.getKey();
                    String admin = (String) dataSnapshot1.child("admin").getValue();

                    DataSnapshot dataSnapshot2 = dataSnapshot1.child(Constant.INVITED_STUDENTS);
                    if (dataSnapshot2.child(user_key).exists()) {
                        String userMeetingKey = dataSnapshot2.child(user_key).getKey();
                        String userMeetingValue = dataSnapshot2.child(user_key).getValue().toString();

                        if (!userMeetingKey.equals(admin)) {
                            HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                            Log.d("hashmap_is", "=" + hashMap);
                            hashMap.put("event_key", student_key);
                            hashMap.put("type", "Meeting");
                            hashMap.put("status", userMeetingValue);

                            eventList.add(hashMap);
                        }
                    }


                }
                Collections.reverse(eventList);

                inviteEventAdaptor.addList(eventList);

              //  ShowDialog.hideDialog();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void openDialog(Context context, int i) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();


        done_tv = (TextView) dialog.findViewById(R.id.logdone_tv);
        cancel_tv = (TextView) dialog.findViewById(R.id.logCancel_tv);
        delete_tv = (TextView) dialog.findViewById(R.id.log_tv);
        View view = (View) dialog.findViewById(R.id.view);


        if (i == 0) {
            delete_tv.setText("Event has been Accepted");
        } else if (i == 1) {
            delete_tv.setText("Event has been Rejected");
        } else if (i == 2) {
            delete_tv.setText("Event has been Accepted");
        }

        done_tv.setVisibility(View.GONE);
        view.setVisibility(View.GONE);
        cancel_tv.setText("Ok");

        cancel_tv.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.logCancel_tv:

                dialog.dismiss();
                break;
        }

    }

    @Override
    public void onClickEventListener(View view, int position, String type, String event_key) {
        switch (view.getId()) {
            case R.id.accept_tv:

                String data=view.getTag().toString();

                String eventdata[]=data.split(";");
                String eventName=eventdata[0];
                String eventLocation=eventdata[1];
                String eventDate=eventdata[2];
                String eventTime=eventdata[3];

                String date[]=eventDate.split(" ");
                String dateevent=date[0];
                String month=date[1];
                String year=date[2];

                String  time[]=eventTime.split(":");
                String hour=time[0];
                String minute=time[1];

                int monthInt = 0;
                Calendar cal = Calendar.getInstance();

                try {
                    cal.setTime(new SimpleDateFormat("MMM").parse(month));
                    monthInt = cal.get(Calendar.MONTH);


                } catch (ParseException e) {
                    e.printStackTrace();

                }
                if (type.equals("Meeting")) {

                    meetingRef.child("Event").child(event_key).child("invited_students").child(StoreData.getUserKeyFromSharedPre(getContext())).setValue(Constant.ACCEPTED_NAME);
                    //    getEventFromMeeting(true,Constant.ACCEPTED_NAME,event_key);
                    openDialog(getContext(), 0);

                } else if (type.equals("Event")) {
                    Log.d("dsff", event_key);
                    eventRef.child("Event").child(event_key).child("invited_students").child(StoreData.getUserKeyFromSharedPre(getContext())).setValue(Constant.ACCEPTED_NAME);
                    //    getEventFromEvent(true,Constant.ACCEPTED_NAME,event_key);
                    openDialog(getContext(), 0);
                }

                Calendar calen = Calendar.getInstance();
                calen.set(Calendar.MONTH, monthInt);
                calen.set(Calendar.YEAR, Integer.parseInt(year));
                calen.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateevent));
                calen.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
                calen.set(Calendar.MINUTE, Integer.parseInt(minute)-30);
                calen.set(Calendar.SECOND, 0);
                int val = (int) System.currentTimeMillis();

                if (checkDrawOverlayPermission()) {

                    String timeformnat=eventDate+" "+eventTime;
                    Bundle bundle=new Bundle();
                    bundle.putString(Constant.EVENT, String.valueOf(val));
                    bundle.putString(Constant.EVENT_NAME, eventName);
                    bundle.putString("EventLocation", eventLocation);
                    bundle.putString("EventDate", timeformnat);

                    Intent _myIntent = new Intent(getContext(), AlarmReceiver.class);
                    _myIntent.putExtra(Constant.EVENT_NAME, bundle);

                    PendingIntent _myPendingIntent = PendingIntent.getBroadcast(getContext(), val, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                    AlarmManager _myAlarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

                    _myAlarmManager.set(AlarmManager.RTC_WAKEUP, calen.getTimeInMillis(), _myPendingIntent);


                }

                Calendar cale = Calendar.getInstance();
                cale.set(Calendar.MONTH, monthInt);
                cale.set(Calendar.YEAR, Integer.parseInt(year));
                cale.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateevent));
                cale.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
                cale.set(Calendar.MINUTE, Integer.parseInt(minute)-5);
                cale.set(Calendar.SECOND, 0);
                int value = (int) System.currentTimeMillis();

                if (checkDrawOverlayPermission()) {

                    String timeformnat=eventDate+" "+eventTime;
                    Bundle bundle=new Bundle();
                    bundle.putString(Constant.EVENT, String.valueOf(val));
                    bundle.putString(Constant.EVENT_NAME, eventName);
                    bundle.putString("EventLocation", eventLocation);
                    bundle.putString("EventDate", timeformnat);

                    Intent _myIntent = new Intent(getContext(), AlarmReceiver.class);
                    _myIntent.putExtra(Constant.EVENT_NAME, bundle);

                    PendingIntent _myPendingIntent = PendingIntent.getBroadcast(getContext(), value, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                    AlarmManager _myAlarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

                    _myAlarmManager.set(AlarmManager.RTC_WAKEUP, cale.getTimeInMillis(), _myPendingIntent);


                }




                break;

            case R.id.reject_tv:
                if (type.equals("Meeting")) {

                    meetingRef.child("Event").child(event_key).child("invited_students").child(StoreData.getUserKeyFromSharedPre(getContext())).setValue(Constant.REJECTED_NAME);
                    //  getEventFromMeeting(true,Constant.REJECTED_NAME,event_key);
                    openDialog(getContext(), 1);
                } else if (type.equals("Event")) {

                    eventRef.child("Event").child(event_key).child("invited_students").child(StoreData.getUserKeyFromSharedPre(getContext())).setValue(Constant.REJECTED_NAME);
                    //  getEventFromEvent(true,Constant.REJECTED_NAME,event_key);
                    openDialog(getContext(), 1);
                }


                break;

            case R.id.maybe_tv:
                if (type.equals("Meeting")) {

                    meetingRef.child("Event").child(event_key).child("invited_students").child(StoreData.getUserKeyFromSharedPre(getContext())).setValue(Constant.MAYBE);
                    //  getEventFromMeeting(true,Constant.MAYBE,evnt_key);
                    openDialog(getContext(), 2);
                } else if (type.equals("Event")) {

                    eventRef.child("Event").child(event_key).child("invited_students").child(StoreData.getUserKeyFromSharedPre(getContext())).setValue(Constant.MAYBE);
                    //  getEventFromEvent(true,Constant.MAYBE,event_key);
                    openDialog(getContext(), 2);
                }

        }


    }


    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(getContext())) {

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getContext().getPackageName()));
            startActivityForResult(intent, 500);
            return false;
        } else {
            return true;
        }
    }
}
