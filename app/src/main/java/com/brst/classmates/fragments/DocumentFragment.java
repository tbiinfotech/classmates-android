package com.brst.classmates.fragments;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.CourseAdaptor;
import com.brst.classmates.adaptors.DocumentAdapter;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.utility.Constant;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.activity.WelcomeActivity.context;

/**
 * A simple {@link Fragment} subclass.
 */
public class DocumentFragment extends Fragment implements ClickRecyclerInterface{

    View view;

    RecyclerViewEmptySupport documentRV;

    TextView resultTv;

    private int count;
    private String[] arrPath;
    private boolean[] thumbnailsselection;
    ArrayList<String> path = new ArrayList<>();

    private Bitmap[] thumbnails;
    ArrayList<String> al = new ArrayList<>();
    GestureDetector detector;

    public DocumentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_document, container, false);

            bindViews();

            getDocument();
        }
        return view;
    }

    private void getDocument() {

        String[] projection = {MediaStore.Files.FileColumns.DATA, MediaStore.Files.FileColumns._ID};
        String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + "="
                + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE;
        Cursor cursor = getContext().getContentResolver().query(
                MediaStore.Files.getContentUri("external"),
                projection,
                selection,
                null,
                null);
        final int image_column_index = cursor.getColumnIndex(MediaStore.Files.FileColumns._ID);
        System.out.println(image_column_index + "-------------index");
        Log.d("image_column_index", String.valueOf(image_column_index));
        this.count = cursor.getCount();
        this.arrPath = new String[this.count];
        this.thumbnailsselection = new boolean[this.count];
        for (int i = 0; i < this.count; i++) {
            cursor.moveToPosition(i);
            int id =
                    cursor.getInt(image_column_index);
            int dataColumnIndex =
                    cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            arrPath[i] =
                    cursor.getString(dataColumnIndex);
            File f = new File(arrPath[i]);
            if (f.getName().endsWith(".pdf")
                    || f.getName().endsWith(".docx")
                    || f.getName().endsWith(".txt")
                    ) {
                path.add(arrPath[i]);

            }
        }
        Log.d("path", path + "");
      //  documentRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
       // documentRV.setAdapter(new DocumentAdapter(getContext(), path));

        DocumentAdapter   documentAdapter = new DocumentAdapter(getContext(),path);
        RecyclerViewEmptySupport.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        documentRV.setLayoutManager(mLayoutManager);
        documentRV.setAdapter(documentAdapter);
        documentRV.setEmptyView(resultTv);

        documentAdapter.setClickListener(this);


       // documentRV.setEmptyView(emptyview);

    }

    private void bindViews() {

        documentRV = (RecyclerViewEmptySupport) view.findViewById(R.id.documentRV);
        resultTv = (TextView) view.findViewById(R.id.resultTv);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_tv.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.INVISIBLE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).profile_iv.setImageResource(R.mipmap.ic_backarr);
        ((WelcomeActivity) getActivity()).profile_iv.setPadding(0,10,0,10);
        ((WelcomeActivity) getActivity()).profile_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
            }
        });
        ((WelcomeActivity) getActivity()).header_tv.setText("Document");

    }

    @Override
    public void onRecyClick(View view, int position) {


        Intent intent = new Intent(context, WelcomeActivity.class);
        intent.putExtra(Constant.FILE_NAME, view.getTag().toString());
        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
        ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
    }

    @Override
    public void onLongClick(View view, int position) {

    }
}
