package com.brst.classmates.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.AssignmentAdaptor;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.utility.Constant;


public class Assign_ExamFragment extends Fragment implements View.OnClickListener {

    View view = null;

    TextView assigns__tv, invite_tv;

    public String assignment;

    FrameLayout assignment_fl, inviteexam_fl;

    AddAssignmentFragment addAssignmentFragment; ;
    AssignmentDetail assignmentDetail = new AssignmentDetail();


    public Assign_ExamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_assign__exam, container, false);

            bindViews(view);

            setListener();
        }

        Bundle bundle = getArguments();

        if (bundle != null) {
            if(bundle.containsKey("assignment")) {
                assigns__tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventborder));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.invite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                assigns__tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                assignment_fl.setVisibility(View.GONE);
                inviteexam_fl.setVisibility(View.VISIBLE);
            }

        }

        return view;
    }

    private void bindViews(View view) {

        assigns__tv = (TextView) view.findViewById(R.id.assigns__tv);
        invite_tv = (TextView) view.findViewById(R.id.invite_tv);

        inviteexam_fl = (FrameLayout) view.findViewById(R.id.inviteexam_fl);
        assignment_fl = (FrameLayout) view.findViewById(R.id.assignment_fl);

    }

    private void setListener() {
        assigns__tv.setOnClickListener(this);
        invite_tv.setOnClickListener(this);
        assignment_fl.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_tv.setText("Tasks");
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
      //  ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);

        ((WelcomeActivity) getActivity()).header_iv.setImageResource(R.mipmap.add);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);

        ((WelcomeActivity) getActivity()).header_iv.setOnClickListener(this);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.header_iv:

                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag("AssignExam_Fragment");

                if (fragment != null && fragment.isVisible()) {

                    addAssignmentFragment = new AddAssignmentFragment();
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), addAssignmentFragment, "AddAssignment_Fragment");
                }
                break;

            case R.id.assigns__tv:

                assigns__tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventleft));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.inviteright));
                assigns__tv.setTextColor(getResources().getColor(R.color.colorWhite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                assignment_fl.setVisibility(View.VISIBLE);
                inviteexam_fl.setVisibility(View.GONE);

                break;

            case R.id.invite_tv:

                assigns__tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventborder));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.invite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                assigns__tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                assignment_fl.setVisibility(View.GONE);
                inviteexam_fl.setVisibility(View.VISIBLE);

                break;


        }

    }
}
