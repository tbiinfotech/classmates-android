package com.brst.classmates.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.FirebaseData;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.StorageMetadata;

import com.netcompss.ffmpeg4android.CommandValidationException;
import com.netcompss.ffmpeg4android.GeneralUtils;
import com.netcompss.loader.LoadJNI;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;


import com.brst.classmates.adaptors.MesssageBoardadaptor;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;


import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconsPopup;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.activity.WelcomeActivity.context;
import static com.brst.classmates.fragments.OneToOneChatFragment.tempraryDirectory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageBoardFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener, ClickRecyclerInterface {

    RecyclerView message_rv;

    ImageView profile_iv, back_msg_iv, selectimage_iv, emoji_iv, send_iv, addcourse_iv;

    String name[] = {"2:09"}, keyimage;

    String picture_Path, stranger, courseKey, mode, videoPath, videoThumbString, tempraryVideoPath;
    Bitmap videoThumb;
    File destination_path;

    View contentRoot;

    ByteArrayOutputStream baos;
    byte[] byteArray;

    Uri imageuri, imageUploadUri, downloadImageUri, videoUri, downloadVideoUri, downloadVideoThumb;

    FirebaseStorage firebaseStorage;
    StorageReference storageReference;

    String displayName = null;

    public static boolean active = false;

    List<String> stringList = new ArrayList<>();

    public static String groupName, groupKey, userList, userFullName, owner;
    HashMap<String, String> hashMap;
    public static String loginUserKey, image;

    List<HashMap<String, String>> list = new ArrayList();
    List<String> tokenlist = new ArrayList();
    List<String> keylist = new ArrayList();
    List<byte[]> byteArrayList = new ArrayList();

    Dialog messageDialog;

    TextView report_tv;

    MesssageBoardadaptor messsageBoardadaptor;

    StringBuilder userName = new StringBuilder();

    Boolean compress = true;

    public static ArrayList<FirebaseData> chatList = new ArrayList<>();
    public static final ArrayList<String> tokenList = new ArrayList<>();
    public static final ArrayList<String> myList = new ArrayList<>();
    public static final ArrayList<String> compressVideo = new ArrayList<>();

    TextView group_tv, users_tv;

    PopupWindow popupWindow;
    LinearLayout root_view;
    LinearLayout memeber_ll;

    EmojiconsPopup popup;
    EmojiconEditText emojiconEditText;
    private EmojIconActions emojIcon;

    FirebaseDatabase firebaseDatabase;

    Fragment fragment;

    DatabaseReference databaseReference, reference;

    ProfileFragment messageBoardFragment = new ProfileFragment();

    int SELECT_VIDEO = 980, SELECT_FILE = 450;

    View view = null;

    private final int STOP_TRANSCODING_MSG = -1;
    private final int FINISHED_TRANSCODING_MSG = 0;
    LoadJNI vk = null;
    String workFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";

    int i = 0;

    public MessageBoardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_message_board, container, false);
        }

        userName.setLength(0);
        loginUserKey = StoreData.getUserKeyFromSharedPre(getContext());
        image = StoreData.getProfileFromSharedPre(getContext());
        userFullName = StoreData.getUserFullNameFromSharedPre(getContext());

        bindViews(view);

        Bundle bundle = getArguments();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        final String utcTime = sdf.format(new Date());
        Date date = null;
        try {
            date = sdf.parse(utcTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        sdf.setTimeZone(TimeZone.getDefault());
        String formattedDate = sdf.format(date);


        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));


        if (bundle != null) {

            groupName = bundle.getString(Constant.GROUP_NAME);
            groupKey = bundle.getString(Constant.GROUP_kEY);
            userList = bundle.getString(Constant.USER_LIST);
            courseKey = bundle.getString(Constant.COURSE_KEY);
            owner = bundle.getString(Constant.OWNER);
            mode = bundle.getString("mode");

            convertStringToHasHMap(userList);
            userName.deleteCharAt(userName.lastIndexOf(","));    //You can check here if string builder has comma to avoid IndexOutofboundException

            group_tv.setText(groupName);
            users_tv.setText(userName);
            memeber_ll.setOnClickListener(this);

            if (owner.equals(loginUserKey)) {
                addcourse_iv.setVisibility(View.VISIBLE);
            } else {
                addcourse_iv.setVisibility(View.GONE);
            }

            Log.d("hello", groupName + "--" + groupKey + "----" + userList + "----" + courseKey + "----" + owner + "----" + mode + "----" + userName);

        }
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();

        firebaseDatabase = FirebaseDatabase.getInstance();
        // databaseReference = firebaseDatabase.getReference("chat").child(groupKey);
        databaseReference = firebaseDatabase.getReference("group").child(courseKey).child(groupKey).child("chat");
        reference = FirebaseDatabase.getInstance().getReference("token").child(groupKey);


        setAdaptor();

        checkNetwork();

        setListener();


        return view;
    }

    private void checkNetwork() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {
            getChats();
        } else {
            ShowDialog.networkDialog(getContext());

            getChats();
        }
    }

    private void getChats() {

        chatList.clear();
        if (NetworkCheck.isNetworkAvailable(getContext())) {
            ShowDialog.showDialog(getContext());
        }

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                chatList = new ArrayList<FirebaseData>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {


                    HashMap<String, String> msgKey = new HashMap<String, String>();

                    FirebaseData firebaseData = new FirebaseData();

                    firebaseData = snapshot.getValue(FirebaseData.class);
                    // msgKey = (HashMap<String, String>) snapshot.getValue();

                    chatList.add(firebaseData);

                }
                try {
                    Log.d("list", chatList.get(chatList.size() - 1).getVideo());
                } catch (Exception e) {

                }

                messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);
                // messsageBoardadaptor.notifyDataSetChanged();
                //   if(NetworkCheck.isNetworkAvailable(getContext())) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

                //  if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }


    private Map<String, String> convertStringToHasHMap(String userList) {

        myList.clear();


        userList = userList.substring(1, userList.length() - 1);           //remove curly brackets
        String[] keyValuePairs = userList.split(",");              //split the string to creat key-value pairs
        Map<String, String> map = new HashMap<>();

        for (String pair : keyValuePairs)                        //iterate over the pairs
        {
            String[] entry = pair.split("=");                   //split the pairs to get key and value
            map.put(entry[0].trim(), entry[1].trim());          //add them to the hashmap and trim whitespaces
        }

        for (String key : map.keySet()) {
            String input = map.get(key);
            String output = input.substring(0, 1).toUpperCase() + input.substring(1);
            userName.append(output);
            userName.append(", ");

            if (!key.equals(loginUserKey)) {

                myList.add(output);

                keylist.add(key);
            }


            if (!key.equals(loginUserKey)) {

                tokenlist.add(key);
            }


        }

        return map;
    }

    private void getToken(final HashMap map) {

        //  if (!key.equals(loginUserKey)) {

        FirebaseDatabase.getInstance().getReference("registered_user").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    if (tokenlist.contains(snapshot.getKey())) {

                        String notificetion = snapshot.child(Constant.NOTIFICATION_KEY).getValue().toString();

                        if (notificetion.equals("true")) {
                            String key = snapshot.getKey();
                            Object token = snapshot.child("token").getValue();

                            // addTokenToFirebase(key, (String) token);

                            sendNotification((String) token, map);
                        }
                    }

                }


                // addTokenToFirebase(key, dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


  /*  private void addTokenToFirebase(String key, String token) {

        HashMap hashMap = new HashMap();
        hashMap.put(Constant.Token_NAME, token);
        reference.child(key).updateChildren(hashMap);
    }*/


    private void setListener() {


        back_msg_iv.setOnClickListener(this);

        selectimage_iv.setOnClickListener(this);


        // emoji_iv.setOnClickListener(this);
        send_iv.setOnClickListener(this);
        report_tv.setOnClickListener(this);
        addcourse_iv.setOnClickListener(this);


    }

    private void setAdaptor() {


        messsageBoardadaptor = new MesssageBoardadaptor(chatList, getContext());

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        message_rv.setLayoutManager(layoutManager);
        message_rv.setAdapter(messsageBoardadaptor);

    }



    /*--------------------------click listener on dialog for capture image and select from gallery-------------------------*/

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

            requestAppPermissions(permission, R.string.permission, 54);
        }
    };


    View.OnClickListener cameraClickLictener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
            requestAppPermissions(permission, R.string.permission, 55);
            // capture_image();

        }
    };


    /*-------------------------------------------capture image and store in sd card-----------------------------*/
    private void capture_image() {
        picture_Path = "";
        destination_path = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = "image";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picture_Path = String.valueOf(new File(f, System.currentTimeMillis() + ".jpg"));
        destination_path = new File(picture_Path);

        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination_path);
        } else {
            imageuri = Uri.fromFile(destination_path);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, Constant.CAMERA_CODE);
    }


    private void bindViews(View view) {

        contentRoot = view.findViewById(R.id.contentRoot);
        message_rv = (RecyclerView) view.findViewById(R.id.message_rv);

        back_msg_iv = (ImageView) view.findViewById(R.id.back_msg_iv);
        memeber_ll = (LinearLayout) view.findViewById(R.id.memeber_ll);
        send_iv = (ImageView) view.findViewById(R.id.send_iv);
        group_tv = (TextView) view.findViewById(R.id.group_tv);
        users_tv = (TextView) view.findViewById(R.id.users_tv);
        report_tv = (TextView) view.findViewById(R.id.report_tv);
        report_tv.setVisibility(View.GONE);


        selectimage_iv = (ImageView) view.findViewById(R.id.selectimage_iv);
        addcourse_iv = (ImageView) view.findViewById(R.id.addcourse_iv);
        //  addcourse_iv.setVisibility(View.VISIBLE);


        emoji_iv = (ImageView) view.findViewById(R.id.emoji_iv);
        emojiconEditText = (EmojiconEditText) view.findViewById(R.id.emojicon_edit_text);

        emojIcon = new EmojIconActions(getContext(), contentRoot, emojiconEditText, emoji_iv);
        emojIcon.ShowEmojIcon();


        emojiconEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    send_iv.setVisibility(View.VISIBLE);
                    selectimage_iv.setVisibility(View.GONE);

                } else {
                    send_iv.setVisibility(View.GONE);
                    selectimage_iv.setVisibility(View.VISIBLE);
                }


            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();


        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.GONE);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_msg_iv:

                int count = getFragmentManager().getBackStackEntryCount();

                AddCourseFragment.hideSoftKeyboard(getContext());
                // ((WelcomeActivity) getActivity()).finish();

                if (count == 1) {
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), new MenuFragment(), "Menu_Fragment");
                } else {
                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                }
             /*   AddCourseFragment.hideSoftKeyboard(getContext());
                Fragment fragment = getFragmentManager().findFragmentByTag("MessageBoard_Fragment");

                if (fragment != null && fragment.isVisible()) {
                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), new GroupFragment(), "group_fragment");

                } else {

                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                }*/

                break;

            case R.id.selectimage_iv:

                ShowDialog.cameraDialog(getContext(), onClickListener, cameraClickLictener);

                break;

/*
            case R.id.emoji_iv:

                if (!popup.isShowing()) {

                    //If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emoji_iv, R.mipmap.ic_action_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else {
                        emojiconEditText.setFocusableInTouchMode(true);
                        emojiconEditText.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(emojiconEditText, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emoji_iv, R.mipmap.ic_smile);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else {
                    popup.dismiss();
                }*/

            //   break;

            case R.id.send_iv:

                sendChat();

                break;

            case R.id.report_tv:

                showStrangerPopup(v);

                break;

            case R.id.memeber_ll:

                Bundle bundle = new Bundle();
                bundle.putString(Constant.USER_LIST, userList);

                UserFragment userFragment = new UserFragment();
                userFragment.setArguments(bundle);

                ReplaceFragment.replace((WelcomeActivity) getActivity(), userFragment, "User_Fragment");

                break;

            case R.id.addcourse_iv:

                String listString = "";

                for (String s : keylist) {
                    listString += s + ",";
                }

                Bundle bundle1 = new Bundle();
                bundle1.putString(Constant.COURSE_KEY, courseKey);
                bundle1.putString(Constant.GROUP_NAME, groupName);
                bundle1.putString(Constant.GROUP_kEY, groupKey);
                bundle1.putString(Constant.KEY_NAME, listString);
                bundle1.putString("mode", mode);
                SearchStudentFragment searchStudentFragment = new SearchStudentFragment();
                searchStudentFragment.setArguments(bundle1);

                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                ReplaceFragment.replace((WelcomeActivity) getActivity(), searchStudentFragment, "SearchStudent_Fragment");

        }

        //  ReplaceFragment.replace(((HomeActivity) getActivity()),messageBoardFragment);

    }


    private void showStrangerPopup(View v) {

        //  List<String> myList = new ArrayList<String>(Arrays.asList(userName.split(",")));

        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.pop_stranger, null);
        popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        RecyclerView stranger_rv = (RecyclerView) popupView.findViewById(R.id.stranger_rv);

      /*  StrangerAdaptor strangerAdaptor = new StrangerAdaptor(getContext(), myList, userPic);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        stranger_rv.setLayoutManager(mLayoutManager);
        stranger_rv.setAdapter(strangerAdaptor);

        strangerAdaptor.setClickListener(this);*/
        //  course_rv.setEmptyView(emptyview);

        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {

                popupWindow.dismiss();


            }

        });
        popupWindow.showAsDropDown(v, 0, 0);


    }

   private void  getLastNode()
    {

    }

    private void sendChat() {

        String key = databaseReference.push().getKey();
        String msg = emojiconEditText.getText().toString();
        /*final HashMap hashMap = new HashMap();
        hashMap.put(Constant.TEXT_NAME, msg);
        hashMap.put(Constant.USERID_NAME, loginUserKey);
        hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
        hashMap.put(Constant.PROFILE_PICTURE, image);
*/
        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setText(msg);
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(image);
        firebaseData.setName(groupName);
      //  firebaseData.setDeliver(false);

        databaseReference.child(key).setValue(firebaseData);

        emojiconEditText.setText("");


        String message = "{\n" +
                "  \"groupKey\": \"" + groupKey + "\",\n" +
                "  \"groupName\": \"" + groupName + "\",\n" +
                "  \"Message\": \"" + groupName + " : " + msg + "\",\n" +
                "  \"SenderName\": \"" + userFullName + "\",\n" +
                "  \"userList\": \"" + userList + "\",\n" +
                "  \"course_key\": \"" + courseKey + "\",\n" +
                "  \"owner\": \"" + owner + "\"\n" +
                "}";

        HashMap map = new HashMap();
        map.put("title", userFullName);
        map.put("body", message);


        getToken(map);


    }


    /*------------------------------------------get image after capture and pic from gallery-----------------------------*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.GALLERY_CODE && resultCode == RESULT_OK) {

            try {


                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
                // Bitmap bitmap = decodeUri(data.getData());

                imageUploadUri = getImageUri(getContext(), bitmap);

                imageUpload();

            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (requestCode == Constant.CAMERA_CODE && resultCode == RESULT_OK) {

            try {

                //       Bitmap bitmap = decodeUri(imageuri);

                // imageUploadUri = getImageUri(getContext(), bitmap);

                imageUploadUri = imageuri;
                imageUpload();

            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (requestCode == SELECT_VIDEO && resultCode == RESULT_OK) {
            videoPath = getPath(data.getData());

            // Log.d("videoSize", videoPath.length() + "----" + videoPath);
            videoUri = data.getData();
            videoThumb = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Images.Thumbnails.MINI_KIND);

            baos = new ByteArrayOutputStream();
            videoThumb.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byteArray = baos.toByteArray();
            videoThumbString = Base64.encodeToString(byteArray, Base64.DEFAULT);

            File myFile = new File(videoUri.toString());

            String path = getPath(videoUri);

            if (videoUri.toString().startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = getActivity().getContentResolver().query(videoUri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    }
                } finally {
                    cursor.close();
                }
            } else if (videoUri.toString().startsWith("file://")) {
                displayName = myFile.getName();
            }

            displayName = displayName.replace(" ", "");

            tempraryVideoPath = tempraryDirectory + "/" + displayName;

            File file1 = new File(videoPath);
// Get length of file in bytes
            double fileSizeInBytes = file1.length();
// Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            double fileSizeInKB = fileSizeInBytes / 1024;
// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            double fileSizeInMB = fileSizeInKB / 1024;

            if (fileSizeInMB <= 10) {

                copyFile(path, displayName);

                FirebaseData firebaseData = new FirebaseData();
                firebaseData.setVideo(tempraryVideoPath);
                firebaseData.setUserid(loginUserKey);
                firebaseData.setTime(SystemTime.getTime());
                firebaseData.setProfile_picture(image);
                firebaseData.setVideoBitmap(videoThumb);
                firebaseData.setFileName(displayName);
                firebaseData.setOriginalPath(videoPath);
                firebaseData.setName(groupName);
                chatList.add(firebaseData);
                messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);

                uploadVideo(videoPath, displayName, byteArray, "");

            } else if (fileSizeInMB > 100) {
                Toast.makeText(getContext(), "Sorry, media size too large", Toast.LENGTH_SHORT).show();
            } else {

                copyFile(path, "1" + displayName);

                File f = new File(tempraryDirectory);
                if (f.mkdirs() || f.isDirectory()) {

                    File file = new File(tempraryVideoPath);
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    runTranscoding(displayName, byteArray, videoPath);
                }
            }

            // copyFile(path, displayName);

            // uploadVideo(displayName);


        } else if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
            try {

                String mime = "";
                Log.d("path", data.getStringExtra(Constant.FILE_NAME) + "");
                String path = data.getStringExtra(Constant.FILE_NAME);
                String fileName[] = path.split("/");
                displayName = fileName[fileName.length - 1];
                Log.d("name", displayName);
                String type = path.substring(path.length() - 3);

                if (type.equals("pdf")) {
                    mime = "application/pdf";
                } else if (type.equals("doc")) {
                    mime = "application/msword";
                } else {
                    mime = "text/plain";
                }
              /*  if (data.getData().toString().startsWith("file://")) {
                    String extension = MimeTypeMap.getFileExtensionFromUrl(data.getData().getPath());
                    Log.d("file", extension + "");
                    if (extension != null) {
                        mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                    }
                } else {
                    ContentResolver cr = getContext().getContentResolver();
                    mime = cr.getType(data.getData());

                }*/
                if (mime.equals("text/plain") || mime.equals("application/msword") || mime.equals("application/pdf")) {

                    File myFile = new File(path);

                 /*   imageUploadUri = data.getData();
                    File myFile = new File(imageUploadUri.toString());

                    String displayName = null;

                    if (imageUploadUri.toString().startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = getActivity().getContentResolver().query(imageUploadUri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (imageUploadUri.toString().startsWith("file://")) {
                        displayName = myFile.getName();
                    }

                    String path = "";
                    if (data.getData().toString().startsWith("file://")) {
                        path = imageUploadUri.getPath();
                    } else {
                        path = getPath(imageUploadUri);
                    }*/
                    copyFile(path, displayName);
                    uploadFile(mime, displayName, myFile, path);
                }
            } catch (Exception e) {
                Log.d("type", e.getMessage());
            }
        }

    }

    private void uploadVideo(final String videoPath, final String displayName, final byte[] byteArray, final String path) {

        if (videoPath != null) {

            File file = new File(videoPath);
            Uri uri = Uri.fromFile(file);
          /*  FirebaseData firebaseData = new FirebaseData();
            firebaseData.setVideo(videoUri.toString());
            firebaseData.setUserid(loginUserKey);
            firebaseData.setTime(SystemTime.getTime());
            firebaseData.setProfile_picture(image);
            firebaseData.setVideoBitmap(videoThumb);
            firebaseData.setFileName(displayName);
            chatList.add(firebaseData);
            messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);*/


            // Create file metadata including the content type
            StorageMetadata metadata = new StorageMetadata.Builder()
                    .setContentType("video/mp4")
                    .build();

            StorageReference reference = storageReference.child("video" + "_" + SystemClock.currentThreadTimeMillis());
            reference.putFile(uri, metadata).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    downloadVideoUri = taskSnapshot.getDownloadUrl();
                    Log.d("path", downloadVideoUri + "");

                    storeThumbnails(displayName, videoPath, byteArray, path);

                }

            });
        }
    }

    private void storeThumbnails(final String displayName, final String videoPath, byte[] byteArray, final String path) {
        Log.d("path", downloadVideoUri + "--" + byteArray);
        if (byteArray != null) {
            StorageReference ref = storageReference.child("images/" + System.currentTimeMillis());
            ref.putBytes(byteArray).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    downloadVideoThumb = taskSnapshot.getDownloadUrl();

                    storeVideoDatabase(displayName, videoPath, path);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {


                }
            });
        }
    }

    private void storeVideoDatabase(String displayName, String videoPath, String path) {

        String key = databaseReference.push().getKey();
        /*HashMap hashMap = new HashMap();
        hashMap.put(Constant.VIDEO_NAME, downloadVideoUri.toString());
        hashMap.put(Constant.USERID_NAME, loginUserKey);
        hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
        hashMap.put(Constant.PROFILE_PICTURE, image);
        hashMap.put(Constant.VIDEO_THUMB, downloadVideoThumb.toString());
        hashMap.put("fileName", displayName);*/

        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setVideo(downloadVideoUri.toString());
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(image);
        firebaseData.setVideothumb(downloadVideoThumb.toString());
        firebaseData.setFileName(displayName);
        firebaseData.setName(groupName);

        if (path != null && !path.isEmpty()) {
            firebaseData.setOriginalPath(path);
        } else {
            firebaseData.setOriginalPath(videoPath);
        }
        //  chatList.add(firebaseData);
        //  messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);

        databaseReference.child(key).setValue(firebaseData);

        String message = "{\n" +
                "  \"groupKey\": \"" + groupKey + "\",\n" +
                "  \"groupName\": \"" + groupName + "\",\n" +
                "  \"Message\": \"" + groupName + " : " + "Video" + "\",\n" +
                "  \"SenderName\": \"" + userFullName + "\",\n" +
                "  \"userList\": \"" + userList + "\",\n" +
                "  \"course_key\": \"" + courseKey + "\",\n" +
                "  \"owner\": \"" + owner + "\"\n" +
                "}";


        HashMap map = new HashMap();
        map.put("title", userFullName);
        map.put("body", message);

        getToken(map);

        String videoPathFile = tempraryDirectory + "/" + displayName;
        String videoPath1 = tempraryDirectory + "/" + "1" + displayName;
        Log.d("video", videoPath);
        File file = new File(videoPathFile);
        file.delete();
        if (file.exists()) {
            try {
                file.getCanonicalFile().delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file.exists()) {
                getContext().deleteFile(file.getName());
            }
        }
        File file1 = new File(videoPath1);
        file1.delete();
        if (file1.exists()) {
            try {
                file1.getCanonicalFile().delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file1.exists()) {
                getContext().deleteFile(file1.getName());
            }
        }

    }


    private void runTranscoding(String displayName, byte[] byteArray, String videoPath) {
        //  ShowDialog.showDialog(getContext());
        //  initProgressDialog(context);
        //   ((MainActivity) context).isVideoCreated = false;
        //  String  videoOriginalPath = tempraryDirectory + "/" + "1"+displayName;
        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setVideo(videoPath);
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(image);
        firebaseData.setVideoBitmap(videoThumb);
        firebaseData.setFileName(displayName);
        firebaseData.setOriginalPath(videoPath);
        firebaseData.setName(groupName);
        chatList.add(firebaseData);
        messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);

        compressVideo.add(displayName);
        byteArrayList.add(byteArray);


        compressVideo(videoPath);
    }

    private void compressVideo(final String videoPath) {

        //  Boolean compress = SharedPreference.getInstance().getIsFirstTime(getContext(), "compress");

        if (compress) {

            new Thread() {
                public void run() {
                    Log.e("TAG", "Worker started");
                    try {

                        //    sleep(5000);
                        runTranscodingUsingLoader(compressVideo.get(i), videoPath);
                        handler.sendEmptyMessage(FINISHED_TRANSCODING_MSG);
                    } catch (Exception e) {
                        ShowDialog.hideDialog();
                        Log.e("TAG", "threadmessage " + e.getMessage());
                    }
                }

            }.start();
        }
    }


    private void runTranscodingUsingLoader(String displayName, String videoPath) {


        String tempraryVideoPath = tempraryDirectory + "/" + displayName;
        compress = false;
        Log.e("TAG", "runTranscodingUsingLoader started...");
        Log.e("TAG", videoPath + "----" + tempraryVideoPath);
        String videoPath1 = tempraryDirectory + "/" + "1" + displayName;
        vk = new LoadJNI();

        String commandStr = "ffmpeg -y -i " + videoPath1 + " -strict experimental -r 30 -ab 48000 -ac 2 -ar 48000 -vcodec mpeg4 -b 4097152 " + tempraryVideoPath;

        Log.e("TAG", commandStr);
        try {
            Log.e("TAG", "vk.going to run.");
            // running regular command with validation
            vk.run(GeneralUtils.utilConvertToComplex(commandStr), workFolder, getActivity());
            Log.e("TAG", "vk.run finished.");

            uploadVideo(tempraryVideoPath, displayName, byteArrayList.get(i), videoPath);

            i++;

            compress = true;
            compressVideo(videoPath);


        } catch (CommandValidationException e) {
            Log.e("TAG", "vk run exeption.", e);
            //  ShowDialog.hideDialog();
        } catch (Throwable e) {
            Log.e("TAG", "vk run exeption.", e);
            //  ShowDialog.hideDialog();
        }


    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e("TAG", "Handler got message");
            //            if (progressDialog != null)
            //            {//                progressDialog.dismiss();
            // stopping the transcoding native


            if (msg.what == STOP_TRANSCODING_MSG) {
                Log.e("TAG", "Got cancel message, calling fexit");
                vk.fExit(context.getApplicationContext());

                //  uploadVideo(tempraryVideoPath, displayName);
            }//            }
        }
    };


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public void copyFile(String inputPath, String displayName) {

        String tempraryFilePath = tempraryDirectory + "/" + displayName;

        Log.e("path", tempraryFilePath);
        Log.e("inputPath", inputPath);

        if (new File(inputPath).exists()) {
            Log.e("path", inputPath);

            InputStream in = null;
            OutputStream out = null;
            try {                //create output directory if it doesn't exist
                File dir = new File(tempraryDirectory);
                if (!dir.exists()) {
                    Log.e("path", dir + "");
                    dir.mkdirs();
                }
                in = new FileInputStream(inputPath);
                // hiddenVideoPath=hiddenVideoDirectory+"/"+System.currentTimeMillis()+".mp4";
                Log.e("TAG", "hiddenVideoPath " + tempraryFilePath);
                out = new FileOutputStream(tempraryFilePath);
                byte[] buffer = new byte[1024];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                in.close();
                in = null;
                // write the output file
                out.flush();
                out.close();
                out = null;
                // delete the original file                // new File(inputPath + inputFile).delete();
                //   deleteRawData(tempraryDirectory);
            } catch (FileNotFoundException fnfe1) {
                Log.e("tag", fnfe1.getMessage());
            } catch (Exception e) {
                Log.e("tag", e.getMessage());
            }
        } else {
            // Log.e("Balli", "temprary video is not available");
            // Toast.makeText(context, "No video is availabe to download", Toast.LENGTH_LONG).show();
        }
    }
    //   ((MainActivity) context).isVideoCreated = true;//

 /*   public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }*/

/*    private void copyFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        } catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }*/

    private void uploadFile(final String mime, final String displayName, File filePath, String path) {


        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setFile(path);
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(image);
        firebaseData.setType(mime);
        firebaseData.setName(groupName);
        if (displayName != null) {
            firebaseData.setFileName(displayName);
        }
        chatList.add(firebaseData);
        messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);

        storageReference.child("file" + "_" + System.currentTimeMillis())
                .putFile(Uri.fromFile(filePath))
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d("error", e.getMessage() + "");
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();
                        //   storeImageDatabase();
                        Log.d("downloadimage", downloadImageUri + "");
                        storeFileDatabase(mime, displayName);
                    }
                });


    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, Constant.TITLE_NAME, null);
        return Uri.parse(path);
    }


    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(selectedImage), null, o2);
    }

    private void imageUpload() {



        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setImage(imageUploadUri.toString());
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(image);
        firebaseData.setName(groupName);
        chatList.add(firebaseData);
        messsageBoardadaptor.addData(chatList, message_rv, messsageBoardadaptor);
        storageReference.child("file" + "_" + System.currentTimeMillis())
                .putFile(imageUploadUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Log.d("error", e.getMessage());
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();
                        Log.d("downloadimage", downloadImageUri + "");
                        storeImageDatabase();
                    }
                });

    }

    private void storeImageDatabase() {

        String key = databaseReference.push().getKey();


        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setImage(downloadImageUri.toString());
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(image);
        firebaseData.setName(groupName);
      //  firebaseData.setDeliver(false);

        databaseReference.child(key).setValue(firebaseData);

        String message = "{\n" +
                "  \"groupKey\": \"" + groupKey + "\",\n" +
                "  \"groupName\": \"" + groupName + "\",\n" +
                "  \"Message\": \"" + groupName + " : " + "Photo" + "\",\n" +
                "  \"SenderName\": \"" + userFullName + "\",\n" +
                "  \"userList\": \"" + userList + "\",\n" +
                "  \"course_key\": \"" + courseKey + "\",\n" +
                "  \"owner\": \"" + owner + "\"\n" +
                "}";


        HashMap map = new HashMap();
        map.put("title", userFullName);
        map.put("body", message);

        // getStoreToken(map);

        getToken(map);
        // ShowDialog.hideDialog();

    }

    private void storeFileDatabase(String mime, String displayName) {


        String key = databaseReference.push().getKey();


        FirebaseData firebaseData = new FirebaseData();
        firebaseData.setFile(downloadImageUri.toString());
        firebaseData.setUserid(loginUserKey);
        firebaseData.setTime(SystemTime.getTime());
        firebaseData.setProfile_picture(image);
        firebaseData.setFileName(displayName);
        firebaseData.setType(mime);
        firebaseData.setName(groupName);
       // firebaseData.setDeliver(false);


        databaseReference.child(key).setValue(firebaseData);

        String message = "{\n" +
                "  \"groupKey\": \"" + groupKey + "\",\n" +
                "  \"groupName\": \"" + groupName + "\",\n" +
                "  \"Message\": \"" + groupName + " : " + "File" + "\",\n" +
                "  \"SenderName\": \"" + userFullName + "\",\n" +
                "  \"userList\": \"" + userList + "\",\n" +
                "  \"course_key\": \"" + courseKey + "\",\n" +
                "  \"owner\": \"" + owner + "\"\n" +
                "}";


        HashMap map = new HashMap();
        map.put("title", userFullName);
        map.put("body", message);

        // getStoreToken(map);

        getToken(map);
        // ShowDialog.hideDialog();

    }

    public void sendNotification(String fcmID, HashMap<String, String> map) {


        String body123 = "{\n\t\"to\": \" " + fcmID + "\",\n\t\"notification\" :" + convert(map) + ",\n\t\"data\" :" + convert1(map) + "\n}";

        Log.e("DataToSend", "" + body123);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");


        RequestBody body = RequestBody.create(mediaType, body123);
        Request request = new Request.Builder()
                .url("https://fcm.googleapis.com/fcm/send")
                .post(body)
                .addHeader("authorization", "key=" + Constant.SERVER_KEY)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")

                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    Log.e("ResponseBALLI", response.body().string());
                    Log.d("sa", "sa");
                    Log.i("MESSAGE", response.message());
                }
            }


        });


    }

    private void notificationBody() {

        JSONObject jsonObject = new JSONObject();
        //  jsonObject.put("")


    }

    public String convert(HashMap<String, String> map) {
        JSONObject obj = null;
        try {


            obj = new JSONObject();
            String message = map.get("body");

            JSONObject meJson = new JSONObject(message);

            //   obj.put("body",  meJson.optString("Message") +" from "+ meJson.optString("groupName") +" by "+ meJson.optString("SenderName") );
            obj.put("body", meJson.optString("Message"));
            obj.put("title", userFullName);
            obj.put("icon", R.mipmap.ic_logo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj.toString();
    }


    public String convert1(HashMap<String, String> map) {
        JSONObject obj = null;
        try {

            obj = new JSONObject(map);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return obj.toString();
    }

    final int blockedLimit = 5;

    @Override
    public void onRecyClick(View view, int position) {

        popupWindow.dismiss();

        final String key = keylist.get(position);

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("registered_user").child(key);


        Query query = databaseReference;


        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                stranger = dataSnapshot.child("stranger").getValue().toString();
                stringList = new ArrayList<String>(Arrays.asList(stranger.split(",")));
                boolean reportedByMe = stringList.contains(loginUserKey);


                if (stringList.size() <= blockedLimit - 1) {
                    if (stranger.isEmpty()) {
                        stranger = loginUserKey;
                        databaseReference.child("stranger").setValue(stranger);

                        Toast.makeText(getContext(), "User Reported", Toast.LENGTH_SHORT).show();

                    } else if (!reportedByMe) {
                        if (stringList.size() == blockedLimit - 1) {
                            databaseReference.child("status").setValue("block");
                        }

                        stranger = stranger + "," + loginUserKey;
                        databaseReference.child("stranger").setValue(stranger);
                        Toast.makeText(getContext(), "User Reported", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 54) {
           /* Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image*//*");
            startActivityForResult(galleryIntent, Constant.GALLERY_CODE);*/
/*
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("file*//*");
            startActivityForResult(intent,5436);*/

            // File file = new File(name);
      /*      String type = "application/msword/pdf/vnd.ms-powerpoint/vnd.ms-excel";
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType(type);
            startActivityForResult(intent,Constant.GALLERY_CODE);*/


        /*    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
            chooseFile.setType("application*//*");
            startActivityForResult(
                    Intent.createChooser(chooseFile, "Choose a file"),
                    Constant.GALLERY_CODE
            );*/


            actionSheet();
        } else if (requestCode == 55) {
            capture_image();
        }
    }


    private void actionSheet() {

        final String[] stringItems = {"Image", "Video", "File"};

        final ActionSheetDialog dialog = new ActionSheetDialog(getContext(), stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                    Intent galleryIntent = new Intent(Intent.ACTION_PICK);
                    galleryIntent.setType("image/*");
                    startActivityForResult(galleryIntent, Constant.GALLERY_CODE);


                } else if (position == 1) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, SELECT_VIDEO);

                } else {
                    //   Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    //  intent.setType("*/*");
                    /*intent.addCategory(Intent.CATEGORY_OPENABLE);

                    try {
                        startActivityForResult(
                                Intent.createChooser(intent, "Select a File to Upload"),
                                SELECT_FILE);
                    } catch (android.content.ActivityNotFoundException ex) {

                        Toast.makeText(getContext(), "Please install a File Manager.", Toast.LENGTH_SHORT).show();
                    }*/

                    DocumentFragment documentFragment = new DocumentFragment();
                    documentFragment.setTargetFragment(MessageBoardFragment.this, SELECT_FILE);
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), documentFragment, "InviteStudent_Fragment");


                }

                dialog.dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }
}
