package com.brst.classmates.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.NetworkCheck;

import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Network_Invite_Fragment extends Fragment implements View.OnClickListener {

    View view = null;
    List<HashMap<String, String>> courseSearchtList = new ArrayList<>();
    List<String> courseKey = new ArrayList<>();
    List<String> courseSearchKey = new ArrayList<>();
    TextView event_tv, invite_tv;
    FrameLayout eventlist_fl, invitelist_fl;

    private FirebaseStorage firebaseStorage;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    StorageReference storageReference;
    List<HashMap<String, String>> dataList = new ArrayList<>();

    List<String> groupList;
    Fragment netFragment;

    public Network_Invite_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_network_invite, container, false);


            event_tv = (TextView) view.findViewById(R.id.event__tv);
            invite_tv = (TextView) view.findViewById(R.id.invite_tv);
            eventlist_fl = (FrameLayout) view.findViewById(R.id.eventlist_fl);
            invitelist_fl = (FrameLayout) view.findViewById(R.id.invitelist_fl);

            event_tv.setOnClickListener(this);
            invite_tv.setOnClickListener(this);

            netFragment = getFragmentManager().findFragmentByTag("netCont_fragmant");
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey("network")) {
                Log.d("networkInvite_fragment", "networkInvite_fragment");
                event_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventborder));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.invite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                event_tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                eventlist_fl.setVisibility(View.GONE);
                invitelist_fl.setVisibility(View.VISIBLE);
            }
            else if (bundle.containsKey("Contact"))
            {
                Log.d("networkInvite_fragment", "networkInvite_fragment-------");
                event_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventborder));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.invite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                event_tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                eventlist_fl.setVisibility(View.GONE);
                invitelist_fl.setVisibility(View.VISIBLE);
                NetworkFragment.searchContactUser_et.setText("");
            }
        }



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getCourseList();
        ((WelcomeActivity) getActivity()).header_tv.setText("Network");
        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);

        ((WelcomeActivity) getActivity()).header_iv.setOnClickListener(this);
     //   ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.event__tv:

                event_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventleft));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.inviteright));
                event_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                eventlist_fl.setVisibility(View.VISIBLE);
                invitelist_fl.setVisibility(View.GONE);
                ContactFragment.searchContactUser_et.setText("");

                break;
            case R.id.header_iv:


                break;

            case R.id.invite_tv:

                event_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.eventborder));
                invite_tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.invite));
                invite_tv.setTextColor(getResources().getColor(R.color.colorWhite));
                event_tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                eventlist_fl.setVisibility(View.GONE);
                invitelist_fl.setVisibility(View.VISIBLE);
                NetworkFragment.searchContactUser_et.setText("");
                //ReplaceFragment.replace(((WelcomeActivity) getActivity()), new ContactFragment(), "Profile_Fragment");

        }

    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else {
//            EventFragment eventFragment=new EventFragment();
//            ReplaceFragment.replace((WelcomeActivity) getActivity(),eventFragment,"Event_Fragment");
        }
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void getCourseList() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {

            ShowDialog.showDialog(getContext());
        }

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("registered_user");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                dataList = new ArrayList<HashMap<String, String>>();
                courseKey = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                    try {
                        HashMap hashMap1 = new HashMap();
                        hashMap1.put(Constant.FIRST_NAME, hashMap.get(Constant.ACTIVITY_NAME).toString());
                        hashMap1.put(Constant.COLLEGE_NAME, hashMap.get(Constant.COLLEGE_NAME).toString());
                        dataList.add(hashMap1);
                        // dataList.add(hashMap.get(Constant.KEY_NAME).toString());

                        courseKey.add(dataSnapshot1.getKey());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                addDataInList(dataList, courseKey);

                //courseAdaptor.notifyDataSetChanged();
                //     if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }
                //  }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }

    public void addDataInList(List<HashMap<String, String>> dataList, List<String> courseKey) {
        this.dataList = dataList;
        this.courseKey = courseKey;

        Log.d("asd", courseKey + "");
        Log.d("asd", dataList + "");
    }
}
