package com.brst.classmates.fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.AssignmentAdaptor;
import com.brst.classmates.adaptors.CommentReplyAdaptor;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageReplyFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener {

    View view;
    RecyclerViewEmptySupport messageReply_rv;
    ImageView emojireply_iv, replySend_iv, cameraReply_iv;
    EmojiconEditText emojicon_edit_text;

    View contentRoot;

    String messageKey, assignKey;

    List<HashMap<String, String>> replyList = new ArrayList<>();

    CommentReplyAdaptor commentReplyAdaptor;

    private EmojIconActions emojIcon;

    String picture_Path;
    File destination_path;
    Uri imageuri, imageUploadUri, downloadImageUri;


    DatabaseReference messageReplyReference;

    public MessageReplyFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_message_reply, container, false);
        }

            bindViews(view);

            setListener();

            setAdaptor();

        Bundle bundle = getArguments();

        if (bundle != null) {
            messageKey = bundle.getString(Constant.MESSKEY_NAME);
            assignKey = bundle.getString(Constant.ASSIGNMENT_NAME);
        }

        messageReplyReference = FirebaseDatabase.getInstance().getReference(Constant.ASSCOMMENT_NAME)
                .child(assignKey).child(messageKey).child(Constant.REPLY_NAME);

        checkNetwork();

        return view;
    }

    private void setAdaptor() {

        commentReplyAdaptor = new CommentReplyAdaptor(replyList, getContext());
      //  RecyclerViewEmptySupport.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        messageReply_rv.setLayoutManager(layoutManager);
        messageReply_rv.setAdapter(commentReplyAdaptor);

        // messageReply_rv.setEmptyView(empty_tv);

    }

    private void checkNetwork() {

        if (!NetworkCheck.isNetworkAvailable(getContext())) {
            ShowDialog.networkDialog(getContext());
        } else {
            getCommentReply();
        }
    }

    private void getCommentReply() {

        ShowDialog.showDialog(getContext());

        messageReplyReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                replyList = new ArrayList<HashMap<String, String>>();

                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = iterable.iterator();
                while (iterator.hasNext()) {

                    HashMap hashMap = new HashMap();

                    DataSnapshot snapshot = iterator.next();

                    String messageKey = snapshot.getKey();
                    String senderName = snapshot.child(Constant.USERMESS_NAME).getValue().toString();
                    String time = snapshot.child(Constant.TIME_NAME).getValue().toString();
                    if (snapshot.child(Constant.REPLY_NAME).exists()) {
                        String message = snapshot.child(Constant.REPLY_NAME).getValue().toString();
                        hashMap.put(Constant.MESSAGE, message);
                    } else if (snapshot.child(Constant.IMAGE_NAME).exists()) {

                        String image = snapshot.child(Constant.IMAGE_NAME).getValue().toString();
                        hashMap.put(Constant.IMAGE_NAME, image);
                    }


                    hashMap.put(Constant.USERMESS_NAME, senderName);
                    hashMap.put(Constant.TIME_NAME, time);

                    //  hashMap.put(Constant.MESSKEY_NAME, messageKey);
                    replyList.add(hashMap);

                }

                commentReplyAdaptor.addData(replyList,commentReplyAdaptor,messageReply_rv);
                commentReplyAdaptor.notifyDataSetChanged();

                ShowDialog.hideDialog();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                ShowDialog.hideDialog();


            }
        });

    }

    private void setListener() {

        replySend_iv.setOnClickListener(this);
        cameraReply_iv.setOnClickListener(this);
    }

    private void bindViews(View view) {

        contentRoot = view.findViewById(R.id.contentRoot);
        messageReply_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.messageReply_rv);
        emojireply_iv = (ImageView) view.findViewById(R.id.emojireply_iv);
        replySend_iv = (ImageView) view.findViewById(R.id.replySend_iv);
        cameraReply_iv = (ImageView) view.findViewById(R.id.cameraReply_iv);
        emojicon_edit_text = (EmojiconEditText) view.findViewById(R.id.emojicon_edit_text);

        emojIcon = new EmojIconActions(getContext(), contentRoot, emojicon_edit_text, emojireply_iv);
        emojIcon.ShowEmojIcon();

        emojicon_edit_text.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    replySend_iv.setVisibility(View.VISIBLE);
                    cameraReply_iv.setVisibility(View.GONE);

                } else {
                    replySend_iv.setVisibility(View.GONE);
                    cameraReply_iv.setVisibility(View.VISIBLE);
                }


            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
    //    ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setText("Replies");
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);


        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.replySend_iv:

                sendReplyToMessage();

                break;

            case R.id.cameraReply_iv:

                ShowDialog.cameraDialog(getContext(), onClickListener, cameraClickLictener);

                break;


        }
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            requestAppPermissions(permission, R.string.permission, 60);
        }
    };


    View.OnClickListener cameraClickLictener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();

            capture_image();

        }
    };


    /*-------------------------------------------capture image and store in sd card-----------------------------*/
    private void capture_image() {
        picture_Path = "";
        destination_path = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = "image";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picture_Path = String.valueOf(new File(f, System.currentTimeMillis() + ".jpg"));
        destination_path = new File(picture_Path);

        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination_path);
        } else {
            imageuri = Uri.fromFile(destination_path);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, Constant.CAMERA_CODE);
    }


    private void sendReplyToMessage() {

        String commentReply = emojicon_edit_text.getText().toString();
        if (commentReply.isEmpty()) {
            Toast.makeText(getContext(), "Can't send empty message.", Toast.LENGTH_SHORT).show();
        } else {
            HashMap hashMap = new HashMap();
            hashMap.put(Constant.USERMESS_NAME, StoreData.getUserFullNameFromSharedPre(getContext()));
            hashMap.put(Constant.TIME_NAME, SystemTime.getTime());
            hashMap.put(Constant.REPLY_NAME, commentReply);

            messageReplyReference.push().updateChildren(hashMap);

            emojicon_edit_text.setText("");
        }


    }

    @Override
    public void onPermissionGranted(int requestCode) {


        if (requestCode == 60) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, Constant.GALLERY_CODE);
        }

    }


    /*------------------------------------------get image after capture and pic from gallery-----------------------------*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.GALLERY_CODE && resultCode == RESULT_OK) {

           // imageUploadUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());

                imageUploadUri = getImageUri(getContext(),bitmap);
                imageUpload();
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (requestCode == Constant.CAMERA_CODE && resultCode == RESULT_OK) {

            try {


                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageuri);
                imageUploadUri=getImageUri(getContext(),bitmap);

                imageUpload();

            } catch (IOException e) {
                e.printStackTrace();
            }
          //  imageUploadUri = imageuri;
           // imageUpload();

        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, Constant.TITLE_NAME, null);
        return Uri.parse(path);
    }

    private void imageUpload() {

        Log.d("sdfs", imageUploadUri + "-------" + imageUploadUri.getLastPathSegment());
        String userName = StoreData.getUserFullNameFromSharedPre(getContext());
        String time = SystemTime.getTime();

     /*   HashMap hashMap = new HashMap();
        hashMap.put(Constant.USERMESS_NAME, userName);
        hashMap.put(Constant.TIME_NAME, time);
        hashMap.put(Constant.IMAGE_NAME, imageUploadUri.toString());

        replyList.add(hashMap);

        commentReplyAdaptor.addData(replyList);
        commentReplyAdaptor.notifyDataSetChanged();*/

        ShowDialog.showDialog(getContext());

        FirebaseStorage.getInstance().getReference().child("image" + "_" + imageUploadUri.getLastPathSegment())
                .putFile(imageUploadUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();
                        storeImageDatabase();
                    }
                });

    }


    private void storeImageDatabase() {

        String userName = StoreData.getUserFullNameFromSharedPre(getContext());
        String time = SystemTime.getTime();

        HashMap hashMap = new HashMap();
        hashMap.put(Constant.USERMESS_NAME, userName);
        hashMap.put(Constant.TIME_NAME, time);
        hashMap.put(Constant.IMAGE_NAME, downloadImageUri.toString());

        messageReplyReference.push().updateChildren(hashMap);

        ShowDialog.hideDialog();

    }
}
