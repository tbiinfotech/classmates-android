package com.brst.classmates.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.Application;
import com.brst.classmates.classses.ConnectivityReceiver;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.DateTimeInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;
import static com.brst.classmates.classses.NetworkCheck.isNetworkAvailable;
import static com.brst.classmates.classses.ShowDialog.networkDialog;
import static com.seatgeek.placesautocomplete.Constants.LOG_TAG;


public class EventFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener, DateTimeInterface, com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ConnectivityReceiver.ConnectivityReceiverListener {


    TextView create_tv, price_tv, description_tv, addimage_tv, setdate_tv, timeTV;

    RadioButton event_rb, meeting_rb;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    String userFullName, loginUserKey;
    private LocationManager locationManager;

    String image = "";

    int k = 0;
    RadioGroup radiogroup;

    RelativeLayout ticket_rl;

    Boolean isShow = false;

    CheckBox eventChk_cb;

    String dataArray[] = new String[3];

    String invite = "";

    List<String> keyList = new ArrayList<>();

    SearchStudentFragment searchStudentFragment;

    InviteCourseFragment inviteCourseFragment;

    List<String> stringList = new ArrayList<>();

    LinearLayout calender_ll, time_ll;

    int count = 0, length = 0;

    Boolean showDialog = false;

    TextView locationET;

    Fragment fragmentEvent;
    String buildArray[] = new String[3];

    EditText price_et, description_et, titleET,location_et;

    ImageView addimage_iv, calender_iv, timeIV, image1_iv, image2_iv;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    String key = "", date, time, location, price, title, desc, image1, image2, image3, admin, type, eventKey, studentList = "";

    //  List list = new ArrayList();
    View view = null;
    public PlacesAutocompleteTextView clientLocationTV;
    //   public AutoCompleteTextView clientLocationTV;
    LinearLayout location_ll;
    private FirebaseStorage firebaseStorage;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, registerUserRef;
    StorageReference storageReference;
    DateTimeInterface dateTimeInterface;
    private int GALLERY_CODE = 101, CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 102;
    private String picturePath = "";
    private File destination;
    private String downloadImageUri = "";
    Bundle bundle;

    Uri imageuri;

    GoogleApiClient googleApiClient;

    List<String> listStudent = new ArrayList<>();

    public EventFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {

            view = inflater.inflate(R.layout.fragment_event, container, false);

            initialzingFirebase();

            fragmentEvent = getFragmentManager().findFragmentByTag("Event_Fragment");

            bindViews(view);

            setListener();

            initFragment();

            if (googleApiClient == null || !googleApiClient.isConnected()) {

                googleApiClient = new GoogleApiClient
                        .Builder(getContext())
                        .enableAutoManage(getActivity(), 0, this)
                        .addApi(Places.GEO_DATA_API)
                        .addApi(Places.PLACE_DETECTION_API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();

            }
        }

        //  int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;


        userFullName = StoreData.getUserFullNameFromSharedPre(getContext());
        loginUserKey = StoreData.getUserKeyFromSharedPre(getContext());

        bundle = getArguments();
        if (bundle != null) {

            create_tv.setText("Update");

            key = bundle.getString("Key");
            date = bundle.getString("date");
            time = bundle.getString("time");
            location = bundle.getString("location");
            //   price = bundle.getString("price");
            title = bundle.getString("title");
            desc = bundle.getString("desc");
            image1 = bundle.getString("image1");
            image2 = bundle.getString("image2");
            image3 = bundle.getString("image3");
            admin = bundle.getString("admin");
            type = bundle.getString("type");
            studentList = bundle.getString("studentList");

            listStudent = new ArrayList<String>(Arrays.asList(studentList.split(",")));


            if (type.equals("Event")) {

                event();

                event_rb.setVisibility(View.VISIBLE);
                meeting_rb.setVisibility(View.GONE);

                event_rb.setChecked(true);

                setdate_tv.setText(date);
                timeTV.setText(time);

                titleET.setText(title);
                //  clientLocationTV.setText(location);
                //   price_et.setText(price);
                description_et.setText(desc);
                locationET.setText(location);

             //   Log.d("dfsf", image1);

             //   picturePath = image1;

                if (image1 != null && !image1.isEmpty()) {

                    // list.add(image1);
                    dataArray[0] = image1;

                    Glide.with(getContext()).load(image1).into(addimage_iv);
                    image1_iv.setVisibility(View.VISIBLE);
                    image1_iv.setEnabled(true);
                    image1_iv.setImageResource(R.mipmap.picture);
                }
                if (image2 != null && !image2.isEmpty()) {

                    //list.add(image2);
                    dataArray[1] = image2;
                    ;
                    image2_iv.setVisibility(View.VISIBLE);
                    Glide.with(getContext()).load(image2).into(image1_iv);
                    image2_iv.setEnabled(true);
                    image2_iv.setImageResource(R.mipmap.picture);
                }

                if (image3 != null && !image3.isEmpty()) {

                    // list.add(image3);
                    dataArray[2] = image3;

                    Glide.with(getContext()).load(image3).into(image2_iv);


                }
            } else if (type.equals("Meeting")) {


                meeting();

                event_rb.setVisibility(View.GONE);
                meeting_rb.setVisibility(View.VISIBLE);

                meeting_rb.setChecked(true);

                setdate_tv.setText(date);
                timeTV.setText(time);

                titleET.setText(title);
                //  clientLocationTV.setText(location);
                locationET.setText(location);
            }

        }
        dateTimeInterface = this;

        return view;
    }


    private void initialzingFirebase() {
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();

        firebaseDatabase = FirebaseDatabase.getInstance();


    }

    @Override
    public void onStart() {
        super.onStart();

        if (googleApiClient != null)
            googleApiClient.connect();
    }

    @Override
    public void onStop() {

        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        googleApiClient.stopAutoManage(getActivity());
        googleApiClient.disconnect();
    }

    private void initFragment() {

        searchStudentFragment = new SearchStudentFragment();
        inviteCourseFragment = new InviteCourseFragment();
    }

    private void setListener() {

        create_tv.setOnClickListener(this);
        calender_ll.setOnClickListener(this);
        time_ll.setOnClickListener(this);
        addimage_iv.setOnClickListener(this);
        image1_iv.setOnClickListener(this);
        image2_iv.setOnClickListener(this);
        eventChk_cb.setOnClickListener(this);
        location_ll.setOnClickListener(this);

    }

    private void bindViews(View view) {

        calender_iv = (ImageView) view.findViewById(R.id.calender_iv);
        timeIV = (ImageView) view.findViewById(R.id.timeIV);
        create_tv = (TextView) view.findViewById(R.id.create_tv);
        setdate_tv = (TextView) view.findViewById(R.id.setdate_tv);
        timeTV = (TextView) view.findViewById(R.id.timeTV);
        //  price_tv = (TextView) view.findViewById(R.id.price_tv);
        description_tv = (TextView) view.findViewById(R.id.description_tv);
      //  addimage_tv = (TextView) view.findViewById(R.id.addimage_tv);

        eventChk_cb = (CheckBox) view.findViewById(R.id.eventChk_cb);

        calender_ll = (LinearLayout) view.findViewById(R.id.calender_ll);
        time_ll = (LinearLayout) view.findViewById(R.id.time_ll);
        location_ll = (LinearLayout) view.findViewById(R.id.location_ll);

        //  ticket_rl = (RelativeLayout) view.findViewById(R.id.ticket_rl);

        titleET = (EditText) view.findViewById(R.id.titleET);
        location_et = (EditText) view.findViewById(R.id.location_et);
        locationET = (TextView) view.findViewById(R.id.locationET);
        //  price_et = (EditText) view.findViewById(R.id.price_et);
        description_et = (EditText) view.findViewById(R.id.description_et);

        addimage_iv = (ImageView) view.findViewById(R.id.image_iv);
        image1_iv = (ImageView) view.findViewById(R.id.image1_iv);
        image2_iv = (ImageView) view.findViewById(R.id.image2_iv);

      //  image1_iv.setEnabled(false);
       // image2_iv.setEnabled(false);

        radiogroup = (RadioGroup) view.findViewById(R.id.radiogroup);
        event_rb = (RadioButton) radiogroup.findViewById(R.id.event_rb);
        meeting_rb = (RadioButton) radiogroup.findViewById(R.id.meeting_rb);

        //  clientLocationTV = (PlacesAutocompleteTextView) view.findViewById(R.id.clientLocationTV);
        //   clientLocationTV = (AutoCompleteTextView) view.findViewById(R.id.clientLocationTV);

        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

       /* clientLocationTV.setResultType(AutocompleteResultType.GEOCODE);
        clientLocationTV.setHistoryManager(null);
        clientLocationTV.setRadiusMeters(Long.valueOf(0));*/
        //  accessLocation();
        //   AutoCompleteTextView autocompleteView = (AutoCompleteTextView) .findViewById(R.id.autocomplete);
        // clientLocationTV.setAdapter(new PlacesAutoCompleteAdapter(getActivity(), R.layout.autocomplete_list_item));

/*
        clientLocationTV.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull com.seatgeek.placesautocomplete.model.Place place) {
                Log.d("placee", "----" + place.description);
                location = place.description;
                clientLocationTV.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
                        Log.d("placee", "places");
                        //   client_name = "";
                      *//*  PlacesDetails pl = new PlacesDetails(getActivity());
                        ArrayList<Double> list = new ArrayList<Double>();
                        String data = pl.getLatLng(placeDetails.place_id, callBacks, "DESTINATION");
                        Log.d("locationFromPlaceid", "--" + data);*//*
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
            }
        });*/

        //    calender_iv.setOnClickListener(this);
        //   timeIV.setOnClickListener(this);
        //   addimage_iv.setOnClickListener(this);

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                if (meeting_rb.isChecked()) {

                    meeting();

                } else {

                    event();
                }

            }
        });


    }

    private void accessLocation() {

        Location location = getLastKnownLocation();

        Log.d("dsd", location + "");
        if (location != null) {
            clientLocationTV.setCurrentLocation(location);


            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException e) {


                e.printStackTrace();
            }

            StringBuilder stringBuilder = new StringBuilder();

            Address address = addresses.get(0);
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {

                if (i < address.getMaxAddressLineIndex() - 1) {
                    stringBuilder.append(address.getAddressLine(i) + ", ");
                }
            }


            String cityName = addresses.get(0).getAddressLine(0);
            String stateName = addresses.get(0).getAddressLine(1);
            String countryName = addresses.get(0).getAddressLine(2);
            String state = addresses.get(0).getAdminArea();

            stringBuilder.append(state);

            clientLocationTV.setText(stringBuilder.toString());
        }
    }

    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                bestLocation = locationManager.getLastKnownLocation(provider);
                //   Toast.makeText(getActivity(), "helo" + bestLocation, Toast.LENGTH_SHORT).show();
                Log.d("hello", "helllo" + bestLocation);


                Location l = locationManager.getLastKnownLocation(provider);

                //resetLocation();

                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {

                    bestLocation = l;
                }
            } else {
                bestLocation = locationManager.getLastKnownLocation(provider);
                //  Toast.makeText(getActivity(), "helo" + bestLocation, Toast.LENGTH_SHORT).show();
                Log.d("hello", "helllo" + bestLocation);
                Location l = locationManager.getLastKnownLocation(provider);

                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {

                    bestLocation = l;
                }
            }
            if (bestLocation == null) {
                return null;
            }


        }
        return bestLocation;
    }

    public void event() {

        description_tv.setVisibility(View.VISIBLE);

        description_et.setVisibility(View.VISIBLE);

    }

    public void meeting() {

        description_tv.setVisibility(View.GONE);

        description_et.setVisibility(View.GONE);

    }



    public void createEvent(final String mode, final boolean b, String s)

    {
        ShowDialog.showDialog(getContext());

        String key = "";

        databaseReference = firebaseDatabase.getReference(Constant.EVENT).child("Event");

        if (bundle != null) {
            key = s;
        } else {
            String keyEvent = databaseReference.push().getKey();
            key = keyEvent;
        }

        HashMap hashMap = new HashMap();
        hashMap.put("title", titleET.getText().toString());
        hashMap.put("location", locationET.getText().toString());

        hashMap.put("date", setdate_tv.getText().toString());
        hashMap.put("time", timeTV.getText().toString());

        if (mode.equals("Event")) {
            // hashMap.put("price", price_et.getText().toString());
            hashMap.put("desc", description_et.getText().toString());
        }
        hashMap.put("type", mode);
        hashMap.put("admin", StoreData.getUserKeyFromSharedPre(getContext()));

        final String finalKey = key;

        for (int i = 0; i < dataArray.length; i++) {

            String picture = dataArray[i];

            if (picture != null) {
                if (!picture.contains("https://firebasestorage.googleapis.com")) {

                    isShow = true;
                    break;
                }
            }
        }

        if (isShow) {

           // Log.d("event", "Xxczx");
            upload(finalKey, true, hashMap);

            isShow = false;
        } else {

            showDialog = true;

            databaseReference.child(key).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {

                    showDialog = false;
                    HashMap hashMap1 = new HashMap();
                    hashMap1.put(StoreData.getUserKeyFromSharedPre(getContext()), Constant.ACCEPTED_NAME);
                    databaseReference.child(finalKey).child(Constant.INVITED_STUDENTS).updateChildren(hashMap1);

                    if (invite.equals(Constant.INDIVI_NAME) || invite.equals(Constant.COURSE_NAME) || invite.equals(Constant.COLLEGE)) {
                        if (keyList.size() > 0) {

                            for (int i = 0; i < keyList.size(); i++) {

                                if (!keyList.get(i).equals(StoreData.getUserKeyFromSharedPre(getContext()))) {
                                    HashMap hashMap2 = new HashMap();
                                    hashMap2.put(keyList.get(i), Constant.WAITING);
                                    databaseReference.child(finalKey).child(Constant.INVITED_STUDENTS).updateChildren(hashMap2);
                                }

                            }
                        }


                    }
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }


                    if (mode.equals("Meeting") && !b) {

                        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                .setTitle("Alert")
                                .setMessage("Meeting created successfully.")
                                .setCancelable(false)
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                                        if (keyList.size() > 0) {

                                            String message = "{\n" +
                                                    "  \"SenderName\": \"" + userFullName + "\",\n" +
                                                    "  \"Message\": \"" + "Meeting:" + " " + titleET.getText().toString() + "\"\n" +
                                                    "}";


                                            HashMap map = new HashMap();
                                            map.put("title", userFullName);
                                            map.put("body", message);


                                            getToken(map);
                                        }

                                    }
                                })
                                .setIcon(R.mipmap.imagesuccess)
                                .show();
                    } else if (mode.equals("Meeting") && b) {
                        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                .setTitle("Alert")
                                .setMessage("Meeting update done.")
                                .setCancelable(false)
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                                        if (keyList.size() > 0) {

                                            String message = "{\n" +
                                                    "  \"SenderName\": \"" + userFullName + "\",\n" +
                                                    "  \"Message\": \"" + "Meeting:" + " " + titleET.getText().toString() + "\"\n" +
                                                    "}";


                                            HashMap map = new HashMap();
                                            map.put("title", userFullName);
                                            map.put("body", message);


                                            getToken(map);
                                        }

                                    }
                                })
                                .setIcon(R.mipmap.imagesuccess)
                                .show();
                    } else if (mode.equals("Event") && !b) {
                        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                .setTitle("Alert")
                                .setMessage("Event Created successfully.")
                                .setCancelable(false)
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                                        if (keyList.size() > 0) {

                                            String message = "{\n" +
                                                    "  \"SenderName\": \"" + userFullName + "\",\n" +
                                                    "  \"Message\": \"" + "Event:" + " " + titleET.getText().toString() + "\"\n" +
                                                    "}";


                                            HashMap map = new HashMap();
                                            map.put("title", userFullName);
                                            map.put("body", message);


                                            getToken(map);
                                        }

                                    }
                                })
                                .setIcon(R.mipmap.imagesuccess)
                                .show();
                    } else if (mode.equals("Event") && b) {
                        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                .setTitle("Alert")
                                .setMessage("Event update done.")
                                .setCancelable(false)
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                                        if (keyList.size() > 0) {

                                            String message = "{\n" +
                                                    "  \"SenderName\": \"" + userFullName + "\",\n" +
                                                    "  \"Message\": \"" + "Event:" + " " + titleET.getText().toString() + "\"\n" +
                                                    "}";


                                            HashMap map = new HashMap();
                                            map.put("title", userFullName);
                                            map.put("body", message);


                                            getToken(map);
                                        }

                                    }
                                })
                                .setIcon(R.mipmap.imagesuccess)
                                .show();
                    }

                }

            });
        }


    }


    private void getYourBackground(final int i) {

        final String[] stringItems = {getResources().getString(R.string.take_photo), getResources().getString(R.string.choose_gallery)};

        final ActionSheetDialog dialog = new ActionSheetDialog(getActivity(), stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {

                image = dataArray[i];

                if (position == 0) {

                    captureImage();

                    if (dataArray.length > i) {
                        dataArray[i] = "0";
                    }


                } else if (position == 1) {


                    try {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_PICK);
                        startActivityForResult(intent, GALLERY_CODE);

                        if (dataArray.length > i) {
                            dataArray[i] = "0";
                        }

                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }


                }
                dialog.dismiss();

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult", "requestCode" + requestCode + "data" + data + "resultCode" + resultCode);

        if (resultCode == RESULT_OK) {
            if (requestCode == 150) {

                invite = Constant.INDIVI_NAME;
                keyList = new ArrayList<>();
                keyList = (ArrayList<String>) data.getSerializableExtra(Constant.CHECK_LIST);


            } else if (requestCode == 200) {
                invite = Constant.COURSE_NAME;
                keyList = new ArrayList<>();
                keyList = (ArrayList<String>) data.getSerializableExtra(Constant.CHECK_LIST);


            } else if (requestCode == 125) {

                String location = data.getStringExtra(Constant.CHECK_LIST);


                locationET.setText(location);
            }

            if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(getContext(), data);


                    locationET.setText(place.getName() + "," + place.getAddress());
                } else if (resultCode == PlacePicker.RESULT_ERROR) {
                    Status status = PlacePicker.getStatus(getContext(), data);
                    // TODO: Handle the error.


                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }
            }

        }
        if (resultCode == RESULT_CANCELED) {
            for (int i = 0; i < dataArray.length; i++) {

                if (dataArray[i] == "0") {


                    dataArray[i] = image;


                }
            }

        }
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {


            picturePath = "";

            picturePath = destination.getAbsolutePath();

            length++;
            for (int i = 0; i < dataArray.length; i++) {

                if (dataArray[i] == "0") {
                    dataArray[i] = picturePath;
                }

//                Log.d("sdsf",dataArray[i]);
            }

            //  list.add(picturePath);

            if (picturePath != null && !picturePath.isEmpty()) {

                Uri uri = Uri.fromFile(new File(picturePath));

                //  if (list.size() > 0) {
                if (dataArray.length > 0) {

                    for (int i = 0; i < dataArray.length; i++) {


                        if (i == 0) {

                            if (dataArray[i] != null) {

                                Glide.with(getActivity()).load(dataArray[i]).into(addimage_iv);


                                image1_iv.setVisibility(View.VISIBLE);
                                image1_iv.setEnabled(true);
                                image1_iv.setImageResource(R.mipmap.picture);
                            }
                        }
                        if (i == 1) {
                            if (dataArray[i] != null) {

                                Glide.with(getActivity()).load(dataArray[i]).into(image1_iv);

                                image2_iv.setVisibility(View.VISIBLE);
                                image2_iv.setEnabled(true);
                                image2_iv.setImageResource(R.mipmap.picture);
                            }
                        }
                        if (i == 2) {
                            if (dataArray[i] != null) {

                                Glide.with(getActivity()).load(dataArray[i]).into(image2_iv);
                            }
                        }
                    }
                }


            }
            //backGroundThread(bitmap);



        }
        if (data != null && requestCode == GALLERY_CODE) {
            try {

                Log.d("imagee", "--" + GALLERY_CODE);
                picturePath = "";
                Uri selectedImage = data.getData();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                Log.d("gallery_path", "" + picturePath);
                cursor.close();


                for (int i = 0; i < dataArray.length; i++) {

                    if (dataArray[i] == "0") {
                        dataArray[i] = picturePath;
                    }
                }
                // list.add(picturePath);

                if (picturePath != null && !picturePath.isEmpty()) {

                    Uri uri = Uri.fromFile(new File(picturePath));

                    if (dataArray.length > 0) {

                        for (int i = 0; i < dataArray.length; i++) {


                            if (i == 0) {

                                if (dataArray[i] != null) {

                                    Glide.with(getActivity()).load(dataArray[i]).into(addimage_iv);


                                    image1_iv.setVisibility(View.VISIBLE);

                                    image1_iv.setEnabled(true);
                                    image1_iv.setImageResource(R.mipmap.picture);
                                }
                            }
                            if (i == 1) {
                                if (dataArray[i] != null) {

                                    Glide.with(getActivity()).load(dataArray[i]).into(image1_iv);

                                    image2_iv.setVisibility(View.VISIBLE);
                                    image2_iv.setEnabled(true);
                                    image2_iv.setImageResource(R.mipmap.picture);
                                }
                            }
                            if (i == 2) {
                                if (dataArray[i] != null) {

                                    Glide.with(getActivity()).load(dataArray[i]).into(image2_iv);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private void captureImage() {

        picturePath = "";
        destination = null;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        picturePath = String.valueOf(new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg"));

        destination = new File(picturePath);

        Log.e("FileDestination", "" + destination);
        Log.e("picturePath", "" + picturePath);
        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination);
        } else {
            imageuri = Uri.fromFile(destination);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

    }


    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_tv.setText("Create event");
        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
      //  ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
/*
        if (bundle != null) {
            ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.VISIBLE);

            ((WelcomeActivity) getActivity()).header_iv.setOnClickListener(this);
        }*/

        Application.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onClick(View v) {

        // ReplaceFragment.replace(((HomeActivity) getActivity()),messageBoardFragment);

        //  popBackStack(((HomeActivity) getActivity()));

        switch (v.getId()) {
            case R.id.create_tv:

                String mode = "";
                if (meeting_rb.isChecked()) {
                    mode = "Meeting";
                } else {
                    mode = "Event";

                }
                if (validateFields(v, mode)) {

                    if (!isNetworkAvailable(getContext())) {
                        ShowDialog.networkDialog(getContext());
                    } else {

                        ShowDialog.showDialog(getContext());
                      AsyncCaller asyncCaller=new AsyncCaller(mode);
                        asyncCaller.execute();


                    }

                }
                break;
            case R.id.image_iv:

                String[] permission = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};
                requestAppPermissions(permission, R.string.permission, 53);

             //   getYourBackground(0);

                break;

            case R.id.image1_iv:

                String[] permission1 = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};
                requestAppPermissions(permission1, R.string.permission, 54);

              //  getYourBackground(1);

                break;

            case R.id.image2_iv:

                String[] permission2 = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};
                requestAppPermissions(permission2, R.string.permission, 55);

              //  getYourBackground(2);

                break;

            case R.id.calender_ll:

                showDateDialog();

                break;
            case R.id.time_ll:


                // Get Current Time
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                c.set(Calendar.MINUTE, minute);

                                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");


                                String time = formatter.format(c.getTime());


                                timeTV.setText(time);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();


                break;

            case R.id.header_iv:

                if (type.equals("Meeting")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("mode", type);
                    bundle.putString("key", key);
                    bundle.putString("studentList", studentList);
                    SearchStudentFragment searchStudentFragment = new SearchStudentFragment();
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), searchStudentFragment, "InviteStudent_Fragment", bundle);
                } else {
                    actionSheetDialog(key, type);
                }
                break;

            case R.id.eventChk_cb:

                eventChk_cb.setChecked(false);

                if (event_rb.isChecked()) {
                    actionSheetDialog("", "Event");
                } else {
                    eventChk_cb.setChecked(true);
                    Bundle bundle = new Bundle();
                    bundle.putString("mode", "Meeting");
                    if (!studentList.isEmpty()) {
                        bundle.putString("studentList", studentList);
                    }
                    SearchStudentFragment searchStudentFragment = new SearchStudentFragment();
                    searchStudentFragment.setArguments(bundle);
                    searchStudentFragment.setTargetFragment(EventFragment.this, 150);
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), searchStudentFragment, "InviteStudent_Fragment");
                }

                break;

            case R.id.location_ll:


                statusCheck();

                break;
        }
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else {

            if (googleApiClient == null || !googleApiClient.isConnected())
                return;

          //  PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();


            try {
                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(getActivity());
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);


                // startActivityForResult(builder.build(getActivity()), PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }

        }
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {

                        location_et.setVisibility(View.VISIBLE);
                        locationET.setVisibility(View.GONE);
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    private void showDateDialog() {

        DialogFragment dialogFragment1 = SelectDateFragment.getInstance(dateTimeInterface);

        dialogFragment1.show(getFragmentManager(), "DatePicker");


    }


    private void actionSheetDialog(final String key, final String mode) {


        final Bundle bundle1 = new Bundle();
        bundle1.putString("mode", mode);


        if (!studentList.isEmpty()) {
            bundle1.putString("studentList", studentList);
        }

        final String[] stringItems = {"Student", "Subject", "Specialization"};

        final ActionSheetDialog dialog = new ActionSheetDialog(getContext(), stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d("zcc", position + "");
                if (position == 0) {

                    eventChk_cb.setChecked(true);

                    SearchStudentFragment searchStudentFragment = new SearchStudentFragment();
                    searchStudentFragment.setArguments(bundle1);
                    searchStudentFragment.setTargetFragment(EventFragment.this, 150);
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), searchStudentFragment, "InviteStudent_Fragment");

                } else if (position == 1) {

                    eventChk_cb.setChecked(true);


                    InviteCourseFragment inviteCourseFragment = new InviteCourseFragment();
                    inviteCourseFragment.setTargetFragment(EventFragment.this, 200);
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), inviteCourseFragment, "Course_Fragment", bundle1);
                } else if (position == 2) {

                    getStudentFromCollegeandInviteThem(mode);

                }

                dialog.dismiss();
            }
        });
    }


    private void getStudentFromCollegeandInviteThem(final String mode) {

        keyList = new ArrayList<>();
        if (!isNetworkAvailable(getContext())) {
            networkDialog(getContext());
        } else {

            ShowDialog.showDialog(getContext());

            firebaseDatabase = FirebaseDatabase.getInstance();
            registerUserRef = firebaseDatabase.getReference(Constant.REGISTERED_NAME);

            registerUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    int i = 0;
                    String myCollegeName = StoreData.getCollegeFromSharedPre(getContext());
                    HashMap invitedStudenthashmap = new HashMap();
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                        HashMap hashMap = (HashMap) dataSnapshot1.getValue();

                        if (hashMap.get("specialization").equals(myCollegeName)) {

                            if (!listStudent.contains(dataSnapshot1.getKey())) {

                                keyList.add(dataSnapshot1.getKey());

                                //   invitedStudenthashmap.put(dataSnapshot1.getKey(), Constant.WAITING);
                            }
                            Log.d("qwerty",keyList+"");
                        }


                    }
                   /* String data = StoreData.getUserKeyFromSharedPre(getActivity());
                    invitedStudenthashmap.put(data, Constant.ACCEPTED_NAME);
                    getInviteListForEvent(invitedStudenthashmap, mode);
*/

                    invite = Constant.COLLEGE;
                    eventChk_cb.setChecked(true);

                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {


                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }


                }
            });
        }
    }


    private void upload(final String key, final boolean b, final HashMap hashMap) {


        if (!ShowDialog.dialog.isShowing()) {


            ShowDialog.showDialog(getContext());

        }

        final StringBuilder stringBuilder = new StringBuilder();


        for (String picture : dataArray) {


            Uri imgUri = Uri.parse("file://" + picture);

            if (picture != null) {


                if (!picture.contains("https://firebasestorage.googleapis.com")) {


                    FirebaseStorage.getInstance().getReference().child(Constant.IMAGE_NAME + "_" + SystemClock.currentThreadTimeMillis())
                            .putFile(imgUri)
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                    Log.d("responsexx", "failure" + "----" + e.getMessage());
                                    //   ShowDialog.hideDialog();

                                    showDialog = false;
                                }
                            })
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                                    String downloadImageUri = String.valueOf(taskSnapshot.getDownloadUrl());

                                    count++;

                                    stringBuilder.append(downloadImageUri + ",");

                                /*    HashMap hashMap1 = new HashMap();
                                    hashMap1.put("image_url", stringBuilder.toString());
*/
                                    String array[] = stringBuilder.toString().split(",");

                                    //    databaseReference.child(key).updateChildren(hashMap1);

                                    for (int i = 0; i < dataArray.length; i++) {

                                        if (dataArray[i] == null) {
                                            count++;
                                        }
                                    }
                                    Log.d("sdsf", count + "---" + array.length);

                                    if (count == (dataArray.length) + 1 || count == dataArray.length) {

                                        showDialog = true;

                                        Log.d("event", "fhghfh");


                                        hashMap.put("image_url", stringBuilder.toString());

                                        databaseReference.child(key).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener() {
                                            @Override
                                            public void onComplete(@NonNull Task task) {

                                                showDialog = false;

                                                Log.d("event", "xzchghfh");
                                                HashMap hashMap1 = new HashMap();
                                                hashMap1.put(loginUserKey, Constant.ACCEPTED_NAME);
                                                databaseReference.child(key).child(Constant.INVITED_STUDENTS).updateChildren(hashMap1);


                                                if (invite.equals(Constant.INDIVI_NAME) || invite.equals(Constant.COURSE_NAME) || invite.equals(Constant.COLLEGE)) {
                                                    if (keyList.size() > 0) {

                                                        for (int i = 0; i < keyList.size(); i++) {

                                                            if (!keyList.get(i).equals(loginUserKey)) {
                                                                HashMap hashMap2 = new HashMap();
                                                                hashMap2.put(keyList.get(i), Constant.WAITING);
                                                                databaseReference.child(key).child(Constant.INVITED_STUDENTS).updateChildren(hashMap2);
                                                            }

                                                        }
                                                    }


                                                }


                                                if (ShowDialog.dialog != null) {
                                                    if (ShowDialog.dialog.isShowing()) {
                                                        ShowDialog.hideDialog();
                                                    }
                                                }


                                                if (bundle != null) {

                                                    if (fragmentEvent != null && fragmentEvent.isVisible()) {

                                                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                                                                .setTitle("Alert")
                                                                .setMessage("Event updated successsfully.")
                                                                .setCancelable(false)
                                                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        dialog.dismiss();

                                                                        ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                                                                        if (keyList.size() > 0) {

                                                                            String message = "{\n" +
                                                                                    "  \"SenderName\": \"" + userFullName + "\",\n" +
                                                                                    "  \"Message\": \"" + "Event:" + " " + titleET.getText().toString() + "\"\n" +
                                                                                    "}";


                                                                            HashMap map = new HashMap();
                                                                            map.put("title", userFullName);
                                                                            map.put("body", message);


                                                                            getToken(map);
                                                                        }
                                                                    }
                                                                })
                                                                .setIcon(R.mipmap.imagesuccess)
                                                                .show();

                                                    }
                                                } else {

                                                    if (fragmentEvent != null && fragmentEvent.isVisible()) {

                                                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                                                                .setTitle("Alert")
                                                                .setMessage("Event Created.")
                                                                .setCancelable(false)
                                                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int which) {

                                                                        dialog.dismiss();

                                                                        ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                                                                        if (keyList.size() > 0) {

                                                                            String message = "{\n" +
                                                                                    "  \"SenderName\": \"" + userFullName + "\",\n" +
                                                                                    "  \"Message\": \"" + "Event:" + " " + titleET.getText().toString() + "\"\n" +
                                                                                    "}";


                                                                            HashMap map = new HashMap();
                                                                            map.put("title", userFullName);
                                                                            map.put("body", message);


                                                                            getToken(map);
                                                                        }
                                                                    }
                                                                })
                                                                .setIcon(R.mipmap.imagesuccess)
                                                                .show();


                                                    }

                                                }


                                            }
                                            // }

                                            //  ReplaceFragment.popBackStack((WelcomeActivity) getActivity());

                                        });

                                    }
                                }
                            });
                } else {
                    stringBuilder.append(picture + ",");

                    Log.d("dfdfd", stringBuilder + "");

                    count++;
                }
            }


        }
        //}
    }


    @Override
    public void setOnDate(int year, int month, int dayOfMonth) {
        String mon = MONTHS[month];

        setdate_tv.setText(dayOfMonth + " " + mon + ", " + year);
    }

    @Override
    public void setOnTime() {

    }

    private boolean validateFields(View v, String mode) {

        if (titleET.getText().toString().isEmpty()) {

            ShowDialog.showSnackbarError(v, "Please enter title of event", getActivity());

            return false;
        } else if (setdate_tv.getText().toString().isEmpty()) {
            ShowDialog.showSnackbarError(v, "Please enter date of event", getActivity());

            return false;
        } else if (timeTV.getText().toString().isEmpty()) {
            ShowDialog.showSnackbarError(v, "Please enter time of event", getActivity());
            return false;
        }
        return true;
    }


    private void getToken(final HashMap map) {

        FirebaseDatabase.getInstance().getReference("registered_user").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d("sds", dataSnapshot.getValue() + "-----" + keyList);

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Log.d("ddsds", snapshot.getKey() + "-----" + keyList);
                    if (!snapshot.getKey().equals(loginUserKey)) {
                        if (keyList.contains(snapshot.getKey())) {

                            String notificetion = snapshot.child(Constant.NOTIFICATION_KEY).getValue().toString();

                            if (notificetion.equals("true")) {
                                String key = snapshot.getKey();
                                Object token = snapshot.child("token").getValue();

                                Log.d("uiosds", token + " " + key);


                                sendNotification((String) token, map);
                            }
                        }

                    }

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void sendNotification(String fcmID, HashMap<String, String> map) {


        String body123 = "{\n\t\"to\": \" " + fcmID + "\",\n\t\"notification\" :" + convert(map) + ",\n\t\"data\" :" + convert1(map) + "\n}";

        Log.e("DataToSend", "" + body123);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");


        RequestBody body = RequestBody.create(mediaType, body123);
        Request request = new Request.Builder()
                .url("https://fcm.googleapis.com/fcm/send")
                .post(body)
                .addHeader("authorization", "key=" + Constant.SERVER_KEY)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")

                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    Log.e("ResponseBALLI", response.body().string());
                    Log.d("sa", "sa");
                    Log.i("MESSAGE", response.message());
                }
            }


        });


    }


    public String convert(HashMap<String, String> map) {
        JSONObject obj = null;
        try {


            obj = new JSONObject();
            String message = map.get("body");

            JSONObject meJson = new JSONObject(message);

            //   obj.put("body",  meJson.optString("Message") +" from "+ meJson.optString("groupName") +" by "+ meJson.optString("SenderName") );
            obj.put("body", meJson.optString("Message"));
            obj.put("title", userFullName);
            obj.put("icon", R.mipmap.logo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("ds", obj + "");
        return obj.toString();
    }


    public String convert1(HashMap<String, String> map) {
        JSONObject obj = null;
        try {

            obj = new JSONObject(map);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("ds", obj + "");
        return obj.toString();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


        if (!isConnected) {
            try {
                ShowDialog.networkDialog(getContext());
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }
            } catch (Exception e) {

            }
        } else if (isConnected) {
            if (ShowDialog.network_dialog != null) {
                if (ShowDialog.network_dialog.isShowing()) {
                    ShowDialog.network_dialog.dismiss();
                }
            }

            if (showDialog == true) {
                try {
                    ShowDialog.showDialog(getContext());
                }catch (Exception e)
                {

                }
            }
        }

    }


    public static boolean isInternetAccessible(Context context) {

        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Couldn't check internet connection", e);
        }

        return false;


    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode==53)
        {
            getYourBackground(0);
        }
        else if (requestCode==54)
        {
            getYourBackground(1);
        }
        else
        {
            getYourBackground(2);
        }

    }

    private class AsyncCaller extends AsyncTask<Void, Void, Boolean> {

        String mode;

        public AsyncCaller(String mode) {

            this.mode=mode;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            boolean networkAccess = isInternetAccessible(getContext());
            return networkAccess;
        }

        @Override
        protected void onPostExecute(Boolean networkAccess) {
            super.onPostExecute(networkAccess);


            if (!networkAccess) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {

                        networkDialog(getContext());
                        ShowDialog.hideDialog();
                    }
                }
            }

            else
            {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {

                        ShowDialog.hideDialog();
                    }
                }
                if (mode.equals("Event")) {

                    if (bundle != null) {

                        createEvent(mode, true, key);

                    } else {


                        createEvent(mode, false, "");
                    }


                } else {

                    if (bundle != null) {

                        createEvent(mode, true, key);

                    } else {


                        createEvent(mode, false, "");

                    }


                }
            }

        }
    }
}
