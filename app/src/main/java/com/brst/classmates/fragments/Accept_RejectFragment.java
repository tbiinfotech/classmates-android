package com.brst.classmates.fragments;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.AcceptAdaptor;
import com.brst.classmates.classses.AlarmReceiver;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.content.Context.ALARM_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class Accept_RejectFragment extends Fragment implements View.OnClickListener, ClickRecyclerInterface {

    RecyclerViewEmptySupport accept_rv;

    String courseKey;
    AddAssignmentFragment addAssignmentFragment;

    View view;

    TextView empty_tv;

    AcceptAdaptor acceptAdaptor;

    List<HashMap<String, String>> assignmentList = new ArrayList<>();

    DatabaseReference assignmentReference;

    static Accept_RejectFragment instance;

    Fragment fragment;

    public Accept_RejectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_accept__reject, container, false);
        }
        instance = new Accept_RejectFragment();
        Bundle bundle = getParentFragment().getArguments();
        if (bundle != null) {
            //  courseKey = bundle.getString(Constant.COURSE_KEY);

        }


        assignmentReference = FirebaseDatabase.getInstance().getReference("AssignmentTask");
        bindViews(view);

        setAdaptor();

        setListener();

        initFragment();

        checkNetwork();


        return view;
    }

    public static Accept_RejectFragment getInstance() {
        return instance;
    }


    private void bindViews(View view) {

        accept_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.accept_rv);
        empty_tv = (TextView) view.findViewById(R.id.empty_tv);

    }

    private void setAdaptor() {

        acceptAdaptor = new AcceptAdaptor(getContext(), assignmentList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        accept_rv.setLayoutManager(mLayoutManager);
        accept_rv.setAdapter(acceptAdaptor);
        accept_rv.setEmptyView(empty_tv);

        acceptAdaptor.setClickListener(this);
    }

    private void setListener() {

        ((WelcomeActivity) getActivity()).header_iv.setOnClickListener(this);
    }

    private void initFragment() {
        addAssignmentFragment = new AddAssignmentFragment();
    }


    private void checkNetwork() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {
            getAssignments();
        } else {
            ShowDialog.networkDialog(getContext());

            getAssignments();
        }
    }

    public void displayAlert(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to exit?").setCancelable(
                false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getAssignments() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {
            ShowDialog.showDialog(getContext());
        }


        final String userLogin = StoreData.getUserKeyFromSharedPre(getContext());

        assignmentReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                assignmentList = new ArrayList<HashMap<String, String>>();
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = iterable.iterator();

                while (iterator.hasNext()) {
                    DataSnapshot snapshot = iterator.next();

                    String courseKey = snapshot.getKey();

                    Iterable<DataSnapshot> iterable1 = snapshot.getChildren();
                    Iterator<DataSnapshot> iterator1 = iterable1.iterator();

                    while (iterator1.hasNext()) {
                        DataSnapshot snapshot1 = iterator1.next();
                        String assignmentKey = snapshot1.getKey();

                        String assignmentName="",assignmentType="",assignmentStatus="",
                                assignmentAccept="",assignmentDate="",assignmentAdmin="",assignmentStudent="",assignmentprofile="",assignmentOwnerName="";
                        if (snapshot1.child(Constant.STUDENT_LIST).child(userLogin).exists() && snapshot1.child(Constant.STUDENT_LIST).child(userLogin).child(Constant.ACCEPT_NAME).getValue().equals(Constant.FALSE_NAME)) {

                        if(snapshot1.child(Constant.ASSIGN_NAME).exists()) {
                             assignmentName = snapshot1.child(Constant.ASSIGN_NAME).getValue().toString();
                        }
                            if(snapshot1.child(Constant.ASSIGNTYPE_NAME).exists()) {
                                 assignmentType = snapshot1.child(Constant.ASSIGNTYPE_NAME).getValue().toString();
                            }
                            if(snapshot1.child(Constant.STUDENT_LIST).exists()) {
                                 assignmentStatus = snapshot1.child(Constant.STUDENT_LIST).child(userLogin).child(Constant.STATUS_NAME).getValue().toString();
                                 assignmentAccept = snapshot1.child(Constant.STUDENT_LIST).child(userLogin).child(Constant.ACCEPT_NAME).getValue().toString();
                            }
                            if(snapshot1.child(Constant.ASSIGNDATE_NAME).exists()) {
                                 assignmentDate = snapshot1.child(Constant.ASSIGNDATE_NAME).getValue().toString();
                            }
                            if(snapshot1.child(Constant.ASSIGNADMIN_NAME).exists()) {
                                 assignmentAdmin = snapshot1.child(Constant.ASSIGNADMIN_NAME).getValue().toString();
                            }
                            if(snapshot1.child(Constant.STUDENT_LIST).exists()) {
                                 assignmentStudent = snapshot1.child(Constant.STUDENT_LIST).getValue().toString();
                            }
                            if(snapshot1.child(Constant.ASSIOWNERPROFILE_NAME).exists()) {
                                 assignmentprofile = snapshot1.child(Constant.ASSIOWNERPROFILE_NAME).getValue().toString();
                            } if(snapshot1.child(Constant.ASSIGNOWNERNAME_NAME).exists()) {
                                 assignmentOwnerName = snapshot1.child(Constant.ASSIGNOWNERNAME_NAME).getValue().toString();
                            }

                            if(snapshot1.child(Constant.ASSIGN_NAME).exists()) {
                                if (!(snapshot1.child(Constant.STUDENT_LIST).child(userLogin)).getKey().equals(assignmentAdmin)) {
                                    HashMap<String, String> hashMap = new HashMap();
                                    hashMap.put(Constant.ASSIGN_NAME, assignmentName);
                                    hashMap.put(Constant.ASSIGNTYPE_NAME, assignmentType);
                                    hashMap.put(Constant.ASSIGNDATE_NAME, assignmentDate);
                                    hashMap.put(Constant.STATUS_NAME, assignmentStatus);
                                    hashMap.put(Constant.KEY_NAME, assignmentKey);
                                    hashMap.put(Constant.COURSE_KEY, courseKey);
                                    hashMap.put(Constant.ASSIGNADMIN_NAME, assignmentAdmin);
                                    hashMap.put(Constant.ASSIOWNERPROFILE_NAME, assignmentprofile);
                                    hashMap.put(Constant.ASSIGNOWNERNAME_NAME, assignmentOwnerName);
                                    hashMap.put(Constant.ACCEPT_NAME, assignmentAccept);

                                    assignmentList.add(hashMap);
                                }
                            }
                        }
                    }


                }

                Collections.reverse(assignmentList);

                acceptAdaptor.addData(assignmentList);
                acceptAdaptor.notifyDataSetChanged();


                if(ShowDialog.dialog!=null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                if(ShowDialog.dialog!=null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();

                        }
                    }

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.header_iv:

                Fragment myFragment = getFragmentManager().findFragmentByTag("AcceptReject_Fragment");

                if (myFragment != null && myFragment.isVisible()) {

                    ReplaceFragment.replace(((WelcomeActivity) getActivity()), addAssignmentFragment, "AddAssignment_Fragment");
                }

                break;
        }

    }

    @Override
    public void onRecyClick(View view, int position) {


        switch (view.getId()) {

            case R.id.acceptassign_tv:

                String data = view.getTag().toString();

                String tagdata[] = data.split(";");

                String key = tagdata[0];
                String assignDate = tagdata[1];
                String assignName = tagdata[2];
                String courseKey = tagdata[3];

                String[] dateFormat = assignDate.split(" ");

                String date = dateFormat[0];
                String month = dateFormat[1];
                String year = dateFormat[2];

                String yearformat[] = year.split("@");
                String yearName = yearformat[0];
                String time = yearformat[1];

                String timeformat[] = time.split(":");
                String hour = timeformat[0];
                String minute = timeformat[1];


                if (month.endsWith(",")) {
                    month = month.substring(0, month.length() - 1);
                }
                int monthInt = 0;
                Calendar cal = Calendar.getInstance();

                try {
                    cal.setTime(new SimpleDateFormat("MMM").parse(month));
                    monthInt = cal.get(Calendar.MONTH);


                } catch (ParseException e) {
                    e.printStackTrace();

                }

                Calendar calen = Calendar.getInstance();
                calen.set(Calendar.MONTH, monthInt);
                calen.set(Calendar.YEAR, Integer.parseInt(yearName));
                calen.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date) - 1);
                calen.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
                calen.set(Calendar.MINUTE, Integer.parseInt(minute));
                calen.set(Calendar.SECOND, 0);
                int val = (int) System.currentTimeMillis();

                if (checkDrawOverlayPermission()) {

                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.CHECKED_NAME, String.valueOf(val));
                    bundle.putString(Constant.ASSIGN_NAME, assignName);
                    bundle.putString(Constant.ASSIGNDATE_NAME, assignDate);
                    Intent _myIntent = new Intent(getContext(), AlarmReceiver.class);
                    _myIntent.putExtra(Constant.BUNDLE_NAME, bundle);

                    PendingIntent _myPendingIntent = PendingIntent.getBroadcast(getContext(), val, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                    AlarmManager _myAlarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

                    _myAlarmManager.set(AlarmManager.RTC_WAKEUP, calen.getTimeInMillis(), _myPendingIntent);

                    assignmentReference.child(courseKey).child(key).child(Constant.STUDENT_LIST).child(StoreData.getUserKeyFromSharedPre(getContext()))
                            .child(Constant.ACCEPT_NAME).setValue(Constant.ACCEPTED_NAME);
                }

                Calendar cale = Calendar.getInstance();
                cale.set(Calendar.MONTH, monthInt);
                cale.set(Calendar.YEAR, Integer.parseInt(yearName));
                cale.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date));
                cale.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour) + 2);
                cale.set(Calendar.MINUTE, Integer.parseInt(minute));
                cale.set(Calendar.SECOND, 0);
                int valcal = (int) System.currentTimeMillis();


                SharedPreference.getInstance().storeData(getContext(), key, String.valueOf(valcal));

                Bundle bundle = new Bundle();
                bundle.putString(Constant.CHECKED_NAME, String.valueOf(valcal));
                bundle.putString(Constant.ASSIGN_NAME, assignName);
                bundle.putString(Constant.ASSIGNDATE_NAME, assignDate);
                bundle.putString("Notification", "notification");
                Intent _myIntent = new Intent(getContext(), AlarmReceiver.class);
                _myIntent.putExtra(Constant.BUNDLE_NAME, bundle);

                PendingIntent _myPendingIntent = PendingIntent.getBroadcast(getContext(), valcal, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                AlarmManager _myAlarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

                _myAlarmManager.set(AlarmManager.RTC_WAKEUP, cale.getTimeInMillis(), _myPendingIntent);

                assignmentReference.child(courseKey).child(key).child(Constant.STUDENT_LIST).child(StoreData.getUserKeyFromSharedPre(getContext()))
                        .child(Constant.ACCEPT_NAME).setValue(Constant.ACCEPTED_NAME);

                break;

            case R.id.rejectassign_tv:

                String key1 = view.getTag().toString();
                String rejectKey[] = key1.split(",");
                String assignKey = rejectKey[0];
                String courseKeyValue = rejectKey[1];
                assignmentReference.child(courseKeyValue).child(assignKey).child(Constant.STUDENT_LIST).child(StoreData.getUserKeyFromSharedPre(getContext()))
                        .child(Constant.ACCEPT_NAME).setValue(Constant.REJECTED_NAME);
                break;
        }

    }

    @Override
    public void onLongClick(View view, int position) {

    }


    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(getContext())) {

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getContext().getPackageName()));
            startActivityForResult(intent, 500);
            return false;
        } else {
            return true;
        }
    }
}
