package com.brst.classmates.fragments;


import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;

import com.brst.classmates.adaptors.ContactInviteAdaptor;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.utility.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactInviteFragment extends Fragment implements TextWatcher,View.OnClickListener {

    RecyclerViewEmptySupport contact_rv;
    EditText searchContactUser_et;
    TextView  empty_tv,invite_tv;

    ArrayList<HashMap<String,String>> searchContactList;
    ArrayList<HashMap<String,String>> contactsList;

    ContactInviteAdaptor contactAdaptor;

    View view=null;

    public ContactInviteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(view==null) {
            view = inflater.inflate(R.layout.fragment_contactinvite, container, false);
        }

        bindViews();



        return view;
    }

    private void bindViews() {

        contact_rv=(RecyclerViewEmptySupport)view.findViewById(R.id.contact_rv);
        searchContactUser_et=(EditText) view.findViewById(R.id.searchContactUser_et);
        empty_tv=(TextView) view.findViewById(R.id.empty_tv);
        invite_tv=(TextView) view.findViewById(R.id.invite_tv);

        searchContactUser_et.addTextChangedListener(this);

        invite_tv.setOnClickListener(this);

        LoadContactsAyscn lca = new LoadContactsAyscn();
        lca.execute();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {


        String s2;
      //  studenSearchtList.clear();

        searchContactList=new ArrayList<>();
        // courseSearchKey.clear();
        String s1 = s.toString().toLowerCase();

        s1 = s1.replace(" ", "");

        Log.d("ssfsf",contactsList+"");

        for (int i = 0; i < contactsList.size(); i++) {

            String firstName = contactsList.get(i).get("ContactName").toLowerCase().replace(" ", "");
          //  String lastName = contactsList.get(i).get(Constant.LAST_NAME).toLowerCase().replace(" ", "");

          //  String fullName = firstName + "" + lastName.replace(" ", "");

            if (firstName.contains(s1)) {
                searchContactList.add(contactsList.get(i));
                // courseSearchKey.add(courseKey.get(i));

            }

        }
        contactAdaptor.addData(searchContactList);
        contactAdaptor.notifyDataSetChanged();


    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.invite_tv:

                List<String> checkList = new ArrayList<>();
                List<HashMap<String, String>> contactList = ((ContactInviteAdaptor) contactAdaptor) .getSelectedContactList();

                for (int i = 0; i < contactList.size(); i++) {
                    if (contactList.get(i).get("boolean").equals(Constant.TRUE_NAME)) {


                        checkList.add(contactList.get(i).get("ContactNumber"));

                    }

                }

                if (checkList.isEmpty()) {
                    ShowDialog.showSnackbarError(view, "Please select Contacts from list", getContext());
                }

                else
                {

                    Log.d("sds",checkList+"");
                    String sms = "Follow the link: https://play.google.com/store/apps/details?id=com.brst.classmates";
                    for(int i=0;i<checkList.size();i++)
                    {

                        try {
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(checkList.get(i), null, sms, null, null);
                            Toast.makeText(getContext(), "SMS Sent to: " + checkList.get(i),
                                    Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(getContext(),
                                    "SMS faild, please try again later!",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }

                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                    break;
                }
        }
    }


    class LoadContactsAyscn extends AsyncTask<Void, Void, ArrayList<HashMap<String,String>>> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pd = ProgressDialog.show(getContext(), "Loading Contacts",
                    "Please Wait");
        }

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(Void... params) {
            // TODO Auto-generated method stub
            contactsList = new ArrayList<>();

            Cursor c = getContext().getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    null, null, null);
            while (c.moveToNext()) {

                String profile="";
                HashMap<String,String> hashMap=new HashMap<>();

                String contactName = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                String phNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                 profile = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
               // contacts.add(contactName + ":" + phNumber+":");
                phNumber = phNumber.replace(" ", "");

                hashMap.put("ContactName",contactName);
                hashMap.put("ContactNumber",phNumber);
                hashMap.put("boolean","false");

                if(profile!=null && !profile.isEmpty()) {
                    hashMap.put("ContactProfile", profile);
                }
                Boolean exist=true;

                for(int i=0;i<contactsList.size();i++)
                {
                    if(contactsList.get(i).get("ContactNumber").equals(phNumber))
                    {
                        exist=false;
                    }
                }

                if(exist==true) {
                    contactsList.add(hashMap);
                }




            }
            c.close();

            Log.d("xczc",contactsList+"");
            return contactsList;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> contacts) {
            // TODO Auto-generated method stub
            super.onPostExecute(contacts);

            pd.cancel();



            ArrayList<HashMap<String,String>> contactList=new ArrayList<>();

            contactList=sortList(contacts);

            Log.d("XX", contacts + "");

            contactAdaptor = new ContactInviteAdaptor(contactList, getContext());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            contact_rv.setLayoutManager(mLayoutManager);
            contact_rv.setAdapter(contactAdaptor);
            contact_rv.setEmptyView(empty_tv);
          //  contact_rv.setEmptyView(emptyview);

        }


    }



    public ArrayList<HashMap<String, String>> sortList(final ArrayList<HashMap<String, String>> contactList) {

        Collections.sort(contactList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                firstValue = s1.get("ContactName").toString();
                secondValue = s2.get("ContactName").toString();

                return firstValue.compareToIgnoreCase(secondValue);
            }
        });

        return contactList;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_iv.setImageResource(R.mipmap.add);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);
        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        ((WelcomeActivity) getActivity()).header_tv.setText("Contacts");
    }


}

