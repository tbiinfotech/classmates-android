package com.brst.classmates.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.CropImageActivity;
import com.brst.classmates.activity.LoginActivity;
import com.brst.classmates.activity.SignupActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.beanclass.UserProfile;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.classses.ShowDialog.showSnackbarError;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener {


    EditText email_user_et, activity_user_et, year_et;

    TextView update_tv, name_tv, logdone_tv, logCancel_tv, log_tv, resetPassword_tv;

    ImageView updateProfile_iv;

    String country_list[] = {"United States", "India", "Canada", "England", "Singapore", "Brazil",
            "South Korea", "Spain", "Italy", "Pakistan", "Australia", "United Kingdom",
            "Germany", "Malaysia", "France", "Poland", "Indonesia",};

    String emailId, countryName, universityName, collegeName, degreeName, year, activities, firstName;
    String fullname, email, country, university, college, degree, yearName, activity, profile, userKey, keyCourse;

    View view = null;

    FirebaseStorage firebaseStorage;
    StorageReference storageReference;

    AutoCompleteTextView country_et, degree_et, university_et, college_et;

    Dialog profileDialog;

    String picture_Path;
    File destination_path;


    Uri imageuri, imageUploadUri, downloadImageUri;

    Fragment fragment;

    List universityList = new ArrayList();


    List collegeList = new ArrayList();
    List degreeList = new ArrayList();
    List specialList = new ArrayList();


    List degreeNew = new ArrayList();
    List specialNew = new ArrayList();
    List<String> collegeKeyList = new ArrayList();
    List<String> degreeKeyList = new ArrayList();


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, universityReference, collegeReference, degreeReference, specialReference;
    ;

    public ProfileFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_profile, container, false);

        }
        fragment = getFragmentManager().findFragmentByTag("Profile_Fragment");

        bindViews(view);

        firebaseReference();

        getDataFromShared();

        setProfile();

        setListener();

        getCollegeList();

        getDegreeList();

        getSpecialList();

        return view;
    }

    /*-------------------------------------------get firebase reference-----------------------------------------*/

    private void firebaseReference() {

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();

        firebaseDatabase = FirebaseDatabase.getInstance();
        collegeReference = firebaseDatabase.getReference(Constant.COLLEGE);

        degreeReference = firebaseDatabase.getReference(Constant.DEGREE_NAME);
        specialReference = firebaseDatabase.getReference("specialization");


    }

   /*----------------------------------------get profile from shared preference---------------------------------*/

    private void getDataFromShared() {

        userKey = StoreData.getUserKeyFromSharedPre(getContext());
        fullname = StoreData.getUserFullNameFromSharedPre(getContext());
        email = StoreData.getUserEmailFromSharedPre(getContext());
        country = StoreData.getCountryFromSharedPre(getContext());
        university = StoreData.getUniversityFromSharedPre(getContext());
        college = StoreData.getCollegeFromSharedPre(getContext());
        degree = StoreData.getDegreeFromSharedPre(getContext());
        yearName = StoreData.getyearFromSharedPre(getContext());
        //  activity = StoreData.getactivityromSharedPre(getContext());
        profile = StoreData.getProfileFromSharedPre(getContext());
        //  keyCourse = StoreData.getCourseKeyFromSharedPre(getContext());


    }


    /*----------------------------get degrr likst--------------------------*/

    private void getDegreeList() {

        degreeReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    String collegeKey = snapshot.getKey();

                    for (DataSnapshot snapshot1 : snapshot.getChildren()) {

                        HashMap hashMap = new HashMap();
                        hashMap.put(Constant.COLLEGE_NAME, collegeKey);

                        hashMap.put(Constant.DEGREE_NAME, snapshot1.child(Constant.DEGREE_NAME).getValue());

                        degreeKeyList.add(snapshot1.getKey());
                        degreeList.add(hashMap);

                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /*-------------------------------get college list------------------------*/

    private void getSpecialList() {


        specialReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    String degreeKey = snapshot.getKey();


                    for (DataSnapshot snapshot1 : snapshot.getChildren()) {

                        HashMap hashMap = new HashMap();

                        hashMap.put(Constant.DEGREE_NAME, degreeKey);
                        if (snapshot1.child("specialization").exists()) {
                            hashMap.put("specialization", snapshot1.child("specialization").getValue());

                            specialList.add(hashMap);


                        }


                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


/*----------------------get unicersity list from firebase---------------------------*/

    private void getCollegeList() {


        collegeReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    if (snapshot.child(Constant.COLLEGE).exists()) {
                        collegeKeyList.add(snapshot.getKey());
                        collegeList.add(snapshot.child(Constant.COLLEGE).getValue());
                    }


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /*------------------------------set user  profile in fields-----------------------------------*/

    private void setProfile() {

        if (profile.equals(Constant.EMPTY_NAME)) {

            Glide.with(ProfileFragment.this).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(updateProfile_iv);
        } else {

            Glide.with(ProfileFragment.this).load(profile).apply(RequestOptions.circleCropTransform()).into(updateProfile_iv);
        }

        email_user_et.setText(email);
        country_et.setText(country);
        university_et.setText(university);
        college_et.setText(college);
        year_et.setText(yearName);
        degree_et.setText(degree);

        name_tv.setText(fullname);

    }
/*----------------------------------set click Listener---------------------------------*/

    private void setListener() {

        resetPassword_tv.setOnClickListener(this);

        ((WelcomeActivity) getActivity()).header_iv.setOnClickListener(this);

    }


    /*-------------------------------------get layout view ids------------------------------------*/

    private void bindViews(View view) {

        email_user_et = (EditText) view.findViewById(R.id.email_user_et);
        country_et = (AutoCompleteTextView) view.findViewById(R.id.country_et);
        college_et = (AutoCompleteTextView) view.findViewById(R.id.college_et);
        university_et = (AutoCompleteTextView) view.findViewById(R.id.university_et);
        degree_et = (AutoCompleteTextView) view.findViewById(R.id.degree_et);
        year_et = (EditText) view.findViewById(R.id.year_et);
        //    activity_user_et = (EditText) view.findViewById(R.id.activity_user_et);

        update_tv = (TextView) view.findViewById(R.id.update_tv);
        resetPassword_tv = (TextView) view.findViewById(R.id.resetPassword_tv);
        name_tv = (TextView) view.findViewById(R.id.name_tv);
        updateProfile_iv = (ImageView) view.findViewById(R.id.updateProfile_iv);

        email_user_et.setEnabled(false);
        country_et.setEnabled(false);
        college_et.setEnabled(false);
        university_et.setEnabled(false);
        degree_et.setEnabled(false);
        year_et.setEnabled(false);


        update_tv.setVisibility(View.GONE);
        resetPassword_tv.setVisibility(View.GONE);


    }

    String collegeKey, degreeKey, specialKey;

    private void getProfileDataFirebase(boolean b) {

        //   universityName = universityName.replaceAll("[^a-zA-Z0-9]", " ");
        universityName = universityName.replaceAll("\\s+", " ").trim();
        //  degreeName = degreeName.replaceAll("[^a-zA-Z0-9]", " ");
        degreeName = degreeName.replaceAll("\\s+", " ").trim();
        collegeName = collegeName.replaceAll("\\s+", " ").trim();

        collegeReference.orderByChild(Constant.COLLEGE).equalTo(universityName).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.getValue() == null) {

                    collegeKey = collegeReference.push().getKey();
                    HashMap hashMap = new HashMap();
                    hashMap.put(Constant.COLLEGE, universityName);
                    collegeReference.child(collegeKey).updateChildren(hashMap);
                } else {
                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                    while (iterator.hasNext()) {
                        DataSnapshot dataSnapshot1 = iterator.next();

                        collegeKey = dataSnapshot1.getKey();
                    }

                }

                degreeReference.child(collegeKey).orderByChild(Constant.DEGREE_NAME).equalTo(degreeName).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        if (dataSnapshot.getValue() == null) {

                            degreeKey = degreeReference.child(collegeKey).push().getKey();
                            HashMap hashMap = new HashMap();
                            hashMap.put(Constant.DEGREE_NAME, degreeName);
                            degreeReference.child(collegeKey).child(degreeKey).updateChildren(hashMap);
                        } else {
                            Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                            while (iterator.hasNext()) {
                                DataSnapshot dataSnapshot1 = iterator.next();

                                degreeKey = dataSnapshot1.getKey();
                            }
                        }

                        specialReference.child(degreeKey).orderByChild("specialization").equalTo(collegeName).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (dataSnapshot.getValue() == null) {

                                    specialKey = specialReference.child(degreeKey).push().getKey();
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("specialization", collegeName);
                                    specialReference.child(degreeKey).child(specialKey).updateChildren(hashMap);
                                } else {
                                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                                    while (iterator.hasNext()) {
                                        DataSnapshot dataSnapshot1 = iterator.next();
                                        specialKey = dataSnapshot1.getKey();
                                    }
                                }

                                databaseReference = firebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME);

                                databaseReference.child(userKey).child(Constant.COLLEGE_NAME).setValue(universityName);
                                databaseReference.child(userKey).child(Constant.EMAILADD_NAME).setValue(emailId);
                                databaseReference.child(userKey).child(Constant.COUNTRY_NAME).setValue(countryName);
                                databaseReference.child(userKey).child(Constant.UNIVERSITY_NAME).setValue(universityName);
                                databaseReference.child(userKey).child("specialization").setValue(collegeName);
                                databaseReference.child(userKey).child(Constant.DEGREE_NAME).setValue(degreeName);
                                databaseReference.child(userKey).child(Constant.YEAR_NAME).setValue(year);
                                //  databaseReference.child(userKey).child(Constant.ACTIVITY_NAME).setValue(activities);


                                if (downloadImageUri != null && !downloadImageUri.equals(Uri.EMPTY)) {

                                    databaseReference.child(userKey).child(Constant.PROFILE_PICTURE).setValue(downloadImageUri.toString());
                                    SharedPreference.getInstance().storeData(getContext(), Constant.PROFILE_PICTURE, downloadImageUri.toString());

                                }

                                SharedPreference.getInstance().storeData(getContext(), Constant.EMAIL_NAME, email);
                                SharedPreference.getInstance().storeData(getContext(), Constant.UNIVERSITY_NAME, universityName);
                                SharedPreference.getInstance().storeData(getContext(), Constant.COLLEGE_NAME, collegeName);
                                SharedPreference.getInstance().storeData(getContext(), Constant.COUNTRY_NAME, countryName);
                                SharedPreference.getInstance().storeData(getContext(), Constant.DEGREE_NAME, degreeName);
                                SharedPreference.getInstance().storeData(getContext(), Constant.YEAR_NAME, year);
                                SharedPreference.getInstance().storeData(getContext(), "collegeKey", collegeKey);


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

     /*
        universityReference.child(universityName).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.getValue()==null) {

                    HashMap hashMap = new HashMap();
                    hashMap.put(Constant.COURSE_NAME, "");
                    universityReference.child(universityName).push().updateChildren(hashMap);
                }


                        collegeReference.child(degreeName).orderByChild("Branch").equalTo(collegeName).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (dataSnapshot.getValue() == null) {
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("Branch", collegeName);
                                    //    hashMap.put(Constant.COURSE_NAME, collegeItem);
                                      // hashMap.put(Constant.KEY_NAME, userKey);
                                    //  collegeReference.child(universityItem).push().updateChildren(hashMap);
                                    //  courseKey = universityReference.child(universityItem).push().getKey();
                                    collegeReference.child(degreeName).push().updateChildren(hashMap);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                      //  degreeReference.child(collegeName).orderByChild(Constant.DEGREE_NAME).equalTo(degreeName).addListenerForSingleValueEvent(new ValueEventListener() {
                        degreeReference.child(universityName).orderByChild(Constant.DEGREE_NAME).equalTo(degreeName).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (dataSnapshot.getValue()==null) {
                                    HashMap hashMap = new HashMap();
                                    hashMap.put(Constant.DEGREE_NAME, degreeName);

                                    degreeReference.child(universityName).push().updateChildren(hashMap);
                                }

                                    databaseReference = firebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME);

                                    databaseReference.child(userKey).child(Constant.UNIVERSITY_NAME).setValue(universityName);
                                    databaseReference.child(userKey).child(Constant.EMAILADD_NAME).setValue(emailId);
                                    databaseReference.child(userKey).child(Constant.COUNTRY_NAME).setValue(countryName);
                                    databaseReference.child(userKey).child(Constant.UNIVERSITY_NAME).setValue(universityName);
                                    databaseReference.child(userKey).child(Constant.COLLEGE_NAME).setValue(collegeName);
                                    databaseReference.child(userKey).child(Constant.DEGREE_NAME).setValue(degreeName);
                                    databaseReference.child(userKey).child(Constant.YEAR_NAME).setValue(year);
                                    //  databaseReference.child(userKey).child(Constant.ACTIVITY_NAME).setValue(activities);


                                    if (downloadImageUri != null && !downloadImageUri.equals(Uri.EMPTY)) {

                                        databaseReference.child(userKey).child(Constant.PROFILE_PICTURE).setValue(downloadImageUri.toString());
                                        SharedPreference.getInstance().storeData(getContext(), Constant.PROFILE_PICTURE, downloadImageUri.toString());

                                    }


                            }

                  *//*          @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }*//*

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/


        //   SharedPreference.getInstance().storeData(getContext(), Constant.COURSE_KEY, courseKey);
        //   SharedPreference.getInstance().storeData(getContext(), Constant.ACTIVITY_NAME, activities);


        if (b) {

            country_et.setEnabled(false);
            college_et.setEnabled(false);
            university_et.setEnabled(false);
            degree_et.setEnabled(false);
            year_et.setEnabled(false);
            //   activity_user_et.setEnabled(false);
            update_tv.setVisibility(View.GONE);


            degree_et.setSelection(0, 0);


        }

    }


  /*  private void sendVerificationEmail() {


        FirebaseAuth.getInstance().getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {

                    Toast.makeText((WelcomeActivity) getActivity(), "Link send to" + " " + FirebaseAuth.getInstance().getCurrentUser().getEmail() + " " + "," + "please verify your email id.", Toast.LENGTH_LONG).show();

                    SharedPreference.getInstance().clearData(getContext(), Constant.EMAIL_NAME);

                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);

                    ((WelcomeActivity) getActivity()).finish();


                } else {
                    Log.d("email", "email not send");
                }

                ShowDialog.hideDialog();

            }
        });
    }*/

 /*   ;

    public void logoutDialog() {

        profileDialog = new Dialog(WelcomeActivity.context);
        profileDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profileDialog.setContentView(R.layout.dialog_logout);
        profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        profileDialog.show();


        logdone_tv = (TextView) profileDialog.findViewById(R.id.logdone_tv);
        logCancel_tv = (TextView) profileDialog.findViewById(R.id.logCancel_tv);
        log_tv = (TextView) profileDialog.findViewById(R.id.log_tv);

        logdone_tv.setText("Update");

        log_tv.setText("if email will be updated then you will be logout from app for updated email verification and then login back with new email id ");
        logdone_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                profileDialog.dismiss();

                //  updateFirebaseDatabase();


            }
        });
        logCancel_tv.setOnClickListener(this);


    }*/


    @Override
    public void onResume() {
        super.onResume();


        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.VISIBLE);

        ((WelcomeActivity) getActivity()).header_iv.setImageResource(R.mipmap.edit);
        ((WelcomeActivity) getActivity()).header_tv.setText("Profile");
        //  ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setImageResource(R.mipmap.ic_toggle);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);

        // ((WelcomeActivity) getActivity()).logout_iv.setOnClickListener(this);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.header_iv:

                Fragment myFragment = getFragmentManager().findFragmentByTag(Constant.PROFILEFRAGMENT_NAME);

                if (myFragment != null && myFragment.isVisible()) {


                    enabledField();

                }

                break;

            case R.id.updateProfile_iv:


                ShowDialog.cameraDialog(getContext(), galleryClickListener, cameraClickLictener);

                break;

            case R.id.update_tv:


                AddCourseFragment.hideSoftKeyboard(getContext());

                getFieldData(v);


                break;


            case R.id.logCancel_tv:

                profileDialog.dismiss();


                break;

            case R.id.resetPassword_tv:


                //sendForgotEmail(getContext());

                break;

            case R.id.logout_iv:

                if (fragment != null && fragment.isVisible()) {
                    PopupMenu popup = new PopupMenu(getContext(), v);
                    popup.getMenuInflater().inflate(R.menu.popup, popup.getMenu());

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {

                            if (item.getTitle().equals("Change Password")) {
                                //sendForgotEmail(getContext());
                            } else if (item.getTitle().equals("Mute")) {
                                SharedPreference.getInstance().storeData(getContext(), Constant.SOUND, Constant.MUTE);
                            } else {
                                SharedPreference.getInstance().storeData(getContext(), Constant.SOUND, Constant.UNMUTE);
                            }
                            return true;
                        }
                    });

                    popup.show();
                }
                break;
        }

    }

/*----------------------------------enable profile fields-------------------------------------*/

    private void enabledField() {

        country_et.setEnabled(true);
        college_et.setEnabled(true);
        university_et.setEnabled(true);
        degree_et.setEnabled(true);
        year_et.setEnabled(true);
//        activity_user_et.setEnabled(true);

        //  college_et.setEnabled(false);
        //  degree_et.setEnabled(false);

        update_tv.setVisibility(View.VISIBLE);

        country_et.requestFocus();


        ArrayAdapter<String> universityAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, collegeList);


        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, country_list);


        country_et.setAdapter(countryAdapter);
        country_et.setThreshold(1);


        university_et.setAdapter(universityAdapter);
        university_et.setThreshold(1);


        updateProfile_iv.setOnClickListener(this);
        update_tv.setOnClickListener(this);

        if (!university_et.getText().toString().isEmpty()) {


            degreeNew.clear();
            degree_et.setEnabled(true);
            // degree_et.getText().clear();

            for (int i = 0; i < degreeList.size(); i++) {
                HashMap hashMap = (HashMap) degreeList.get(i);

                String collegeName = hashMap.get(Constant.COLLEGE_NAME).toString();
                //  Log.d("data", collegeName + "-----" + collegeKeyList.size());
                // if (collegeKeyList.size()>0) {
                if (i < collegeKeyList.size()) {
                    if (collegeName.equals(collegeKeyList.get(i))) {
                        String college = hashMap.get(Constant.DEGREE_NAME).toString();

                        degreeNew.add(college);
                        // }
                    }
                }


            }

            ArrayAdapter<String> degreeAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_dropdown_item_1line, degreeNew);

            degree_et.setAdapter(degreeAdapter);
            degree_et.setThreshold(1);
        }


        if (!degree_et.getText().toString().isEmpty()) {
            specialNew.clear();

            college_et.setEnabled(true);
            //  college_et.getText().clear();


            for (int i = 0; i < specialList.size(); i++) {
                HashMap hashMap = (HashMap) specialList.get(i);

                String degreeKey = hashMap.get(Constant.DEGREE_NAME).toString();
                if (i < degreeKeyList.size()) {
                    if (degreeKey.equals(degreeKeyList.get(i))) {
                        String specialization = hashMap.get("specialization").toString();

                        specialNew.add(specialization);
                    }
                }

            }

            ArrayAdapter<String> collegeAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_dropdown_item_1line, specialNew);

            college_et.setAdapter(collegeAdapter);
            college_et.setThreshold(1);
        }
        university_et.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                degreeNew.clear();
                degree_et.setEnabled(true);
                degree_et.getText().clear();

                for (int i = 0; i < degreeList.size(); i++) {
                    HashMap hashMap = (HashMap) degreeList.get(i);

                    String collegeName = hashMap.get(Constant.COLLEGE_NAME).toString();

                    if (collegeName.equals(collegeKeyList.get(position))) {
                        String college = hashMap.get(Constant.DEGREE_NAME).toString();

                        degreeNew.add(college);
                    }


                }

                ArrayAdapter<String> degreeAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_dropdown_item_1line, degreeNew);

                degree_et.setAdapter(degreeAdapter);
                degree_et.setThreshold(1);
            }
        });


        degree_et.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                specialNew.clear();

                college_et.setEnabled(true);
                college_et.getText().clear();


                for (int i = 0; i < specialList.size(); i++) {
                    HashMap hashMap = (HashMap) specialList.get(i);

                    String degreeKey = hashMap.get(Constant.DEGREE_NAME).toString();

                    if (degreeKey.equals(degreeKeyList.get(position))) {
                        String specialization = hashMap.get("specialization").toString();

                        specialNew.add(specialization);
                    }

                }


                ArrayAdapter<String> collegeAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_dropdown_item_1line, specialNew);

                college_et.setAdapter(collegeAdapter);
                college_et.setThreshold(1);


            }
        });

    }

    /*--------------------------------------------get the data of fields---------------------------------*/

    private void getFieldData(View v) {

        emailId = email_user_et.getText().toString();
        universityName = university_et.getText().toString();
        collegeName = college_et.getText().toString();
        countryName = country_et.getText().toString();
        degreeName = degree_et.getText().toString();
        year = year_et.getText().toString();
        firstName = name_tv.getText().toString();
        //activities = activity_user_et.getText().toString();

        if (countryName.isEmpty()) {
            showSnackbarError(v, getString(R.string.entercountry_name), getContext());
        } else if (universityName.isEmpty()) {
            showSnackbarError(v, getString(R.string.enteruniversity_name), getContext());
        } else if (collegeName.isEmpty()) {
            showSnackbarError(v, getString(R.string.entercollege_name), getContext());
        } else if (degreeName.isEmpty()) {
            showSnackbarError(v, getString(R.string.enterdegree_name), getContext());
        } else if (year.isEmpty()) {
            showSnackbarError(v, getString(R.string.enteryear_name), getContext());
        } /*else if (activities.isEmpty()) {
            showSnackbarError(v, getString(R.string.enteractivity_name), getContext());

        }*/ else {
            if (!NetworkCheck.isNetworkAvailable(getContext())) {
                ShowDialog.networkDialog(getContext());
            } else {
                if (imageUploadUri != null && !imageUploadUri.equals(Uri.EMPTY)) {

                    imageUpload();

                } else {

                    getProfileDataFirebase(true);

                }
            }
        }

   /*  else  if (imageUploadUri != null && !imageUploadUri.equals(Uri.EMPTY)) {

            imageUpload();

        } else {

            getProfileDataFirebase(true);

        }*/

    }

    private void updateFirebaseDatabase() {

        ShowDialog.showDialog(getContext());

        FirebaseAuth.getInstance().fetchProvidersForEmail(emailId).addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<ProviderQueryResult> task) {

                if (task.isSuccessful()) {

                    if (task.getResult().getProviders().isEmpty()) {

                        //  updateEmail();

                    } else {

                        Toast.makeText(getContext(), "This email already registered", Toast.LENGTH_LONG).show();

                        ShowDialog.hideDialog();
                    }

                } else {


                    Toast.makeText(getContext(), "Server side problem", Toast.LENGTH_SHORT).show();

                    ShowDialog.hideDialog();
                }
            }
        });


    }

    private void updateEmail() {


        FirebaseAuth.getInstance().getCurrentUser().updateEmail(emailId).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {

                  /*  snapshot.getRef().child(Constant.EMAILADD_NAME).setValue(emailId);
                    snapshot.getRef().child(Constant.COUNTRY_NAME).setValue(countryName);
                    snapshot.getRef().child(Constant.UNIVERSITY_NAME).setValue(universityName);
                    snapshot.getRef().child(Constant.COLLEGE_NAME).setValue(collegeName);
                    snapshot.getRef().child(Constant.DEGREE_NAME).setValue(degreeName);
                    snapshot.getRef().child(Constant.YEAR_NAME).setValue(year);
                    snapshot.getRef().child(Constant.ACTIVITY_NAME).setValue(activities);
                    if (downloadImageUri != null && !downloadImageUri.equals(Uri.EMPTY)) {


                        snapshot.getRef().child(Constant.PROFILE_PICTURE).setValue(downloadImageUri.toString());
                    }*/

                    //   getProfileData(false);

                    // sendVerificationEmail();


                } else {
                    Log.d("sds", "email not updated" + task.getException().getMessage());
                    //  Toast.makeText(getContext(),"email not updated",Toast.LENGTH_LONG).show();

                    if (task.getException().getMessage().equals("This operation is sensitive and requires recent authentication. Log in again before retrying this request.")) {
                        Toast.makeText(getContext(), "Pleasee login again to update email.", Toast.LENGTH_LONG).show();
                    }
                    ShowDialog.hideDialog();
                }
            }
        });
    }

    View.OnClickListener galleryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();

            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

            requestAppPermissions(permission, R.string.permission, 54);


        }
    };


    View.OnClickListener cameraClickLictener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            requestAppPermissions(permission, R.string.permission, 55);


        }
    };

    private void capture_image() {
        picture_Path = "";
        destination_path = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = "image";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picture_Path = String.valueOf(new File(f, System.currentTimeMillis() + ".jpg"));
        destination_path = new File(picture_Path);
        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination_path);
        } else {
            imageuri = Uri.fromFile(destination_path);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, Constant.CAMERA_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.GALLERY_CODE && resultCode == RESULT_OK) {


            Uri image = data.getData();

            Intent intent = new Intent(getContext(), CropImageActivity.class);
            intent.putExtra("imageuri", image.toString());
            startActivityForResult(intent, Constant.CROP_IMAGE);


        } else if (requestCode == Constant.CAMERA_CODE && resultCode == RESULT_OK) {

            Intent intent = new Intent(getContext(), CropImageActivity.class);
            intent.putExtra("imageuri", imageuri.toString());
            startActivityForResult(intent, Constant.CROP_IMAGE);

        } else if (requestCode == Constant.CROP_IMAGE && resultCode == RESULT_OK) {

            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getActivity().openFileInput("myImage"));
                imageUploadUri = getImageUri(getContext(), bitmap);

                setProfileImage();

            } catch (FileNotFoundException e) {
                e.printStackTrace();

            }

        }


    }

    /*--------------------------get the uri from bitmap----------------------------*/

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, Constant.TITLE_NAME, null);
        return Uri.parse(path);
    }

/*-------------------------------------set image into imageciew------------------------------*/

    private void setProfileImage() {

        Glide.with(ProfileFragment.this).load(imageUploadUri).apply(RequestOptions.circleCropTransform()).into(updateProfile_iv);
    }


/*----------------------------------uplosd image to firebase------------------------------------------*/

    private void imageUpload() {

        ShowDialog.showDialog(getContext());

        storageReference.child(Constant.IMAGE_NAME + "_" + System.currentTimeMillis())
                .putFile(imageUploadUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                        ShowDialog.hideDialog();

                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();


                        ShowDialog.hideDialog();


                        getProfileDataFirebase(true);


                    }
                });

    }


    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 54) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, Constant.GALLERY_CODE);
        }

        if (requestCode == 55) {
            capture_image();
        }

    }


}
