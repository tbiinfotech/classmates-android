package com.brst.classmates.fragments;


import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.*;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.ALARM_SERVICE;
import static com.brst.classmates.classses.ShowDialog.networkDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddAssignmentFragment extends Fragment implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    CheckBox chk_cb;

    Assign_ExamFragment assign_examFragment;

    TextView addAssign_tv;

    public static TextView setdate_tv;

    ImageView calender_iv;

    View view = null;

    DatabaseReference assignmentreference;

    EditText assignment_et, assignmenttype_et;
    TextView course_et;

    Spinner status_sp, assignmenttype_sp;

    Fragment fragment;

    LinearLayout calender_ll;

    Bundle bundle;

    String reminderVal = "";

    String university, courseKey = "", assignment, type, dateformat, status, publish = "", loginUerKey, assignmentKey, courseName, userFullName, userKeyInvited = "";

    ArrayList<String> keyList;

    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    static AddAssignmentFragment instance = new AddAssignmentFragment();


    public AddAssignmentFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_addassignment, container, false);
        }

        loginUerKey = StoreData.getUserKeyFromSharedPre(getContext());
        userFullName = StoreData.getUserFullNameFromSharedPre(getContext());

        bindViews(view);

        bundle = getArguments();
        if (bundle != null) {

            courseKey = bundle.getString(Constant.COURSE_KEY);
            assignment = bundle.getString(Constant.ASSIGNMENT_NAME);
            type = bundle.getString(Constant.ASSIGNTYPE_NAME);
            status = bundle.getString(Constant.STATUS_NAME);
            assignmentKey = bundle.getString(Constant.KEY_NAME);
            dateformat = bundle.getString(Constant.ASSIGNDATE_NAME);
            courseName = bundle.getString(Constant.COURSE_NAME);
            userKeyInvited = bundle.getString(Constant.STUDENT_LIST);
            Log.d("zxc", userKeyInvited);
            assignment_et.setText(assignment);
            setdate_tv.setText(dateformat);
            course_et.setText(courseName);

            assignmenttype_sp.setSelection(getIndex(assignmenttype_sp, type));

            reminderVal = SharedPreference.getInstance().getData(getContext(), assignmentKey);

            status_sp.setSelection(getIndex(status_sp, status));

        }

        setListener();

        initFragmant();

        return view;
    }

    private int getIndex(Spinner assignmenttype_sp, String type) {

        int index = 0;

        for (int i = 0; i < assignmenttype_sp.getCount(); i++) {

            if (assignmenttype_sp.getItemAtPosition(i).equals(type)) {
                index = i;
            }

        }

        return index;
    }

    public static AddAssignmentFragment getInstance() {
        return instance;
    }

    /*-------------------------------get fragment-----------------------*/

    private void initFragmant() {

        assign_examFragment = new Assign_ExamFragment();
    }



    /*---------------------get layout id-----------------------------------------*/

    private void bindViews(View view) {

        fragment = getFragmentManager().findFragmentByTag("AddAssignment_Fragment");

        university = StoreData.getUniversityFromSharedPre(getContext());

        chk_cb = (CheckBox) view.findViewById(R.id.chk_cb);

        addAssign_tv = (TextView) view.findViewById(R.id.addAssign_tv);
        setdate_tv = (TextView) view.findViewById(R.id.setdate_tv);

        calender_iv = (ImageView) view.findViewById(R.id.calender_iv);
        status_sp = (Spinner) view.findViewById(R.id.status_sp);
        assignmenttype_sp = (Spinner) view.findViewById(R.id.assignmenttype_sp);
        calender_ll = (LinearLayout) view.findViewById(R.id.calender_ll);

        assignment_et = (EditText) view.findViewById(R.id.assignment_et);

        course_et = (TextView) view.findViewById(R.id.course_et);


    }


    /*-------------------------set click listener---------------------------------*/

    private void setListener() {
        addAssign_tv.setOnClickListener(this);
        chk_cb.setOnClickListener(this);
        calender_ll.setOnClickListener(this);
        course_et.setOnClickListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setText("Add Assignment");
        //  ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        Application.getInstance().setConnectivityListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.addAssign_tv:

                getFieldsData(v);


                break;

            case R.id.chk_cb:

                chk_cb.setChecked(false);

                actionSheet();

                break;

            case R.id.calender_ll:

                showDateDialog();

                break;

            case R.id.course_et:

                CourseFragment courseFragment = new CourseFragment();
                courseFragment.setTargetFragment(AddAssignmentFragment.this, 1200);
                ReplaceFragment.replace((WelcomeActivity) getActivity(), courseFragment, "courseAssign_Fragment");

                break;
        }

    }

    /*-----------------------check field empty or not-----------------------------*/
    private void getFieldsData(View v) {

        courseName = course_et.getText().toString();
        assignment = assignment_et.getText().toString();
        type = assignmenttype_sp.getSelectedItem().toString();
        dateformat = setdate_tv.getText().toString();
        status = status_sp.getSelectedItem().toString();
        if (course_et.getText().toString().isEmpty()) {
            ShowDialog.showSnackbarError(v, getString(R.string.courseAssign_name), getContext());
        } else if (assignment.isEmpty()) {
            ShowDialog.showSnackbarError(v, getString(R.string.assignment_name), getContext());
        } else if (dateformat.isEmpty()) {
            ShowDialog.showSnackbarError(v, getString(R.string.duedate_name), getContext());
        } else {

            checkNetwork();

        }
    }

    private void checkNetwork() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {

            createAssignment();
        } else {
            ShowDialog.networkDialog(getContext());
        }
    }

    /*------------------------------get course key from unicersity list--------------------*/

    private void createAssignment() {

        String keyAlarm = "";

        assignmentreference = FirebaseDatabase.getInstance().getReference("AssignmentTask").child(courseKey);

        if (bundle != null) {
            assignmentreference = assignmentreference.child(assignmentKey);

        } else {

            assignmentreference = assignmentreference.push();
            keyAlarm = assignmentreference.getKey();

        }

        HashMap hashMap = new HashMap();
        hashMap.put("assignment_name", assignment);
        hashMap.put("assignment_type", type);
        hashMap.put("courseName", courseName);
        hashMap.put("due_date", dateformat);
        hashMap.put("owner_name", StoreData.getUserFullNameFromSharedPre(getContext()));
        hashMap.put("owner_profile", StoreData.getProfileFromSharedPre(getContext()));
        hashMap.put("assignment_admin", StoreData.getUserKeyFromSharedPre(getContext()));

        assignmentreference.updateChildren(hashMap);

        assignmentreference = assignmentreference.child(Constant.STUDENT_LIST);

        HashMap hashMap1 = new HashMap();
        hashMap1.put(Constant.STATUS_NAME, status);
        hashMap1.put(Constant.ACCEPT_NAME, Constant.ACCEPTED_NAME);

        assignmentreference.child(loginUerKey).updateChildren(hashMap1);

        if (publish.equals("")) {

            course_et.setText("");
            assignment_et.getText().clear();
            setdate_tv.setText("");
            if (bundle != null) {

                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                        .setTitle("Alert")
                        .setMessage("Update done.")
                        .setCancelable(false)
                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                            }
                        })
                        .setIcon(R.mipmap.imagesuccess)
                        .show();

                assignmentreference.child(loginUerKey).child(Constant.STATUS_NAME).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Log.d("zcx", dataSnapshot.getValue() + "");

                        String status = dataSnapshot.getValue().toString();
                        if (status.equals("Submitted")) {
                            if (!reminderVal.equals("null")) {
                                Intent _myIntent = new Intent(getContext(), AlarmReceiver.class);
                                PendingIntent _myPendingIntent = PendingIntent.getBroadcast(getContext(), Integer.parseInt(reminderVal), _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                                AlarmManager _myAlarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

                                _myAlarmManager.cancel(_myPendingIntent);


                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                        .setTitle("Alert")
                        .setMessage("Assignment created successfully.")
                        .setCancelable(false)
                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                            }
                        })
                        .setIcon(R.mipmap.imagesuccess)
                        .show();
            }


        }


        if (publish.equals(Constant.INDIVI_NAME)) {

            getUserkeyUnderCourse(false);

        } else if (publish.equals(Constant.COURSE_NAME)) {

            getUserkeyUnderCourse(true);

        }


        String dateTime[] = dateformat.split(" |\\,|\\@|\\:");

        String date = dateTime[0];
        String month = dateTime[1];
        String year = dateTime[3];
        String hour = dateTime[4];
        String minute = dateTime[5];


        int monthInt = 0;
        Calendar cal = Calendar.getInstance();

        try {
            cal.setTime(new SimpleDateFormat("MMM").parse(month));
            monthInt = cal.get(Calendar.MONTH);


        } catch (ParseException e) {
            e.printStackTrace();

        }
        if (bundle == null) {
            if (checkDrawOverlayPermission()) {
                Calendar cale = Calendar.getInstance();
                cale.set(Calendar.MONTH, monthInt);
                cale.set(Calendar.YEAR, Integer.parseInt(year));
                cale.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date));
                cale.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour) + 2);
                cale.set(Calendar.MINUTE, Integer.parseInt(minute));
                cale.set(Calendar.SECOND, 0);
                int valcal = (int) System.currentTimeMillis();

                SharedPreference.getInstance().storeData(getContext(), keyAlarm, String.valueOf(valcal));

                Bundle bundle = new Bundle();
                bundle.putString(Constant.CHECKED_NAME, String.valueOf(valcal));
                bundle.putString(Constant.ASSIGN_NAME, assignment);
                bundle.putString(Constant.ASSIGNDATE_NAME, dateformat);
                bundle.putString("Notification", "notification");
                Intent _myIntent = new Intent(getContext(), AlarmReceiver.class);
                _myIntent.putExtra(Constant.BUNDLE_NAME, bundle);

                PendingIntent _myPendingIntent = PendingIntent.getBroadcast(getContext(), valcal, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                AlarmManager _myAlarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

                _myAlarmManager.set(AlarmManager.RTC_WAKEUP, cale.getTimeInMillis(), _myPendingIntent);

            }
        }
    }

    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(getContext())) {

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getContext().getPackageName()));
            startActivityForResult(intent, 500);
            return false;
        } else {
            return true;
        }
    }

    /*---------------------------add assignment to user----------------------------*/

    private AssignmentFirebase setDataAsssignmentClass() {

        AssignmentFirebase assignmentFirebase = new AssignmentFirebase();
        assignmentFirebase.setAssignment_name(assignment);
        assignmentFirebase.setAssignment_type(type);
        assignmentFirebase.setDue_date(dateformat);
        assignmentFirebase.setCourseName(courseName);

        assignmentFirebase.setAssignment_admin(StoreData.getUserKeyFromSharedPre(getContext()));
        assignmentFirebase.setOwner_name(StoreData.getUserFullNameFromSharedPre(getContext()));
        assignmentFirebase.setOwner_profile(StoreData.getProfileFromSharedPre(getContext()));


        return assignmentFirebase;

    }

    /*--------------------get user key ---------------------------*/

    private void getUserkeyUnderCourse(boolean b) {

        if (!b) {

            for (int i = 0; i < keyList.size(); i++) {

                HashMap hashMap = new HashMap();

                hashMap.put(Constant.STATUS_NAME, "In Progress");

                if (keyList.get(i).equals(loginUerKey)) {
                    hashMap.put(Constant.ACCEPT_NAME, Constant.ACCEPTED_NAME);
                } else {
                    hashMap.put(Constant.ACCEPT_NAME, Constant.FALSE_NAME);
                }

                assignmentreference.child(keyList.get(i)).updateChildren(hashMap);

            }


            if (fragment != null && fragment.isVisible()) {

                assignment_et.getText().clear();
                course_et.setText("");
                setdate_tv.setText("");
                chk_cb.setChecked(false);

                if (bundle != null) {
                    AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                            .setTitle("Alert")
                            .setMessage("Update done.")
                            .setCancelable(false)
                            .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                                }
                            })
                            .setIcon(R.mipmap.imagesuccess)
                            .show();

                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                            .setTitle("Alert")
                            .setMessage("Assignment created successfully.")
                            .setCancelable(false)
                            .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                                }
                            })
                            .setIcon(R.mipmap.imagesuccess)
                            .show();


                }
            }

            String message = "{\n" +
                    "  \"SenderName\": \"" + userFullName + "\",\n" +
                    "  \"Message\": \"" + "Assignment:" + " " + assignment + "\"\n" +
                    "}";


            HashMap map = new HashMap();
            map.put("title", userFullName);
            map.put("body", message);


            getToken(map);


        } else {

            FirebaseDatabase.getInstance().getReference(Constant.COURSE_NAME).orderByChild(courseKey).equalTo(Constant.COURSE_NAME).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    keyList = new ArrayList<String>();

                    List<String> userKeyInvitedList = new ArrayList<String>(Arrays.asList(userKeyInvited.split(",")));

                    if (dataSnapshot.getValue() != null) {

                        Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                        Iterator<DataSnapshot> iterator = iterable.iterator();
                        while (iterator.hasNext()) {
                            DataSnapshot snapshot = iterator.next();
                            String userkeycourse = snapshot.getKey();
                            if (!userKeyInvitedList.contains(userkeycourse)) {
                                HashMap hashMap = new HashMap();
                                hashMap.put(Constant.STATUS_NAME, "In Progress");
                                if (!userkeycourse.equals(loginUerKey)) {

                                    keyList.add(userkeycourse);

                                    hashMap.put(Constant.ACCEPT_NAME, Constant.FALSE_NAME);
                                    assignmentreference.child(userkeycourse).updateChildren(hashMap);
                                }

                            }
                        }

                        if (fragment != null && fragment.isVisible()) {

                            assignment_et.getText().clear();

                            setdate_tv.setText("");
                            chk_cb.setChecked(false);
                            course_et.setText("");

                            if (bundle != null)

                            {

                                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                        .setTitle("Alert")
                                        .setMessage("Update done.")
                                        .setCancelable(false)
                                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();

                                                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                                            }
                                        })
                                        .setIcon(R.mipmap.imagesuccess)
                                        .show();
                            } else {

                                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                        .setTitle("Alert")
                                        .setMessage("Assignment created successfully.")
                                        .setCancelable(false)
                                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();

                                                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                                            }
                                        })
                                        .setIcon(R.mipmap.imagesuccess)
                                        .show();
                            }

                        }


                        String message = "{\n" +
                                "  \"SenderName\": \"" + userFullName + "\",\n" +
                                "  \"Message\": \"" + "Assignment:" + " " + assignment + "\"\n" +
                                "}";


                        HashMap map = new HashMap();
                        map.put("title", userFullName);
                        map.put("body", message);


                        getToken(map);


                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
    }


    private void getToken(final HashMap map) {

        FirebaseDatabase.getInstance().getReference("registered_user").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ;

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    if (keyList.contains(snapshot.getKey())) {

                        String notificetion = snapshot.child(Constant.NOTIFICATION_KEY).getValue().toString();

                        if (notificetion.equals("true")) {
                            String key = snapshot.getKey();
                            Object token = snapshot.child("token").getValue();

                            sendNotification((String) token, map);
                        }
                    }

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void sendNotification(String fcmID, HashMap<String, String> map) {


        String body123 = "{\n\t\"to\": \" " + fcmID + "\",\n\t\"notification\" :" + convert(map) + ",\n\t\"data\" :" + convert1(map) + "\n}";

        Log.e("DataToSend", "" + body123);

        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");


        RequestBody body = RequestBody.create(mediaType, body123);
        Request request = new Request.Builder()
                .url("https://fcm.googleapis.com/fcm/send")
                .post(body)
                .addHeader("authorization", "key=" + Constant.SERVER_KEY)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")

                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                } else {
                    Log.e("ResponseBALLI", response.body().string());
                    Log.d("sa", "sa");
                    Log.i("MESSAGE", response.message());
                }
            }


        });


    }

    public String convert(HashMap<String, String> map) {
        JSONObject obj = null;
        try {


            obj = new JSONObject();
            String message = map.get("body");

            JSONObject meJson = new JSONObject(message);

            obj.put("body", meJson.optString("Message"));
            obj.put("title", userFullName);
            obj.put("icon", R.mipmap.logo);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return obj.toString();
    }


    public String convert1(HashMap<String, String> map) {
        JSONObject obj = null;
        try {

            obj = new JSONObject(map);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj.toString();
    }

    /*---------------------------show dateDialog to select date----------------------*/

    private void showDateDialog() {

        Bundle bundle = new Bundle();
        bundle.putString("date", "date");

        DialogFragment dialogFragment = new SelectDateFragment();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(getFragmentManager(), "DatePicker");

    }


    public void populateSetDate(int year, int month, int dayOfMonth, int hour, int minute) {

        String mon = MONTHS[month];

        String hours = String.format("%02d", hour);
        String minutes = String.format("%02d", minute);

        setdate_tv.setText(dayOfMonth + " " + mon + ", " + year + "@" + hours + ":" + minutes);
    }

    /*-----------------------------show option on click publish to user-----------------------------*/
    private void actionSheet() {

        final String[] stringItems = {"Subject", "Individual"};

        final ActionSheetDialog dialog = new ActionSheetDialog(getContext(), stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View v, int position, long id) {

                if (position == 0) {

                    chk_cb.setChecked(true);

                    publish = Constant.COURSE_NAME;

                } else if (position == 1) {

                    if (courseKey.equals("") && courseKey.isEmpty()) {
                        ShowDialog.showSnackbarError(view, getString(R.string.courseAssign_name), getContext());
                    } else {
                        chk_cb.setChecked(true);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constant.COURSE_KEY, courseKey);

                        if (userKeyInvited != null && !userKeyInvited.isEmpty()) {
                            bundle.putString(Constant.STUDENT_LIST, userKeyInvited);
                        }
                        CourseUserFragment courseUserFragment = new CourseUserFragment();
                        courseUserFragment.setArguments(bundle);
                        courseUserFragment.setTargetFragment(AddAssignmentFragment.this, 1000);
                        ReplaceFragment.replace((WelcomeActivity) getActivity(), courseUserFragment, "courseAssign_Fragment");

                    }
                }
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1000) {

                publish = Constant.INDIVI_NAME;
                keyList = new ArrayList<>();
                keyList = (ArrayList<String>) data.getSerializableExtra(Constant.CHECK_LIST);


            } else if (requestCode == 1200) {
                Bundle bundle = data.getBundleExtra(Constant.BUNDLE_NAME);
                String courseName = bundle.getString(Constant.COURSE_NAME);
                courseKey = bundle.getString(Constant.COURSE_KEY);

                course_et.setText(courseName);
            }

        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


        if (!isConnected) {
            try {
                ShowDialog.networkDialog(getContext());
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }
            } catch (Exception e) {

            }
        }
    }



   /* private class AsyncCaller extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            boolean networkAccess = isInternetAccessible(getContext());
            return networkAccess;
        }

        @Override
        protected void onPostExecute(Boolean networkAccess) {
            super.onPostExecute(networkAccess);

            Log.d("xcx", "zc" + "---" + networkAccess);
            if (!networkAccess) {
                Log.d("xcx", "zc");
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        Log.d("xcx", "zxzzc");
                        networkDialog(getContext());
                        ShowDialog.hideDialog();
                    }
                }
            }
        }
    }*/
}
