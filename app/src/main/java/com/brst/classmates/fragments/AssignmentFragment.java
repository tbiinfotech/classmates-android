package com.brst.classmates.fragments;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.AssignmentAdaptor;
import com.brst.classmates.classses.AssignmentFirebase;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


public class AssignmentFragment extends Fragment {

    RecyclerViewEmptySupport assignment_rv;

    // String assignment_list[] = {"Anthropology", "Agriculture & Forestry", "Accounting & Finance", "Architecture", "Chemistry"};
    AssignmentDetail assignmentDetail = new AssignmentDetail();
    CardView card_cv;
    View view;
    String courseKey;
    List<String> invitedUserKey = new ArrayList<>();
    TextView empty_tv, course_tv;

    int submit = 0, inProgress = 0;

    List<HashMap<String, String>> assignmentList;
    List<HashMap<String, String>> assignment_list = new ArrayList<>();

    DatabaseReference assignmentReference;

    AssignmentAdaptor assignmentAdaptor;

    public AssignmentFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_assignment, container, false);
        }


        Bundle bundle = getParentFragment().getArguments();
        if (bundle != null) {
            // courseKey = bundle.getString(Constant.COURSE_KEY);

        }
        assignmentReference = FirebaseDatabase.getInstance().getReference("AssignmentTask");

        bindViews(view);

        setAdaptor();

        checkNetwork();

        return view;
    }

    private void checkNetwork() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {

            getAssignments();

        } else {

          //  ShowDialog.networkDialog(getContext());

            getAssignments();

        }
    }

    public Boolean isOnline() {
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            if(reachable){
                System.out.println("Internet access");
                return reachable;
            }
            else{
                System.out.println("No Internet access");
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
        return false;
    }

    private void getAssignments() {

        // ShowDialog.showDialog(getContext());

        final String userLogin = StoreData.getUserKeyFromSharedPre(getContext());

        assignmentReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                assignmentList = new ArrayList<HashMap<String, String>>();
                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = iterable.iterator();

                while (iterator.hasNext()) {
                    DataSnapshot snapshot = iterator.next();

                    String courseKey = snapshot.getKey();

                    // Log.d("dfsd",assignmentKey);

                    Iterable<DataSnapshot> iterable1 = snapshot.getChildren();
                    Iterator<DataSnapshot> iterator1 = iterable1.iterator();
                    String assignmentName = "", assignmentType = "", assignmentStatus = "", assignmentAccept = "", assignmentDate = "", assignmentAdmin = "", assignmentStudent = "", courseName = "";
                    while (iterator1.hasNext()) {
                        DataSnapshot snapshot1 = iterator1.next();
                        String assignmentKey = snapshot1.getKey();

                        if (snapshot1.child(Constant.ASSIGNDATE_NAME).exists()) {

                            assignmentDate = snapshot1.child(Constant.ASSIGNDATE_NAME).getValue().toString();

                            String[] due_date = assignmentDate.split(" |\\,|\\@|\\:");
                            String date = due_date[0];
                            String month = due_date[1];
                            String year = due_date[3];
                            String hour = due_date[4];
                            String minute = due_date[5];
                            Date dateFormat = null;
                            try {
                                dateFormat = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(dateFormat);
                            int monthVal = cal.get(Calendar.MONTH);

                            Calendar calendar1 = Calendar.getInstance();
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(Calendar.DATE, Integer.parseInt(date));
                            calendar.set(Calendar.MONTH, monthVal);
                            calendar.set(Calendar.YEAR, Integer.parseInt(year));
                            calendar.set(Calendar.HOUR, Integer.parseInt(hour));
                            calendar.set(Calendar.MINUTE, Integer.parseInt(minute));

                            //   if (calendar.getTimeInMillis() calendar1.getTimeInMillis()) {

                            Log.d("dsdsf",calendar.getTime()+"-----"+calendar1.getTime());

                            Log.d("sfd",calendar.getTime().before(calendar1.getTime())+"");
                            if (calendar.getTime().before(calendar1.getTime())) {
                                assignmentReference.child(courseKey).child(assignmentKey).removeValue();

                                Log.d("zxcxz", "xzc" + "----" + assignmentReference);


                            }

                            //    if (calendar.getTimeInMillis() > calendar1.getTimeInMillis()) {
                            if (calendar.getTime().before(calendar1.getTime()) || calendar.getTime().after(calendar1.getTime())) {
                                if (snapshot1.exists()) {
                                    if (snapshot1.child(Constant.STUDENT_LIST).child(userLogin).exists()) {

                                        if (snapshot1.child(Constant.ASSIGN_NAME).exists()) {
                                            assignmentName = snapshot1.child(Constant.ASSIGN_NAME).getValue().toString();
                                        }
                                        if (snapshot1.child(Constant.ASSIGNTYPE_NAME).exists()) {
                                            assignmentType = snapshot1.child(Constant.ASSIGNTYPE_NAME).getValue().toString();
                                        }
                                        if (snapshot1.child(Constant.STUDENT_LIST).child(userLogin).child(Constant.STATUS_NAME).exists()) {

                                            assignmentStatus = snapshot1.child(Constant.STUDENT_LIST).child(userLogin).child(Constant.STATUS_NAME).getValue().toString();
                                        }
                                        if (snapshot1.child(Constant.STUDENT_LIST).child(userLogin).child(Constant.ACCEPT_NAME).exists()) {
                                            assignmentAccept = snapshot1.child(Constant.STUDENT_LIST).child(userLogin).child(Constant.ACCEPT_NAME).getValue().toString();
                                        }
                                        if (snapshot1.child(Constant.ASSIGNDATE_NAME).exists()) {
                                            assignmentDate = snapshot1.child(Constant.ASSIGNDATE_NAME).getValue().toString();
                                        }
                                        if (snapshot1.child(Constant.ASSIGNADMIN_NAME).exists()) {
                                            assignmentAdmin = snapshot1.child(Constant.ASSIGNADMIN_NAME).getValue().toString();
                                        }
                                        if (snapshot1.child("courseName").exists()) {
                                            courseName = snapshot1.child("courseName").getValue().toString();
                                        }
                                        if (snapshot1.child(Constant.STUDENT_LIST).exists()) {

                                            invitedUserKey = new ArrayList<String>();
                                            DataSnapshot snapshot11 = snapshot1.child(Constant.STUDENT_LIST);

                                            Iterator<DataSnapshot> iteratedValue = snapshot11.getChildren().iterator();


                                            while (iteratedValue.hasNext()) {
                                                DataSnapshot dataSnapshot1 = iteratedValue.next();
                                                String key = dataSnapshot1.getKey();
                                                Log.d("xsd", dataSnapshot1 + "---" + key);

                                                invitedUserKey.add(key);
                                            }


                                        }

                                        if (assignmentAccept.equals(Constant.ACCEPTED_NAME)) {

                                            String userKeyInvited = "";

                                            for (String s : invitedUserKey) {
                                                userKeyInvited += s + ",";
                                            }
                                            Log.d("xzc", invitedUserKey + "");
                                            HashMap<String, String> hashMap = new HashMap();
                                            hashMap.put(Constant.ASSIGN_NAME, assignmentName);
                                            hashMap.put(Constant.ASSIGNTYPE_NAME, assignmentType);
                                            hashMap.put(Constant.ASSIGNDATE_NAME, assignmentDate);
                                            hashMap.put(Constant.STATUS_NAME, assignmentStatus);
                                            hashMap.put(Constant.KEY_NAME, assignmentKey);
                                            hashMap.put(Constant.SUBMITNUM_NAME, String.valueOf(submit));
                                            hashMap.put(Constant.PROGRESSNUM_NAME, String.valueOf(inProgress));
                                            hashMap.put(Constant.COURSE_KEY, courseKey);
                                            hashMap.put(Constant.ASSIGNADMIN_NAME, assignmentAdmin);
                                            hashMap.put(Constant.COURSE_NAME, courseName);
                                            hashMap.put(Constant.STUDENT_LIST, userKeyInvited);

                                            assignmentList.add(hashMap);
                                        }

                                    }
                                }
                            }
                        }
                    }


                }

                Collections.reverse(assignmentList);

                assignmentAdaptor.addData(assignmentList);
                assignmentAdaptor.notifyDataSetChanged();

                //  ShowDialog.hideDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                if(ShowDialog.dialog.isShowing()) {
                    ShowDialog.hideDialog();
                }
            }
        });
    }

    private void bindViews(View view) {

        assignment_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.assignment_rv);
        card_cv = (CardView) view.findViewById(R.id.card_cv);
        empty_tv = (TextView) view.findViewById(R.id.empty_tv);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        assignment_rv.addOnItemTouchListener(new AssignmentAdaptor.RecyclerTouchListener(getContext(), assignment_rv, new ClickRecyclerInterface() {
            @Override
            public void onRecyClick(View view, int position) {

                String assignmentData = view.getTag().toString();
                Log.d("dfd", assignmentData);
                Bundle bundle = new Bundle();
                bundle.putString(Constant.ASSIGNDATE_NAME, assignmentData);
                assignmentDetail.setArguments(bundle);
                ReplaceFragment.replace(((WelcomeActivity) getActivity()), assignmentDetail, "AssignmentDetail_Fragment");

            }

            @Override
            public void onLongClick(View view, int position) {

                String assignment = view.getTag().toString();
                String assignmentData[] = assignment.split(";");
                String assignmentKey = assignmentData[0];
                String assignmentAdmin = assignmentData[7];
                String courseKey = assignmentData[5];
                Log.d("sdd", assignmentAdmin + "--------" + StoreData.getUserKeyFromSharedPre(getContext()));
                if (assignmentAdmin.equals(StoreData.getUserKeyFromSharedPre(getContext()))) {

                    ShowDialog.getInstance().showDeleteDialog(getContext(), assignmentKey, courseKey, 2);
                }
            }
        }));


    }

    private void setAdaptor() {
        assignmentAdaptor = new AssignmentAdaptor(assignment_list, getActivity());
        RecyclerViewEmptySupport.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        assignment_rv.setLayoutManager(mLayoutManager);
        assignment_rv.setAdapter(assignmentAdaptor);
        assignment_rv.setEmptyView(empty_tv);

        // assignmentAdaptor.setClickListener(this);


    }


}
