package com.brst.classmates.fragments;


import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.AssignDetailAdapter;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.AlarmReceiver;
import com.brst.classmates.classses.BeanClass;
import com.brst.classmates.classses.ListviewHeight;
import com.brst.classmates.classses.ReplaceFragment;

import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.classses.Utility;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.ALARM_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssignmentDetail extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener {

    View view;
    AddAssignmentFragment addAssignmentFragment = new AddAssignmentFragment();

    AssignDetailAdapter assignDetailAdapter;
    public static ExpandableListView expand_el;

    String selectedList = "";


    List<HashMap<String, String>> listCoomentReply = new ArrayList<>();
    HashMap<String, List> listDataChild;
    ScrollView scroll_sv;

    Boolean checkbool = false;


    ArrayList<BeanClass> commentsArrayList = new ArrayList<BeanClass>();

    TextView type_tv, time_tv, update_tv;
    TextView submit_tv, progres_tv;

    View contentRoot;

    int check = 0;

    String assignmentName, assignmentType, assignmentStatus, assignmentDate, assignmentKey, courseKey, courseName, adminName, userKeyInvited;

    TextView status_tv, progress_tv, done_tv, assignmentNm;
    PopupWindow popupWindow;

    ImageView camera_iv;

    Spinner status_sp;

    String picture_Path;
    File destination_path;
    Uri imageuri, imageUploadUri, downloadImageUri;

    EmojiconEditText comment_et;
    ImageView emoji_iv, send_iv;

    String reminderVal;

    List<BeanClass> replyList = new ArrayList<>();

    private EmojIconActions emojIcon;

    Fragment fragment;

    DatabaseReference databaseReference;


    public AssignmentDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (view == null) {
            view = inflater.inflate(R.layout.fragment_assignment_detail, container, false);
        }

        fragment = getFragmentManager().findFragmentByTag("AssignmentDetail_Fragment");

        bindViews(view);

        setListener();


        Bundle bundle = getArguments();

        if (bundle != null) {
            String assignmentData = bundle.getString(Constant.ASSIGNDATE_NAME);
            String assignment[] = assignmentData.split(";");
            assignmentKey = assignment[0];
            assignmentName = assignment[1];
            assignmentType = assignment[2];
            assignmentStatus = assignment[3];
            assignmentDate = assignment[4];
            courseKey = assignment[5];
            courseName = assignment[6];
            adminName = assignment[7];
            userKeyInvited = assignment[8];

            type_tv.setText(assignmentType);
            time_tv.setText(assignmentDate);
            status_tv.setText(assignmentStatus);
            assignmentNm.setText(assignmentName);

            reminderVal = SharedPreference.getInstance().getData(getContext(), assignmentKey);

            fetchSubmitProgress();
        }
        //     databaseReference = FirebaseDatabase.getInstance().getReference(Constant.ASSCOMMENT_NAME).child(assignmentKey);

        //  setAdaptor();


        //prepareData();

        //   Utility.setListViewHeightBasedOnChildren(expand_el);
      /*  expand_el.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:

                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:

                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }


                v.onTouchEvent(event);
                return true;
            }
        });*/


        return view;
    }
/*
    private void setAdaptor() {


        listDataChild = new HashMap<String, List>();
        listCoomentReply = new ArrayList<HashMap<String, String>>();

        assignDetailAdapter = new AssignDetailAdapter(getContext(), commentsArrayList);

        expand_el.setAdapter(assignDetailAdapter);


    }*/

    private void setListener() {

        update_tv.setOnClickListener(this);
        // camera_iv.setOnClickListener(this);

    }

    private void fetchSubmitProgress() {

        FirebaseDatabase.getInstance().getReference("AssignmentTask").child(courseKey).child(assignmentKey)
                .child(Constant.STUDENT_LIST).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int submit = 0, inProgress = 0;

                Log.d("dsfd", dataSnapshot + "");

                Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                Iterator<DataSnapshot> iterator = iterable.iterator();

                while (iterator.hasNext()) {
                    DataSnapshot snapshot = iterator.next();

                    String assignmentStatus = snapshot.child(Constant.STATUS_NAME).getValue().toString();

                    if (assignmentStatus.equals(Constant.SUBMIT_NAME)) {

                        submit++;

                    } else {

                        inProgress++;
                    }

                }

                submit_tv.setText(String.valueOf(submit));
                progres_tv.setText(String.valueOf(inProgress));


            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void bindViews(View view) {

        contentRoot = view.findViewById(R.id.contentRoot);
        // expand_el = (ExpandableListView) view.findViewById(R.id.expand_el);
        status_tv = (TextView) view.findViewById(R.id.status_tv);
        assignmentNm = (TextView) view.findViewById(R.id.assignmentName);
        type_tv = (TextView) view.findViewById(R.id.type_tv);
        time_tv = (TextView) view.findViewById(R.id.time_tv);
        progres_tv = (TextView) view.findViewById(R.id.progress_tv);
        submit_tv = (TextView) view.findViewById(R.id.submit_tv);
        update_tv = (TextView) view.findViewById(R.id.update_tv);
        status_sp = (Spinner) view.findViewById(R.id.status_sp);
        //   comment_et = (EmojiconEditText) view.findViewById(R.id.comment_et);
        scroll_sv = (ScrollView) view.findViewById(R.id.scroll_sv);
        emoji_iv = (ImageView) view.findViewById(R.id.emoji_iv);
        send_iv = (ImageView) view.findViewById(R.id.send_iv);
        //  camera_iv = (ImageView) view.findViewById(R.id.camera_iv);

        //emojIcon = new EmojIconActions(getContext(), contentRoot, comment_et, emoji_iv);
        // emojIcon.ShowEmojIcon();

       /* comment_et.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                scroll_sv.smoothScrollTo(0, comment_et.getTop());
                scroll_sv.post(new Runnable() {
                                   @Override
                                   public void run() {
                                       comment_et.requestFocus();

                                   }

                               }


                );
                return false;
            }
        });*/


       /* comment_et.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    send_iv.setVisibility(View.VISIBLE);
                    camera_iv.setVisibility(View.GONE);

                } else {
                    send_iv.setVisibility(View.GONE);
                    camera_iv.setVisibility(View.VISIBLE);
                }


            }

        });*/
    }

    /* private void prepareData() {

         ShowDialog.showDialog(getContext());

         databaseReference.addValueEventListener(new ValueEventListener() {
             @Override
             public void onDataChange(DataSnapshot dataSnapshot) {

                 if (dataSnapshot != null) {

                     Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                     Iterator<DataSnapshot> iterator = iterable.iterator();


                     commentsArrayList = new ArrayList<BeanClass>();


                     while (iterator.hasNext()) {

                         DataSnapshot snapshot = iterator.next();

                         BeanClass beanClass = new BeanClass();

                         String messageKey = snapshot.getKey();
                         String senderName = snapshot.child(Constant.MESSAGE).child(Constant.USERMESS_NAME).getValue().toString();
                         String time = snapshot.child(Constant.MESSAGE).child(Constant.TIME_NAME).getValue().toString();
                         if (snapshot.child(Constant.MESSAGE).child(Constant.MESSAGE).exists()) {
                             String message = snapshot.child(Constant.MESSAGE).child(Constant.MESSAGE).getValue().toString();
                             beanClass.setCommentReply(message);
                         } else if (snapshot.child(Constant.MESSAGE).child(Constant.IMAGE_NAME).exists()) {
                             String image = snapshot.child(Constant.MESSAGE).child(Constant.IMAGE_NAME).getValue().toString();

                             beanClass.setImage(image);
                         }

                         beanClass.setName(senderName);
                         beanClass.setTime(time);

                         beanClass.setMessageKey(messageKey);
                         beanClass.setAssignmentKey(assignmentKey);


                         Iterable<DataSnapshot> iterableReply = snapshot.child(Constant.REPLY_NAME).getChildren();
                         Iterator<DataSnapshot> iteratorReply = iterableReply.iterator();


                         ArrayList<HashMap<String, String>> listReply = new ArrayList<HashMap<String, String>>();

                         while (iteratorReply.hasNext()) {

                             HashMap hashMap = new HashMap();

                             DataSnapshot snapshoReply = iteratorReply.next();

                             String replyTime = snapshoReply.child(Constant.TIME_NAME).getValue().toString();
                             String replyUser = snapshoReply.child(Constant.USERMESS_NAME).getValue().toString();

                             if (snapshoReply.child(Constant.REPLY_NAME).exists()) {
                                 String replyMess = snapshoReply.child(Constant.REPLY_NAME).getValue().toString();
                                 hashMap.put(Constant.MESSAGE, replyMess);
                             } else if (snapshoReply.child(Constant.IMAGE_NAME).exists()) {
                                 String image = snapshoReply.child(Constant.IMAGE_NAME).getValue().toString();

                                 hashMap.put(Constant.IMAGE_NAME, image);
                             }

                             hashMap.put(Constant.USERMESS_NAME, replyUser);
                             hashMap.put(Constant.TIME_NAME, replyTime);


                             listReply.add(hashMap);

                         }

                         beanClass.setArrayList(listReply);

                         commentsArrayList.add(beanClass);
                     }
                     assignDetailAdapter.addData(commentsArrayList, assignDetailAdapter, expand_el);
                     assignDetailAdapter.notifyDataSetChanged();

                     ShowDialog.hideDialog();
                 }
             }

             @Override
             public void onCancelled(DatabaseError databaseError) {

                 ShowDialog.hideDialog();

             }
         });

     }
 */
    @Override
    public void onResume() {
        super.onResume();
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
     //   ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setText("Assignments Details");
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_iv.setImageResource(R.mipmap.edit);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        ((WelcomeActivity) getActivity()).header_iv.setOnClickListener(this);

        status_sp.setVisibility(View.GONE);
        status_tv.setVisibility(View.VISIBLE);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
           /* case R.id.send_iv:

                sendDataToFirebase();

                break;*/

            case R.id.progress_tv:

                status_tv.setText(progress_tv.getText());

                popupWindow.dismiss();

                break;

            case R.id.done_tv:

                status_tv.setText(done_tv.getText());

                popupWindow.dismiss();

                break;

            case R.id.header_iv:
                if (fragment != null && fragment.isVisible()) {

                    status_tv.setVisibility(View.GONE);
                    status_sp.setVisibility(View.VISIBLE);
                    status_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (check > 0) {
                                String selectedList = parent.getItemAtPosition(position).toString();

                                updateStatus(selectedList);
                            }
                            check++;


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }

                    });
                }

                break;

            case R.id.update_tv:


                if (!adminName.equals(StoreData.getUserKeyFromSharedPre(getContext()))) {

                    if (checkbool == false) {


                        status_tv.setVisibility(View.GONE);
                        status_sp.setVisibility(View.VISIBLE);
                        status_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (check > 0) {

                                    selectedList = parent.getItemAtPosition(position).toString();

                                    checkbool = true;

                                }
                                check++;


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }

                        });
                    } else {
                        updateStatus(selectedList);
                    }
                } else {

                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.ASSIGNMENT_NAME, assignmentName);
                    bundle.putString(Constant.ASSIGNTYPE_NAME, assignmentType);
                    bundle.putString(Constant.STATUS_NAME, assignmentStatus);
                    bundle.putString(Constant.COURSE_KEY, courseKey);
                    bundle.putString(Constant.KEY_NAME, assignmentKey);
                    bundle.putString(Constant.ASSIGNDATE_NAME, assignmentDate);
                    bundle.putString(Constant.COURSE_NAME, courseName);
                    bundle.putString(Constant.STUDENT_LIST, userKeyInvited);

                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), new AddAssignmentFragment(), "AddAssignment_Fragment", bundle);


                    break;
                }


        }
    }

    private void sendDataToFirebase() {

        String comment = comment_et.getText().toString();
        if (comment.isEmpty()) {
            Toast.makeText(getContext(), "Can't send empty message.", Toast.LENGTH_SHORT).show();
        } else {
            String userName = StoreData.getUserFullNameFromSharedPre(getContext());
            String time = SystemTime.getTime();

            HashMap hashMap = new HashMap();
            hashMap.put(Constant.USERMESS_NAME, userName);
            hashMap.put(Constant.TIME_NAME, time);
            hashMap.put(Constant.MESSAGE, comment);

            databaseReference.push().child(Constant.MESSAGE).updateChildren(hashMap);

            comment_et.setText("");

        }


    }

    private void updateStatus(final String selectedList) {

        FirebaseDatabase.getInstance().getReference("AssignmentTask").child(courseKey)
                .child(assignmentKey).child(Constant.STUDENT_LIST)
                .child(StoreData.getUserKeyFromSharedPre(getContext())).child(Constant.STATUS_NAME).setValue(selectedList).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {
                    status_sp.setVisibility(View.GONE);
                    status_tv.setVisibility(View.VISIBLE);
                    status_tv.setText(selectedList);

                    checkbool = false;


                    FirebaseDatabase.getInstance().getReference("AssignmentTask").child(courseKey)
                            .child(assignmentKey).child(Constant.STUDENT_LIST)
                            .child(StoreData.getUserKeyFromSharedPre(getContext())).child(Constant.STATUS_NAME).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            String status = dataSnapshot.getValue().toString();

                            if (status.equals("Submitted")) {

                                if(!reminderVal.equals("null"))
                                {
                                    Intent _myIntent = new Intent(getContext(), AlarmReceiver.class);
                                    PendingIntent _myPendingIntent = PendingIntent.getBroadcast(getContext(), Integer.parseInt(reminderVal), _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                                    AlarmManager _myAlarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

                                    _myAlarmManager.cancel(_myPendingIntent);


                                }

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }
        });
    }

    private void popupStatus(Context context, View v) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_status, null);

        progress_tv = (TextView) popupView.findViewById(R.id.progress_tv);
        done_tv = (TextView) popupView.findViewById(R.id.done_tv);


        popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {

                popupWindow.dismiss();

            }

        });
        popupWindow.showAsDropDown(v, 0, 0);

        progress_tv.setOnClickListener(this);
        done_tv.setOnClickListener(this);

    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            requestAppPermissions(permission, R.string.permission, 54);
        }
    };


    View.OnClickListener cameraClickLictener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();

            capture_image();

        }
    };

    /*-------------------------------------------capture image and store in sd card-----------------------------*/
    private void capture_image() {
        picture_Path = "";
        destination_path = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = "image";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picture_Path = String.valueOf(new File(f, System.currentTimeMillis() + ".jpg"));
        destination_path = new File(picture_Path);

        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination_path);
        } else {
            imageuri = Uri.fromFile(destination_path);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, Constant.CAMERA_CODE);
    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 54) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, Constant.GALLERY_CODE);
        }

    }

    /*------------------------------------------get image after capture and pic from gallery-----------------------------*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.GALLERY_CODE && resultCode == RESULT_OK) {

            imageUploadUri = data.getData();
            imageUpload();

        } else if (requestCode == Constant.CAMERA_CODE && resultCode == RESULT_OK) {


            imageUploadUri = imageuri;
            imageUpload();

        }

    }

    private void imageUpload() {

/*        String userName = StoreData.getUserFullNameFromSharedPre(getContext());
        String time = SystemTime.getTime();

        BeanClass beanClass=new BeanClass();
        beanClass.setTime(time);
        beanClass.setImage(imageUploadUri.toString());
        beanClass.setName(userName);
        beanClass.setMessageKey("");


        commentsArrayList.add(beanClass);

        assignDetailAdapter.addData(commentsArrayList,assignDetailAdapter,expand_el);*/

        ShowDialog.showDialog(getContext());

        FirebaseStorage.getInstance().getReference().child("image" + "_" + imageUploadUri.getLastPathSegment())
                .putFile(imageUploadUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();
                        storeImageDatabase();
                    }
                });

    }

    private void storeImageDatabase() {

        String userName = StoreData.getUserFullNameFromSharedPre(getContext());
        String time = SystemTime.getTime();

        HashMap hashMap = new HashMap();
        hashMap.put(Constant.USERMESS_NAME, userName);
        hashMap.put(Constant.TIME_NAME, time);
        hashMap.put(Constant.IMAGE_NAME, downloadImageUri.toString());

        databaseReference.push().child(Constant.MESSAGE).updateChildren(hashMap);


        ShowDialog.hideDialog();

    }
}
