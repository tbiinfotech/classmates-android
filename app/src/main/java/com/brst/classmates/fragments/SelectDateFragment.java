package com.brst.classmates.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;

import com.brst.classmates.classses.TimePickerFragment;
import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.interfaces.DateTimeInterface;
import com.brst.classmates.utility.Constant;

import java.util.Calendar;



/**
 * Created by brst-pc89 on 7/24/17.
 */

public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    Bundle bundlefrg;

    static DateTimeInterface dateTimeInterfacee;

    public static SelectDateFragment getInstance(DateTimeInterface dateTimeInterface) {
        dateTimeInterfacee = dateTimeInterface;
        return new SelectDateFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        bundlefrg=getArguments();

        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        String month=MONTHS[mm];

        DatePickerDialog datePickerDialog= new DatePickerDialog(getActivity(), this, yy, mm, dd);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);

        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        if(bundlefrg!=null) {

            Bundle bundle = new Bundle();
            bundle.putInt(Constant.YEAR_NAME, year);
            bundle.putInt(Constant.MONTH, month);
            bundle.putInt(Constant.DAY, dayOfMonth);
            DialogFragment newFragment = new TimePickerFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getFragmentManager(), "TimePicker");
        }

        else
        {
            dateTimeInterfacee.setOnDate(year, month , dayOfMonth);

        }

    }


}
