package com.brst.classmates.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.ContactAdapter;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment implements TextWatcher, View.OnClickListener {

    RecyclerViewEmptySupport contact_rv;
    public static EditText searchContactUser_et;
    TextView empty_tv, invite_tv;
    List<HashMap<String, String>> courseSearchtList = new ArrayList<>();
    List<String> courseKey = new ArrayList<>();
    List<String> courseSearchKey = new ArrayList<>();
    ArrayList<HashMap<String, String>> searchContactList;
    ArrayList<HashMap<String, String>> contactsList;

    ContactAdapter contactAdaptor;
    private FirebaseStorage firebaseStorage;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    StorageReference storageReference;
    List<HashMap<String, String>> dataList = new ArrayList<>();
    ImageView iv_search;
    List<String> groupList;
    View view = null;
    public static Context Contacts;

    public ContactFragment() {
        // Required empty public constructor
        Contacts = getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_contact, container, false);
        }
        NetworkFragment.searchContactUser_et.setText("");
        bindViews();

        getCourseList();

        return view;
    }


    private void getCourseList() {


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Constant.INVITED_USER);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                dataList = new ArrayList<HashMap<String, String>>();
                courseKey = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Log.d("datasnapshot", dataSnapshot1.getKey());
                    HashMap hashMap = (HashMap) dataSnapshot1.getValue();

                    try {
                        HashMap hashMap1 = new HashMap();

                        hashMap1.put("to", hashMap.get("to").toString());
                        hashMap1.put("request_status", hashMap.get("request_status").toString());
                        hashMap1.put("SENDER", hashMap.get("SENDER").toString());
                        hashMap1.put("from", hashMap.get("from").toString());
                        hashMap1.put("RECEIVER", hashMap.get("RECEIVER").toString());

                        hashMap1.put(Constant.COLLEGE_NAME, hashMap.get(Constant.COLLEGE_NAME).toString());
                        hashMap1.put(Constant.UNIVERSITY_NAME, hashMap.get(Constant.UNIVERSITY_NAME).toString());
                        hashMap1.put(Constant.COUNTRY_NAME, hashMap.get(Constant.COUNTRY_NAME).toString());
                        hashMap1.put(Constant.LAST_NAME, hashMap.get(Constant.LAST_NAME).toString());
                        hashMap1.put(Constant.YEAR_NAME, hashMap.get(Constant.YEAR_NAME).toString());
                        hashMap1.put(Constant.DEGREE_NAME, hashMap.get(Constant.DEGREE_NAME).toString());

                    /*    hashMap1.put("Sender"+Constant.COLLEGE_NAME, hashMap.get(Constant.COLLEGE_NAME).toString());
                        hashMap1.put("Sender"+Constant.UNIVERSITY_NAME, hashMap.get(Constant.UNIVERSITY_NAME).toString());
                        hashMap1.put("Sender"+Constant.COUNTRY_NAME, hashMap.get(Constant.COUNTRY_NAME).toString());
                        hashMap1.put("Sender"+Constant.LAST_NAME, hashMap.get(Constant.LAST_NAME).toString());
                        hashMap1.put("Sender"+Constant.YEAR_NAME, hashMap.get(Constant.YEAR_NAME).toString());
                        hashMap1.put("Sender"+Constant.DEGREE_NAME, hashMap.get(Constant.DEGREE_NAME).toString());*/

                        hashMap1.put("Sender" + Constant.COLLEGE_NAME, hashMap.get("Sendercollege_name").toString());
                        hashMap1.put("Sender" + Constant.UNIVERSITY_NAME, hashMap.get("Senderuniversity_name").toString());
                        hashMap1.put("Sender" + Constant.COUNTRY_NAME, hashMap.get("Sendercountry_name").toString());
                        //   hashMap1.put("Sender"+Constant.LAST_NAME, hashMap.get(Constant.LAST_NAME).toString());
                        hashMap1.put("Sender" + Constant.YEAR_NAME, hashMap.get("Senderyear").toString());
                        hashMap1.put("Sender" + Constant.DEGREE_NAME, hashMap.get("Senderdegree").toString());


                        //  hashMap1.put(Constant.PROFILE_PICTURE, hashMap.get(Constant.PROFILE_PICTURE).toString());

                        try {
                            try {
                                hashMap1.put("profile_picture", hashMap.get("profile_picture").toString());
                            } catch (Exception e) {
                            }
                            try {
                                hashMap1.put("RECEIVER_PROFILE", hashMap.get("RECEIVER_PROFILE").toString());

                            } catch (Exception e) {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Log.d("data",hashMap1+"");

                        if (hashMap.get("to").toString().equals(StoreData.getUserKeyFromSharedPre(getActivity()))) {

                            dataList.add(hashMap1);


                            courseKey.add(dataSnapshot1.getKey());
                        } else if (hashMap.get("from").toString().equals(StoreData.getUserKeyFromSharedPre(getActivity()))) {
                            dataList.add(hashMap1);


                            courseKey.add(dataSnapshot1.getKey());
                        }


                    } catch (Exception e) {

                        e.printStackTrace();
                    }


                }

                Log.d("list",dataList+"");
                contactAdaptor = new ContactAdapter(dataList, getContext(), courseKey);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                contact_rv.setLayoutManager(mLayoutManager);
                contact_rv.setAdapter(contactAdaptor);
                contact_rv.setEmptyView(empty_tv);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }


    private void bindViews() {

        contact_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.contact_rv);
        searchContactUser_et = (EditText) view.findViewById(R.id.searchContactUser_et);
        empty_tv = (TextView) view.findViewById(R.id.empty_tv);
        invite_tv = (TextView) view.findViewById(R.id.invite_tv);
        iv_search = (ImageView) view.findViewById(R.id.iv_search);
        searchContactUser_et.addTextChangedListener(this);
        iv_search.setVisibility(View.GONE);
        invite_tv.setOnClickListener(this);


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {


        String s2;
        //  studenSearchtList.clear();

        searchContactList = new ArrayList<>();
        // courseSearchKey.clear();
        String s1 = s.toString().toLowerCase();

        s1 = s1.replace(" ", "");

        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).get("request_status").equals("Request Pending")) {
                String RECEIVER = dataList.get(i).get("RECEIVER").toLowerCase().replace(" ", "");

                if (RECEIVER.contains(s1)) {
                    searchContactList.add(dataList.get(i));
                    // courseSearchKey.add(courseKey.get(i));

                }
            } else {
                String SENDER = dataList.get(i).get("SENDER").toLowerCase().replace(" ", "");

                if (SENDER.contains(s1)) {
                    searchContactList.add(dataList.get(i));
                    // courseSearchKey.add(courseKey.get(i));

                }
            }
        }

        contactAdaptor = new ContactAdapter(searchContactList, getContext(), courseKey);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        contact_rv.setLayoutManager(mLayoutManager);
        contact_rv.setAdapter(contactAdaptor);
        contact_rv.setEmptyView(empty_tv);


    }


    public ArrayList<HashMap<String, String>> sortList(final ArrayList<HashMap<String, String>> contactList) {

        Collections.sort(contactList, new Comparator<HashMap>() {
            @Override
            public int compare(HashMap s1, HashMap s2) {

                String firstValue = "", secondValue = "";

                firstValue = s1.get("ContactName").toString();
                secondValue = s2.get("ContactName").toString();

                return firstValue.compareToIgnoreCase(secondValue);
            }
        });

        return contactList;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.GONE);
     //   ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_iv.setImageResource(R.mipmap.add);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);
        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        ((WelcomeActivity) getActivity()).header_tv.setText("");
    }

    @Override
    public void onClick(View v) {

    }
}

