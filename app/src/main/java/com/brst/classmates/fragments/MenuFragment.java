package com.brst.classmates.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.GridAdaptor;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.ReplaceFragment;

import java.util.List;

import static com.brst.classmates.activity.WelcomeActivity.context;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener {

    GridView menu_gv;

    String text[] = {"Courses", "Events", "Tasks", "Chat", "Notes", "Network"};

    RecyclerView menu_rv;

    LinearLayout course_ll, event_ll, task_ll, chat_ll, notes_ll, network_ll, contact_ll;

    GridAdaptor gridAdaptor;
    Event_Invite_Fragment event_invite_fragment;
    ProfileFragment profileFragment;
    MessageBoardFragment messageBoardFragment;
    Assign_ExamFragment assign_examFragment;
    CourseFragment courseFragment;
    UserFragment userFragment;
    Fragment menufragment;


    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu, container, false);


        bindViews(view);

        initFragment();

        setListener();

        return view;
    }

    private void bindViews(View view) {

        course_ll = (LinearLayout) view.findViewById(R.id.course_ll);
        event_ll = (LinearLayout) view.findViewById(R.id.event_ll);
        task_ll = (LinearLayout) view.findViewById(R.id.task_ll);
        chat_ll = (LinearLayout) view.findViewById(R.id.chat_ll);
     //   notes_ll = (LinearLayout) view.findViewById(R.id.notes_ll);
        network_ll = (LinearLayout) view.findViewById(R.id.network_ll);
       // contact_ll = (LinearLayout) view.findViewById(R.id.contact_ll);

        menufragment = getFragmentManager().findFragmentByTag("Menu_Fragment");


    }


    private void initFragment() {

        event_invite_fragment = new Event_Invite_Fragment();
        profileFragment = new ProfileFragment();
        assign_examFragment = new Assign_ExamFragment();
        messageBoardFragment = new MessageBoardFragment();
        userFragment = new UserFragment();
    }

    private void setListener() {


        course_ll.setOnClickListener(this);
        event_ll.setOnClickListener(this);
        task_ll.setOnClickListener(this);
        chat_ll.setOnClickListener(this);
      //  notes_ll.setOnClickListener(this);
        network_ll.setOnClickListener(this);
//        contact_ll.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.INVISIBLE);
        ((WelcomeActivity) getActivity()).header_tv.setText("Menu");
        ((WelcomeActivity) getActivity()).header_iv.setImageResource(R.mipmap.add);
      //  ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.CENTER);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.course_ll:

                ReplaceFragment.replace((WelcomeActivity) getActivity(), new CourseFragment(), "Course_Fragment");

                break;


            case R.id.event_ll:

                ReplaceFragment.replace((WelcomeActivity) getActivity(), event_invite_fragment, "EventInvite_Fragment");
                break;

            case R.id.task_ll:

               // ReplaceFragment.replace((WelcomeActivity) getActivity(), courseFragment, "courseAssign_Fragment");

                ReplaceFragment.replace((WelcomeActivity) getActivity(), new Assign_ExamFragment(), "AssignExam_Fragment");
                break;

            case R.id.chat_ll:

                ReplaceFragment.replace((WelcomeActivity) getActivity(), new GroupFragment(), "Group_Fragment");

           /*     Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://firebasestorage.googleapis.com/v0/b/gradsproject-5d9fc.appspot.com/o/file_1507617886853?alt=media&token=d31aa458-32d1-4715-baa9-d718f32bfc64"));
                intent.setType("application/pdf");
                PackageManager pm = context.getPackageManager();
                List<ResolveInfo> activities = pm.queryIntentActivities(intent, 0);
                if (activities.size() > 0) {
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "hi there is no pdf viewer in your system", Toast.LENGTH_SHORT).show();

                }
*/
              /*  Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setAction(Intent.ACTION_VIEW);
                String type = "application/pdf";
                intent.setDataAndType(  Uri.parse("https://firebasestorage.googleapis.com/v0/b/gradsproject-5d9fc.appspot.com/o/file_1507617886853?alt=media&token=d31aa458-32d1-4715-baa9-d718f32bfc64"), type);
                context.startActivity(intent);*/

                break;

          /*  case R.id.notes_ll:

               // ReplaceFragment.replace((WelcomeActivity) getActivity(), courseFragment, "CourseList_Fragment");
                ReplaceFragment.replace((WelcomeActivity) getActivity(), new NotesFragment(), "Notes_Fragment");


                break;*/

            case R.id.network_ll:

            ReplaceFragment.replace((WelcomeActivity) getActivity(), new Network_Invite_Fragment(), "network_Fragment");


                break;

       /*     case R.id.contact_ll:

                String[] permission = {Manifest.permission.READ_CONTACTS, Manifest.permission.SEND_SMS};

                requestAppPermissions(permission, R.string.permission, 500);

                break;*/
        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void readContacts() {

        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

        startActivityForResult(intent, 1000);

    }


    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

                if (reqCode==1000 && resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();

                    Cursor cursorID = getContext().getContentResolver().query(contactData,
                            new String[]{ContactsContract.Contacts._ID},
                            null, null, null);
                    String contactID = null;
                    if (cursorID.moveToFirst()) {

                        contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
                    }
                    Cursor cursorPhone = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                                    ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                                    ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                            new String[]{contactID},
                            null);
                    String contactNumber;
                    if (cursorPhone.moveToFirst()) {

                         contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


                        String sms = "Follow the link: https://play.google.com/store/apps/details?id=com.brst.classmates";
                        try {
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(contactNumber, null, sms, null, null);
                            Toast.makeText(getContext(), "SMS Sent to: "+contactNumber,
                                    Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(getContext(),
                                    "SMS faild, please try again later!",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }

                    cursorPhone.close();
        }
    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode==500)
        {
            readContacts();
        }

    }

}
