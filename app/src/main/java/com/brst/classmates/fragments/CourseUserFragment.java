package com.brst.classmates.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.CourseUserAdaptor;
import com.brst.classmates.adaptors.StudentAdaptor;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.activity.WelcomeActivity.context;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseUserFragment extends Fragment implements View.OnClickListener, TextWatcher {

    View view = null;

    EditText searchUserCourse_et;

    RecyclerViewEmptySupport studentcourse_rv;

    TextView empty_tv, invite_tv;
    String courseKey, userKeyInvited;
    CourseUserAdaptor courseUserAdaptor;
    String loginUserKey;

    List<HashMap<String, String>> studentCourseList = new ArrayList<>();
    List<String> userKeyList;
    List<String> userKeyInvitedList = new ArrayList<>();


    List<HashMap<String, String>> studenSearchtCourseList = new ArrayList<>();

    public CourseUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_course_user, container, false);

            Bundle bundle = getArguments();

            if (bundle != null) {
                courseKey = bundle.getString(Constant.COURSE_KEY);

                if (bundle.containsKey(Constant.STUDENT_LIST)) {
                    userKeyInvited = bundle.getString(Constant.STUDENT_LIST);

                    userKeyInvitedList = new ArrayList<String>(Arrays.asList(userKeyInvited.split(",")));
                }
            }

            loginUserKey=StoreData.getUserKeyFromSharedPre(getContext());

            bindViews();

            setAdaptor();

            setListener();

            if (!NetworkCheck.isNetworkAvailable(getContext())) {
                ShowDialog.networkDialog(getContext());

                getUserUnderCourse();
            } else {

                getUserUnderCourse();
            }


        }

        return view;
    }

    private void bindViews() {

        empty_tv = (TextView) view.findViewById(R.id.empty_tv);
        invite_tv = (TextView) view.findViewById(R.id.invite_tv);
        studentcourse_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.studentcourse_rv);
        searchUserCourse_et = (EditText) view.findViewById(R.id.searchUserCourse_et);

        searchUserCourse_et.addTextChangedListener(this);
    }

    private void setAdaptor() {

        courseUserAdaptor = new CourseUserAdaptor(getContext(), studentCourseList);
        RecyclerViewEmptySupport.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        studentcourse_rv.setLayoutManager(layoutManager);
        studentcourse_rv.setAdapter(courseUserAdaptor);
        studentcourse_rv.setEmptyView(empty_tv);

    }

    private void setListener() {

        invite_tv.setOnClickListener(this);
    }

    private void getUserUnderCourse() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {
            ShowDialog.showDialog(getContext());
        }
        FirebaseDatabase.getInstance().getReference(Constant.COURSE_NAME).orderByChild(courseKey).equalTo(Constant.COURSE_NAME).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                userKeyList = new ArrayList<String>();
                studentCourseList = new ArrayList<>();
                if (dataSnapshot.getValue() != null) {
                    Iterable<DataSnapshot> iterable = dataSnapshot.getChildren();
                    Iterator<DataSnapshot> iterator = iterable.iterator();
                    while (iterator.hasNext()) {
                        DataSnapshot snapshot = iterator.next();
                        String userKey = snapshot.getKey();

                        Log.d("Xzc", userKeyInvitedList + "");
                        if (!userKeyInvitedList.contains(userKey))
                        //   String userKey = snapshot.getKey();
                        {
                            if (!userKey.equals(StoreData.getUserKeyFromSharedPre(getContext()))) {
                                userKeyList.add(userKey);
                            }

                        }

                    }
                    if (userKeyList.size() > 0) {
                        getStudentKey(userKeyList);
                    } else {
                        courseUserAdaptor.addList(studentCourseList);
                        courseUserAdaptor.notifyDataSetChanged();

                       // if(NetworkCheck.isNetworkAvailable(getContext())) {

                        if(ShowDialog.dialog!=null) {
                            if (ShowDialog.dialog.isShowing()) {
                                ShowDialog.hideDialog();
                            }
                        }
                    }
                } else {
                 //   if(NetworkCheck.isNetworkAvailable(getContext())) {
                    if(ShowDialog.dialog!=null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
             //   if(NetworkCheck.isNetworkAvailable(getContext())) {
                if(ShowDialog.dialog!=null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });
    }

    private void getStudentKey(List<String> keyList) {

        for (int i = 0; i < keyList.size(); i++) {
            String userKey = keyList.get(i);

            getStudentData(userKey);
        }


    }

    private void getStudentData(final String userKey) {

        FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).child(userKey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {

                    String firstName = "", lastName = "";

                    HashMap<String, String> hashMap = new HashMap();

                    if (dataSnapshot.child(Constant.FIRST_NAME).exists()) {
                        firstName = dataSnapshot.child(Constant.FIRST_NAME).getValue().toString();
                    }
                    if (dataSnapshot.child(Constant.LAST_NAME).exists()) {
                        lastName = dataSnapshot.child(Constant.LAST_NAME).getValue().toString();
                    }

                    String FullName = firstName + " " + lastName;

                    hashMap.put(Constant.FULL_NAME, FullName);
                    hashMap.put(Constant.KEY_NAME, userKey);
                    hashMap.put(Constant.CHECKED_NAME, Constant.FALSE_NAME);
                    if (dataSnapshot.child(Constant.PROFILE_PICTURE).exists()) {

                        hashMap.put(Constant.PROFILE_PICTURE, dataSnapshot.child(Constant.PROFILE_PICTURE).getValue().toString());
                    }
                    studentCourseList.add(hashMap);
                    courseUserAdaptor.addList(studentCourseList);
                    courseUserAdaptor.notifyDataSetChanged();


                }
                if(ShowDialog.dialog.isShowing()) {
                    ShowDialog.hideDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                if(ShowDialog.dialog.isShowing()) {
                    ShowDialog.hideDialog();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.invite_tv:

                getCheckedList();

                break;
        }
    }

    private void getCheckedList() {

        List<String> checkList = new ArrayList<>();
        List<HashMap<String, String>> studentList = ((CourseUserAdaptor) courseUserAdaptor).getStudentist();

        for (int i = 0; i < studentList.size(); i++) {
            if (studentList.get(i).get(Constant.CHECKED_NAME).equals(Constant.TRUE_NAME)) {
                checkList.add(studentList.get(i).get(Constant.KEY_NAME));
            }
        }

        if (checkList.size() == 0) {
            ShowDialog.showSnackbarError(view, "Please select student from list", getContext());
        } else {


            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.putExtra(Constant.CHECK_LIST, (Serializable) checkList);
            getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
            ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
        }


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        Log.d("sdfsf", "sad00zxz");

        String s2;
        studenSearchtCourseList.clear();

        String s1 = s.toString().toLowerCase();

        s1 = s1.replace(" ", "");

        for (int i = 0; i < studentCourseList.size(); i++) {

            String fullName = studentCourseList.get(i).get(Constant.FULL_NAME).toLowerCase().replace(" ", "");

            if (fullName.contains(s1)) {
                studenSearchtCourseList.add(studentCourseList.get(i));
                // courseSearchKey.add(courseKey.get(i));

            }

        }
        courseUserAdaptor.addList(studenSearchtCourseList);
        courseUserAdaptor.notifyDataSetChanged();


    }


    @Override
    public void afterTextChanged(Editable s) {

        Log.d("sdfsf", "sad00");

      /*  String s2;
        studenSearchtCourseList.clear();
      //  courseSearchKey.clear();
        String s1 = s.toString().toLowerCase();

        s1 = s1.replace(" ", "");

        Log.d("sdf",studentCourseList+"");

        for (int i = 0; i < studentCourseList.size(); i++) {
            HashMap hashMap = studentCourseList.get(i);
            String course = hashMap.get(Constant.COURSE_NAME).toString();

            String username = course.toLowerCase().replace(" ", "");

            if (username.contains(s1)) {
                studenSearchtCourseList.add(studentCourseList.get(i));
              //  courseSearchKey.add(courseKey.get(i));

            }

        }
        courseUserAdaptor.addList(studenSearchtCourseList);
        courseUserAdaptor.notifyDataSetChanged();*/

    }
}
