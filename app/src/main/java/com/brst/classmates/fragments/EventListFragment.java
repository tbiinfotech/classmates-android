package com.brst.classmates.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.EventAdaptor;
import com.brst.classmates.beanclass.EventAttendingCandidates;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class EventListFragment extends Fragment implements ClickRecyclerInterface, View.OnClickListener {

    RecyclerViewEmptySupport event_rv;
    String name[] = {"Birthday Party", "Birthday Party", "Birthday Party"};
    private ArrayList<HashMap> main_eventList, event_list, meetingList;
    List<HashMap<String, String>> event_list_invited;
    ArrayList<EventAttendingCandidates> al_users;
    public static List<ArrayList<EventAttendingCandidates>> invited_user;
    EventFragment eventFragment;//=new EventFragment();
    EventAdaptor eventAdaptor;
    View view = null;
    String user_key = "";
    TextView delete_tv, deletedone_tv, deleteCancel_tv, result;
    List<String> courseKey = new ArrayList<>();
    List<HashMap<String, String>> dataList = new ArrayList<>();
    ;
    Dialog delete_dialog;

    private DatabaseReference databaseReference;
    DatabaseReference eventRef, meetingRef;
    FirebaseDatabase firebaseDatabase;

    public EventListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_event_list, container, false);
            user_key = StoreData.getUserKeyFromSharedPre(getActivity());

            getUserList();

            eventFragment = new EventFragment();


            event_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.event_rv);
            result = (TextView) view.findViewById(R.id.result);

            main_eventList = new ArrayList<>();
            eventAdaptor = new EventAdaptor(main_eventList, getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            event_rv.setLayoutManager(mLayoutManager);
            event_rv.setAdapter(eventAdaptor);
            event_rv.setEmptyView(result);

            eventAdaptor.setClickListener(this);
        }

        initFireBaseDataBase();

        return view;
    }


    private void initFireBaseDataBase() {


        firebaseDatabase = FirebaseDatabase.getInstance();
        eventRef = firebaseDatabase.getReference(Constant.EVENT);
        meetingRef = firebaseDatabase.getReference(Constant.EVENT);

        if (!NetworkCheck.isNetworkAvailable(getContext()))

        {
            ShowDialog.networkDialog(getContext());

            getEventFromEvent();
        } else {
            getEventFromEvent();
        }
        //   getEventFromMeeting();
    }

    private synchronized void getEventFromEvent() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {
            ShowDialog.showDialog(getContext());
        }

        eventRef.child("Event").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap hashMap1 = null;
                event_list = new ArrayList<HashMap>();
                event_list_invited = new ArrayList<>();
                invited_user = new ArrayList<>();

                Iterator<DataSnapshot> iteratedValue = dataSnapshot.getChildren().iterator();

                while (iteratedValue.hasNext()) {
                    DataSnapshot dataSnapshot1 = iteratedValue.next();
                    String student_key = dataSnapshot1.getKey();

                    DataSnapshot dataSnapshot2 = dataSnapshot1.child(Constant.INVITED_STUDENTS);

                    if (dataSnapshot2.child(user_key).exists()) {
                        String userEventKey = dataSnapshot2.child(user_key).getKey();
                        String userEventValue = dataSnapshot2.child(user_key).getValue().toString();

                        hashMap1 = new HashMap();
                        if (userEventValue.equals(Constant.ACCEPTED_NAME) || userEventValue.equals(Constant.MAYBE)) {

                            int attending = 0, nonAttending = 0, mayBe = 0;
                            al_users = new ArrayList<>();
                            HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                            hashMap.put("event_key", student_key);

                            Iterator<DataSnapshot> iterator = dataSnapshot2.getChildren().iterator();

                            while (iterator.hasNext()) {
                                DataSnapshot snapshot1 = iterator.next();


                                EventAttendingCandidates obj_event_attend = new EventAttendingCandidates();

                                if (snapshot1.getValue().equals("Accepted")) {

                                    attending++;

                                    for (int i = 0; i < dataList.size(); i++) {
                                        if (dataList.get(i).get("KEY").equals(snapshot1.getKey().toString())) {
                                            HashMap mHashMap = new HashMap<>();
                                            mHashMap = dataList.get(i);
                                            if (mHashMap.containsKey(Constant.FIRST_NAME)) {
                                                hashMap1.put(Constant.FIRST_NAME, mHashMap.get(Constant.FIRST_NAME).toString());
                                            }
                                            if (mHashMap.containsKey(Constant.PROFILE_PICTURE)) {
                                                hashMap1.put(Constant.PROFILE_PICTURE, mHashMap.get(Constant.PROFILE_PICTURE).toString());
                                            }

                                            hashMap1.put(Constant.ATTENDING, "Attending");
                                            if (mHashMap.containsKey(Constant.FIRST_NAME)) {
                                                obj_event_attend.setName(mHashMap.get(Constant.FIRST_NAME).toString());
                                            }
                                            obj_event_attend.setAtteding("Attending");
                                        //    try {
                                            if (mHashMap.containsKey(Constant.PROFILE_PICTURE)) {
                                                obj_event_attend.setProfile_pic(mHashMap.get(Constant.PROFILE_PICTURE).toString());
                                            }
                                           /* } catch (Exception e) {
                                            }*/
                                            al_users.add(obj_event_attend);
                                        }

                                    }
                                } else if (snapshot1.getValue().equals("Rejected")) {
                                    nonAttending++;

                                    for (int i = 0; i < dataList.size(); i++) {
                                        if (dataList.get(i).get("KEY").equals(snapshot1.getKey().toString())) {
                                            HashMap<String, String> mHashMap = new HashMap<>();
                                            mHashMap = dataList.get(i);
                                            if (mHashMap.containsKey(Constant.FIRST_NAME)) {
                                                hashMap1.put(Constant.FIRST_NAME + "REJECTED", mHashMap.get(Constant.FIRST_NAME).toString());
                                            }
                                            if (mHashMap.containsKey(Constant.PROFILE_PICTURE)) {
                                                hashMap1.put(Constant.PROFILE_PICTURE + "REJECTED", mHashMap.get(Constant.PROFILE_PICTURE).toString());
                                            }
                                            if (mHashMap.containsKey(Constant.FIRST_NAME)) {
                                                obj_event_attend.setName(mHashMap.get(Constant.FIRST_NAME).toString());
                                            }
                                            obj_event_attend.setAtteding("Not Attending");
                                            // try {
                                            if (mHashMap.containsKey(Constant.PROFILE_PICTURE)) {
                                                obj_event_attend.setProfile_pic(mHashMap.get(Constant.PROFILE_PICTURE).toString());
                                            }
                                            // } catch (Exception e) {
                                            //}
                                            al_users.add(obj_event_attend);
                                            hashMap1.put("REJECTED", "Rejected");
                                            //  event_list_invited.add(hashMap1);
                                        }
                                    }
                                } else if (snapshot1.getValue().equals("Maybe") || snapshot1.getValue().equals("Waiting")) {
                                    mayBe++;


                                    for (int i = 0; i < dataList.size(); i++) {
                                        if (dataList.get(i).get("KEY").equals(snapshot1.getKey().toString())) {
                                            HashMap mHashMap = new HashMap<>();
                                            mHashMap = dataList.get(i);
                                            obj_event_attend.setName(mHashMap.get(Constant.FIRST_NAME).toString());
                                            obj_event_attend.setAtteding("May be");
                                            try {
                                                obj_event_attend.setProfile_pic(mHashMap.get(Constant.PROFILE_PICTURE).toString());
                                            } catch (Exception e) {
                                            }

                                            al_users.add(obj_event_attend);
                                            hashMap1.put(Constant.FIRST_NAME + "WAITING", mHashMap.get(Constant.FIRST_NAME).toString());
                                            hashMap1.put("WAITING", "May be");
                                            try {
                                                hashMap1.put(Constant.PROFILE_PICTURE + "WAITING", mHashMap.get(Constant.PROFILE_PICTURE).toString());
                                            } catch (Exception e) {
                                            }


                                        }
                                    }


                                }

                                event_list_invited.add(hashMap1);
                            }

                            Log.d("czxc", attending + " ---" + nonAttending + "---" + mayBe);
                            hashMap.put("attending", String.valueOf(attending));
                            hashMap.put("nonattending", String.valueOf(nonAttending));
                            hashMap.put("mayBe", String.valueOf(mayBe));
                            invited_user.add(al_users);

                            event_list.add(hashMap);

                            Log.e("TOTAL_USERS", al_users.toString());
                        }

                    }


                }

                Collections.reverse(event_list);
                Collections.reverse(invited_user);
                eventAdaptor.addList(event_list);
              /*  try {
                    if (NetworkCheck.isNetworkAvailable(getContext())) {*/
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }
               /* } catch (Exception e) {
                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

             /*   try {
                    if (NetworkCheck.isNetworkAvailable(getContext())) {*/
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();

                    }
                }
               /* }
                catch (Exception e)
                {

                }*/

            }
        });
        // }
    }


    //REGISTERED USERS TO GET INVITED USERS DETAILS

    private void getUserList() {


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("registered_user");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                dataList = new ArrayList<HashMap<String, String>>();
                courseKey = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                    //  try {
                    HashMap hashMap1 = new HashMap();
                    if (hashMap.containsKey(Constant.FIRST_NAME)) {
                        hashMap1.put(Constant.FIRST_NAME, hashMap.get(Constant.FIRST_NAME).toString());
                    }
                    if (hashMap.containsKey("specialization")) {
                        hashMap1.put(Constant.COLLEGE_NAME, hashMap.get("specialization").toString());
                    }
                    if (hashMap.containsKey(Constant.COLLEGE_NAME)) {
                        hashMap1.put(Constant.UNIVERSITY_NAME, hashMap.get(Constant.COLLEGE_NAME).toString());
                    }
                    if (hashMap.containsKey(Constant.COUNTRY_NAME)) {
                        hashMap1.put(Constant.COUNTRY_NAME, hashMap.get(Constant.COUNTRY_NAME).toString());
                    }
                    if (hashMap.containsKey(Constant.DEGREE_NAME)) {
                        hashMap1.put(Constant.DEGREE_NAME, hashMap.get(Constant.DEGREE_NAME).toString());
                    }
                    if (hashMap.containsKey(Constant.FIRST_NAME)) {
                        hashMap1.put(Constant.FIRST_NAME, hashMap.get(Constant.FIRST_NAME).toString());
                    }
                    if (hashMap.containsKey(Constant.LAST_NAME)) {
                        hashMap1.put(Constant.LAST_NAME, hashMap.get(Constant.LAST_NAME).toString());
                    }
                    if (hashMap.containsKey(Constant.YEAR_NAME)) {

                        hashMap1.put(Constant.YEAR_NAME, hashMap.get(Constant.YEAR_NAME).toString());
                    }
                    hashMap1.put("KEY", dataSnapshot1.getKey());


                    //  try {

                    if (hashMap.containsKey(Constant.PROFILE_PICTURE)) {
                        hashMap1.put(Constant.PROFILE_PICTURE, hashMap.get(Constant.PROFILE_PICTURE).toString());
                    }
                    //  } catch (Exception e) {
                    //  }


                    dataList.add(hashMap1);

                    // dataList.add(hashMap.get(Constant.KEY_NAME).toString());

                    courseKey.add(dataSnapshot1.getKey());
                    // hashMap1.put(Constant.KEY_NAME, hashMap.get(dataSnapshot1.getKey()));



                  /*  } catch (Exception e) {
                        e.printStackTrace();

                    }*/

                }


               /* contactAdaptor = new NetworkAdapter(dataList, getContext(), courseKey, selected_option);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                contact_rv.setLayoutManager(mLayoutManager);
                contact_rv.setAdapter(contactAdaptor);
                contact_rv.setEmptyView(empty_tv);*/


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }









   /* private synchronized void getEventFromMeeting() {

        Log.d("bhxc", "sds");
        meetingRef.child("Meeting").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                main_eventList = new ArrayList<HashMap>();
                meetingList = new ArrayList<HashMap>();

                Log.d("bhxc", main_eventList + "");

                Iterator<DataSnapshot> iteratedValue = dataSnapshot.getChildren().iterator();

                while (iteratedValue.hasNext()) {
                    DataSnapshot dataSnapshot1 = iteratedValue.next();
                    String student_key = dataSnapshot1.getKey();
                    Log.d("student_key ", "= " + student_key);


                    DataSnapshot dataSnapshot2 = dataSnapshot1.child(Constant.INVITED_STUDENTS);
                    if (dataSnapshot2.child(user_key).exists()) {
                        String userMeetingKey = dataSnapshot2.child(user_key).getKey();
                        String userMeetingValue = dataSnapshot2.child(user_key).getValue().toString();
                        if (userMeetingValue.equals(Constant.ACCEPTED_NAME)) {
                            HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                            hashMap.put("type", "Meeting");
                            hashMap.put("event_key", student_key);


                            meetingList.add(hashMap);
                        }

                    }


                }


                Collections.reverse(main_eventList);

                eventAdaptor.addList(main_eventList);

                // ShowDialog.hideDialog();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }*/

    @Override
    public void onRecyClick(View view, int position) {

        switch (view.getId()) {
            case R.id.editevent_tv:

                List<String> list = (List<String>) view.getTag();
                Log.d("zcxz", list + "");
                String data = view.getTag().toString();
                String event[] = data.split(";");
                Log.d("df", data);
               /* String key = event[0];
                String type = event[1];
                String date = event[2];
                String time = event[3];
                String location = event[4];
                String price = event[5];
                String title = event[6];
                String desc = event[7];
                String image_url = event[8];
                String admin = event[9];*/

                String key = list.get(0);
                String type = list.get(1);
                String date = list.get(2);
                String time = list.get(3);
                String location = list.get(4);
                String price = list.get(5);
                String title = list.get(6);
                String desc = list.get(7);
                String image_url1 = list.get(8);
                String image_url2 = list.get(9);
                String image_url3 = list.get(10);
                String admin = list.get(11);
                String studentKey = list.get(12);

                actionSheet(key, type, date, time, location, price, title, desc, image_url1, image_url2, image_url3, admin, studentKey);

                break;

        }


    }

    @Override
    public void onLongClick(View view, int position) {

    }

    private void actionSheet(final String key, final String type, final String date, final String time, final String location, final String price, final String title, final String desc, final String image1, final String image2, final String image3, final String admin, final String studentList) {


        final String[] stringItems = {"Cancel Event", "Edit"};

        final ActionSheetDialog dialog = new ActionSheetDialog(getContext(), stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {


                    ShowDialog.getInstance().showDeleteDialog(getContext(), type, key, 6);

                } else if (position == 1) {

                    Bundle bundle = new Bundle();
                    bundle.putString("Key", key);
                    bundle.putString("date", date);
                    bundle.putString("time", time);
                    bundle.putString("location", location);
                    bundle.putString("price", price);
                    bundle.putString("title", title);
                    bundle.putString("desc", desc);
                    bundle.putString("image1", image1);
                    bundle.putString("image2", image2);
                    bundle.putString("image3", image3);
                    bundle.putString("admin", admin);
                    bundle.putString("type", type);
                    bundle.putString("studentList", studentList);

                    eventFragment = new EventFragment();
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), eventFragment, "Event_Fragment", bundle);

                }

                dialog.dismiss();
            }
        });
    }

    private void deleteDialog(Context context, boolean b) {


        delete_dialog = new Dialog(context);
        delete_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        delete_dialog.setContentView(R.layout.dialog_logout);
        delete_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        delete_dialog.show();

        // container_fl.getForeground().setAlpha(220);

        deletedone_tv = (TextView) delete_dialog.findViewById(R.id.logdone_tv);
        deleteCancel_tv = (TextView) delete_dialog.findViewById(R.id.logCancel_tv);
        delete_tv = (TextView) delete_dialog.findViewById(R.id.log_tv);
        View view = (View) delete_dialog.findViewById(R.id.view);

        if (b) {
            delete_tv.setText("Do you want to delete?");
            deletedone_tv.setOnClickListener(this);
            deleteCancel_tv.setOnClickListener(this);
        } else {
            delete_tv.setText("Event has been deleted");
            deletedone_tv.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            deleteCancel_tv.setText("Ok");
            deleteCancel_tv.setOnClickListener(this);

        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.logdone_tv:

                delete_dialog.dismiss();

                deleteDialog(getContext(), false);

                break;

            case R.id.logCancel_tv:

                delete_dialog.dismiss();

                break;

        }
    }

}
