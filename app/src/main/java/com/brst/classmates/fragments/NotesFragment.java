package com.brst.classmates.fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.brst.classmates.R;
import com.brst.classmates.activity.CropImageActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.iceteck.silicompressorr.SiliCompressor;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.classses.ShowDialog.networkDialog;
import static com.google.android.gms.cast.MediaTrack.TYPE_VIDEO;
import static com.seatgeek.placesautocomplete.Constants.LOG_TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotesFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener {


    View view = null;
    EditText lecture_et;
    TextView course_tv, addLecture_tv;
    ImageView lecture_iv;
    VideoView lecture_vd;

    String courseKey;
    private String picturePath = "";
    private File destination;
    Uri imageuri, imageUploadUri, downloadImageUri;

    Uri capturedUri = null;
    Uri compressUri = null;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseStorage firebaseStorage;


    private int VIDEO_CAPTURE = 101, CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 102, CROP_IMAGE = 103;

    public NotesFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_notes, container, false);
        }

        bindView();

        setListener();

        initFirebase();

        return view;
    }

    private void initFirebase() {

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("lecture");
        firebaseStorage = FirebaseStorage.getInstance();
    }

    private void setListener() {

        course_tv.setOnClickListener(this);
        lecture_iv.setOnClickListener(this);
        addLecture_tv.setOnClickListener(this);


        lecture_vd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                lectureSheet();
                return false;
            }
        });

    }

    private void bindView() {

        course_tv = (TextView) view.findViewById(R.id.course_tv);
        addLecture_tv = (TextView) view.findViewById(R.id.addLecture_tv);

        lecture_et = (EditText) view.findViewById(R.id.lecture_et);

        lecture_iv = (ImageView) view.findViewById(R.id.lecture_iv);
        lecture_vd = (VideoView) view.findViewById(R.id.lecture_vd);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.course_tv:

                CourseFragment courseFragment = new CourseFragment();
                courseFragment.setTargetFragment(NotesFragment.this, 1300);
                ReplaceFragment.replace((WelcomeActivity) getActivity(), courseFragment, "courseAssign_Fragment");

                break;


            case R.id.lecture_iv:

                lectureSheet();

                break;

            case R.id.addLecture_tv:

                if (validate(v)) {
                    // uploadImage();

                    ShowDialog.showDialog(getContext());

                    new AsyncCaller().execute();
                }

                break;

        }


    }

    private void uploadImage() {

        //   ShowDialog.showDialog(getContext());

        firebaseStorage.getReference("image_" + System.currentTimeMillis())
                .putFile(imageUploadUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                downloadImageUri = taskSnapshot.getDownloadUrl();


                addLectureInFirebase();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Log.d("xfx", e.getMessage());

            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                Log.d("fdsf", taskSnapshot.getUploadSessionUri() + "");
                ShowDialog.hideDialog();

            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {

                Log.d("dsfs", taskSnapshot.getUploadSessionUri() + "");
                Log.d("dsfs", "fgfdg");

            }
        });

    }

    private void addLectureInFirebase() {

        HashMap hashMap = new HashMap();
        hashMap.put("courseName", course_tv.getText().toString());
        hashMap.put("lectureName", lecture_et.getText().toString());
        hashMap.put("lectureShot", downloadImageUri.toString());

        databaseReference.child(courseKey).push().updateChildren(hashMap)
                .addOnSuccessListener(new OnSuccessListener() {
                    @Override
                    public void onSuccess(Object o) {

                        Dialog alertDialog = new AlertDialog.Builder(getContext())
                                .setTitle("Classmate")
                                .setMessage("Lecture added successfully.")
                                .setCancelable(false)
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setIcon(R.mipmap.imagesuccess)
                                .show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Log.d("sssf", e.getMessage());
                ShowDialog.hideDialog();
            }
        });
    }

    private boolean validate(View v) {
        if (course_tv.getText().toString().isEmpty()) {
            ShowDialog.showSnackbarError(v, "Please enter course name.", getContext());
            return false;
        } else if (lecture_et.getText().toString().isEmpty()) {
            ShowDialog.showSnackbarError(v, "Please enter lecture name.", getContext());
            return false;
        } else if (imageUploadUri == null) {
            ShowDialog.showSnackbarError(v, "Please add lecture shot or lecture recording", getContext());
            return false;
        }

        return true;
    }


    private void lectureSheet() {


        final String[] stringItems = {"Lecture shots", "Lecture recordings"};

        final ActionSheetDialog dialog = new ActionSheetDialog(getContext(), stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                    String[] permission = {Manifest.permission.CAMERA};

                    requestAppPermissions(permission, R.string.permission, 55);


                } else if (position == 1) {

                    String[] permission = {Manifest.permission.CAMERA};

                    requestAppPermissions(permission, R.string.permission, 54);


                }

                dialog.dismiss();
            }
        });
    }

    private void captureImage() {

        picturePath = "";
        destination = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = Constant.IMAGE_NAME;
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picturePath = String.valueOf(new File(f, System.currentTimeMillis() + getString(R.string.formattype_name)));
        destination = new File(picturePath);

        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination);
        } else {
            imageuri = Uri.fromFile(destination);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_tv.setText("Lecture");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1300) {
                Bundle bundle = data.getBundleExtra(Constant.BUNDLE_NAME);
                String courseName = bundle.getString(Constant.COURSE_NAME);
                courseKey = bundle.getString(Constant.COURSE_KEY);

                course_tv.setText(courseName);
            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                Intent intent = new Intent(getContext(), CropImageActivity.class);
                intent.putExtra(Constant.URIIMAGE_NAME, imageuri.toString());
                startActivityForResult(intent, CROP_IMAGE);
            } else if (requestCode == CROP_IMAGE && resultCode == RESULT_OK) {

                Bitmap bitmap = null;
                try {
                    bitmap = BitmapFactory.decodeStream(getContext().openFileInput(getString(R.string.imagemy_name)));
                    imageUploadUri = getImageUri(getContext(), bitmap);

                    setImage();


                } catch (FileNotFoundException e) {
                    e.printStackTrace();


                }


            } else if (requestCode == VIDEO_CAPTURE) {
                Log.d("dfs","sdf");
                Log.d("fsd", data.getData().toString().length() + "");
                File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getContext().getPackageName() + "/media/videos");
                if (f.mkdirs() || f.isDirectory())
                    //compress and output new video specs
                    new VideoCompressAsyncTask(getContext()).execute(data.getData().toString(), f.getPath());
            }
        }
    }


    public class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
        }



        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            try {

                filePath = SiliCompressor.with(mContext).compressVideo(paths[0], paths[1]);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            Log.d("sf", compressedFilePath.length() + "");

            Display display = getActivity().getWindowManager().getDefaultDisplay();
            int width = display.getWidth();
            int height = display.getHeight();

            lecture_vd.setLayoutParams(new FrameLayout.LayoutParams(width, height));

            lecture_vd.setVideoPath(compressedFilePath);
            lecture_vd.start();


        }
    }

    private void setImage() {

        Glide.with(getContext()).load(imageUploadUri).into(lecture_iv);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, Constant.TITLE_NAME, null);
        return Uri.parse(path);
    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 54) {
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            if (intent.resolveActivity(getContext().getPackageManager()) != null) {

                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                //    capturedUri = Uri.fromFile(createMediaFile(TYPE_VIDEO));
                // intent.putExtra(MediaStore.EXTRA_OUTPUT, capturedUri);

                startActivityForResult(intent, VIDEO_CAPTURE);


            }


            lecture_iv.setVisibility(View.GONE);
            lecture_vd.setVisibility(View.VISIBLE);

        } else if (requestCode == 55) {

            captureImage();

            lecture_iv.setVisibility(View.VISIBLE);
            lecture_vd.setVisibility(View.GONE);
        }

    }


    public static boolean isInternetAccessible() {

        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Couldn't check internet connection", e);
        }

        return false;


    }

    private class AsyncCaller extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            boolean networkAccess = isInternetAccessible();
            return networkAccess;
        }

        @Override
        protected void onPostExecute(Boolean networkAccess) {
            super.onPostExecute(networkAccess);


            if (!networkAccess) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {

                        networkDialog(getContext());
                        ShowDialog.hideDialog();
                    }
                }
            } else {
                uploadImage();
            }
        }
    }
}
