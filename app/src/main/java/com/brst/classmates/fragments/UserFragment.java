package com.brst.classmates.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.StrangerAdaptor;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserFragment extends Fragment implements View.OnClickListener, ClickRecyclerInterface {


    RecyclerView userList_rv;

    String userList, stranger, loginUserKey;
    TextView users_tv;


    ImageView back_msg_iv, addcourse_iv;

    List<String> stringList = new ArrayList<>();


    View view = null;

    StrangerAdaptor strangerAdaptor;

    List<String> userName = new ArrayList<>();
    List<String> userKey = new ArrayList<>();
    // List<String> userPic = new ArrayList<>();
    List<HashMap> userPic = new ArrayList<>();

    public UserFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_user, container, false);
        }
        loginUserKey = StoreData.getUserKeyFromSharedPre(getContext());

        Bundle bundle = getArguments();

        if (bundle != null) {

            userList = bundle.getString(Constant.USER_LIST);

            convertStringToHasHMap(userList);
        }


        checkNetwork();


        bindViews(view);

        return view;
    }

    private void checkNetwork() {

        if (!NetworkCheck.isNetworkAvailable(getContext())) {
            ShowDialog.networkDialog(getContext());

            getProfilePic();
        } else {
            getProfilePic();
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    private void getProfilePic() {

        userPic.clear();
        Log.d("qwertydata", userPic + "");
        userPic = new ArrayList<>();
        for (int i = 0; i < userKey.size(); i++) {

            FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).child(userKey.get(i)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap hashMap = new HashMap();
                    if (dataSnapshot.child(Constant.PROFILE_PICTURE).exists()) {
                        //  userPic.add(dataSnapshot.child(Constant.PROFILE_PICTURE).getValue().toString());
                        hashMap.put(Constant.PROFILE_PICTURE, dataSnapshot.child(Constant.PROFILE_PICTURE).getValue().toString());
                    } else {
                        ;
                        //  userPic.add("null");
                        hashMap.put(Constant.PROFILE_PICTURE, "null");
                    }
                    if (dataSnapshot.child(Constant.COLLEGE_NAME).exists()) {
                        //  userPic.add(dataSnapshot.child(Constant.PROFILE_PICTURE).getValue().toString());
                        hashMap.put(Constant.COLLEGE_NAME, dataSnapshot.child(Constant.COLLEGE_NAME).getValue().toString());
                    } else {
                        //  userPic.add("null");
                        hashMap.put(Constant.COLLEGE_NAME, "null");
                    }
                    if (dataSnapshot.child(Constant.UNIVERSITY_NAME).exists()) {
                        //  userPic.add(dataSnapshot.child(Constant.PROFILE_PICTURE).getValue().toString());
                        hashMap.put(Constant.UNIVERSITY_NAME, dataSnapshot.child(Constant.UNIVERSITY_NAME).getValue().toString());
                    } else {
                        //  userPic.add("null");
                        hashMap.put(Constant.UNIVERSITY_NAME, "null");
                    }
                    if (dataSnapshot.child(Constant.COUNTRY_NAME).exists()) {
                        //  userPic.add(dataSnapshot.child(Constant.PROFILE_PICTURE).getValue().toString());
                        hashMap.put(Constant.COUNTRY_NAME, dataSnapshot.child(Constant.COUNTRY_NAME).getValue().toString());
                    } else {
                        //  userPic.add("null");
                        hashMap.put(Constant.COUNTRY_NAME, "null");
                    }
                    if (dataSnapshot.child(Constant.DEGREE_NAME).exists()) {
                        //  userPic.add(dataSnapshot.child(Constant.PROFILE_PICTURE).getValue().toString());
                        hashMap.put(Constant.DEGREE_NAME, dataSnapshot.child(Constant.DEGREE_NAME).getValue().toString());
                    } else {
                        //  userPic.add("null");
                        hashMap.put(Constant.DEGREE_NAME, "null");
                    }
                    if (dataSnapshot.child(Constant.YEAR_NAME).exists()) {
                        //  userPic.add(dataSnapshot.child(Constant.PROFILE_PICTURE).getValue().toString());
                        hashMap.put(Constant.YEAR_NAME, dataSnapshot.child(Constant.YEAR_NAME).getValue().toString());
                    } else {
                        //  userPic.add("null");
                        hashMap.put(Constant.YEAR_NAME, "null");
                    }
                    userPic.add(hashMap);

                    if (userPic.size() == userKey.size()) {
                        strangerAdaptor.addData(userPic);
                        strangerAdaptor.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


    }

    private void bindViews(View view) {

        userList_rv = (RecyclerView) view.findViewById(R.id.userList_rv);
        users_tv = (TextView) view.findViewById(R.id.users_tv);
        back_msg_iv = (ImageView) view.findViewById(R.id.back_msg_iv);
        addcourse_iv = (ImageView) view.findViewById(R.id.addcourse_iv);

        back_msg_iv.setOnClickListener(this);

        users_tv.setVisibility(View.GONE);
        addcourse_iv.setVisibility(View.GONE);

        strangerAdaptor = new StrangerAdaptor(getContext(), userName, userPic, userKey);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        userList_rv.setLayoutManager(mLayoutManager);
        userList_rv.setAdapter(strangerAdaptor);

        strangerAdaptor.setClickListener(this);

    }

    private Map<String, String> convertStringToHasHMap(String userList) {

        //userKey = new ArrayList<>();
        userList = userList.substring(1, userList.length() - 1);           //remove curly brackets
        String[] keyValuePairs = userList.split(",");              //split the string to creat key-value pairs
        Map<String, String> map = new HashMap<>();

        for (String pair : keyValuePairs)                        //iterate over the pairs
        {
            String[] entry = pair.split("=");                   //split the pairs to get key and value
            map.put(entry[0].trim(), entry[1].trim());          //add them to the hashmap and trim whitespaces
        }

        for (String key : map.keySet()) {
            String input = map.get(key);
            String output = input.substring(0, 1).toUpperCase() + input.substring(1);

            // if (!key.equals(loginUserKey)) {

            userName.add(output);

            userKey.add(key);


            // }
        }

        Log.d("sds", userName + "-----" + userKey);

        return map;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_msg_iv:

                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
        }
    }

    final int blockedLimit = 5;

    @Override
    public void onRecyClick(final View view, final int position) {


        switch (view.getId()) {
            case R.id.report_tv:

                if (!NetworkCheck.isNetworkAvailable(getContext())) {
                    ShowDialog.networkDialog(getContext());
                } else {
                    final String key = userKey.get(position);

                    final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).child(key);


                    Query query = databaseReference;


                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if (dataSnapshot.child("stranger").exists()) {
                                stranger = dataSnapshot.child("stranger").getValue().toString();
                            }
                            stringList = new ArrayList<String>(Arrays.asList(stranger.split(",")));
                            boolean reportedByMe = stringList.contains(loginUserKey);


                            if (stringList.size() <= blockedLimit - 1) {
                                if (stranger.isEmpty()) {
                                    stranger = loginUserKey;
                                    databaseReference.child("stranger").setValue(stranger);

                                    Toast.makeText(getContext(), "User Reported", Toast.LENGTH_SHORT).show();

                                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());


                                } else if (!reportedByMe) {
                                    if (stringList.size() == blockedLimit - 1) {
                                        databaseReference.child("status").setValue("block");

                                    }

                                    stranger = stranger + "," + loginUserKey;
                                    databaseReference.child("stranger").setValue(stranger);
                                    Toast.makeText(getContext(), "User Reported", Toast.LENGTH_SHORT).show();


                                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                break;
        }
    }

    @Override
    public void onLongClick(View view, int position) {

    }


}
