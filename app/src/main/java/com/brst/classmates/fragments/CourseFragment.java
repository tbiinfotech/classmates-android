package com.brst.classmates.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.brst.classmates.R;
import com.brst.classmates.activity.CropImageActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.CourseAdaptor;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermissionF;
import com.brst.classmates.classses.Application;
import com.brst.classmates.classses.ConnectivityReceiver;
import com.brst.classmates.classses.FirebaseData;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;

import com.bumptech.glide.request.RequestOptions;

import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.HttpURLConnection;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;

import static com.brst.classmates.activity.WelcomeActivity.context;
import static com.brst.classmates.classses.NetworkCheck.isNetworkAvailable;
import static com.brst.classmates.classses.ShowDialog.networkDialog;
import static com.brst.classmates.classses.ShowDialog.showSnackbarError;
import static com.brst.classmates.fragments.AddCourseFragment.hideSoftKeyboard;
import static com.google.android.gms.internal.zzagy.runOnUiThread;
import static com.seatgeek.placesautocomplete.Constants.LOG_TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseFragment extends AbsRuntimeMarshmallowPermissionF implements View.OnClickListener, ClickRecyclerInterface, TextWatcher, ConnectivityReceiver.ConnectivityReceiverListener {

    RecyclerViewEmptySupport course_rv;

    TextView universitycourse_tv, emptyview;

    List<HashMap<String, String>> courseSearchtList = new ArrayList<>();
    List<String> courseKey = new ArrayList<>();
    List<String> courseSearchKey = new ArrayList<>();

    // ImageView drawer_iv;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Boolean exist;

    EditText course_at;
    Boolean check = false;

    Uri imageuri, imageUploadUri, downloadImageUri;

    String picture_Path;
    File destination_path;

    Fragment fragmentCourseList, fragmentGroup, fragment_Course, fragment_Courseassign;

    AddCourseFragment addCourseFragment;
    AddAssignmentFragment addAssignmentFragment;
    Accept_RejectFragment accept_rejectFragment;
    CourseAdaptor courseAdaptor;

    SearchStudentFragment searchStudentFragment;

    Dialog lectureDialog;

    ImageView uniSearch_iv;

    List<HashMap<String, String>> dataList = new ArrayList<>();

    List<String> groupList;

    String groupName, universityName, university, collegeKey;

    Bundle bundle;
    View view = null;

    public CourseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {

            view = inflater.inflate(R.layout.fragment_course, container, false);
        }

        university = SharedPreference.getInstance().getData(getContext(), Constant.UNIVERSITY_NAME);
        collegeKey = StoreData.getCollegeKeyFromSharedPre(getContext());


        bindViews(view);

        bundle = getArguments();

        if (bundle != null) {

            if (bundle.containsKey(Constant.CREATEGROUP_NAME)) {
                groupName = bundle.getString(Constant.CREATEGROUP_NAME);
            }
        }

        networkCheck();

        initFragment();

        setListener();

        checkVisibleFragment();

        setAdaptor();


        return view;
    }

    private void checkVisibleFragment() {

        fragmentCourseList = getFragmentManager().findFragmentByTag("CourseList_Fragment");
        fragmentGroup = getFragmentManager().findFragmentByTag("CourseGroup_Fragment");
        fragment_Course = getFragmentManager().findFragmentByTag("Course_Fragment");
        fragment_Courseassign = getFragmentManager().findFragmentByTag("courseAssign_Fragment");


    }


    private void networkCheck() {


        if (!isNetworkAvailable(getContext())) {

            networkDialog(getContext());


            getCourseList();

        } else {

            getCourseList();


        }
    }

    private void getCourseList() {

        if (NetworkCheck.isNetworkAvailable(getContext())) {

            ShowDialog.showDialog(getContext());
        }

        firebaseDatabase = FirebaseDatabase.getInstance();
        // databaseReference = firebaseDatabase.getReference("university").child(university);
        databaseReference = firebaseDatabase.getReference("subjects").child(collegeKey);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                dataList = new ArrayList<HashMap<String, String>>();
                courseKey = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                    HashMap hashMap = (HashMap) dataSnapshot1.getValue();

                    HashMap hashMap1 = new HashMap();
                    // if (!hashMap.get(Constant.COURSE_NAME).equals("")) {
                    if (dataSnapshot1.child("subject").exists()) {
                        hashMap1.put(Constant.COURSE_NAME, hashMap.get("subject").toString());
                        hashMap1.put(Constant.KEY_NAME, hashMap.get(Constant.KEY_NAME).toString());
                        dataList.add(hashMap1);
                        // dataList.add(hashMap.get(Constant.KEY_NAME).toString());

                        courseKey.add(dataSnapshot1.getKey());

                    }

                }

                courseAdaptor.addDataInList(dataList, courseKey);

                courseAdaptor.notifyDataSetChanged();
                //     if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }
                //  }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }


    private void bindViews(View view) {

        course_rv = (RecyclerViewEmptySupport) view.findViewById(R.id.course_rv);
        course_at = (EditText) view.findViewById(R.id.course_at);
        uniSearch_iv = (ImageView) view.findViewById(R.id.uniSearch_iv);
        // universitycourse_tv = (TextView) view.findViewById(R.id.universitycourse_tv);
        emptyview = (TextView) view.findViewById(R.id.result);

        // universitycourse_tv.setText("Courses from " + university + " university");

        course_at.addTextChangedListener(this);

    }


    private void setAdaptor() {

        courseAdaptor = new CourseAdaptor(dataList, courseKey, fragment_Course != null && !fragment_Course.isVisible(), getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        course_rv.setLayoutManager(mLayoutManager);
        course_rv.setAdapter(courseAdaptor);
        course_rv.setEmptyView(emptyview);


    }

    private void initFragment() {

        addCourseFragment = new AddCourseFragment();
        addAssignmentFragment = new AddAssignmentFragment();
        accept_rejectFragment = new Accept_RejectFragment();
        searchStudentFragment = new SearchStudentFragment();
    }

    private void setListener() {
        ((WelcomeActivity) getActivity()).header_iv.setOnClickListener(this);

        uniSearch_iv.setOnClickListener(this);


    }

    @Override
    public void onResume() {
        super.onResume();


        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        // ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_iv.setImageResource(R.mipmap.add);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);
        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        if (fragment_Course != null && fragment_Course.isVisible()) {
            ((WelcomeActivity) getActivity()).header_tv.setText("Subjects");

            ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.VISIBLE);
            courseAdaptor.setClickListener(this);

        } else {

            ((WelcomeActivity) getActivity()).header_tv.setText("Select Subjects");
            ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
            courseAdaptor.setClickListener(this);
        } /*else {
            ((WelcomeActivity) getActivity()).header_tv.setText("Courses");
            ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
            courseAdaptor.setClickListener(this);
        }*/

        Application.getInstance().setConnectivityListener(this);


        //    new AsyncCaller().execute();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.header_iv:

                hideSoftKeyboard(getContext());
                course_at.getText().clear();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.UNIVERSITY_NAME, university);
                addCourseFragment.setArguments(bundle);

                ReplaceFragment.replace(((WelcomeActivity) getActivity()), addCourseFragment, "AddCourse_Fragment");


                break;


        }

    }


    EditText editText;

    @Override
    public void onRecyClick(View view, int position) {

        switch (view.getId()) {

            case R.id.course_tv:

                String userkey=StoreData.getUserKeyFromSharedPre(getContext());
            /*    if (fragmentCourseList != null && fragmentCourseList.isVisible()) {

                    lectureSheet();

                } else if (fragment_Courseassign != null && fragment_Courseassign.isVisible()) {

                    final String courseName = ((List<String>) view.getTag()).get(0).toString();
                    final String courseKeyValue = ((List<String>) view.getTag()).get(1).toString();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.COURSE_NAME, courseName);
                    Assign_ExamFragment assign_examFragment = new Assign_ExamFragment();
                    assign_examFragment.setArguments(bundle);
                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), assign_examFragment, "AssignExam_Fragment");

                }*/
                String keycourse = view.getTag().toString();
                HashMap hashMap1=new HashMap();
                hashMap1.put(keycourse,Constant.COURSE_NAME);
                FirebaseDatabase.getInstance().getReference(Constant.COURSE_NAME).child(userkey).updateChildren(hashMap1);

                break;

            case R.id.delete_iv:

                String key = view.getTag().toString();
                ShowDialog.getInstance().showDeleteDialog(getContext(), key, university, 0);


                break;

            case R.id.add_iv:


                final String courseName = ((List<String>) view.getTag()).get(0).toString();
                final String courseKeyValue = ((List<String>) view.getTag()).get(1).toString();

                Log.d("courseKey", courseKeyValue);

                if (fragmentGroup != null && fragmentGroup.isVisible()) {


                    createGroup(courseName, courseKeyValue);

                }

                if (fragmentCourseList != null && fragmentCourseList.isVisible()) {

                    lectureSheet();

                } else if (fragment_Courseassign != null && fragment_Courseassign.isVisible()) {

                    HashMap hashMap = new HashMap();
                    hashMap.put(courseKeyValue, Constant.COURSE_NAME);

                    FirebaseDatabase.getInstance().getReference(Constant.COURSE_NAME).child(StoreData.getUserKeyFromSharedPre(getContext()))
                            .updateChildren(hashMap);


                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.COURSE_KEY, courseKeyValue);
                    bundle.putString(Constant.COURSE_NAME, courseName);

                    Intent intent = new Intent(context, WelcomeActivity.class);
                    intent.putExtra(Constant.BUNDLE_NAME, bundle);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
                    ReplaceFragment.popBackStack((WelcomeActivity) getActivity());

                }

                break;
        }
    }

    private void createGroup(String courseName, String courseKeyValue) {


        groupList = new ArrayList<>();

        editText = ShowDialog.getInstance().groupDialog(getContext(), onClickListener, getOnClickListener);

        editText.setTag(courseName + "," + courseKeyValue);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 19) {

                    showSnackbarError(editText, "You can't add more than 20 characters.", getContext());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void onPause() {
        super.onPause();

        check = false;

    }


    View.OnClickListener cameraClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();
            capture_image();

        }
    };

    View.OnClickListener galleryClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();

            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            requestAppPermissions(permission, R.string.permission, 54);

        }
    };


    private void capture_image() {
        picture_Path = "";
        destination_path = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = "image";
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picture_Path = String.valueOf(new File(f, System.currentTimeMillis() + ".jpg"));
        destination_path = new File(picture_Path);
        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".my.package.name.provider", destination_path);
        } else {
            imageuri = Uri.fromFile(destination_path);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, Constant.CAMERA_CODE);
    }

    View.OnClickListener getOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.cameraDialog(getContext(), galleryClick, cameraClick);
        }
    };

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Context context = ShowDialog.getInstance().group_dialog.getContext();

            InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);


            final String groupname = editText.getText().toString();

            if (groupname.startsWith(" ") || groupname.isEmpty()) {

                ShowDialog.showSnackbarError(v, "Please enter group name.", getContext());

            } else if (!isNetworkAvailable(getContext())) {
                ShowDialog.networkDialog(getContext());
            } else {

                ShowDialog.showDialog(getContext());
                AsyncCaller asyncCaller = new AsyncCaller(groupname);
                asyncCaller.execute();

            }


        }
    };

    private void addGroup(final String groupname) {

        final String[] abc = editText.getTag().toString().split(",");

        FirebaseDatabase.getInstance().getReference(Constant.CREATEGROUP_NAME).child(abc[1]).orderByChild(Constant.GROUP_NAME).equalTo(groupname).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {
                    //  Toast.makeText(getContext(), "This group name already exist in " + abc[0], Toast.LENGTH_SHORT).show();

                    if (getContext() != null) {
                        ShowDialog.getInstance().showExitDialog(getContext());
                    }


                } else {
                    ShowDialog.getInstance().group_dialog.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.COURSE_KEY, abc[1]);
                    bundle.putString(Constant.GROUP_NAME, groupname);
                    bundle.putString("mode", "other");

                    if (downloadImageUri != null && !downloadImageUri.equals(Uri.EMPTY)) {
                        bundle.putString(Constant.PROFILE_PICTURE, downloadImageUri.toString());
                    }
                    SearchStudentFragment searchStudentFragment = new SearchStudentFragment();
                    searchStudentFragment.setArguments(bundle);
                    if (fragmentGroup != null && fragmentGroup.isVisible()) {
                        ReplaceFragment.popBackStackAll((WelcomeActivity) getActivity(), 2);
                        ReplaceFragment.replace((WelcomeActivity) getActivity(), searchStudentFragment, "SearchStudent_Fragment");
                    }
                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void imageUpload(final String groupname) {

        ShowDialog.showDialog(getContext());

        FirebaseStorage.getInstance().getReference().child(Constant.IMAGE_NAME + "_" + imageUploadUri.getLastPathSegment())
                .putFile(imageUploadUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                        ShowDialog.hideDialog();

                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();


                        ShowDialog.hideDialog();


                        addGroup(groupname);


                    }
                });

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            check = true;
        }

        Log.e("ABV", "HSGBH");
    }

    private void lectureSheet() {


        final String[] stringItems = {"Choose Option ", "Lecture shots", "Lecture recordings"};

        final ActionSheetDialog dialog = new ActionSheetDialog(getContext(), stringItems, null);
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 1) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivity(intent);

                } else if (position == 2) {

                   /* Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
                    startActivity(intent);*/
                }

                dialog.dismiss();
            }
        });
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {


    }

    @Override
    public void onTextChanged(CharSequence s1, int start, int before, int count) {


    }

    @Override
    public void afterTextChanged(Editable s) {


        String s2;
        courseSearchtList.clear();
        courseSearchKey.clear();
        String s1 = s.toString().toLowerCase();

        s1 = s1.replace(" ", "");

        for (int i = 0; i < dataList.size(); i++) {
            HashMap hashMap = dataList.get(i);
            String course = hashMap.get(Constant.COURSE_NAME).toString();

            String username = course.toLowerCase().replace(" ", "");

            if (username.contains(s1)) {
                courseSearchtList.add(dataList.get(i));
                courseSearchKey.add(courseKey.get(i));

            }

        }
        courseAdaptor.addDataInList(courseSearchtList, courseSearchKey);
        courseAdaptor.notifyDataSetChanged();

    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 54) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, Constant.GALLERY_CODE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.GALLERY_CODE && resultCode == RESULT_OK) {


            Uri image = data.getData();

            Intent intent = new Intent(getContext(), CropImageActivity.class);
            intent.putExtra("imageuri", image.toString());
            startActivityForResult(intent, Constant.CROP_IMAGE);


        } else if (requestCode == Constant.CAMERA_CODE && resultCode == RESULT_OK) {

            Intent intent = new Intent(getContext(), CropImageActivity.class);
            intent.putExtra("imageuri", imageuri.toString());
            startActivityForResult(intent, Constant.CROP_IMAGE);

        } else if (requestCode == Constant.CROP_IMAGE && resultCode == RESULT_OK) {

            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getActivity().openFileInput("myImage"));
                //  bitmap=decodeUri()
                imageUploadUri = getImageUri(getContext(), bitmap);

                setProfileImage();

            } catch (FileNotFoundException e) {
                e.printStackTrace();

            }

        }


    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(selectedImage), null, o2);
    }

     /*--------------------------get the uri from bitmap----------------------------*/

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, Constant.TITLE_NAME, null);
        return Uri.parse(path);
    }

    /*-------------------------------------set image into imageciew------------------------------*/

    private void setProfileImage() {

        Glide.with(CourseFragment.this).load(imageUploadUri).apply(RequestOptions.circleCropTransform()).into(ShowDialog.getInstance().group_iv);

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


        if (!isConnected) {

            try {
                ShowDialog.networkDialog(getContext());
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }
            } catch (Exception e) {

            }
        }
    }


    public static boolean isInternetAccessible(Context context) {

        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Couldn't check internet connection", e);
        }

        return false;


    }

    private class AsyncCaller extends AsyncTask<Void, Void, Boolean> {

        String groupname;

        public AsyncCaller(String groupname) {

            this.groupname = groupname;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            boolean networkAccess = isInternetAccessible(getContext());
            return networkAccess;
        }

        @Override
        protected void onPostExecute(Boolean networkAccess) {
            super.onPostExecute(networkAccess);


            if (!networkAccess) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {

                        networkDialog(getContext());
                        ShowDialog.hideDialog();
                    }
                }
            } else {

                if (ShowDialog.dialog.isShowing()) {
                    ShowDialog.hideDialog();
                }
                if (imageUploadUri != null && !imageUploadUri.equals(Uri.EMPTY)) {


                    imageUpload(groupname);

                } else {

                    addGroup(groupname);


                }
            }
        }
    }
}
