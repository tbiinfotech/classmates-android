package com.brst.classmates.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.InviteCourseAdaptor;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.activity.WelcomeActivity.context;
import static com.brst.classmates.classses.NetworkCheck.isNetworkAvailable;
import static com.brst.classmates.classses.ShowDialog.networkDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class InviteCourseFragment extends Fragment implements View.OnClickListener, TextWatcher {

    RecyclerView invitecourse_rv;

    // ImageView drawer_iv;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, eventdatabaseReference;
    AddCourseFragment addCourseFragment;
    AddAssignmentFragment addAssignmentFragment;
    Accept_RejectFragment accept_rejectFragment;
    InviteCourseAdaptor inviteCourseAdaptor;
    TextView invitecourse_tv;

    EditText inviteCourse_tv;

    String eventKey;

    Fragment fragment;

    List<String> listStudentKey = new ArrayList<>();

    ArrayList<HashMap<String, String>> courseSearchtList = new ArrayList<>();

    SearchStudentFragment searchStudentFragment;

    String title, location, time, date, price, desc, studentKey,collegeKey;
    String imageArray[];

    View view = null;
    private String university;
    private ArrayList<HashMap<String, String>> dataList, courseDataList;
    private String mode = "", key = "";
    //ArrayList name=new ArrayList();

    int count = 0;

    public InviteCourseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_invite_course_fragmet, container, false);
        }

        university = SharedPreference.getInstance().getData(getContext(), Constant.UNIVERSITY_NAME);
        collegeKey=StoreData.getCollegeKeyFromSharedPre(getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();

        eventdatabaseReference = firebaseDatabase.getReference(Constant.EVENT).child("Event");

        getDataFromBundle();
        bindViews(view);


        initFragment();

        setListener();
        networkCheck();
        return view;
    }


    private void getDataFromBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mode = bundle.getString("mode", "");
            if (mode.equals("Event") || mode.equals("Meeting")) {

                // key = bundle.getString("key", "");

                title = bundle.getString("title");
                location = bundle.getString("location");
                date = bundle.getString("date");
                time = bundle.getString("time");
                imageArray = bundle.getStringArray("imageArray");
                price = bundle.getString("price");
                desc = bundle.getString("desc");
                if (bundle.containsKey("key")) {
                    key = bundle.getString("key");
                }
                if (bundle.containsKey("studentList")) {
                    studentKey = bundle.getString("studentList");

                    listStudentKey = new ArrayList<String>(Arrays.asList(studentKey.split(",")));
                }
            }
        }
    }

    private void getInviteListForEvent(HashMap hashMap) {

        if (key.isEmpty()) {

            String data = StoreData.getUserKeyFromSharedPre(getContext());
            HashMap hashMap1 = new HashMap();
            hashMap1.put("title", title);
            hashMap1.put("location", location);
            hashMap1.put("date", date);
            hashMap1.put("time", time);
            hashMap1.put("type", mode);
            hashMap1.put("admin", data);
            hashMap1.put("price", price);
            hashMap1.put("desc", desc);

            final String keyEvent = eventdatabaseReference.push().getKey();
            eventdatabaseReference.child(keyEvent).updateChildren(hashMap1);

            eventKey = keyEvent;
        } else {
            eventKey = key;
        }

        eventdatabaseReference.child(eventKey).child(Constant.INVITED_STUDENTS).updateChildren(hashMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                //Event_Invite_Fragment event_invite_fragment=new Event_Invite_Fragment();
                //ReplaceFragment.replace((WelcomeActivity) getActivity(), event_invite_fragment, "InviteStudent_Fragment");
                // ReplaceFragment.popBackStackAll((WelcomeActivity) getActivity(), 2);
                if (key.isEmpty()) {
                    imageUpload(eventKey);

                } else {

                    Toast.makeText(getContext(), "Student invited successfully", Toast.LENGTH_SHORT).show();

                    ReplaceFragment.popBackStackAll((WelcomeActivity) getActivity(), 2);

                    ShowDialog.hideDialog();
                }

            }
        });
    }

    private void imageUpload(final String key) {


        // ShowDialog.showDialog(getContext());

        final StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < imageArray.length; i++) {

            String picture = imageArray[i];
            Uri imgUri = Uri.parse("file://" + picture);


            FirebaseStorage.getInstance().getReference().child(Constant.IMAGE_NAME + "_" + SystemClock.currentThreadTimeMillis())
                    .putFile(imgUri)
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Log.d("response", "failure" + "----" + e.getMessage());
                            ShowDialog.hideDialog();
                        }
                    })
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            String downloadImageUri = String.valueOf(taskSnapshot.getDownloadUrl());

                            count++;

                            stringBuilder.append(downloadImageUri + ",");

                            HashMap hashMap = new HashMap();
                            hashMap.put("image_url", stringBuilder.toString());

                            eventdatabaseReference.child(key).updateChildren(hashMap);

                            for (int i = 0; i < imageArray.length; i++) {

                                if (imageArray[i] == null) {
                                    count++;
                                }
                            }

                            Log.d("count", count + "");

                            if (count == (imageArray.length) + 1 || count == imageArray.length) {
                                Log.d("count", "fg");
                                Toast.makeText(getContext(), "Student invited successfully", Toast.LENGTH_SHORT).show();

                                ReplaceFragment.popBackStackAll((WelcomeActivity) getActivity(), 2);

                                ShowDialog.hideDialog();

                            }

                        }
                    });
        }
    }


    private void networkCheck() {


        if (!isNetworkAvailable(getContext())) {
            networkDialog(getContext());

            getCourseList();

        } else {

            getCourseList();

        }
    }

    private void getCourseList() {
        if (NetworkCheck.isNetworkAvailable(getContext())) {

            ShowDialog.showDialog(getContext());
        }

        firebaseDatabase = FirebaseDatabase.getInstance();
      //  databaseReference = firebaseDatabase.getReference("university").child(university);
        databaseReference = firebaseDatabase.getReference("subjects").child(collegeKey);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                dataList = new ArrayList<HashMap<String, String>>();
               // if (NetworkCheck.isNetworkAvailable(getContext())) {
                if(ShowDialog.dialog!=null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                   // if (!hashMap.get(Constant.COURSE_NAME).equals("")) {
                    if (dataSnapshot1.child("subject").exists()) {

                        HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                        hashMap.put(Constant.CHECKED_NAME, Constant.FALSE_NAME);
                        hashMap.put(Constant.COURSE_KEY, dataSnapshot1.getKey());

                        dataList.add(hashMap);
                    }

                }
                setAdaptor();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

               // if (NetworkCheck.isNetworkAvailable(getContext())) {
                if(ShowDialog.dialog!=null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }


            }
        });
    }

    private void getStudentFromCourseAndInvite(final ArrayList<String> selectedDataList) {


        if (!isNetworkAvailable(getContext())) {
            networkDialog(getContext());
        } else {

            ShowDialog.showDialog(getContext());

            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference(Constant.COURSE_NAME);

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    List<String> newCheckList = new ArrayList<String>();

                    courseDataList = new ArrayList<HashMap<String, String>>();

                    // ShowDialog.hideDialog();
                    if (dataSnapshot.getValue() != null) {

                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                            Log.d("asda", dataSnapshot1.getKey() + "");
                            HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                            hashMap.put(Constant.STUDENT_KEY, dataSnapshot1.getKey());


                            courseDataList.add(hashMap);

                        }
                        HashMap hashMap = new HashMap();

                        for (int i = 0; i < selectedDataList.size(); i++) {


                            String selectedDataHashMap = selectedDataList.get(i);


                            for (int j = 0; j < courseDataList.size(); j++) {
                                HashMap courseHashMap = courseDataList.get(j);

                                for (Object data : courseHashMap.keySet()) {

                                    String data_str = (String) data;
                                    if (data_str.equals(selectedDataHashMap)) {

                                        Log.d("xzc,", listStudentKey + "");

                                        if (!listStudentKey.contains(courseDataList.get(j).get(Constant.STUDENT_KEY))) {
                                            //  hashMap.put(courseDataList.get(j).get(Constant.STUDENT_KEY), Constant.WAITING);

                                            newCheckList.add(courseDataList.get(j).get(Constant.STUDENT_KEY));
                                        }
                                    }

                                }


                            }

                        }


                        Intent intent = new Intent(context, WelcomeActivity.class);
                        intent.putExtra(Constant.CHECK_LIST, (Serializable) newCheckList);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
                        ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                        ShowDialog.hideDialog();

                    } else {
                        ShowDialog.hideDialog();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    ShowDialog.hideDialog();


                }
            });
        }
    }

    private void bindViews(View view) {

        invitecourse_rv = (RecyclerView) view.findViewById(R.id.invitecourse_rv);
        invitecourse_tv = (TextView) view.findViewById(R.id.invitecourse_tv);

        inviteCourse_tv = (EditText) view.findViewById(R.id.inviteCourse_tv);

        inviteCourse_tv.addTextChangedListener(this);

        //  drawer_iv=(ImageView)view.findViewById(R.id.drawer_iv);
    }

    private void setAdaptor() {

        inviteCourseAdaptor = new InviteCourseAdaptor(dataList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        invitecourse_rv.setLayoutManager(mLayoutManager);
        invitecourse_rv.setAdapter(inviteCourseAdaptor);

        // courseAdaptor.setClickListener(this);
    }

    private void initFragment() {

        addCourseFragment = new AddCourseFragment();
        addAssignmentFragment = new AddAssignmentFragment();
        accept_rejectFragment = new Accept_RejectFragment();
        searchStudentFragment = new SearchStudentFragment();
    }

    private void setListener() {
        // ((HomeActivity) getActivity()).header_iv.setOnClickListener(this);

        invitecourse_tv.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((WelcomeActivity) getActivity()).header_tv.setText("Invite");
        ((WelcomeActivity) getActivity()).header_ll.setVisibility(View.VISIBLE);
        ((WelcomeActivity) getActivity()).header_iv.setVisibility(View.GONE);
     //   ((WelcomeActivity) getActivity()).logout_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).profile_iv.setVisibility(View.GONE);
        ((WelcomeActivity) getActivity()).header_tv.setGravity(Gravity.LEFT);
        ((WelcomeActivity) getActivity()).header_ll.setBackgroundResource(R.drawable.btn_background);

        ((WelcomeActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

           /* case R.id.header_iv:

                ReplaceFragment.replace(((WelcomeActivity) getActivity()), addCourseFragment, "AddCourse_Fragment");*/

            case R.id.invitecourse_tv:

                checkSelectedData();


                break;
        }

    }

    private void checkSelectedData() {
        ArrayList<String> selectedDataList = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            HashMap<String, String> hashMap = dataList.get(i);


            if (hashMap.get(Constant.CHECKED_NAME).equals(Constant.TRUE_NAME)) {

                selectedDataList.add(hashMap.get(Constant.COURSE_KEY));
            }

        }
        if (selectedDataList.size() == 0) {

            ShowDialog.showSnackbarError(view, "Please select student from list", getContext());
        } else {
            getStudentFromCourseAndInvite(selectedDataList);
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        String s2;
        courseSearchtList.clear();
        //  courseSearchKey.clear();
        String s1 = s.toString().toLowerCase();

        s1 = s1.replace(" ", "");

        //    Log.d("sdf",studentCourseList+"");

        for (int i = 0; i < dataList.size(); i++) {
            HashMap hashMap = dataList.get(i);
            String course = hashMap.get(Constant.COURSE_NAME).toString();

            String username = course.toLowerCase().replace(" ", "");

            if (username.contains(s1)) {
                courseSearchtList.add(dataList.get(i));
                //  courseSearchKey.add(courseKey.get(i));

            }

        }
        inviteCourseAdaptor.addDataInList(courseSearchtList);
        inviteCourseAdaptor.notifyDataSetChanged();

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
