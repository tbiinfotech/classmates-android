package com.brst.classmates.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Binder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.ProfileActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.adaptors.ContactAdapter;
import com.brst.classmates.adaptors.NetworkAdapter;
import com.brst.classmates.beanclass.User;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatProfile extends Fragment {

    TextView tv_name, tv_college, tv_country, tv_university, tv_invite, tv_last_name;
    String KEY = "", FROM = "";
    ImageView iv_profile;
    public static int fragment = 0;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, reference;
    LinearLayout lay_header;
    TextView tv_title, tv_year, tv_degree;
    ImageView back_iv;
    List<HashMap<String, String>> dataList = new ArrayList<>();
    List<HashMap<String, String>> courseSearchtList = new ArrayList<>();
    List<String> courseKey = new ArrayList<>();
    List<String> courseSearchKey = new ArrayList<>();
    ArrayList<HashMap<String, String>> searchContactList;
    ArrayList<HashMap<String, String>> contactsList;
    View view;

    public ChatProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_chat_profile, container, false);

            init();


            firebaseDatabase = FirebaseDatabase.getInstance();
            databaseReference = firebaseDatabase.getReference(Constant.INVITED_USER);
        }

        return view;
    }

    private void init() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            KEY = bundle.getString("KEY");
            FROM = bundle.getString("FROM");
        }
      /*  KEY = getIntent().getStringExtra("KEY");
        try {
            FROM = getIntent().getStringExtra("FROM");
        } catch (Exception e) {
        }*/

        Log.d("data", KEY + "----" + FROM);
        tv_degree = (TextView) view.findViewById(R.id.tv_degree);
        lay_header = (LinearLayout) view.findViewById(R.id.lay_header);
        back_iv = (ImageView) view.findViewById(R.id.back_iv);
        back_iv.setImageResource(R.mipmap.ic_backarr);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_title.setText("Profile");
        tv_title.setTextColor(Color.parseColor("#ffffff"));
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // finish();
                ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
            }
        });
        lay_header.setBackgroundColor(Color.parseColor("#FF0278B5"));
        tv_invite = (TextView) view.findViewById(R.id.tv_invite);
        iv_profile = (ImageView) view.findViewById(R.id.iv_profile);
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_college = (TextView) view.findViewById(R.id.tv_college);
        tv_country = (TextView) view.findViewById(R.id.tv_country);
        tv_university = (TextView) view.findViewById(R.id.tv_university);
        tv_last_name = (TextView) view.findViewById(R.id.tv_last_name);
        tv_year = (TextView) view.findViewById(R.id.tv_year);

        if (bundle.containsKey(Constant.COLLEGE_NAME))
        {
            tv_invite.setVisibility(View.GONE);
            Glide.with(getContext()).load(bundle.getString("imagePath")).apply(new RequestOptions().circleCrop()).into(iv_profile);
            tv_name.setText(bundle.getString("userName"));
            tv_college.setText(bundle.getString(Constant.COLLEGE_NAME));
            tv_country.setText(bundle.getString(Constant.COUNTRY_NAME));
            tv_university.setText(bundle.getString(Constant.UNIVERSITY_NAME));
            tv_year.setText(bundle.getString(Constant.YEAR_NAME));
            tv_degree.setText(bundle.getString(Constant.DEGREE_NAME));
        }
        else {
            try {

                if (!FROM.equals("CONTACT")) {

                    tv_name.setText(NetworkAdapter.hashMaps.get(Constant.FIRST_NAME) + " " + NetworkAdapter.hashMaps.get(Constant.LAST_NAME));
                    tv_last_name.setText(NetworkAdapter.hashMaps.get(Constant.LAST_NAME));

                    tv_college.setText(NetworkAdapter.hashMaps.get(Constant.COLLEGE_NAME));
                    tv_country.setText(NetworkAdapter.hashMaps.get(Constant.COUNTRY_NAME));
                    tv_university.setText(NetworkAdapter.hashMaps.get(Constant.UNIVERSITY_NAME));
                    tv_year.setText(NetworkAdapter.hashMaps.get(Constant.YEAR_NAME));
                    tv_degree.setText(NetworkAdapter.hashMaps.get(Constant.DEGREE_NAME));

                    try {

                        Glide.with(getContext()).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                        iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                        //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                    } catch (Exception e) {
                    }


                } else {

                    tv_last_name.setText(ContactAdapter.hashMaps.get(Constant.LAST_NAME));
                    if (ContactAdapter.hashMaps.get("request_status").equals("Contact request from this user.")) {
                        tv_invite.setText("Accept Invite");
                        tv_name.setText(ContactAdapter.hashMaps.get("SENDER"));
                        tv_college.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COLLEGE_NAME));
                        tv_country.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COUNTRY_NAME));
                        tv_university.setText(ContactAdapter.hashMaps.get("Sender" + Constant.UNIVERSITY_NAME));
                        tv_year.setText(ContactAdapter.hashMaps.get("Sender" + Constant.YEAR_NAME));
                        tv_degree.setText(ContactAdapter.hashMaps.get("Sender" + Constant.DEGREE_NAME));


                        try {

                            Glide.with(getContext()).load(ContactAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                            iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                            //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                        } catch (Exception e) {
                        }

                    } else if (ContactAdapter.hashMaps.get("request_status").equals("Request Pending")) {
                        tv_invite.setText("Request Pending");
                        tv_name.setText(ContactAdapter.hashMaps.get("RECEIVER") + " " + ContactAdapter.hashMaps.get(Constant.LAST_NAME));

                /*    tv_college.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COLLEGE_NAME));
                    tv_country.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COUNTRY_NAME));
                    tv_university.setText(ContactAdapter.hashMaps.get("Sender" + Constant.UNIVERSITY_NAME));
                    tv_year.setText(ContactAdapter.hashMaps.get("Sender" + Constant.YEAR_NAME));
                    tv_degree.setText(ContactAdapter.hashMaps.get("Sender" + Constant.DEGREE_NAME));*/

                        tv_college.setText(ContactAdapter.hashMaps.get(Constant.COLLEGE_NAME));
                        tv_country.setText(ContactAdapter.hashMaps.get(Constant.COUNTRY_NAME));
                        tv_university.setText(ContactAdapter.hashMaps.get(Constant.UNIVERSITY_NAME));
                        tv_year.setText(ContactAdapter.hashMaps.get(Constant.YEAR_NAME));
                        tv_degree.setText(ContactAdapter.hashMaps.get(Constant.DEGREE_NAME));


                        try {

                            Glide.with(getContext()).load(ContactAdapter.hashMaps.get(Constant.RECEIVER_PROFILE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                            iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.RECEIVER_PROFILE));
                            //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                        } catch (Exception e) {
                        }

                    } else {
                        tv_invite.setText("Chat");


                        if (ContactAdapter.Info.equals("sender")) {
                            // tv_name.setText(ContactAdapter.hashMaps.get("RECEIVER") + " " + ContactAdapter.hashMaps.get(Constant.LAST_NAME));
                            tv_name.setText(ContactAdapter.hashMaps.get("SENDER"));
                            tv_college.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COLLEGE_NAME));
                            tv_country.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COUNTRY_NAME));
                            tv_university.setText(ContactAdapter.hashMaps.get("Sender" + Constant.UNIVERSITY_NAME));
                            tv_year.setText(ContactAdapter.hashMaps.get("Sender" + Constant.YEAR_NAME));
                            tv_degree.setText(ContactAdapter.hashMaps.get("Sender" + Constant.DEGREE_NAME));


                            try {

                                Glide.with(getContext()).load(ContactAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                                iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                                //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                            } catch (Exception e) {
                            }

                        } else {

                            if (ContactAdapter.Info.equals("receiver")) {
                                // tv_name.setText(ContactAdapter.hashMaps.get("SENDER"));
                                tv_name.setText(ContactAdapter.hashMaps.get("RECEIVER") + " " + ContactAdapter.hashMaps.get(Constant.LAST_NAME));
                                tv_college.setText(ContactAdapter.hashMaps.get(Constant.COLLEGE_NAME));
                                tv_country.setText(ContactAdapter.hashMaps.get(Constant.COUNTRY_NAME));
                                tv_university.setText(ContactAdapter.hashMaps.get(Constant.UNIVERSITY_NAME));
                                tv_year.setText(ContactAdapter.hashMaps.get(Constant.YEAR_NAME));
                                tv_degree.setText(ContactAdapter.hashMaps.get(Constant.DEGREE_NAME));


                                try {

                                    Glide.with(getContext()).load(ContactAdapter.hashMaps.get(Constant.RECEIVER_PROFILE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                                    iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.RECEIVER_PROFILE));
                                    //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                                } catch (Exception e) {
                                }

                            }
                        }
                    }
                }
            } catch (Exception e) {

            }
        }

        tv_invite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (tv_invite.getText().toString().equals("Accept Invite")) {
                    databaseReference.child(KEY).child("request_status").setValue("Accepted");
                    tv_invite.setText("Chat");
                }
              /*  if (tv_invite.getText().toString().equals("Request Pending")) {

                }*/ else if (tv_invite.getText().toString().equals("Connect")) {

                    final Map<String, User> users = new HashMap<String, User>();
                    users.put(KEY
                            , new User(StoreData.getUserKeyFromSharedPre(getContext()), KEY, "SENT"));

                    DatabaseReference rootRef = firebaseDatabase.getReference(Constant.INVITED_USER);

                    DatabaseReference cineIndustryRef = rootRef.push();
                    String key = cineIndustryRef.getKey();
                    Map<String, Object> map = new HashMap<>();
                    map.put("to", KEY);
                    map.put("request_status", "sent");
                    map.put("SENDER", StoreData.getUserFullNameFromSharedPre(getContext()));
                    map.put("from", StoreData.getUserKeyFromSharedPre(getContext()));


                    map.put("Sender" + Constant.COLLEGE_NAME, StoreData.getCollegeFromSharedPre(getContext()));
                    map.put("Sender" + Constant.UNIVERSITY_NAME, StoreData.getUniversityFromSharedPre(getContext()));
                    map.put("Sender" + Constant.COUNTRY_NAME, StoreData.getCountryFromSharedPre(getContext()));
                    map.put(Constant.PROFILE_PICTURE, StoreData.getProfileFromSharedPre(getContext()));
                    map.put("Sender" + Constant.YEAR_NAME, StoreData.getyearFromSharedPre(getContext()));
                    map.put("Sender" + Constant.DEGREE_NAME, StoreData.getDegreeFromSharedPre(getContext()));


                    map.put(Constant.LAST_NAME, NetworkAdapter.hashMaps.get(Constant.LAST_NAME));
                    map.put(Constant.YEAR_NAME, NetworkAdapter.hashMaps.get(Constant.YEAR_NAME));
                    map.put(Constant.DEGREE_NAME, NetworkAdapter.hashMaps.get(Constant.DEGREE_NAME));

                    map.put(Constant.COUNTRY_NAME, NetworkAdapter.hashMaps.get(Constant.COUNTRY_NAME));
                    map.put(Constant.COLLEGE_NAME, NetworkAdapter.hashMaps.get(Constant.COLLEGE_NAME));
                    map.put(Constant.UNIVERSITY_NAME, NetworkAdapter.hashMaps.get(Constant.UNIVERSITY_NAME));

                    map.put("RECEIVER", NetworkAdapter.hashMaps.get(Constant.FIRST_NAME));
                    map.put("RECEIVER_PROFILE", NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                    //and os on
                    cineIndustryRef.updateChildren(map);


            /*  databaseReference.child(KEY).setValue(users, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                      //  databaseReference.setValue(users);

                    }
                });*/


                    tv_invite.setText("Invite Sent");
                    tv_invite.setClickable(false);
                } else if (tv_invite.getText().toString().equals("Chat")){
                    //    Log.d("data", ContactAdapter.Info.equals("receiver") + "----" + ContactAdapter.Info.equals("sender"));
                    //    Log.d("image", iv_profile.getTag().toString() + "---" + tv_name.getText().toString() + "---" + KEY);
                    Bundle bundle = new Bundle();
                    if (ContactAdapter.Info.equals("receiver")) {
                        bundle.putString("imagePath", ContactAdapter.hashMaps.get(Constant.RECEIVER_PROFILE));
                    } else {
                        bundle.putString("imagePath", ContactAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                    }
                    Log.d("key",KEY);
                    bundle.putString("userName", tv_name.getText().toString());
                    bundle.putString("chatKey", KEY);
                    bundle.putString("to",ContactAdapter.hashMaps.get("to"));
                    bundle.putString("from",ContactAdapter.hashMaps.get("from"));
                    bundle.putString("sender",ContactAdapter.hashMaps.get("SENDER"));
                    bundle.putString("receiver",ContactAdapter.hashMaps.get("RECEIVER"));
                    bundle.putString(Constant.PROFILE_PICTURE,ContactAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                    bundle.putString("RECEIVER_PROFILE",ContactAdapter.hashMaps.get("RECEIVER_PROFILE"));
                  /*  Intent mIntent = new Intent(getContext(), WelcomeActivity.class);
                    mIntent.putExtras(bundle);
                    fragment = 3;
                    startActivity(mIntent);
                    finish();*/
                  //  ReplaceFragment.popBackStack((WelcomeActivity) getActivity());
                    ReplaceFragment.replace((WelcomeActivity) getActivity(), new OneToOneChatFragment(), "OneToOne_Fragment", bundle);

                }


            }
        });


    }


}
