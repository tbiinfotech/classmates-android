package com.brst.classmates.utility;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class Constant {

    public static final String MONTH = "month";
    public static final String DAY = "day";


    public static final String BUNDLE_NAME = "bundle";

    public static final int SPLASH_TIME_OUT = 1200;
    public static final int GALLERY_CODE = 100;
    public static final int CAMERA_CODE = 200;
    public static final int CROP_PIC_REQUEST_CODE = 300;
    public static final int CROP_IMAGE = 400;

    public static final String PREFERENCE_NAME = "classmate_pref";
    public static final String EMAIL_NAME = "emailAddress";
    public static final String KEY_NAME = "userKey";
    public static final String SOUND = "sound";
    public static final String MUTE = "mute";
    public static final String UNMUTE = "unmute";
    public static final String CHAT = "chat";
    public static final String ATTENDING = "attending";
    public static final String FILE_NAME = "file";


    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String FULL_NAME = "full_name";
    public static final String EMAILADD_NAME = "email";
    public static final String COUNTRY_NAME = "country_name";
    public static final String UNIVERSITY_NAME = "university_name";
    public static final String COLLEGE_NAME = "college_name";

    public static final String YEAR_NAME = "year";
    public static final String DEGREE_NAME = "degree";
    public static final String ACTIVITY_NAME = "activity_name";
    public static final String RECEIVER_PROFILE = "RECEIVER_PROFILE";
    public static final String PROFILE_PICTURE = "profile_picture";
    public static final String UNIVERSITY = "university";
    public static final String COLLEGE = "college";
    public static final String REGISTERED_NAME = "registered_user";
    public static final String OWNER = "owner";
    public static final String GROUP_ICON = "groupicon";
    public static final String EVENT = "EventDetail";
    public static final String INVITED_STUDENTS = "invited_students";
    public static final String INVITED_USER = "InviteUser";

    public static final String GROUP_NAME = "groupName";
    public static final String GROUP_kEY = "groupKey";
    public static final String USER_LIST = "userList";
    public static final String USER_NAME = "users";
    public static final String CREATEGROUP_NAME = "group";
    public static final String COURSE_KEY = "course_key";
    public static final String NOTIFICATION_KEY = "notification";

    public static final String TEXT_NAME = "text";
    public static final String USERID_NAME = "userid";
    public static final String IMAGE_NAME = "image";

    public static final String VIDEO_THUMB = "videothumb";
    public static final String VIDEO_NAME = "video";

    public static final String STUDENT_KEY = "student_key";
    public static final String CHECKED_NAME = "checked";
    public static final String TRUE_NAME = "true";
    public static final String FALSE_NAME = "false";
    public static final String CHECK_LIST = "checklist";
    public static final String STUDENT_LIST = "student";

    public static final String COURSE_NAME = "course";
    public static final String CREDIT_NAME = "credit";
    public static final String TERM_NAME = "term";
    public static final String TIME_NAME = "time";

    public static final String Token_NAME = "token";
    public static final String BASE_URL = "token";
    public static final String MESSAGE = "Message";
    public static final String SENDER_NAME = "SenderName";

    public static final String EMPTY_NAME = "null";
    public static final String TITLE_NAME = "Title";
    public static final String INACTIVE_NAME = "inActive";
    public static final String URIIMAGE_NAME = "imageuri";

    public static final String CONFIRM_NAME = "confirm";
    public static final String STATUS_NAME = "status";
    public static final String BLOCK_NAME = "block";

    public static final String ASSIGNMENT_NAME = "assignment";
    public static final String INDIVI_NAME = "individual";
    public static final String ASSIGN_NAME = "assignment_name";
    public static final String ASSIGNTYPE_NAME = "assignment_type";
    public static final String ASSIGNADMIN_NAME = "assignment_admin";
    public static final String ASSIGNDATE_NAME = "due_date";
    public static final String SUBMITNUM_NAME = "submit_number";
    public static final String PROGRESSNUM_NAME = "progress_number";
    public static final String SUBMIT_NAME = "Submitted";

    public static final String ACCEPT_NAME = "accept";
    public static final String USERMESS_NAME = "name";
    public static final String ASSCOMMENT_NAME = "AssignmentComment";
    public static final String PROFILEFRAGMENT_NAME = "Profile_Fragment";
    public static final String ASSIOWNERPROFILE_NAME = "owner_profile";
    public static final String ASSIGNOWNERNAME_NAME = "owner_name";

    public static final String ACCEPTED_NAME = "Accepted";
    public static final String REJECTED_NAME = "Rejected";
    public static final String MAYBE = "Maybe";
    public static final String WAITING = "Waiting";
    public static final String MESSKEY_NAME = "messageKey";
    public static final String REPLY_NAME = "commentReply";
    public static final String EVENT_NAME = "eventName";
    public static final String ISFIRSTTIME = "isFirstTime";

    public static final String SERVER_KEY = "AAAAvk26vHo:APA91bFSbazQlmUVgtfeWZ2uckU3vM2JqoxPbsoMO7h4TjoWeZ4NNriFxEmnjXF-20JJgUR6DPR7qKL9O1R3kuw2R7PEUe7WwBCfnjlTP4Sw0sxJpPENgZdpnBxWGD2CWFfyGLfge8tR";
    //public static final String SERVER_KEY = "AAAAINfZgw4:APA91bGB0GY075UVC9r4ZefB8Q4crdpVlyW0-JISGD48WocUl8EBNCAy7IfKb8nXzjWKn1jwrbtE-YIlp9xCVEMJb6bjM4gDw1hKqMXqP10XbWS6NRNNB5_sInKHes4oy5hefCHvH_gq";

}
