package com.brst.classmates.beanclass;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Ravi Tamada on 07/10/16.
 * www.androidhive.info
 */

@IgnoreExtraProperties
public class User {

    public String name,from,to,status;
    public String email,sent;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public User() {
    }

    public User(String from,String to,String status ) {
        this.to = to;
        this.from = from; this.status = status;
    }
}
