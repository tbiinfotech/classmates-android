package com.brst.classmates.interfaces;

import android.view.View;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public interface OnClickEvent {

    public void onClickEventListener(View view, int position, String type, String event_key);


}
