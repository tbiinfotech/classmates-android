package com.brst.classmates.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by brst-pc89 on 7/18/17.
 */

public interface RetrofitApi {

    @POST("fcm/send")

    Call<String> sendNotification();


}
