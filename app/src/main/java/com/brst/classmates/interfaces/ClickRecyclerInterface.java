package com.brst.classmates.interfaces;

import android.view.View;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public interface ClickRecyclerInterface {

    public void onRecyClick(View view,int position);

    public void onLongClick(View view,int position);
}
