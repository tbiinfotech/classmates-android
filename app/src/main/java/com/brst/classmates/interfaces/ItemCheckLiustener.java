package com.brst.classmates.interfaces;

import android.content.ClipData;

/**
 * Created by brst-pc89 on 7/13/17.
 */

public interface ItemCheckLiustener {

    public  void itemCheck(ClipData.Item item);

    public void itemUnCheck(ClipData.Item item);

}
