package com.brst.classmates.sharedpreference;

import android.content.Context;

import com.brst.classmates.utility.Constant;

/**
 * Created by brst-pc89 on 7/22/17.
 */

public class StoreData {


    public static String userKey, fullname, email, country, university, college, degree, yearName, activity, profile,notification,collegeKey;

    public static void storeDataToSharedPref(Context context, String key, String userFullName, String email, String university, String college, String country, String degree, String year, String notification, String collegeKey) {
        SharedPreference.getInstance().storeData(context, Constant.KEY_NAME, key);
        SharedPreference.getInstance().storeData(context, Constant.EMAIL_NAME, email);
        SharedPreference.getInstance().storeData(context, Constant.FULL_NAME, userFullName);
        SharedPreference.getInstance().storeData(context, Constant.UNIVERSITY_NAME, university);
        SharedPreference.getInstance().storeData(context, Constant.COLLEGE_NAME, college);
        SharedPreference.getInstance().storeData(context, Constant.COUNTRY_NAME, country);
        SharedPreference.getInstance().storeData(context, Constant.DEGREE_NAME, degree);
        SharedPreference.getInstance().storeData(context, Constant.YEAR_NAME, year);
     //   SharedPreference.getInstance().storeData(context, Constant.ACTIVITY_NAME, activity);
        SharedPreference.getInstance().storeData(context, Constant.NOTIFICATION_KEY, notification);
        SharedPreference.getInstance().storeData(context, "collegeKey", collegeKey);
    }

    public  static void storeProfileToSharedPref(Context context,String image)
    {
        SharedPreference.getInstance().storeData(context, Constant.PROFILE_PICTURE, image);
    }

    public static String getNotificationFromSharedPre(Context context) {
        notification = SharedPreference.getInstance().getData(context, Constant.NOTIFICATION_KEY);
        return notification;
    }

    public static String getCollegeKeyFromSharedPre(Context context) {
        collegeKey = SharedPreference.getInstance().getData(context, "collegeKey");
        return collegeKey;
    }

    public static String getUserKeyFromSharedPre(Context context) {
        userKey = SharedPreference.getInstance().getData(context, Constant.KEY_NAME);
        return userKey;
    }

    public static String getUserFullNameFromSharedPre(Context context) {
        fullname = SharedPreference.getInstance().getData(context, Constant.FULL_NAME);

        return fullname;
    }

    public static String getUserEmailFromSharedPre(Context context) {

        email = SharedPreference.getInstance().getData(context, Constant.EMAIL_NAME);

        return email;

    }

    public static String getCountryFromSharedPre(Context context) {
        country = SharedPreference.getInstance().getData(context, Constant.COUNTRY_NAME);

        return country;
    }

    public static String getUniversityFromSharedPre(Context context) {
        university = SharedPreference.getInstance().getData(context, Constant.UNIVERSITY_NAME);

        return university;
    }


    public static String getCollegeFromSharedPre(Context context) {
        college = SharedPreference.getInstance().getData(context, Constant.COLLEGE_NAME);
        return college;
    }

    public static String getDegreeFromSharedPre(Context context) {
        degree = SharedPreference.getInstance().getData(context, Constant.DEGREE_NAME);

        return degree;
    }

    public static String getyearFromSharedPre(Context context) {
        yearName = SharedPreference.getInstance().getData(context, Constant.YEAR_NAME);

        return yearName;
    }

   /* public static String getactivityromSharedPre(Context context) {
        activity = SharedPreference.getInstance().getData(context, Constant.ACTIVITY_NAME);

        return activity;
    }*/

    public static String getProfileFromSharedPre(Context context) {

        profile = SharedPreference.getInstance().getData(context, Constant.PROFILE_PICTURE);

        return profile;
    }
}
