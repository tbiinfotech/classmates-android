package com.brst.classmates.sharedpreference;



import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.brst.classmates.utility.Constant;

import static com.brst.classmates.utility.Constant.PREFERENCE_NAME;

/**
 * Created by brst-pc89 on 7/10/17.
 */

public class SharedPreference {


    private static SharedPreference instance = null;

    public static SharedPreference getInstance() {
        if (instance == null) {
            instance = new SharedPreference();
        }

        return instance;
    }

    public SharedPreferences getPreference(Context context) {
        return context.getSharedPreferences(PREFERENCE_NAME,Context.MODE_PRIVATE);
    }

    public void storeData(Context context, String key, String value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(key, value);
        editor.apply();


    }

    public void storeFirstTime(Context context, String key, Boolean value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putBoolean(key, value);
        editor.apply();


    }

    public void clearData(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        if (preferences != null)
            preferences.edit().remove(key).apply();

    }

    public String getData(Context context, String key) {

        Log.e("getdata", "getdata");

        return getPreference(context).getString(key, "null");
    }

    public Boolean getIsFirstTime(Context context,String value) {

        Log.e("getdata", "getdata");

        return getPreference(context).getBoolean(value, true);
    }}
