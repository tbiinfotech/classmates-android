package com.brst.classmates.classses;

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.utility.Constant;

/**
 * Created by brst-pc89 on 7/20/17.
 */

public class NotificationReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        playNotificationSound(context);
    }

    public void playNotificationSound(Context context) {
        try {

            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(context, notification);
                r.play();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
