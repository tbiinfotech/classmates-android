package com.brst.classmates.classses;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by brst-pc89 on 7/10/17.
 */

public class FirebseDatabase {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    public FirebaseDatabase getFirebaseDatabase() {

        firebaseDatabase=FirebaseDatabase.getInstance();
        return firebaseDatabase;
    }

    public DatabaseReference getDatabaseReference() {

        databaseReference=firebaseDatabase.getReference("course");
        return databaseReference;
    }
}
