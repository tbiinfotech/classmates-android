package com.brst.classmates.classses;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.brst.classmates.R;
import com.brst.classmates.activity.LoginActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.HashMap;

import static com.brst.classmates.fragments.OneToOneChatFragment.tempraryDirectory;

/**
 * Created by brst-pc89 on 6/29/17.
 */

public class ShowDialog {

    public static ProgressDialog dialog;
    public static Dialog profileDialog;


    /*--------------------progress dialog---------------------->*/


    public static void resultdialog(final Context context, String msg) {
        final Dialog login_dialog = new Dialog(context);
        final TextView tv_ok, tv_dialogtext;

        login_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        login_dialog.setContentView(R.layout.dialog_validation);

        WindowManager.LayoutParams lp = login_dialog.getWindow().getAttributes();
        lp.dimAmount = 0.8f;
        login_dialog.getWindow().setAttributes(lp);

        login_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        tv_ok = (TextView) login_dialog.findViewById(R.id.tv_ok);
        tv_dialogtext = (TextView) login_dialog.findViewById(R.id.tv_dialogtext);
        tv_dialogtext.setText(msg);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login_dialog.dismiss();


            }
        });
        login_dialog.show();
    }


    public static void showDialog(Context context) {


        dialog = ProgressDialog.show(context, "", "");
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.progress_dialog);
        GeometricProgressView progressView15 = (GeometricProgressView) dialog.findViewById(R.id.progressView15);
        progressView15.setType(GeometricProgressView.TYPE.KITE);
        progressView15.setFigurePaddingInDp(1);
        progressView15.setNumberOfAngles(30);

        Log.d("cxzc", "zxc");
        dialog.show();


    }

    public static void hideDialog() {

        try {
            dialog.dismiss();
        } catch (Exception e) {

        }
    }



    /*------------------------------Snackbar--------------------------->*/

    public static void showSnackbarError(View v, String message, Context context) {
        Snackbar snackbar = Snackbar.make(v, message, Snackbar.LENGTH_LONG).setActionTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        View view = snackbar.getView();
        view.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
        snackbar.show();
    }




    /*----------------------------Check valid email adress------------------------------->*/

    public static boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }




    /*----------------------------------hide popupWindow----------------------------->*/

    public static void hidePopup(PopupWindow popupWindow, FrameLayout container_fl) {
        popupWindow.dismiss();
        container_fl.getForeground().setAlpha(0);
    }



    /*-------------------------------------------open cameraDailog----------------------------------->*/


    public static void cameraDialog(final Context context, View.OnClickListener onClickListener, View.OnClickListener cameraClickLictener) {


        profileDialog = new Dialog(context);
        profileDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profileDialog.setContentView(R.layout.custom_picturedialog);
        profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        profileDialog.show();


        ImageView dialog_camera = (ImageView) profileDialog.findViewById(R.id.dialog_camera);
        ImageView dialog_gallery = (ImageView) profileDialog.findViewById(R.id.dialog_gallery);


        dialog_camera.setOnClickListener(cameraClickLictener);
        dialog_gallery.setOnClickListener(onClickListener);

    }





    /*------------------------header right popup window for logout and list for meassageboard fragmet -----------------------*/


    public static void showPopup(View popup, final Context context, final boolean b) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_messagelist, null);

        TextView text_tv = (TextView) popupView.findViewById(R.id.text_tv);

        if (!b) {
            text_tv.setText("Logout");
        }


        final PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);

        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {

                popupWindow.dismiss();


            }

        });
        popupWindow.showAsDropDown(popup, 0, 0);

        text_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!b) {
                    SharedPreference.getInstance().clearData(context, Constant.EMAIL_NAME);
                    String key = SharedPreference.getInstance().getData(context, Constant.KEY_NAME);
                    HashMap hashMap = new HashMap<>();
                    hashMap.put("status", "inActive");
                    FirebaseDatabase.getInstance().getReference("registered_user").child(key).updateChildren(hashMap);
                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);

                    ((WelcomeActivity) context).finish();
                }

            }
        });
    }


    /*------------------------------network dialog----------------------------*/
    public static Dialog network_dialog;

    public static void networkDialog(Context context) {


        network_dialog = new Dialog(context);
        network_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        network_dialog.setContentView(R.layout.dialog_logout);
        network_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        network_dialog.show();

        TextView network_done = (TextView) network_dialog.findViewById(R.id.logdone_tv);
        TextView network_cancel = (TextView) network_dialog.findViewById(R.id.logCancel_tv);
        TextView network_tv = (TextView) network_dialog.findViewById(R.id.log_tv);
        View view = (View) network_dialog.findViewById(R.id.view);


        network_tv.setText("Network not available");
        network_done.setVisibility(View.GONE);
        view.setVisibility(View.GONE);
        network_cancel.setText("Ok");
        network_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                network_dialog.dismiss();
            }
        });


    }

    public Dialog group_dialog;
    public ImageView group_iv;

    public EditText groupDialog(Context context, View.OnClickListener onClickListener, View.OnClickListener getOnClickListener) {

        group_dialog = new Dialog(context);
        group_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        group_dialog.setContentView(R.layout.custom_group);
        group_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        group_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        group_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        group_dialog.show();

        EditText dialogemail_et = (EditText) group_dialog.findViewById(R.id.dialogemail_et);
        TextView dialogdone_tv = (TextView) group_dialog.findViewById(R.id.dialogdone_tv);
        TextInputLayout email_input_layout = (TextInputLayout) group_dialog.findViewById(R.id.email_input_layout);
        TextView header_text = (TextView) group_dialog.findViewById(R.id.header_text);
        group_iv = (ImageView) group_dialog.findViewById(R.id.group_iv);

        group_iv.setOnClickListener(getOnClickListener);

        header_text.setText("Create Group");
        // email_input_layout.setHint("Group Name");

        dialogdone_tv.setOnClickListener(onClickListener);


        return dialogemail_et;
    }

    static ShowDialog instance = new ShowDialog();

    public static ShowDialog getInstance() {
        return instance;
    }

    AlertDialog alertDialog;

    public void showExitDialog(Context context) {
        alertDialog = new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage("This group name already exist.")
                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.mipmap.alert)
                .show();
    }


    public void showDeleteDialog(Context context, final String key, final String parentKey, final int b) {

        String deleteCon = "", deleteMesss = "";
        if (key.equals("Event") || key.equals("Meeting")) {

            deleteCon = "Cancel Confirmation";
            deleteMesss = "Are you sure, you want to Cancel?";
        } else {
            deleteCon = "Delete Confirmation";
            deleteMesss = "Are you sure, you want to Delete?";
        }
        alertDialog = new AlertDialog.Builder(context)
                .setTitle(deleteCon)
                .setMessage(deleteMesss)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (b == 0) {

                            FirebaseDatabase.getInstance().getReference("university").child(parentKey).child(key).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if (task.isSuccessful()) {

                                    }

                                }
                            });
                        } else if (b == 1) {
                            FirebaseDatabase.getInstance().getReference(Constant.CREATEGROUP_NAME).orderByChild(key).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                                        FirebaseDatabase.getInstance().getReference(Constant.CREATEGROUP_NAME).child(snapshot.getKey()).child(key).removeValue();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } else if (b == 2) {
                            FirebaseDatabase.getInstance().getReference("AssignmentTask").child(parentKey).child(key).removeValue();
                        } else {

                            if (key.equals("Event")) {
                                FirebaseDatabase.getInstance().getReference(Constant.EVENT).child("Event").child(parentKey).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if (task.isSuccessful()) {

                                        }
                                    }
                                });
                            } else if (key.equals("Meeting")) {

                                FirebaseDatabase.getInstance().getReference(Constant.EVENT).child("Event").child(parentKey).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if (task.isSuccessful()) {

                                        }
                                    }
                                });
                            }
                        }

                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.mipmap.ic_delete)
                .show();
    }


    public void showImage(final Context context, String image, String time, String groupName, String video, String fileName, Bitmap videoThumb) {

        String timeMsg;
        String timeNew[]=time.split(" ");
        if (time.contains("/") || timeNew.length==3) {
            timeMsg= timeNew[1] + " " + timeNew[2];
        }
        else
        {
            timeMsg=timeNew[3];
        }
        Log.d("data", "video----"+video );
        String tempraryFilePath = tempraryDirectory + "/" + fileName;
        File file1 = new File(tempraryFilePath);
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        dialog.show();
        MediaController mediaController = null;
        final VideoView videoView_vv = (VideoView) dialog.findViewById(R.id.videoView_vv);
        final ImageView video_iv = (ImageView) dialog.findViewById(R.id.video_iv);
        ImageView chat_iv = (ImageView) dialog.findViewById(R.id.chat_iv);
        final ImageView play_iv = (ImageView) dialog.findViewById(R.id.play_iv);
        final ImageView cou_bck_iv = (ImageView) dialog.findViewById(R.id.cou_bck_iv);
        TextView groupName_tv = (TextView) dialog.findViewById(R.id.groupName_tv);
        TextView time_tv = (TextView) dialog.findViewById(R.id.time_tv);
        final ProgressBar progress = (ProgressBar) dialog.findViewById(R.id.progress);

        final LinearLayout header_ll = (LinearLayout) dialog.findViewById(R.id.header_ll);
        final LinearLayout Linear_ll = (LinearLayout) dialog.findViewById(R.id.Linear_ll);
        FrameLayout frame_fl = (FrameLayout) dialog.findViewById(R.id.frame_fl);
        FrameLayout video_fl = (FrameLayout) dialog.findViewById(R.id.video_fl);

        groupName_tv.setText(groupName);
        time_tv.setText(timeMsg);
        if (video != null && !video.isEmpty()) {

            video_fl.setVisibility(View.VISIBLE);
            MediaController mc = new MediaController(context);
            mc.setAnchorView(videoView_vv);
            mc.setMediaPlayer(videoView_vv);

            videoView_vv.setMediaController(mc);

          /*  Display display = context.getWindowManager().getDefaultDisplay();
            int width = display.getWidth();
            int height = display.getHeight();

            //   videoview.setLayoutParams(new FrameLayout.LayoutParams(550,550));

            videoView_vv.setLayoutParams(new FrameLayout.LayoutParams(width,height));*/
          /*  Log.d("data", file1 + "");
              if (file1.exists()) {
            videoView_vv.setVideoURI(Uri.fromFile(file1));
             }
             else {*/
                   videoView_vv.setVideoURI(Uri.parse(video));
            //  }
            videoView_vv.setVisibility(View.GONE);

            chat_iv.setVisibility(View.GONE);

            if (videoThumb!=null) {

                video_iv.setVisibility(View.VISIBLE);
                video_iv.setImageBitmap(videoThumb);
                progress.setVisibility(View.GONE);

                play_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        videoView_vv.setVisibility(View.VISIBLE);
                        video_iv.setVisibility(View.GONE);
                        videoView_vv.start();
                        play_iv.setVisibility(View.GONE);
                    }
                });

            } else {

                Glide.with(context).load(image).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progress.setVisibility(View.GONE);

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        progress.setVisibility(View.GONE);
                        play_iv.setVisibility(View.VISIBLE);


                        play_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                videoView_vv.setVisibility(View.VISIBLE);
                                video_iv.setVisibility(View.GONE);
                                videoView_vv.start();
                                play_iv.setVisibility(View.GONE);
                            }
                        });

                        return false;
                    }
                }).into(video_iv);

            }

            videoView_vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {

                    play_iv.setVisibility(View.VISIBLE);
                }
            });

            videoView_vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {

                }
            });


        } else {
      /*  Glide.with(context).load(image).apply(RequestOptions.placeholderOf(R.mipmap.picture)).into(chat_iv);*/
            video_fl.setVisibility(View.GONE);
            chat_iv.setVisibility(View.VISIBLE);
            Glide.with(context).load(image).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    progress.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    progress.setVisibility(View.GONE);
                    return false;
                }
            }).into(chat_iv);
        }

        cou_bck_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.setCanceledOnTouchOutside(true);
    }
}
