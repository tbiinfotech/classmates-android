package com.brst.classmates.classses;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.fragments.MessageBoardFragment;
import com.brst.classmates.utility.Constant;

import static android.content.Context.ALARM_SERVICE;
import static com.brst.classmates.activity.WelcomeActivity.context;


/**
 * Created by brst-pc89 on 8/9/17.
 */

public class AlarmReceiver extends BroadcastReceiver {

    Ringtone r;

    String value, assignName, assignDate, location = "";

    @Override
    public void onReceive(final Context context, Intent intent) {

        try {

                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                r = RingtoneManager.getRingtone(context, notification);
                r.play();


        } catch (Exception e) {
            e.printStackTrace();
        }

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alarm);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialog.setCanceledOnTouchOutside(false);

        TextView ass_tv = (TextView) dialog.findViewById(R.id.ass_tv);
        TextView date_tv = (TextView) dialog.findViewById(R.id.date_tv);
        TextView assignment_tv = (TextView) dialog.findViewById(R.id.assignment_tv);
        TextView datesub_tv = (TextView) dialog.findViewById(R.id.datesub_tv);
        TextView location_tv = (TextView) dialog.findViewById(R.id.location_tv);

        LinearLayout location_ll = (LinearLayout) dialog.findViewById(R.id.location_ll);

        Bundle bundle = intent.getBundleExtra(Constant.BUNDLE_NAME);
        Bundle bundleEvent = intent.getBundleExtra(Constant.EVENT_NAME);

        if (bundle != null) {
            value = bundle.getString(Constant.CHECKED_NAME);
            assignName = bundle.getString(Constant.ASSIGN_NAME);
            assignDate = bundle.getString(Constant.ASSIGNDATE_NAME);

            location_ll.setVisibility(View.GONE);
        } else if (bundleEvent != null) {
            value = bundleEvent.getString(Constant.EVENT);
            assignName = bundleEvent.getString(Constant.EVENT_NAME);
            location = bundleEvent.getString("EventLocation");
            assignDate = bundleEvent.getString("EventDate");
            if (location != null && !location.isEmpty()) {
                location_ll.setVisibility(View.VISIBLE);
                location_tv.setText(location);
            }
            assignment_tv.setText("Event:");
            datesub_tv.setText("Date:");


        }

        Log.d("fdfd", bundle + "-----" + value + "----" + assignName + "---" + assignDate);

        ass_tv.setText(assignName);
        date_tv.setText(assignDate);

        if (bundle != null) {


            if (bundle.containsKey("Notification")) {

                final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

                Intent intentNotification = new Intent(context, WelcomeActivity.class);
                Bundle extras = new Bundle();
                extras.putString("notification", "notificationLocal");
                intent.putExtras(extras);

                PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intentNotification, PendingIntent.FLAG_CANCEL_CURRENT
                );


                showSmallNotification(mBuilder, "Assignment: " + assignName, "Due_Date: " + assignDate, resultPendingIntent);


            } else {
                dialog.show();

                TextView logdone_tv = (TextView) dialog.findViewById(R.id.logdone_tv);

                logdone_tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent _myIntent = new Intent(context, AlarmReceiver.class);
                        PendingIntent _myPendingIntent = PendingIntent.getBroadcast(context, Integer.parseInt(value), _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                        AlarmManager _myAlarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

                        _myAlarmManager.cancel(_myPendingIntent);

                        r.stop();

                        dialog.dismiss();

                    }
                });

            }


        }

        if (bundleEvent != null) {
            dialog.show();

            TextView logdone_tv = (TextView) dialog.findViewById(R.id.logdone_tv);

            logdone_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent _myIntent = new Intent(context, AlarmReceiver.class);
                    PendingIntent _myPendingIntent = PendingIntent.getBroadcast(context, Integer.parseInt(value), _myIntent, PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);
                    AlarmManager _myAlarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

                    _myAlarmManager.cancel(_myPendingIntent);

                    r.stop();

                    dialog.dismiss();

                }
            });

        }

    }


    private void showSmallNotification(NotificationCompat.Builder mBuilder, String title, String message, PendingIntent resultPendingIntent) {

        NotificationCompat.BigTextStyle inboxStyle = new NotificationCompat.BigTextStyle();

        inboxStyle.bigText(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.splashlogo)
                //  notification=mBuilder.setTicker(title)
                .setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setStyle(inboxStyle)
                //    .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.logo))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);

        r.stop();
    }
}
