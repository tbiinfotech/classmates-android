package com.brst.classmates.classses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 8/8/17.
 */

public class BeanClass {

    ArrayList<HashMap<String, String>> arrayList;

    public ArrayList<HashMap<String, String>> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<HashMap<String, String>> arrayList) {
        this.arrayList = arrayList;
    }

    String name;
    String time;
    String commentReply;
    String messageKey;
    String assignmentKey;
    String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAssignmentKey() {
        return assignmentKey;
    }

    public void setAssignmentKey(String assignmentKey) {
        this.assignmentKey = assignmentKey;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getCommentReply() {
        return commentReply;
    }

    public void setCommentReply(String commentReply) {
        this.commentReply = commentReply;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
