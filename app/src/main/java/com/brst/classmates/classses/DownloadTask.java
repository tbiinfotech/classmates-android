package com.brst.classmates.classses;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.widget.BaseExpandableListAdapter;
import android.widget.Toast;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.brst.classmates.fragments.OneToOneChatFragment.tempraryDirectory;

/**
 * Created by brst-pc89 on 3/29/17.
 */
public class DownloadTask extends AsyncTask<String, Integer, String> {

    Context context;
    PowerManager.WakeLock mWakeLock;
    public static ProgressDialog mProgressDialog;
    String fileName;
    OutputStream output = null;

    String tempraryDirectory = Environment.getExternalStorageDirectory() + "/classmate";
    File f;
    // MessageBoardadaptor messageBoardadaptor;
    BaseExpandableListAdapter baseExpandableListAdapter;

    public DownloadTask(Context context, ProgressDialog mProgressDialog, String fileName) {
        this.context = context;

        this.mProgressDialog = mProgressDialog;
        this.fileName = fileName;

    }

    @Override
    protected String doInBackground(String... sUrl) {
        InputStream input = null;

        HttpURLConnection connection = null;
        try {
            URL url = new URL(sUrl[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            int fileLength = connection.getContentLength();
            Log.d("fdf", fileLength + "");


            input = connection.getInputStream();
            //      File file = new File(Environment.getExternalStorageDirectory(), "VideoDownload");
//
            // f=new File(Environment.getExternalStorageDirectory() + File.separator   + "Sample.mp4");
            String tempraryFilePath = tempraryDirectory + "/" + fileName;

            File dir = new File(tempraryDirectory);
            if (!dir.exists()) {
                Log.e("path", dir + "");
                dir.mkdirs();
            }
            f = new File(tempraryFilePath);

            output = new FileOutputStream(tempraryFilePath);


            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                Log.d("count", count + "");
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire();
        mProgressDialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // if we get here, length is known, now set indeterminate to false
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgress(progress[0]);
        //  Log.d("progress",progress[0]+"");

        if (mProgressDialog.getProgress() == 100) {
            String video_path = f.getAbsolutePath();


            //  SqliteDataBase.INSERT_VIDEO(context,videoId,video_path,videoTitle,artist,date,videoRuntime,songKey_array,instructor_array,mp4Name,youTube);


        } else {
            String video_path = f.getAbsolutePath();

        }
    }

    @Override
    protected void onPostExecute(String result) {

        //   Log.d("fgbfghresult",result);
        //  STORE_VIDEO_PATH(videoId)

        mWakeLock.release();
        mProgressDialog.dismiss();
        if (result != null) {
            mProgressDialog.show();
            //  Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
            Log.d("downloaderror", result);
        } else {
            Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();


          /*  if(baseExpandableListAdapter instanceof PopularAdaptor) {
                ((PopularAdaptor) baseExpandableListAdapter).refreshMe();*/
        }

    /*        else if(baseExpandableListAdapter instanceof ChristianAdaptor) {
                ((ChristianAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof ChristmasAdaptor) {
                ((ChristmasAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof BrowserAdaptor) {
                ((BrowserAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof GospelAdaptor) {
                ((GospelAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof ThemeAdaptor) {
                ((ThemeAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof OrganAdaptor) {
                ((OrganAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof TechinqueAdaptor) {
                ((TechinqueAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof TechinqueAdaptor) {
                ((TheoryAdaptor) baseExpandableListAdapter).refreshMe();
            }
            else if(baseExpandableListAdapter instanceof AdaptorFavourite)
            {
                ((AdaptorFavourite) baseExpandableListAdapter).refreshMe();
            }

            else
            {
                ((SearchAdaptor) baseExpandableListAdapter).refreshMe();
            }*/
        //  }
    }
}