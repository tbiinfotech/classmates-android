package com.brst.classmates.classses;

import android.text.format.DateFormat;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by brst-pc89 on 7/15/17.
 */

public class SystemTime {

    static SystemTime instance = new SystemTime();

    public static SystemTime getInstance() {
        return instance;
    }

    public static String getTime() {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        final String utcTime = sdf.format(new Date());

        return utcTime;

    }


    public static Date gmttoLocalDate(Date date) {

        String timeZone = Calendar.getInstance().getTimeZone().getID();
        Date local = new Date(date.getTime() + TimeZone.getTimeZone(timeZone).getOffset(date.getTime()));

        Log.d("fdg", local + "");
        return local;
    }

    public static String get_time2(String date) {
        Log.e("date", "" + date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        String time = null;
        Calendar c = Calendar.getInstance();
        String formattedDateJOBJ = simpleDateFormat.format(c.getTime());

        Date server_date_time = null;
        try {
            server_date_time = simpleDateFormat.parse(date);
            Log.e("server_date_time", "" + server_date_time);

            Date current_date_time = simpleDateFormat.parse(formattedDateJOBJ);
            Log.e("current_date_time", "" + current_date_time);

            //  long diff = current_date_time.getTime() - server_date_time.getTime() - 120000 - 32000;
            //    time = millisToLongDHMS(diff);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        return time;
    }

    public final static long ONE_SECOND = 1000;
    public final static long SECONDS = 60;

    public final static long ONE_MINUTE = ONE_SECOND * 60;
    public final static long MINUTES = 60;

    public final static long ONE_HOUR = ONE_MINUTE * 60;
    public final static long HOURS = 24;

    public final static long ONE_DAY = ONE_HOUR * 24;
    public final static long MONTHS = ONE_DAY * 30;

    public String millisToLongDHMS(long duration) {
        StringBuffer res = new StringBuffer();
        long temp = 0;
        if (duration >= ONE_SECOND) {

            temp = duration / MONTHS;
            if (temp > 0) {
                duration -= temp * MONTHS;
                res.append(temp).append(" month").append(temp > 1 ? "s" : "");
                return res.toString() + " ago";
                // .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_DAY;
            if (temp > 0) {
                duration -= temp * ONE_DAY;
                res.append(temp).append(" day").append(temp > 1 ? "(s)" : "");
                return res.toString() + " ago";
                // .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_HOUR;
            if (temp > 0) {
                duration -= temp * ONE_HOUR;
                res.append(temp).append(" hour").append(temp > 1 ? "s" : "");
                return res.toString() + " ago";
                // .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_MINUTE;
            if (temp > 0) {
                duration -= temp * ONE_MINUTE;
                res.append(temp).append(" minute").append(temp > 1 ? "s" : "");
                return res.toString() + " ago";
            }

            if (!res.toString().equals("") && duration >= ONE_SECOND) {
                res.append(" and ");
            }

            temp = duration / ONE_SECOND;
            if (temp > 0) {
                res.append(temp).append(" second").append(temp > 1 ? "s" : "");
                return res.toString() + " ago";
            }
            return res.toString();
        } else {
            return "0 second";
        }
    }

    int datelocal = 0, monthlocal = 0;
    String dateformat = null, timeformat = null;
    String dayWeek = null;
    String time;

    public String getLocalTime(String time) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = df.parse(time);
            int minutelocal = date.getMinutes();
            int hourlocal = date.getHours();
            int daylocal = date.getDay();
            datelocal = date.getDate();
            int yearlocal = date.getYear();
            monthlocal = date.getMonth() + 1;


            String datenew = String.format("%02d", datelocal);
            String monthnew = String.format("%02d", monthlocal);
            String hournew = String.format("%02d", hourlocal);
            String minutenew = String.format("%02d", minutelocal);

            if (hourlocal >= 0 && hourlocal < 12) {

                if (hournew.equals("00")) {
                    hournew = hournew.replace("00", "12");
                }

                timeformat = hournew + ":" + minutenew + " AM";
            } else if (hourlocal >= 12 && hourlocal <= 24) {

                if (hourlocal > 12) {
                    hournew = String.valueOf(hourlocal - 12);
                }

                timeformat = hournew + ":" + minutenew + " PM";
            }

            dateformat = datenew + "/" + monthnew;

            switch (daylocal) {
                case 1:
                    dayWeek = "Mon";
                    break;

                case 2:

                    dayWeek = "Tue";
                    break;

                case 3:

                    dayWeek = "Wed";

                    break;
                case 4:

                    dayWeek = "Thr";
                    break;

                case 5:

                    dayWeek = "Fri";
                    break;

                case 6:

                    dayWeek = "Sat";
                    break;


            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        int seconds = c.get(Calendar.SECOND);
        int minute = c.get(Calendar.MINUTE);
        int year = c.get(Calendar.YEAR);
        int day = c.get(Calendar.DAY_OF_WEEK);
        int month = c.get(Calendar.MONTH) + 1;
        int monthday = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR);
        int datecurrent = c.get(Calendar.DATE);


        int datedifference = datecurrent - datelocal;
        int monthdifference = month - monthlocal;


        if (monthdifference == 0) {
            if (datedifference == 0) {

                time = "Today " + timeformat;

            } else if (datedifference > 0 && datedifference <= 7) {


                time = dayWeek + " " + timeformat;
            } else {

                time = dateformat + " " + timeformat;
            }
        } else {


            time = dateformat + " " + timeformat;
        }

        return time;
    }

    public String getDate(String time) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = df.parse(time);
            int minutelocal = date.getMinutes();
            int hourlocal = date.getHours();
            int daylocal = date.getDay();
            datelocal = date.getDate();
            int yearlocal = date.getYear();
            monthlocal = date.getMonth() + 1;
            int secLocal=date.getSeconds();



            String datenew = String.format("%02d", datelocal);
            String monthnew = String.format("%02d", monthlocal);
            String hournew = String.format("%02d", hourlocal);
            String minutenew = String.format("%02d", minutelocal);
            String secondnew = String.format("%02d", secLocal);

            if (hourlocal >= 0 && hourlocal < 12) {

                if (hournew.equals("00")) {
                    hournew = hournew.replace("00", "12");
                }

                timeformat = hournew + ":" + minutenew + ":" + secondnew + " AM";

            }
            else if (hourlocal >= 12 && hourlocal <= 24) {

                /*if (hourlocal > 12) {
                    hournew = String.valueOf(hourlocal - 12);
                }*/

                timeformat = hournew + ":" + minutenew + ":" + secondnew + " PM";
            }
            dateformat = datenew + "/" + monthnew+"/"+yearlocal;

        } catch (Exception e) {

        }

        return  dateformat+" "+timeformat;
    }

}
