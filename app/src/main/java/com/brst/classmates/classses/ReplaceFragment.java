package com.brst.classmates.classses;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.fragments.PlaceFragment;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class ReplaceFragment {

    public static void replace(AppCompatActivity activity, Fragment fragment, String fragment_tag)
    {
      //  FragmentManager fragmentManager =HomeActivity.homeActivity.getSupportFragmentManager();
        FragmentManager fragmentManager =activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.container_fl, fragment,fragment_tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }



    public static void replace(AppCompatActivity activity, Fragment fragment, String fragment_tag, Bundle bundle)
    {
        //  FragmentManager fragmentManager =HomeActivity.homeActivity.getSupportFragmentManager();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager =activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.container_fl, fragment,fragment_tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

  /*  public static void replaceBack(AppCompatActivity activity, Fragment fragment, String fragment_tag)
    {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.container_fl, fragment,fragment_tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
*/

    public static void popBackStack(WelcomeActivity activity) {

        FragmentManager manager =activity.getSupportFragmentManager();
        manager.popBackStack();
    }

    public static void popBackStackAll(WelcomeActivity activity,int count) {


        for (int i=0;i<count;i++)
        {
            FragmentManager manager =activity.getSupportFragmentManager();
            manager.popBackStack();
        }


    }
}
