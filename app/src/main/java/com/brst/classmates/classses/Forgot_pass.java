package com.brst.classmates.classses;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.LoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import static com.brst.classmates.classses.NetworkCheck.isNetworkAvailable;
import static com.brst.classmates.classses.ShowDialog.isValidEmail;
import static com.brst.classmates.classses.ShowDialog.networkDialog;
import static com.brst.classmates.classses.ShowDialog.showSnackbarError;

/**
 * Created by brst-pc89 on 7/21/17.
 */

public class Forgot_pass {

public static Dialog forgot_dialog;

    public static EditText dialogemail_et;
    public static TextView dialogdone_tv,header_text;
    public static String email_address;

    public static void forgot_dialog(final Context context, boolean b) {

        forgot_dialog = new Dialog(context);
        forgot_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgot_dialog.setContentView(R.layout.custom_forgotpass);
        forgot_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        forgot_dialog.show();

        dialogemail_et = (EditText) forgot_dialog.findViewById(R.id.dialogemail_et);
        dialogdone_tv = (TextView) forgot_dialog.findViewById(R.id.dialogdone_tv);
        header_text = (TextView) forgot_dialog.findViewById(R.id.header_text);

        if(true)
        {
            header_text.setText("Reset Password");
        }

        dialogdone_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email_address = dialogemail_et.getText().toString();

                if (email_address.isEmpty()) {
                    showSnackbarError(v, "please enter email address", context);
                } else if (!isValidEmail(email_address)) {
                    showSnackbarError(v, "please enter correct email_address", context);
                } else {

                    if (isNetworkAvailable(context)) {

                        sendForgotEmail(context);

                    } else {
                        networkDialog(context);
                    }
                }
            }
        });
    }


    public static void sendForgotEmail(final Context context) {

        ShowDialog.showDialog(context);


        FirebaseAuth.getInstance().sendPasswordResetEmail(email_address)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {

                            Toast.makeText(context, "Please check your email to reset password.", Toast.LENGTH_LONG).show();
                            dialogemail_et.getText().clear();
                            forgot_dialog.dismiss();
                            //  AddCourseFragment.hideSoftKeyboard(context);


                        } else {
                            if (task.getException().getMessage().equals("There is no user record corresponding to this identifier. The user may have been deleted.")) {
                                Toast.makeText(context, "This email is not registered.", Toast.LENGTH_LONG).show();


                            }
                        }

                        ShowDialog.hideDialog();
                    }
                });
    }


}
