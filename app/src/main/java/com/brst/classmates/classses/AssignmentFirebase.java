package com.brst.classmates.classses;

/**
 * Created by brst-pc89 on 7/24/17.
 */

public class AssignmentFirebase {

    String assignment_name;
    String assignment_type;
    String status;
    String due_date;
    String student;
    String owner_name;
    String owner_profile;
    String courseName;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getOwner_profile() {
        return owner_profile;
    }

    public void setOwner_profile(String owner_profile) {
        this.owner_profile = owner_profile;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }



    public String getAssignment_admin() {
        return assignment_admin;
    }

    public void setAssignment_admin(String assignment_admin) {
        this.assignment_admin = assignment_admin;
    }

    String assignment_admin;

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssignment_type() {
        return assignment_type;
    }

    public void setAssignment_type(String assignment_type) {
        this.assignment_type = assignment_type;
    }

    public String getAssignment_name() {
        return assignment_name;
    }

    public void setAssignment_name(String assignment_name) {
        this.assignment_name = assignment_name;
    }



}
