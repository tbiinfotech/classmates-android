package com.brst.classmates.classses;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TimePicker;
import android.widget.Toast;

import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.utility.Constant;

import java.util.Calendar;

/**
 * Created by brst-pc89 on 8/10/17.
 */



public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {


    int year,month,day;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        Bundle bundle=getArguments();

         year=bundle.getInt(Constant.YEAR_NAME);
         month=bundle.getInt(Constant.MONTH);
         day=bundle.getInt(Constant.DAY);
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
/*
        Calendar datetime = Calendar.getInstance();
        Calendar c = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        datetime.set(Calendar.MINUTE, minute);
        if (datetime.getTimeInMillis() > c.getTimeInMillis()) {*/
          /*  //it's after current
            int hour = hourOfDay % 12;
            btnPickStartTime.setText(String.format("%02d:%02d %s", hour == 0 ? 12 : hour,
                    minute, hourOfDay < 12 ? "am" : "pm"));*/

            AddAssignmentFragment.getInstance().populateSetDate(year,month,day,hourOfDay,minute);
     //   }
        /* else {
            //it's before current'
            Toast.makeText(getContext(), "Invalid Time", Toast.LENGTH_LONG).show();
        }*/

     //   AddAssignmentFragment.getInstance().populateSetDate(year,month,day,hourOfDay,minute);
    }
}
