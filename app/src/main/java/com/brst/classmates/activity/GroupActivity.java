package com.brst.classmates.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.adaptors.GroupAdaptor;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.CourseFragment;
import com.brst.classmates.fragments.MenuFragment;
import com.brst.classmates.fragments.MessageBoardFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.brst.classmates.classses.NetworkCheck.isNetworkAvailable;

public class GroupActivity extends Activity implements View.OnClickListener/*, ClickRecyclerInterface*/ {

    RecyclerViewEmptySupport groupmessage_rv;

    String name[] = {"group1", "group1", "group1"}, groupKey;

    ImageView addcourse_iv, back_msg_iv;

    List<HashMap<String, String>> userList = new ArrayList<>();
    List<String> msg = new ArrayList<>();
    List<String> lasttime = new ArrayList<>();
    List<String> groupKeValue = new ArrayList<>();

    HashMap<String, String> hashMap = new HashMap<>();

    TextView dialogdone_tv, header_text, group_tv, users_tv, emptyview;

    EditText dialogemail_et;

    Query query;

    FirebaseDatabase firebaseDatabase;

   GroupActivity fragment;

    public static String userKey, university;

    DatabaseReference databaseReference;

    List<HashMap<String, String>> groupName = new ArrayList<>();
    List<HashMap<String, String>> msgLisi = new ArrayList<>();
    List<String> groupUserName = new ArrayList<>();

    GroupAdaptor groupAdaptor;

    Dialog group_dialog;


    View view;

    boolean isShowing = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_group);


        userKey = SharedPreference.getInstance().getData( GroupActivity.this, Constant.KEY_NAME);
        university = SharedPreference.getInstance().getData( GroupActivity.this, Constant.UNIVERSITY_NAME);


        bindViews();

        setAdaptor();

        ChecNetwork();

        fragment = GroupActivity.this;

        // fetchGroupName();

        Log.e("QQQQQQQQQQQ", "dsfsdfds");


        setListener();
    }

    private void ChecNetwork() {

        if (isNetworkAvailable( GroupActivity.this)) {

            fetchGroupName();

        } else

        {

            ShowDialog.networkDialog( GroupActivity.this);

            fetchGroupName();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        isShowing = false;

    }

    int a = 0;

    private void fetchGroupName() {


        if (NetworkCheck.isNetworkAvailable( GroupActivity.this)) {
            ShowDialog.showDialog( GroupActivity.this);
        }
        //  }


        firebaseDatabase = FirebaseDatabase.getInstance();
        query = firebaseDatabase.getReference(Constant.CREATEGROUP_NAME).orderByChild(Constant.USER_NAME);

        firebaseDatabase.getReference(Constant.CREATEGROUP_NAME).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null) {

                    getUsers();


                } else {

                    groupAdaptor.addData(groupName);
                    groupAdaptor.notifyDataSetChanged();
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });


    }


    private void getUsers() {


        query.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String groupIcon = "";

                groupName = new ArrayList<>();
                for (DataSnapshot course : dataSnapshot.getChildren()) {

                    String courseKey = course.getKey();


                    for (DataSnapshot child : course.getChildren()) {

                        groupKey = child.getKey();
                        String usersName = child.child(Constant.USER_NAME).getValue() != null ? child.child(Constant.USER_NAME).getValue().toString() : "";
                        String group = child.child(Constant.GROUP_NAME).getValue() != null ? child.child(Constant.GROUP_NAME).getValue().toString() : "";
                        String owner = child.child(Constant.OWNER).getValue() != null ? child.child(Constant.OWNER).getValue().toString() : "";
                        if (child.child(Constant.GROUP_ICON).exists()) {
                            groupIcon = child.child(Constant.GROUP_ICON).getValue() != null ? child.child(Constant.GROUP_ICON).getValue().toString() : "";
                        }

                        HashMap<String, String> hashMap = new HashMap();

                        if (usersName.contains(userKey)) {

                            hashMap.put(Constant.GROUP_kEY, groupKey);
                            hashMap.put(Constant.GROUP_NAME, group);
                            hashMap.put(Constant.COURSE_KEY, courseKey);
                            hashMap.put(Constant.OWNER, owner);
                            if (groupIcon != null) {
                                hashMap.put(Constant.GROUP_ICON, groupIcon);
                            }
                            hashMap.put(Constant.USER_NAME, usersName);

                            Log.d("ssf",usersName);

                            groupName.add(hashMap);

                            // }
                        }
                    }
                }

                groupAdaptor.addData(groupName);
                groupAdaptor.notifyDataSetChanged();

                if (fragment != null ) {
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                if (fragment != null ) {
                    if (ShowDialog.dialog != null) {
                        if (ShowDialog.dialog.isShowing()) {
                            ShowDialog.hideDialog();
                        }
                    }
                }



            }
        });

    }


    

    private void bindViews() {

        groupmessage_rv = (RecyclerViewEmptySupport) findViewById(R.id.groupmessage_rv);
        addcourse_iv = (ImageView)  findViewById(R.id.addcourse_iv);
        back_msg_iv = (ImageView)  findViewById(R.id.back_msg_iv);

        group_tv = (TextView)  findViewById(R.id.group_tv);
        users_tv = (TextView)  findViewById(R.id.users_tv);
        emptyview = (TextView)  findViewById(R.id.result);

        users_tv.setVisibility( View.GONE);
        back_msg_iv.setVisibility( View.GONE);

        groupmessage_rv.addOnItemTouchListener(new GroupAdaptor.RecyclerTouchListener( GroupActivity.this, groupmessage_rv, new ClickRecyclerInterface() {
            @Override
            public void onRecyClick(View view, int position) {


                List<HashMap<String, String>> studentList = ((GroupAdaptor) groupAdaptor).getUserList();

                HashMap<String, String> hashMap = studentList.get(position);

                MessageBoardFragment messageBoardFragment = new MessageBoardFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.GROUP_NAME,  view.getTag().toString());
                bundle.putString(Constant.GROUP_kEY, hashMap.get(Constant.GROUP_kEY));
                bundle.putString(Constant.USER_LIST, hashMap.get(Constant.USER_LIST));
                bundle.putString(Constant.COURSE_KEY, hashMap.get(Constant.COURSE_KEY));
                bundle.putString(Constant.OWNER, hashMap.get(Constant.OWNER));

                messageBoardFragment.setArguments(bundle);

                ReplaceFragment.replace((WelcomeActivity)   WelcomeActivity.context, messageBoardFragment, "Message_Fragment");


            }


            @Override
            public void onLongClick(View view, int position) {

                String uni = "";

                List<HashMap<String, String>> studentList = ((GroupAdaptor) groupAdaptor).getUserList();
                HashMap<String, String> hashMap = studentList.get(position);

                final String groupKey = hashMap.get(Constant.GROUP_kEY);
                final String owner = hashMap.get(Constant.OWNER);

                if (owner.equals(userKey)) {

                    ShowDialog.getInstance().showDeleteDialog( GroupActivity.this, groupKey, uni, 1);
                }


            }


        }));
    }

    private void setAdaptor() {

        groupAdaptor = new GroupAdaptor( GroupActivity.this, groupName);
         RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( GroupActivity.this);
        groupmessage_rv.setLayoutManager(mLayoutManager);
        groupmessage_rv.setAdapter(groupAdaptor);
        groupmessage_rv.setEmptyView(emptyview);


    }

    private void setListener() {

        addcourse_iv.setOnClickListener(this);
        back_msg_iv.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();


        ((WelcomeActivity)  WelcomeActivity.context).header_ll.setVisibility( View.GONE);

        ((WelcomeActivity)WelcomeActivity.context).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.addcourse_iv:

                CourseFragment courseFragment = new CourseFragment();
                //  ReplaceFragment.popBackStack((WelcomeActivity)   WelcomeActivity.context);
                ReplaceFragment.replace((WelcomeActivity)   WelcomeActivity.context, courseFragment, "CourseGroup_Fragment");

                break;

            case R.id.back_msg_iv:

                Fragment fragment = getFragmentManager().findFragmentByTag("group_fragment");
                if (fragment != null && fragment.isVisible()) {
                    ReplaceFragment.popBackStack((WelcomeActivity)   WelcomeActivity.context);
                    ReplaceFragment.replace((WelcomeActivity)   WelcomeActivity.context, new MenuFragment(), "Menus_fragment");

                } else {

                    ReplaceFragment.popBackStack((WelcomeActivity)   WelcomeActivity.context);
                }

                break;

        }
    }


}