package com.brst.classmates.activity;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.adaptors.ContactAdapter;
import com.brst.classmates.adaptors.NavigationAdaptor;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermission;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.Assign_ExamFragment;
import com.brst.classmates.fragments.ContactFragment;
import com.brst.classmates.fragments.ContactInviteFragment;
import com.brst.classmates.fragments.GroupFragment;
import com.brst.classmates.fragments.NetworkFragment;
import com.brst.classmates.fragments.Event_Invite_Fragment;
import com.brst.classmates.fragments.MenuFragment;
import com.brst.classmates.fragments.MessageBoardFragment;
import com.brst.classmates.fragments.OneToOneChatFragment;
import com.brst.classmates.fragments.ProfileFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class WelcomeActivity extends AbsRuntimeMarshmallowPermission implements View.OnClickListener, ClickRecyclerInterface {

    FrameLayout container_fl;
    MenuFragment menuFragment;
    MessageBoardFragment messageBoardFragment;

    OneToOneChatFragment oneToOneChatFragment;

    RecyclerView lst_menu_items;

    public LinearLayout header_ll;

    public TextView header_tv;

    private NavigationView navigationView;
    public static DrawerLayout drawer;

    public Boolean isFirstTime;

    Fragment menufragment, profilefragment;

    String list[] = {"Home", "Update Profile", "Passsword Reset", "Import Contacts", "Notification", "Logout"};
    Bundle extras;
    public ImageView header_iv, profile_iv;
    String shared_data;

    public static View logout_iv;

    Boolean doubleBackToExitPressedOnce = false;
    Boolean exit = false;
    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        context = WelcomeActivity.this;

        Intent intent = getIntent();
        extras = intent.getExtras();


        isFirstTime = SharedPreference.getInstance().getIsFirstTime(getApplicationContext(), Constant.ISFIRSTTIME);
        if (isFirstTime) {
            SharedPreference.getInstance().storeFirstTime(getApplicationContext(), Constant.ISFIRSTTIME, false);
        }

        menufragment = getSupportFragmentManager().findFragmentByTag("Menu_Fragment");
        profilefragment = getSupportFragmentManager().findFragmentByTag("Profile_Fragment");

        shared_data = SharedPreference.getInstance().getData(getApplicationContext(), Constant.EMAIL_NAME);

        bindViews();

        initFragment();

        setListener();

        setUpNavigationView();

        try {

            if (shared_data.equals("null")) {

                Intent intent1 = new Intent(getApplication(), MainActivity.class);
                startActivity(intent1);

            } else {
                Log.d("extras",extras+"");
                if (extras != null) {
                    Log.d("extras",extras+"------");
                    if (extras.containsKey("notification")) {

                        exit = true;
                        String notification = extras.getString("notification");
                        String groupKey = extras.getString(Constant.GROUP_kEY);
                        String groupName = extras.getString(Constant.GROUP_NAME);
                        String userList = extras.getString(Constant.USER_LIST);
                        String courseKey = extras.getString(Constant.COURSE_KEY);
                        String owner = extras.getString(Constant.OWNER);
                        String message = extras.getString(Constant.TEXT_NAME);

                        String messageArray[] = message.split(":");
                        String messName = messageArray[0];

                        if (messName.equals("Assignment") || notification.equals("notificationLocal")) {
                            Bundle bundle = new Bundle();
                            if (messName.equals("Assignment")) {
                                bundle.putString("assignment", messName);
                            }
                            ReplaceFragment.replace(this, new Assign_ExamFragment(), "Assignment_Fragment", bundle);
                        } else if (messName.equals("Event") || messName.equals("Meeting")) {
                            Bundle bundle = new Bundle();
                            bundle.putString("event", "Event");
                            ReplaceFragment.replace(this, new Event_Invite_Fragment(), "eventInvite_fragment", bundle);
                        } else if (extras.containsKey(Constant.GROUP_NAME)) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constant.GROUP_NAME, groupName);
                            bundle.putString(Constant.GROUP_kEY, groupKey);
                            bundle.putString(Constant.USER_LIST, userList);
                            bundle.putString(Constant.COURSE_KEY, courseKey);
                            bundle.putString(Constant.OWNER, owner);


                            messageBoardFragment.setArguments(bundle);

                            replaceFragment(messageBoardFragment, "MessageBoard_Fragment");
                        } else if (extras.containsKey(Constant.CHAT)) {

                            String chatKey = extras.getString(Constant.GROUP_kEY);
                            String senderName = extras.getString(Constant.SENDER_NAME);
                            String senderImage = extras.getString(Constant.IMAGE_NAME);

                            Bundle bundle = new Bundle();
                            bundle.putString("imagePath", senderImage);
                            bundle.putString("userName", senderName);
                            bundle.putString("chatKey", chatKey);

                            OneToOneChatFragment oneToOneChatFragment = new OneToOneChatFragment();
                            oneToOneChatFragment.setArguments(bundle);
                            ReplaceFragment.replace(WelcomeActivity.this, oneToOneChatFragment, "OneToOne_Fragment");


                        }
                    }
                } else {
                    Log.d("extras",extras+"gfgfhfghfg-----");
                    replaceFragment(menuFragment, "Menu_Fragment");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 500) {
            // readContacts();

            ReplaceFragment.replace(((WelcomeActivity) context), new ContactInviteFragment(), "ContactInvite_Fragment");
        }
    }



 /*   @Override
    public void onPermissionGranted(int requestCode) {

        if (requestCode == 500) {
           // readContacts();

            ReplaceFragment.replace(WelcomeActivity.this,new NetworkFragment(),"Contact_Fragment");
        }

    }*/


    public void replace() {
        replaceFragment(new GroupFragment(), "Group Fragment");
    }

    private void readContacts() {

     /*   Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

        startActivityForResult(intent, 1000);*/

        final int REQUEST_CODE_PICK_CONTACT = 1000;
        final int MAX_PICK_CONTACT = 10;

        Intent phonebookIntent = new Intent("intent.action.INTERACTION_TOPMENU");
        phonebookIntent.putExtra("additional", "phone-multi");
        phonebookIntent.putExtra("maxRecipientCount", MAX_PICK_CONTACT);
        phonebookIntent.putExtra("FromMMS", true);
        startActivityForResult(phonebookIntent, REQUEST_CODE_PICK_CONTACT);

    }

    private void setUpNavigationView() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.pass_reset:

                        sendForgotEmail();


                        break;

                    case R.id.update_profile:

                        ReplaceFragment.replace(((WelcomeActivity) context), new ProfileFragment(), "Profile_Fragment");
                        break;

                    case R.id.mute:

                        SharedPreference.getInstance().storeData(context, Constant.SOUND, Constant.MUTE);
                        break;

                    case R.id.unmute:

                        SharedPreference.getInstance().storeData(context, Constant.SOUND, Constant.UNMUTE);

                        break;

                    case R.id.logout:

                        //   FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).child(StoreData.getUserKeyFromSharedPre(context)).child(Constant.NOTIFICATION_KEY).setValue("false");
                        SharedPreference.getInstance().clearData(context, Constant.EMAIL_NAME);
                        String key = SharedPreference.getInstance().getData(context, Constant.KEY_NAME);
                        HashMap hashMap = new HashMap<>();
                        hashMap.put("status", "inActive");
                        hashMap.put(Constant.NOTIFICATION_KEY, "false");
                        FirebaseDatabase.getInstance().getReference("registered_user").child(key).updateChildren(hashMap);
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);

                        finish();

                        break;

                }

                drawer.closeDrawers();
                return false;
            }
        });
    }

    private void setListener() {

//        logout_iv.setOnClickListener(this);
        profile_iv.setOnClickListener(this);
    }

    private void replaceFragment(Fragment fragment, String message_fragment) {

        ReplaceFragment.replace(this, fragment, message_fragment);
        // ReplaceFragment.replace(WelcomeActivity.this,new SearchStudentFragment(),"SearchStudent_Fragment");
    }


    private void initFragment() {
       /* if (ProfileActivity.fragment == 3) {
            // messageBoardFragment = new MessageBoardFragment();
            //  GroupFragment groupFragment = new GroupFragment();
            Bundle bundle = getIntent().getExtras();

            OneToOneChatFragment oneToOneChatFragment = new OneToOneChatFragment();
            oneToOneChatFragment.setArguments(bundle);
            ReplaceFragment.replace(WelcomeActivity.this, oneToOneChatFragment, "OneToOne_Fragment");
        } else {*/
            menuFragment = new MenuFragment();
            messageBoardFragment = new MessageBoardFragment();
      //  }

    }

    private void bindViews() {

        container_fl = (FrameLayout) findViewById(R.id.container_fl);


        header_tv = (TextView) findViewById(R.id.header_tv);

        lst_menu_items = (RecyclerView) findViewById(R.id.lst_menu_items);

        header_iv = (ImageView) findViewById(R.id.header_iv);

        // logout_iv = (View) findViewById(R.id.logout_iv);

        // logout_iv.setVisibility(View.VISIBLE);

        profile_iv = (ImageView) findViewById(R.id.profile_iv);

        header_ll = (LinearLayout) findViewById(R.id.header_ll);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

       /* NavigationAdaptor navigationAdaptor=new NavigationAdaptor(getBaseContext(),list);
        lst_menu_items.setAdapter(navigationAdaptor);*/

        NavigationAdaptor navigationAdaptor = new NavigationAdaptor(list, context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        lst_menu_items.setLayoutManager(mLayoutManager);
        lst_menu_items.setAdapter(navigationAdaptor);

        navigationAdaptor.setClickListener(this);


        //   drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }


    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        Fragment fragment=getSupportFragmentManager().findFragmentById(R.id.container_fl);
        if (fragment instanceof ProfileFragment)
        {
           ReplaceFragment.popBackStack(this);
            ReplaceFragment.replace(this,new MenuFragment(),"menu_fragment");
        }

        else {

            if (exit) {

                exit = false;

                finish();
                Intent intent = new Intent(this, WelcomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            } else {
                if (count != 1) {

                    super.onBackPressed();

                    drawer.closeDrawers();

                } else if (ProfileActivity.fragment == 3) {
                    ProfileActivity.fragment = 0;
                    finish();
                } else {

                    if (doubleBackToExitPressedOnce) {
                        super.onBackPressed();
                        drawer.closeDrawers();

                        finish();
                    }
                    drawer.closeDrawers();
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                }

            }
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.logout_iv:
                ShowDialog.showPopup(v, context, false);
                break;

            case R.id.profile_iv:

               /* ReplaceFragment.replace(((WelcomeActivity) context),new ProfileFragment(),"Profile_Fragment");*/
                Log.d("open", "open");
                drawer.openDrawer(navigationView);

                break;
        }


    }

    public static void sendForgotEmail() {

        ShowDialog.showDialog(context);


        FirebaseAuth.getInstance().sendPasswordResetEmail(StoreData.getUserEmailFromSharedPre(context))
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {

                            Toast.makeText(context, "Please check your email to reset password.", Toast.LENGTH_LONG).show();

                            //  AddCourseFragment.hideSoftKeyboard(context);


                        } else {
                            if (task.getException().getMessage().equals("There is no user record corresponding to this identifier. The user may have been deleted.")) {
                                Toast.makeText(context, "This email is not registered.", Toast.LENGTH_LONG).show();


                            }
                        }

                        ShowDialog.hideDialog();
                    }
                });
    }


    @Override
    public void onRecyClick(View view, int position) {

        if (position == 0) {
            ReplaceFragment.popBackStack((WelcomeActivity) context);
            ReplaceFragment.replace(((WelcomeActivity) context), new MenuFragment(), "Menu_Fragment");
        }

        if (position == 1) {
            ReplaceFragment.popBackStack((WelcomeActivity) context);
            ReplaceFragment.replace(((WelcomeActivity) context), new ProfileFragment(), "Profile_Fragment");

        } else if (position == 2) {

            sendForgotEmail();

        } else if (position == 3) {

            String[] permission = {Manifest.permission.READ_CONTACTS, Manifest.permission.SEND_SMS};

            requestAppPermissions(permission, R.string.permission, 500);

            // ReplaceFragment.replace(WelcomeActivity.this, new NetworkFragment(), "Contact_Fragment");
        } else if (position == 4) {


            SharedPreference.getInstance().storeData(context, Constant.SOUND, Constant.MUTE);


        } else if (position == 5) {

            SharedPreference.getInstance().clearData(context, Constant.EMAIL_NAME);
            String key = SharedPreference.getInstance().getData(context, Constant.KEY_NAME);
            HashMap hashMap = new HashMap<>();
            hashMap.put("status", "inActive");
            FirebaseDatabase.getInstance().getReference("registered_user").child(key).updateChildren(hashMap);

            Intent intent = new Intent(context, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);

            finish();

        }

        drawer.closeDrawers();
    }

    @Override
    public void onLongClick(View view, int position) {

    }


    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (reqCode == 1000 && resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();

            Cursor cursorID = getContentResolver().query(contactData,
                    new String[]{ContactsContract.Contacts._ID},
                    null, null, null);
            String contactID = null;
            if (cursorID.moveToFirst()) {

                contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
            }
            Cursor cursorPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                            ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                    new String[]{contactID},
                    null);
            String contactNumber;
            if (cursorPhone.moveToFirst()) {

                contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


                String sms = "Follow the link: https://play.google.com/store/apps/details?id=com.brst.classmates";
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(contactNumber, null, sms, null, null);
                    Toast.makeText(getApplicationContext(), "SMS Sent to: " + contactNumber,
                            Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again later!",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            cursorPhone.close();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();


        countDownTimer.start();

    }

    @Override
    protected void onResume() {
        super.onResume();

        countDownTimer.cancel();
    }

    CountDownTimer countDownTimer=new CountDownTimer(60000*5, 1000) {

        public void onTick(long millisUntilFinished) {

        }

        public void onFinish() {


            finish();
        }

    };

}