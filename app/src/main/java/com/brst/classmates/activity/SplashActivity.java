package com.brst.classmates.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.brst.classmates.R;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermission;
import com.brst.classmates.classses.FirebaseData;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.utility.Constant;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.HashMap;

import static com.brst.classmates.activity.WelcomeActivity.context;
import static com.brst.classmates.utility.Constant.SPLASH_TIME_OUT;

public class SplashActivity extends AppCompatActivity {

    public static FirebaseAuth mAuth;
    public static FirebaseAuth.AuthStateListener mAuthListener;

    String shared_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent ExplicitIntent = getIntent();

        if (ExplicitIntent != null) {
            try {

                String str = ExplicitIntent.getStringExtra("body");
                if (str != null) {
                    JSONObject jsonObject = new JSONObject(str);
                    String groupID = "", courseKey = "";
                    Bundle extras = new Bundle();
                    extras.putString("notification", "notification");
                    if (jsonObject.has(Constant.GROUP_kEY)) {
                        groupID = jsonObject.getString(Constant.GROUP_kEY);

                        extras.putString(Constant.GROUP_kEY, groupID);
                    }
                    if (jsonObject.has(Constant.GROUP_NAME)) {
                        String groupName = jsonObject.getString(Constant.GROUP_NAME);


                        extras.putString(Constant.GROUP_NAME, groupName);
                    }
                    if (jsonObject.has(Constant.MESSAGE)) {
                        String message = jsonObject.getString(Constant.MESSAGE);
                        extras.putString(Constant.TEXT_NAME, message);
                    }

                    if (jsonObject.has(Constant.SENDER_NAME)) {
                        String senderName = jsonObject.getString(Constant.SENDER_NAME);
                        extras.putString(Constant.SENDER_NAME, senderName);
                    }
                    if (jsonObject.has(Constant.USER_LIST)) {
                        String userList = jsonObject.getString(Constant.USER_LIST);

                        extras.putString(Constant.USER_LIST, userList);
                    }
                    if (jsonObject.has(Constant.COURSE_KEY)) {
                        courseKey = jsonObject.getString(Constant.COURSE_KEY);

                        extras.putString(Constant.COURSE_KEY, courseKey);
                    }
                    if (jsonObject.has(Constant.OWNER)) {
                        String owner = jsonObject.getString(Constant.OWNER);

                        extras.putString(Constant.OWNER, owner);
                    }
                    if (jsonObject.has(Constant.IMAGE_NAME)) {
                        String image = jsonObject.getString(Constant.IMAGE_NAME);

                        extras.putString(Constant.IMAGE_NAME, image);
                        ;
                    }
                    if (jsonObject.has(Constant.CHAT)) {
                        String chat = jsonObject.getString(Constant.CHAT);

                        extras.putString(Constant.CHAT, chat);

                    }
                    if (jsonObject.has("msg_key")) {

                        String msg_key = jsonObject.getString("msg_key");
                        FirebaseData firebaseData = new FirebaseData();
                        firebaseData.setDeliver(true);

                        HashMap hashMap = new HashMap();
                        hashMap.put("deliver", true);
                        FirebaseDatabase.getInstance().getReference("group").child(courseKey).child(groupID).child("chat").child(msg_key).updateChildren(hashMap);
                    }

                    Intent intent = new Intent(getApplication(), WelcomeActivity.class);

                    intent.putExtras(extras);

                    startActivity(intent);
                    finish();

                } else {
                    setContentView(R.layout.activity_splash);
                    shared_data = SharedPreference.getInstance().getData(getApplicationContext(), Constant.EMAIL_NAME);

                    moveNext();
                }
            } catch (Exception e) {

            }
        } else {

            setContentView(R.layout.activity_splash);


            shared_data = SharedPreference.getInstance().getData(getApplicationContext(), Constant.EMAIL_NAME);

            moveNext();
        }
    }


    private void moveNext() {

        new Handler().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {
                                          Log.d("data", "data");
                                          checkSharedData();

                                          finish();
                                      }
                                  }, SPLASH_TIME_OUT
        );
    }

    private void checkSharedData() {


        if (shared_data.equals("null")) {

            Intent intent = new Intent(getApplication(), MainActivity.class);
            startActivity(intent);

        } /*else {
            Intent ExplicitIntent = getIntent();

            try {
                if (ExplicitIntent != null) {
                    String str = ExplicitIntent.getStringExtra("body");

                    JSONObject jsonObject = new JSONObject(str);
                    String groupID = "", courseKey = "";
                    Bundle extras = new Bundle();
                    extras.putString("notification", "notification");
                    if (jsonObject.has(Constant.GROUP_kEY)) {
                        groupID = jsonObject.getString(Constant.GROUP_kEY);

                        extras.putString(Constant.GROUP_kEY, groupID);
                    }
                    if (jsonObject.has(Constant.GROUP_NAME)) {
                        String groupName = jsonObject.getString(Constant.GROUP_NAME);


                        extras.putString(Constant.GROUP_NAME, groupName);
                    }
                    if (jsonObject.has(Constant.MESSAGE)) {
                        String message = jsonObject.getString(Constant.MESSAGE);
                        extras.putString(Constant.TEXT_NAME, message);
                    }

                    if (jsonObject.has(Constant.SENDER_NAME)) {
                        String senderName = jsonObject.getString(Constant.SENDER_NAME);
                        extras.putString(Constant.SENDER_NAME, senderName);
                    }
                    if (jsonObject.has(Constant.USER_LIST)) {
                        String userList = jsonObject.getString(Constant.USER_LIST);

                        extras.putString(Constant.USER_LIST, userList);
                    }
                    if (jsonObject.has(Constant.COURSE_KEY)) {
                        courseKey = jsonObject.getString(Constant.COURSE_KEY);

                        extras.putString(Constant.COURSE_KEY, courseKey);
                    }
                    if (jsonObject.has(Constant.OWNER)) {
                        String owner = jsonObject.getString(Constant.OWNER);

                        extras.putString(Constant.OWNER, owner);
                    }
                    if (jsonObject.has(Constant.IMAGE_NAME)) {
                        String image = jsonObject.getString(Constant.IMAGE_NAME);

                        extras.putString(Constant.IMAGE_NAME, image);
                        ;
                    }
                    if (jsonObject.has(Constant.CHAT)) {
                        String chat = jsonObject.getString(Constant.CHAT);

                        extras.putString(Constant.CHAT, chat);

                    }
                    if (jsonObject.has("msg_key")) {

                        String msg_key = jsonObject.getString("msg_key");
                        FirebaseData firebaseData = new FirebaseData();
                        firebaseData.setDeliver(true);

                        HashMap hashMap = new HashMap();
                        hashMap.put("deliver", true);
                        FirebaseDatabase.getInstance().getReference("group").child(courseKey).child(groupID).child("chat").child(msg_key).updateChildren(hashMap);
                    }

                    Intent intent = new Intent(getApplication(), WelcomeActivity.class);

                    intent.putExtras(extras);

                    startActivity(intent);


                }*/ else {

            Intent intent = new Intent(getApplication(), WelcomeActivity.class);
            startActivity(intent);
        }

           /* } catch (Exception e) {

                Intent intent = new Intent(getApplication(), WelcomeActivity.class);
                startActivity(intent);
            }*/

        // }
    }


}
