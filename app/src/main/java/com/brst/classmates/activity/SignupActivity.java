package com.brst.classmates.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.Spinner;


import com.brst.classmates.R;
import com.brst.classmates.beanclass.UserProfile;
import com.brst.classmates.classses.AbsRuntimeMarshmallowPermission;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.AddCourseFragment;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.brst.classmates.activity.SplashActivity.mAuth;
import static com.brst.classmates.classses.ShowDialog.isValidEmail;
import static com.brst.classmates.classses.ShowDialog.showSnackbarError;

public class SignupActivity extends AbsRuntimeMarshmallowPermission implements View.OnClickListener {

    EditText firstName_et, lastName_et, emailAdd_et_, pass_et, degree_et, year_et, activity_et;

    TextInputLayout country_inputLayout, university_inputLayout, college_inputLayout;

    String country_list[] = {"United States", "India", "Canada", "England", "Singapore", "Brazil",
            "South Korea", "Spain", "Italy", "Pakistan", "Australia", "United Kingdom",
            "Germany", "Malaysia", "France", "Poland", "Indonesia",};

    String countryItem = "", universityItem = "", collegeItem = "", firstName = "", lastName = "",
            email = "", password = "", degree = "", year = "", activityName = "";

    String picture_Path;
    File destination_path;

    TextView sign_tv;

    AutoCompleteTextView country_act, university_act, college_act, degree_act;

    ImageView profilesign_iv, back_iv;

    Uri imageUploadUri, downloadImageUri, imageuri;



    List collegeList = new ArrayList();
    List degreeList = new ArrayList();
    List specialList = new ArrayList();


    List degreeNew = new ArrayList();
    List specialNew = new ArrayList();
    List<String> collegeKeyList = new ArrayList();
    List<String> degreeKeyList = new ArrayList();


    FirebaseStorage firebaseStorage;
    StorageReference storageReference;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, universityReference, collegeReference, degreeReference, specialReference;
    String key;
    SQLiteDatabase database;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        context = SignupActivity.this;

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Constant.REGISTERED_NAME);

        collegeReference = firebaseDatabase.getReference(Constant.COLLEGE);

        degreeReference = firebaseDatabase.getReference(Constant.DEGREE_NAME);
        specialReference = firebaseDatabase.getReference("specialization");

        mAuth = FirebaseAuth.getInstance();

        bindViews();

        setListener();

        getCollegeList();

        getDegreeList();

        getSpecialList();

    }

    /*----------------------------get degrr likst--------------------------*/

    private void getDegreeList() {

        degreeReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    String collegeKey = snapshot.getKey();

                    for (DataSnapshot snapshot1 : snapshot.getChildren()) {

                        HashMap hashMap = new HashMap();
                        hashMap.put(Constant.COLLEGE_NAME, collegeKey);

                        hashMap.put(Constant.DEGREE_NAME, snapshot1.child(Constant.DEGREE_NAME).getValue());

                        degreeKeyList.add(snapshot1.getKey());
                        degreeList.add(hashMap);

                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /*-------------------------------get college list------------------------*/

    private void getSpecialList() {


        specialReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    String degreeKey = snapshot.getKey();


                    for (DataSnapshot snapshot1 : snapshot.getChildren()) {

                        HashMap hashMap = new HashMap();

                            hashMap.put(Constant.DEGREE_NAME, degreeKey);
                            if (snapshot1.child("specialization").exists()) {
                                hashMap.put("specialization", snapshot1.child("specialization").getValue());

                                specialList.add(hashMap);


                        }


                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


/*----------------------get unicersity list from firebase---------------------------*/

    private void getCollegeList() {


        collegeReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    if (snapshot.child(Constant.COLLEGE).exists())
                    {
                        collegeKeyList.add(snapshot.getKey());
                        collegeList.add(snapshot.child(Constant.COLLEGE).getValue());
                    }


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

/*-----------------------------------------get id of views------------------------------------------*/

    private void bindViews() {


        sign_tv = (TextView) findViewById(R.id.sign_tv);
        profilesign_iv = (ImageView) findViewById(R.id.profilesign_iv);
        back_iv = (ImageView) findViewById(R.id.back_iv);

        firstName_et = (EditText) findViewById(R.id.firstName_et);
        lastName_et = (EditText) findViewById(R.id.lastName_et);
        emailAdd_et_ = (EditText) findViewById(R.id.emailAdd_et_);

        pass_et = (EditText) findViewById(R.id.pass_et);
        year_et = (EditText) findViewById(R.id.year_et);

        college_inputLayout = (TextInputLayout) findViewById(R.id.college_inputLayout);
        university_inputLayout = (TextInputLayout) findViewById(R.id.university_inputLayout);
        country_inputLayout = (TextInputLayout) findViewById(R.id.country_inputLayout);

        country_act = (AutoCompleteTextView) findViewById(R.id.country_act);
        university_act = (AutoCompleteTextView) findViewById(R.id.university_act);
        college_act = (AutoCompleteTextView) findViewById(R.id.college_act);
        degree_act = (AutoCompleteTextView) findViewById(R.id.degree_act);
        // college_act.setEnabled(false);
        // degree_act.setEnabled(false);


        ArrayAdapter<String> universityAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, collegeList);

        university_act.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                degreeNew.clear();
                degree_act.setEnabled(true);
                degree_act.getText().clear();

                for (int i = 0; i < degreeList.size(); i++) {
                    HashMap hashMap = (HashMap) degreeList.get(i);

                    String collegeName = hashMap.get(Constant.COLLEGE_NAME).toString();

                    if (collegeName.equals(collegeKeyList.get(position))) {
                        String college = hashMap.get(Constant.DEGREE_NAME).toString();

                        degreeNew.add(college);
                    }

                }

                ArrayAdapter<String> degreeAdapter = new ArrayAdapter<String>(SignupActivity.this,
                        android.R.layout.simple_dropdown_item_1line, degreeNew);

                degree_act.setAdapter(degreeAdapter);
                degree_act.setThreshold(1);
            }
        });

        degree_act.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {


                specialNew.clear();

                college_act.setEnabled(true);
                college_act.getText().clear();

                for (int i = 0; i < specialList.size(); i++) {
                    HashMap hashMap = (HashMap) specialList.get(i);

                    String degreeKey = hashMap.get(Constant.DEGREE_NAME).toString();

                    if (degreeKey.equals(degreeKeyList.get(position))) {
                        String specialization = hashMap.get("specialization").toString();

                        specialNew.add(specialization);
                    }

                }

                ArrayAdapter<String> collegeAdapter = new ArrayAdapter<String>(SignupActivity.this,
                        android.R.layout.simple_dropdown_item_1line, specialNew);

                college_act.setAdapter(collegeAdapter);
                college_act.setThreshold(1);
            }
        });


        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, country_list);


        country_act.setAdapter(countryAdapter);
        country_act.setThreshold(1);


        university_act.setAdapter(universityAdapter);
        university_act.setThreshold(1);


    }


    /*----------------------------------set click listener------------------------------ */

    private void setListener() {

        sign_tv.setOnClickListener(this);

        profilesign_iv.setOnClickListener(this);

        back_iv.setOnClickListener(this);

    }

/*--------------------------click listener on dialog for capture image and select from gallery-------------------------*/

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();

            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

            requestAppPermissions(permission, R.string.permission, 54);


        }
    };


    View.OnClickListener cameraClickLictener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ShowDialog.profileDialog.dismiss();

            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            requestAppPermissions(permission, R.string.permission, 55);


        }
    };




    /*---------------------------------set profile image in image view after capture and teke from gallery------------------------ */

    private void setProfileImage() {

        Glide.with(this).load(imageUploadUri).apply(RequestOptions.circleCropTransform()).into(profilesign_iv);
    }


    /*--------------------------------get text field data on button click------------------------------------------*/

    private void getFieldData(View v) {

        firstName = firstName_et.getText().toString();
        lastName = lastName_et.getText().toString();
        email = emailAdd_et_.getText().toString();
        password = pass_et.getText().toString();
        degree = degree_act.getText().toString();
        year = year_et.getText().toString();
        countryItem = country_act.getText().toString();
        collegeItem = college_act.getText().toString();
        universityItem = university_act.getText().toString();

         checkFieldEmpty(v);


    }


    /*----------------------------------check field empty or not-----------------------------------------*/

    private void checkFieldEmpty(View v) {


        if (firstName.isEmpty()) {

            showSnackbarError(v, getString(R.string.enterfirst_name), context);
        } else if (lastName.isEmpty()) {
            showSnackbarError(v, getString(R.string.enterlast_name), context);
        } else if (email.isEmpty()) {
            showSnackbarError(v, getString(R.string.enteremail_name), context);
        } else if (!isValidEmail(email)) {
            showSnackbarError(v, getString(R.string.entercorrectemail_name), context);
        } else if (password.isEmpty()) {
            showSnackbarError(v, getString(R.string.enterpassword_name), context);
        } else if (password.length() < 6) {
            showSnackbarError(v, getString(R.string.enterpasswordlength_name), context);
        } else if (countryItem.isEmpty()) {
            showSnackbarError(v, getString(R.string.entercountry_name), context);
        } else if (universityItem.isEmpty()) {
            showSnackbarError(v, getString(R.string.enteruniversity_name), context);
        } else if (collegeItem.isEmpty()) {
            showSnackbarError(v, getString(R.string.entercollege_name), context);
        } else if (degree.isEmpty()) {
            showSnackbarError(v, getString(R.string.enterdegree_name), context);
        } else if (year.isEmpty()) {
            showSnackbarError(v, getString(R.string.enteryear_name), context);
        } else if (imageUploadUri == null) {

            storeDataFirebaseAuth(true);


        } else {
            imageUpload();

        }

    }

    /*----------------------------------------create user in firebase auth with email and password----------------------------*/
    private void storeDataFirebaseAuth(boolean b) {

        if (b) {

            ShowDialog.showDialog(context);
        }


        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (!task.isSuccessful()) {

                            if (task.getException().getMessage().equals(getString(R.string.checkemail_name))) {
                                Toast.makeText(getApplication(), getString(R.string.alreadyemail_name), Toast.LENGTH_LONG).show();

                                ShowDialog.hideDialog();
                            }

                        } else {

                            storeFirebaseDatabase();

                        }


                    }
                });


    }

    /*--------------------------store data in firebase database ---------------------------------------------*/
    String collegeKey, degreeKey, specialKey;

    private void storeFirebaseDatabase() {

       // universityItem = universityItem.replaceAll("[^a-zA-Z0-9]", " ");
        universityItem = universityItem.replaceAll("\\s+", " ").trim();
       // degree = degree.replaceAll("[^a-zA-Z0-9]", " ");
        degree = degree.replaceAll("\\s+", " ").trim();
        collegeItem = collegeItem.replaceAll("\\s+", " ");

        collegeReference.orderByChild(Constant.COLLEGE).equalTo(universityItem).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.getValue() == null) {

                    collegeKey = collegeReference.push().getKey();
                    HashMap hashMap = new HashMap();
                    hashMap.put(Constant.COLLEGE, universityItem);
                    collegeReference.child(collegeKey).updateChildren(hashMap);
                } else {
                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                    while (iterator.hasNext()) {
                        DataSnapshot dataSnapshot1 = iterator.next();

                        collegeKey = dataSnapshot1.getKey();
                    }

                }

                degreeReference.child(collegeKey).orderByChild(Constant.DEGREE_NAME).equalTo(degree).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {



                        if (dataSnapshot.getValue() == null) {

                            degreeKey = degreeReference.child(collegeKey).push().getKey();
                            HashMap hashMap = new HashMap();
                            hashMap.put(Constant.DEGREE_NAME, degree);
                            degreeReference.child(collegeKey).child(degreeKey).updateChildren(hashMap);
                        } else {
                            Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                            while (iterator.hasNext()) {
                                DataSnapshot dataSnapshot1 = iterator.next();

                                degreeKey = dataSnapshot1.getKey();
                            }
                        }

                        specialReference.child(degreeKey).orderByChild("specialization").equalTo(collegeItem).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (dataSnapshot.getValue() == null) {

                                    specialKey = specialReference.child(degreeKey).push().getKey();
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("specialization", collegeItem);
                                    specialReference.child(degreeKey).child(specialKey).updateChildren(hashMap);
                                } else {
                                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                                    while (iterator.hasNext()) {
                                        DataSnapshot dataSnapshot1 = iterator.next();
                                        specialKey = dataSnapshot1.getKey();
                                    }
                                }

                                key = databaseReference.push().getKey();
                                UserProfile userProfile = new UserProfile();
                                userProfile.setFirst_name(firstName);
                                userProfile.setLast_name(lastName);
                                userProfile.setEmail(email);
                                userProfile.setCountry_name(countryItem);
                               // userProfile.setUniversity_name(universityItem);
                                userProfile.setCollege_name(universityItem);
                                userProfile.setSpecialization(collegeItem);
                                userProfile.setDegree(degree);
                                userProfile.setYear(year);
                                userProfile.setStatus(Constant.INACTIVE_NAME);
                                userProfile.setStranger("");
                                userProfile.setNotification("false");

                                if (downloadImageUri != null && !downloadImageUri.equals(Uri.EMPTY)) {
                                    userProfile.setProfile_picture(downloadImageUri.toString());
                                }
                                databaseReference.child(key).setValue(userProfile, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                        if (!user.isEmailVerified()) {
                                            sendVerificationEmail();
                                        }

                                        FirebaseAuth.getInstance().signOut();

                                    }
                                });
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*universityReference.child(universityItem).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.getValue() == null) {

                    HashMap hashMap = new HashMap();
                    hashMap.put(Constant.COURSE_NAME, "");
                    universityReference.child(universityItem).push().updateChildren(hashMap);
                }




                        collegeReference.child(degree).orderByChild("Branch").equalTo(collegeItem).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {


                                if (dataSnapshot.getValue() == null) {
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("Branch", collegeItem);


                                    collegeReference.child(degree).push().updateChildren(hashMap);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });



                        degreeReference.child(universityItem).orderByChild(Constant.DEGREE_NAME).equalTo(degree).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {


                                if (dataSnapshot.getValue() == null) {


                                    HashMap hashMap = new HashMap();
                                    hashMap.put(Constant.DEGREE_NAME, degree);


                                    degreeReference.child(universityItem).push().updateChildren(hashMap);
                                }

                                   key = databaseReference.push().getKey();
                                    UserProfile userProfile = new UserProfile();
                                    userProfile.setFirst_name(firstName);
                                    userProfile.setLast_name(lastName);
                                    userProfile.setEmail(email);
                                    userProfile.setCountry_name(countryItem);
                                    userProfile.setUniversity_name(universityItem);
                                    userProfile.setCollege_name(collegeItem);
                                    userProfile.setDegree(degree);
                                    userProfile.setYear(year);
                                    userProfile.setStatus(Constant.INACTIVE_NAME);
                                    userProfile.setStranger("");
                                    userProfile.setNotification("false");

                                    if (downloadImageUri != null && !downloadImageUri.equals(Uri.EMPTY)) {
                                        userProfile.setProfile_picture(downloadImageUri.toString());
                                    }
                                    databaseReference.child(key).setValue(userProfile, new DatabaseReference.CompletionListener() {
                                        @Override
                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                            if (!user.isEmailVerified()) {
                                                sendVerificationEmail();
                                            }

                                            FirebaseAuth.getInstance().signOut();

                                        }
                                    });


                            }


                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
*/

    }


    /*------------------------------------image uplosd in firebase storage-----------------------------------*/
    private void imageUpload() {

        ShowDialog.showDialog(context);

        storageReference.child(Constant.IMAGE_NAME + "_" + System.currentTimeMillis())
                .putFile(imageUploadUri)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                    }
                })
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        downloadImageUri = taskSnapshot.getDownloadUrl();


                        storeDataFirebaseAuth(false);
                    }
                });

    }

    /*------------------------------------------get image after capture and pic from gallery-----------------------------*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.GALLERY_CODE && resultCode == RESULT_OK) {

            Uri image = data.getData();

            Intent intent = new Intent(getApplicationContext(), CropImageActivity.class);
            intent.putExtra(Constant.URIIMAGE_NAME, image.toString());
            startActivityForResult(intent, Constant.CROP_IMAGE);


        } else if (requestCode == Constant.CAMERA_CODE && resultCode == RESULT_OK) {


            Intent intent = new Intent(getApplicationContext(), CropImageActivity.class);
            intent.putExtra(Constant.URIIMAGE_NAME, imageuri.toString());
            startActivityForResult(intent, Constant.CROP_IMAGE);

        } else if (requestCode == Constant.CROP_IMAGE && resultCode == RESULT_OK) {

            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getApplication().openFileInput(getString(R.string.imagemy_name)));
                imageUploadUri = getImageUri(getApplication(), bitmap);
                setProfileImage();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, Constant.TITLE_NAME, null);
        return Uri.parse(path);
    }



    /*------------------------------button click according to id.......................*/

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.sign_tv:

                AddCourseFragment.hideSoftKeyboard(context);
                getFieldData(v);

                break;

            case R.id.profilesign_iv:


                ShowDialog.cameraDialog(context, onClickListener, cameraClickLictener);

                break;

            case R.id.back_iv:

                AddCourseFragment.hideSoftKeyboard(context);

                finish();

                break;


        }

    }

    /*-------------------------send email for verification----------------------------*/

    private void sendVerificationEmail() {

        Log.d("data", "dsfdf");

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        Log.d("data", "dsfdf");


                        if (task.isSuccessful()) {

                            Log.d("data", "dsfdf");


                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.putExtra(Constant.CONFIRM_NAME, Constant.CONFIRM_NAME);

                            startActivity(intent);

                            finish();


                        }

                        ShowDialog.hideDialog();
                    }
                });
    }


    /*-----------------------start firebase auth listner ------------------------------------------*/
    @Override
    protected void onStart() {
        super.onStart();

        // mAuth.addAuthStateListener(mAuthListener);
    }

    /*--------------------------stop firebase auth listener-------------------------------------*/
    @Override
    protected void onStop() {
        super.onStop();

      /*  if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }*/
    }

    /*-------------------------------------------capture image and store in sd card-----------------------------*/
    private void capture_image() {
        picture_Path = "";
        destination_path = null;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String folder_main = Constant.IMAGE_NAME;
        File f = new File(Environment.getExternalStorageDirectory(), folder_main);


        if (!f.exists()) {
            f.mkdirs();
        }
        picture_Path = String.valueOf(new File(f, System.currentTimeMillis() + getString(R.string.formattype_name)));
        destination_path = new File(picture_Path);

        if (Build.VERSION.SDK_INT >= 24) {
            imageuri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".my.package.name.provider", destination_path);
        } else {
            imageuri = Uri.fromFile(destination_path);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageuri);
        startActivityForResult(intent, Constant.CAMERA_CODE);
    }

    @Override
    public void onPermissionGranted(int requestCode) {


        if (requestCode == 54) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType(getString(R.string.imagetype_name));
            startActivityForResult(galleryIntent, Constant.GALLERY_CODE);
        }

        if (requestCode == 55) {
            capture_image();
        }
    }
}
