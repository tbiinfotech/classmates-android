package com.brst.classmates.activity;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.classses.Application;
import com.brst.classmates.classses.ConnectivityReceiver;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.AddCourseFragment;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import static com.brst.classmates.activity.SplashActivity.mAuth;
import static com.brst.classmates.classses.NetworkCheck.isNetworkAvailable;
import static com.brst.classmates.classses.ShowDialog.hideDialog;
import static com.brst.classmates.classses.ShowDialog.isValidEmail;
import static com.brst.classmates.classses.ShowDialog.networkDialog;
import static com.brst.classmates.classses.ShowDialog.showSnackbarError;
import static com.seatgeek.placesautocomplete.Constants.LOG_TAG;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    TextView dialogdone_tv, login_tv, forgotpass_tv, network_done, network_cancel, network_tv;

    EditText email_et, password_et, dialogemail_et;

    String email, password, email_address, status;

    String key, firstName, lastName, image, emailName, country, university, college, degree, year, activity;

    Context context;

    Dialog confirm_dialog;
    TextView confirmdone_tv;


    Dialog forgot_dialog, network_dialog;

    SQLiteDatabase database;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        context = LoginActivity.this;

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Constant.REGISTERED_NAME);

        mAuth = FirebaseAuth.getInstance();

        bindViews();

        setListener();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            confirm_dialog(context);
        }


    }


    private void bindViews() {


        login_tv = (TextView) findViewById(R.id.login_tv);
        forgotpass_tv = (TextView) findViewById(R.id.forgotpass_tv);

        email_et = (EditText) findViewById(R.id.email_et);
        password_et = (EditText) findViewById(R.id.password_et);
    }

    private void setListener() {


        login_tv.setOnClickListener(this);
        forgotpass_tv.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.login_tv:

                AddCourseFragment.hideSoftKeyboard(context);
                getFieldData(v);

               /* Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
                startActivity(intent);
*/
                break;

            case R.id.forgotpass_tv:

                forgot_dialog(LoginActivity.this);

                break;

            case R.id.logCancel_tv:

                network_dialog.dismiss();

                break;

            case R.id.dialogdone_tv:

                email_address = dialogemail_et.getText().toString();

                if (email_address.isEmpty()) {
                    showSnackbarError(v, getString(R.string.enteremail_name), context);
                } else if (!isValidEmail(email_address)) {
                    showSnackbarError(v, getString(R.string.entercorrectemail_name), context);
                } else {

                    if (isNetworkAvailable(context)) {

                        sendForgotEmail();
                    } else {
                        networkDialog(LoginActivity.this);
                    }
                }
                break;

            case R.id.confirmdone_tv:
                confirm_dialog.dismiss();

                break;

        }

    }

    private void sendForgotEmail() {

        ShowDialog.showDialog(context);


        FirebaseAuth.getInstance().sendPasswordResetEmail(email_address)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {

                            Toast.makeText(LoginActivity.this, getString(R.string.resetemail_name), Toast.LENGTH_LONG).show();
                            dialogemail_et.getText().clear();
                            forgot_dialog.dismiss();
                            //  AddCourseFragment.hideSoftKeyboard(context);


                        } else {
                            if (task.getException().getMessage().equals(getString(R.string.userrecord_name))) {
                                Toast.makeText(getApplication(), getString(R.string.emailnotregister_name), Toast.LENGTH_LONG).show();


                            }
                        }

                        ShowDialog.hideDialog();
                    }
                });
    }


    private void getFieldData(View v) {

        email = email_et.getText().toString();
        password = password_et.getText().toString();
        checkFieldEmpty(v);
    }

    private void checkFieldEmpty(View v) {

        if (email.isEmpty()) {

            showSnackbarError(v, getString(R.string.enteremail_name), context);

        } else if (!isValidEmail(email)) {
            showSnackbarError(v, getString(R.string.entercorrectemail_name), context);
        } else if (password.isEmpty()) {

            showSnackbarError(v, getString(R.string.enterpassword_name), context);

        } else if (password.length() < 6) {
            showSnackbarError(v, getString(R.string.passlength_name), context);
        } else {
            Log.d("zczc", isNetworkAvailable(context) + "");

            boolean isConnected = ConnectivityReceiver.isConnected();
            if (isNetworkAvailable(context)) {

                loginAuth();

                //  new AsyncCaller().execute();
                //  ShowDialog.showDialog(context);

            } else {

                networkDialog(context);
            }
        }

        // networkCheck(isConnected);

        // }

    }

    public void networkCheck(boolean isConnected) {
        if (isConnected) {

            loginAuth();

        } else {

            networkDialog(context);
        }
    }


    public static boolean isInternetAccessible() {

        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return (urlc.getResponseCode() == 200);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Couldn't check internet connection", e);
        }

        return false;


    }

    private class AsyncCaller extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            boolean networkAccess = isInternetAccessible();
            return networkAccess;
        }

        @Override
        protected void onPostExecute(Boolean networkAccess) {
            super.onPostExecute(networkAccess);

            if (!networkAccess) {

                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {

                        networkDialog(context);
                        ShowDialog.hideDialog();
                    }
                }
            } else {
                //checkCourseUniversityExist();
                loginAuth();
            }
        }
    }


    private void loginAuth() {

        //   if(!ShowDialog.dialog.isShowing()) {
        ShowDialog.showDialog(context);
        //  }

        mAuth.signInWithEmailAndPassword(email_et.getText().toString(), password_et.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (!task.isSuccessful()) {

                            if (task.getException().getMessage().equals(getString(R.string.userrecord_name))) {
                                Toast.makeText(getApplication(), getString(R.string.emailnotregister_name), Toast.LENGTH_LONG).show();


                            } else if (task.getException().getMessage().equals(getString(R.string.invalidpassword_name))) {
                                Toast.makeText(getApplication(), getString(R.string.incorrectpassword_name), Toast.LENGTH_LONG).show();
                            }

                            if (ShowDialog.dialog.isShowing()) {

                                hideDialog();
                            }
                        } else {

                            checkIfEmailVerified();

                        }
                    }


                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Application.getInstance().setConnectivityListener(this);
    }

    public void checkIfEmailVerified() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user.isEmailVerified()) {

            getUserKey();


        } else {

            Toast.makeText(getApplication(), getString(R.string.notverify_name), Toast.LENGTH_SHORT).show();

        }

        hideDialog();

    }

    String collegeKey;

    private void getUserKey() {

        Query query = databaseReference.orderByChild(Constant.EMAILADD_NAME).equalTo(email);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (final DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    key = dataSnapshot1.getKey();
                    firstName = dataSnapshot1.child(Constant.FIRST_NAME).getValue().toString();
                    lastName = dataSnapshot1.child(Constant.LAST_NAME).getValue().toString();
                    status = dataSnapshot1.child(Constant.STATUS_NAME).getValue().toString();

                    if (dataSnapshot1.child(Constant.PROFILE_PICTURE).exists()) {

                        image = dataSnapshot1.child(Constant.PROFILE_PICTURE).getValue().toString();
                    }
                    emailName = dataSnapshot1.child(Constant.EMAILADD_NAME).getValue().toString();

                    country = dataSnapshot1.child(Constant.COUNTRY_NAME).getValue().toString();
                    university = dataSnapshot1.child(Constant.COLLEGE_NAME).getValue().toString();
                    college = dataSnapshot1.child("specialization").getValue().toString();
                    year = dataSnapshot1.child(Constant.YEAR_NAME).getValue().toString();
                    degree = dataSnapshot1.child(Constant.DEGREE_NAME).getValue().toString();
                    final String notification = dataSnapshot1.child(Constant.NOTIFICATION_KEY).getValue().toString();
                    FirebaseDatabase.getInstance().getReference(Constant.COLLEGE).orderByChild(Constant.COLLEGE)
                            .equalTo(university).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            ///  while (dataSnapshot.getChildren().iterator().hasNext()) {
                            DataSnapshot dataSnapshot2 = dataSnapshot.getChildren().iterator().next();

                            collegeKey = dataSnapshot2.getKey();

                            if (!status.equals(Constant.BLOCK_NAME)) {

                                String userFullName = firstName + " " + lastName;

                                StoreData.storeDataToSharedPref(getApplicationContext(), key, userFullName, emailName, university, college, country, degree, year, notification, collegeKey);
                                StoreData.storeProfileToSharedPref(getApplicationContext(), image);

                                email_et.getText().clear();
                                password_et.getText().clear();

                                String token = FirebaseInstanceId.getInstance().getToken();

                                sendTokenToFirebase(key, token);

                                Intent intent = new Intent(LoginActivity.this, WelcomeActivity.class);
                                startActivity(intent);


                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.block_name), Toast.LENGTH_SHORT).show();
                            }

                            ShowDialog.hideDialog();

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                /*    if (!status.equals(Constant.BLOCK_NAME)) {

                        String userFullName = firstName + " " + lastName;

                        StoreData.storeDataToSharedPref(getApplicationContext(), key, userFullName, emailName, university, college, country, degree, year, notification, collegeKey);
                        StoreData.storeProfileToSharedPref(getApplicationContext(), image);

                        email_et.getText().clear();
                        password_et.getText().clear();

                        String token = FirebaseInstanceId.getInstance().getToken();

                        sendTokenToFirebase(key, token);

                        Intent intent = new Intent(LoginActivity.this, WelcomeActivity.class);
                        startActivity(intent);


                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.block_name), Toast.LENGTH_SHORT).show();
                    }

                    ShowDialog.hideDialog();*/
                }

                   /*     @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    })*/
                ;

                // }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


                ShowDialog.hideDialog();
            }
        });


    }

    private void sendTokenToFirebase(String key, String token) {

        HashMap hashMap = new HashMap<>();
        hashMap.put("token", token);
        hashMap.put("status", "active");
        databaseReference.child(key).updateChildren(hashMap);
        databaseReference.child(key).updateChildren(hashMap);


    }

    private void forgot_dialog(LoginActivity loginActivity) {

        forgot_dialog = new Dialog(loginActivity);
        forgot_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgot_dialog.setContentView(R.layout.custom_forgotpass);
        forgot_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        forgot_dialog.show();

        dialogemail_et = (EditText) forgot_dialog.findViewById(R.id.dialogemail_et);
        dialogdone_tv = (TextView) forgot_dialog.findViewById(R.id.dialogdone_tv);

        dialogdone_tv.setOnClickListener(this);
    }


    private void confirm_dialog(Context context) {

        confirm_dialog = new Dialog(context);
        confirm_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirm_dialog.setContentView(R.layout.custom_confirm);
        confirm_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirm_dialog.show();


        confirmdone_tv = (TextView) confirm_dialog.findViewById(R.id.confirmdone_tv);

        confirmdone_tv.setOnClickListener(this);
    }


    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    protected void onStop() {
        super.onStop();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        //  networkCheck(isConnected);

    }
}
