package com.brst.classmates.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;

public class MainActivity extends Activity implements OnClickListener {

    TextView logintxt_tv, signuptxt_tv;
    String shared_data = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        shared_data = StoreData.getUserEmailFromSharedPre(getApplicationContext());

        bindViews();

        setListener();

    }

    /*--------------------set click listener----------------------------*/

    private void setListener() {

        logintxt_tv.setOnClickListener(this);
        signuptxt_tv.setOnClickListener(this);
    }


    /*-------------------------------get layout id-----------------------*/

    private void bindViews() {

        logintxt_tv = (TextView) findViewById(R.id.logintxt_tv);
        signuptxt_tv = (TextView) findViewById(R.id.signuptxt_tv);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.logintxt_tv:

                checkSharedData();

                break;


            case R.id.signuptxt_tv:

                Intent signupIntent = new Intent(MainActivity.this, SignupActivity.class);
                startActivity(signupIntent);

                break;


        }

    }

    /*-----------------------check email in sharaedpreference---------------------------*/

    private void checkSharedData() {


        if (shared_data.equals(Constant.EMPTY_NAME)) {

            Intent intent = new Intent(getApplication(), LoginActivity.class);
            startActivity(intent);

            finish();
        } else {
            Intent intent = new Intent(getApplication(), WelcomeActivity.class);
            startActivity(intent);
        }
    }


}
