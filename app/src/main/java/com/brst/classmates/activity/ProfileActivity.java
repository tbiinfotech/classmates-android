package com.brst.classmates.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.adaptors.ContactAdapter;
import com.brst.classmates.adaptors.NetworkAdapter;
import com.brst.classmates.beanclass.User;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.ContactFragment;
import com.brst.classmates.fragments.GroupFragment;
import com.brst.classmates.fragments.MenuFragment;
import com.brst.classmates.fragments.NotesFragment;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 19/9/17.
 */

public class ProfileActivity extends AppCompatActivity {
    TextView tv_name, tv_college, tv_country, tv_university, tv_invite, tv_last_name;
    String KEY = "", FROM = "";
    ImageView iv_profile;
    public static int fragment = 0;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, reference;
    LinearLayout lay_header;
    TextView tv_title, tv_year, tv_degree;
    ImageView back_iv;
    List<HashMap<String, String>> dataList = new ArrayList<>();
    List<HashMap<String, String>> courseSearchtList = new ArrayList<>();
    List<String> courseKey = new ArrayList<>();
    List<String> courseSearchKey = new ArrayList<>();
    ArrayList<HashMap<String, String>> searchContactList;
    ArrayList<HashMap<String, String>> contactsList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Constant.INVITED_USER);

        //getCourseList();
    }










/*
    private void getCourseList() {



        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Constant.INVITED_USER);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                dataList = new ArrayList<HashMap<String, String>>();
                courseKey = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                    Log.d("asda", dataSnapshot1.getKey() + "");
                    HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                    try {
                        HashMap hashMap1 = new HashMap();
                        HashMap hashMap2 = new HashMap();

                        hashMap1.put("to", hashMap.get("to").toString());
                        hashMap1.put("request_status", hashMap.get("request_status").toString());
                        hashMap1.put("SENDER", hashMap.get("SENDER").toString());
                        hashMap1.put("from", hashMap.get("from").toString());
                        hashMap1.put("RECEIVER", hashMap.get("RECEIVER").toString());
                        dataList.add(hashMap1);
                        // dataList.add(hashMap.get(Constant.KEY_NAME).toString());

                        courseKey.add(dataSnapshot1.getKey());
                    }

                    catch (Exception e) {

                        e.printStackTrace();
                    }


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }
*/

    private void init() {
        KEY = getIntent().getStringExtra("KEY");
        try {
            FROM = getIntent().getStringExtra("FROM");
        } catch (Exception e) {
        }

        Log.d("data", KEY + "----" + FROM);
        tv_degree = (TextView) findViewById(R.id.tv_degree);
        lay_header = (LinearLayout) findViewById(R.id.lay_header);
        back_iv = (ImageView) findViewById(R.id.back_iv);
        back_iv.setImageResource(R.mipmap.ic_backarr);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("Profile");
        tv_title.setTextColor(Color.parseColor("#ffffff"));
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lay_header.setBackgroundColor(Color.parseColor("#FF0278B5"));
        tv_invite = (TextView) findViewById(R.id.tv_invite);
        iv_profile = (ImageView) findViewById(R.id.iv_profile);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_college = (TextView) findViewById(R.id.tv_college);
        tv_country = (TextView) findViewById(R.id.tv_country);
        tv_university = (TextView) findViewById(R.id.tv_university);
        tv_last_name = (TextView) findViewById(R.id.tv_last_name);
        tv_year = (TextView) findViewById(R.id.tv_year);
        try {

            if (!FROM.equals("CONTACT")) {

                tv_name.setText(NetworkAdapter.hashMaps.get(Constant.FIRST_NAME) + " " + NetworkAdapter.hashMaps.get(Constant.LAST_NAME));
                tv_last_name.setText(NetworkAdapter.hashMaps.get(Constant.LAST_NAME));

                tv_college.setText(NetworkAdapter.hashMaps.get(Constant.COLLEGE_NAME));
                tv_country.setText(NetworkAdapter.hashMaps.get(Constant.COUNTRY_NAME));
                tv_university.setText(NetworkAdapter.hashMaps.get(Constant.UNIVERSITY_NAME));
                tv_year.setText(NetworkAdapter.hashMaps.get(Constant.YEAR_NAME));
                tv_degree.setText(NetworkAdapter.hashMaps.get(Constant.DEGREE_NAME));

                try {

                    Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                    iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                    //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                } catch (Exception e) {
                }


            } else {

                tv_last_name.setText(ContactAdapter.hashMaps.get(Constant.LAST_NAME));
                if (ContactAdapter.hashMaps.get("request_status").equals("Contact request from this user.")) {
                    tv_invite.setText("Accept Invite");
                    tv_name.setText(ContactAdapter.hashMaps.get("SENDER"));
                    tv_college.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COLLEGE_NAME));
                    tv_country.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COUNTRY_NAME));
                    tv_university.setText(ContactAdapter.hashMaps.get("Sender" + Constant.UNIVERSITY_NAME));
                    tv_year.setText(ContactAdapter.hashMaps.get("Sender" + Constant.YEAR_NAME));
                    tv_degree.setText(ContactAdapter.hashMaps.get("Sender" + Constant.DEGREE_NAME));


                    try {

                        Glide.with(ProfileActivity.this).load(ContactAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                        iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                        //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                    } catch (Exception e) {
                    }

                } else if (ContactAdapter.hashMaps.get("request_status").equals("Request Pending")) {
                    tv_invite.setText("Request Pending");
                    tv_name.setText(ContactAdapter.hashMaps.get("RECEIVER") + " " + ContactAdapter.hashMaps.get(Constant.LAST_NAME));

                /*    tv_college.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COLLEGE_NAME));
                    tv_country.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COUNTRY_NAME));
                    tv_university.setText(ContactAdapter.hashMaps.get("Sender" + Constant.UNIVERSITY_NAME));
                    tv_year.setText(ContactAdapter.hashMaps.get("Sender" + Constant.YEAR_NAME));
                    tv_degree.setText(ContactAdapter.hashMaps.get("Sender" + Constant.DEGREE_NAME));*/

                    tv_college.setText(ContactAdapter.hashMaps.get(Constant.COLLEGE_NAME));
                    tv_country.setText(ContactAdapter.hashMaps.get(Constant.COUNTRY_NAME));
                    tv_university.setText(ContactAdapter.hashMaps.get(Constant.UNIVERSITY_NAME));
                    tv_year.setText(ContactAdapter.hashMaps.get(Constant.YEAR_NAME));
                    tv_degree.setText(ContactAdapter.hashMaps.get(Constant.DEGREE_NAME));


                    try {

                        Glide.with(ProfileActivity.this).load(ContactAdapter.hashMaps.get(Constant.RECEIVER_PROFILE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                        iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.RECEIVER_PROFILE));
                        //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                    } catch (Exception e) {
                    }

                } else {
                    tv_invite.setText("Chat");


                    if (ContactAdapter.Info.equals("sender")) {
                        // tv_name.setText(ContactAdapter.hashMaps.get("RECEIVER") + " " + ContactAdapter.hashMaps.get(Constant.LAST_NAME));
                        tv_name.setText(ContactAdapter.hashMaps.get("SENDER"));
                        tv_college.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COLLEGE_NAME));
                        tv_country.setText(ContactAdapter.hashMaps.get("Sender" + Constant.COUNTRY_NAME));
                        tv_university.setText(ContactAdapter.hashMaps.get("Sender" + Constant.UNIVERSITY_NAME));
                        tv_year.setText(ContactAdapter.hashMaps.get("Sender" + Constant.YEAR_NAME));
                        tv_degree.setText(ContactAdapter.hashMaps.get("Sender" + Constant.DEGREE_NAME));


                        try {

                            Glide.with(ProfileActivity.this).load(ContactAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                            iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                            //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                        } catch (Exception e) {
                        }

                    } else {

                        if (ContactAdapter.Info.equals("receiver")) {
                            // tv_name.setText(ContactAdapter.hashMaps.get("SENDER"));
                            tv_name.setText(ContactAdapter.hashMaps.get("RECEIVER") + " " + ContactAdapter.hashMaps.get(Constant.LAST_NAME));
                            tv_college.setText(ContactAdapter.hashMaps.get(Constant.COLLEGE_NAME));
                            tv_country.setText(ContactAdapter.hashMaps.get(Constant.COUNTRY_NAME));
                            tv_university.setText(ContactAdapter.hashMaps.get(Constant.UNIVERSITY_NAME));
                            tv_year.setText(ContactAdapter.hashMaps.get(Constant.YEAR_NAME));
                            tv_degree.setText(ContactAdapter.hashMaps.get(Constant.DEGREE_NAME));


                            try {

                                Glide.with(ProfileActivity.this).load(ContactAdapter.hashMaps.get(Constant.RECEIVER_PROFILE)).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(iv_profile);
                                iv_profile.setTag(NetworkAdapter.hashMaps.get(Constant.RECEIVER_PROFILE));
                                //  Glide.with(ProfileActivity.this).load(NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE)).apply(new RequestOptions().placeholder(R.mipmap.user)).into(iv_profile);
                            } catch (Exception e) {
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {

        }

        tv_invite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (tv_invite.getText().toString().equals("Accept Invite")) {
                    databaseReference.child(KEY).child("request_status").setValue("Accepted");
                    tv_invite.setText("Chat");
                }
                if (tv_invite.getText().toString().equals("Request Pending")) {

                } else if (tv_invite.getText().toString().equals("Connect")) {

                    final Map<String, User> users = new HashMap<String, User>();
                    users.put(KEY
                            , new User(StoreData.getUserKeyFromSharedPre(ProfileActivity.this), KEY, "SENT"));

                    DatabaseReference rootRef = firebaseDatabase.getReference(Constant.INVITED_USER);

                    DatabaseReference cineIndustryRef = rootRef.push();
                    String key = cineIndustryRef.getKey();
                    Map<String, Object> map = new HashMap<>();
                    map.put("to", KEY);
                    map.put("request_status", "sent");
                    map.put("SENDER", StoreData.getUserFullNameFromSharedPre(ProfileActivity.this));
                    map.put("from", StoreData.getUserKeyFromSharedPre(ProfileActivity.this));


                    map.put("Sender" + Constant.COLLEGE_NAME, StoreData.getCollegeFromSharedPre(ProfileActivity.this));
                    map.put("Sender" + Constant.UNIVERSITY_NAME, StoreData.getUniversityFromSharedPre(ProfileActivity.this));
                    map.put("Sender" + Constant.COUNTRY_NAME, StoreData.getCountryFromSharedPre(ProfileActivity.this));
                    map.put(Constant.PROFILE_PICTURE, StoreData.getProfileFromSharedPre(ProfileActivity.this));
                    map.put("Sender" + Constant.YEAR_NAME, StoreData.getyearFromSharedPre(ProfileActivity.this));
                    map.put("Sender" + Constant.DEGREE_NAME, StoreData.getDegreeFromSharedPre(ProfileActivity.this));


                    map.put(Constant.LAST_NAME, NetworkAdapter.hashMaps.get(Constant.LAST_NAME));
                    map.put(Constant.YEAR_NAME, NetworkAdapter.hashMaps.get(Constant.YEAR_NAME));
                    map.put(Constant.DEGREE_NAME, NetworkAdapter.hashMaps.get(Constant.DEGREE_NAME));

                    map.put(Constant.COUNTRY_NAME, NetworkAdapter.hashMaps.get(Constant.COUNTRY_NAME));
                    map.put(Constant.COLLEGE_NAME, NetworkAdapter.hashMaps.get(Constant.COLLEGE_NAME));
                    map.put(Constant.UNIVERSITY_NAME, NetworkAdapter.hashMaps.get(Constant.UNIVERSITY_NAME));

                    map.put("RECEIVER", NetworkAdapter.hashMaps.get(Constant.FIRST_NAME));
                    map.put("RECEIVER_PROFILE", NetworkAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                    //and os on
                    cineIndustryRef.updateChildren(map);


            /*  databaseReference.child(KEY).setValue(users, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                      //  databaseReference.setValue(users);

                    }
                });*/


                    tv_invite.setText("Invite Sent");
                    tv_invite.setClickable(false);
                } else {
                   Log.d("data",ContactAdapter.Info.equals("receiver")+"----"+ContactAdapter.Info.equals("sender"));
                    Log.d("image", iv_profile.getTag().toString() + "---" + tv_name.getText().toString()+"---"+KEY);
                    Bundle bundle = new Bundle();
                    if (ContactAdapter.Info.equals("receiver")) {
                        bundle.putString("imagePath", ContactAdapter.hashMaps.get(Constant.RECEIVER_PROFILE));
                    }
                    else
                    {
                        bundle.putString("imagePath", ContactAdapter.hashMaps.get(Constant.PROFILE_PICTURE));
                    }
                    bundle.putString("userName", tv_name.getText().toString());
                    bundle.putString("chatKey",KEY);
                    Intent mIntent = new Intent(ProfileActivity.this, WelcomeActivity.class);
                    mIntent.putExtras(bundle);
                    fragment = 3;
                    startActivity(mIntent);
                    finish();

                }


            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent(this,WelcomeActivity.class);
        startActivity(intent);
    }
}
