package com.brst.classmates.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.brst.classmates.R;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

public class CropImageActivity extends AppCompatActivity {
    int IMAGE_CRO=25;
 //   Bitmap bitmap;
  public static String  image;
     CropImageView cropImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        cropImageView =(CropImageView)findViewById(R.id.cropImageView);
        Button save=(Button)findViewById(R.id.save);
        Button cancel=(Button)findViewById(R.id.cancel);
//        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .start(this);
        final Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        Uri imageUri= Uri.parse(bundle.getString("imageuri"));

//        String image=bundle.getString("imageuri");
//        Log.d("imagepath",image);
//        File file=new File(image);
//          Log.d("fsf",file+"");
//        long length= file.length();
//        length=length/1024;
//        Log.d("sd",length+"");
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        Bitmap bitmap = BitmapFactory.decodeFile(image, options);
//        Log.d("dsd",bitmap+"");

         cropImageView.setImageUriAsync(imageUri);
     //  cropImageView.setImageBitmap(bitmap);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                crop();
            }
        });
       cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
    public String createImageFromBitmap(Bitmap bitmap) {
        String fileName = "myImage";
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }

private  void crop(){



    runOnUiThread(new Runnable() {
        @Override
        public void run() {
            Bitmap bit = cropImageView.getCroppedImage();
            //cropImageView.getCroppedImageAsync();
            Log.d("dfgsd", String.valueOf(bit.getByteCount()/1000000));
            createImageFromBitmap(bit);
            Intent intent = new Intent();
            // intent.putExtra("image",);
            setResult(Activity.RESULT_OK, intent);
            finish();

        }
    });

}

}
