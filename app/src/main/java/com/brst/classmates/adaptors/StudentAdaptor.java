package com.brst.classmates.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 7/3/17.
 */

public class StudentAdaptor extends RecyclerViewEmptySupport.Adapter<StudentAdaptor.MyViewHolder> {

    List<HashMap<String, String>> studentList;
    List<String> groupUser;


    Context context;

    ClickRecyclerInterface clickRecyclerInterface;

    public StudentAdaptor(List<HashMap<String, String>> studentList, Context context) {

        this.studentList = studentList;
        this.groupUser = groupUser;
        this.context = context;
    }

    @Override
    public StudentAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_student, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final StudentAdaptor.MyViewHolder holder, final int position) {

        final HashMap<String, String> hashMap = studentList.get(position);
        String firstName = hashMap.get(Constant.FIRST_NAME);
        String lastName = hashMap.get(Constant.LAST_NAME);
        String profile = hashMap.get(Constant.PROFILE_PICTURE);

        String fullNmae = firstName + " " + lastName;

        holder.chk_cb.setChecked(Boolean.parseBoolean(studentList.get(position).get(Constant.CHECKED_NAME)));

        holder.chk_cb.setTag(studentList.get(position));

        holder.chk_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                HashMap<String, String> hashMap1 = (HashMap) cb.getTag();


                if (cb.isChecked()) {
                    studentList.get(position).put(Constant.CHECKED_NAME, Constant.TRUE_NAME);


                } else {
                    studentList.get(position).put(Constant.CHECKED_NAME, Constant.FALSE_NAME);

                }


            }
        });


        holder.student_tv.setText(fullNmae);

        Glide.with(context).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.userprofile_iv);
    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView student_tv;
        ImageView userprofile_iv;

        CheckBox chk_cb;

        public MyViewHolder(View itemView) {
            super(itemView);

            student_tv = (TextView) itemView.findViewById(R.id.student_tv);
            userprofile_iv = (ImageView) itemView.findViewById(R.id.userprofile_iv);
            chk_cb = (CheckBox) itemView.findViewById(R.id.chk_cb);
        }
    }

    public void addData(List<HashMap<String, String>> studentList) {
        this.studentList = studentList;
        this.groupUser = groupUser;
    }

    public List<HashMap<String, String>> getStudentist() {

        return studentList;
    }
}
