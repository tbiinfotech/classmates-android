package com.brst.classmates.adaptors;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.ProfileActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.fragments.ChatProfile;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.brst.classmates.activity.CropImageActivity.image;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

    List<HashMap<String, String>> contacts;
    Context context;
    List<String> courseKey;
    List<String> courseKey_invite;
    AddAssignmentFragment addAssignmentFragment = new AddAssignmentFragment();
    public static HashMap<String, String> hashMaps = null;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    int visibility;
    public static String Info = "";

    public ContactAdapter(List<HashMap<String, String>> contacts, Context context, List<String> courseKey) {

        this.contacts = contacts;
        this.context = context;
        this.courseKey = courseKey;

    }


    @Override
    public ContactAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contact, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactAdapter.MyViewHolder holder, final int position) {

        HashMap<String, String> hashMap = contacts.get(position);
        String to = hashMap.get("to").toString();
        String request_status = hashMap.get("request_status").toString();
        String SENDER = hashMap.get("SENDER").toString();
        String RECEIVER = hashMap.get("RECEIVER").toString();

        String from = hashMap.get("from").toString();

        Log.d("profile",hashMap.get(Constant.PROFILE_PICTURE)+"------"+hashMap.get(Constant.RECEIVER_PROFILE));
        // Reference to an image file in Cloud Storage
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("myimage");

        String contactProfile = "";


        if (hashMap.get("from").toString().equals(StoreData.getUserKeyFromSharedPre(context))) {

            Log.d("picprofile","dasd");
            if (request_status.equals("Accepted")) {
                Log.d("picprofile","dasdfdd");
                holder.name_tv.setText(RECEIVER);
                holder.number_tv.setText("You are friends.");
                Info = "receiver";
            } else {
                Log.d("picprofile","dasdfdf");
                holder.number_tv.setText("Request Pending");
                holder.name_tv.setText(RECEIVER);
                hashMap.put("request_status", "Request Pending");
            }


         //   try {

                if (hashMap.containsKey(Constant.RECEIVER_PROFILE)) {

                    contactProfile = hashMap.get(Constant.RECEIVER_PROFILE);
                    Log.d("picprofile",contactProfile);
                }

                if (contactProfile != null && !contactProfile.isEmpty()) {
                    Log.d("picprofile","mndasd");

                    Glide.with(context).load(contactProfile).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
                } else {
                    Log.d("picprofile","dasfdd");
                    Glide.with(context).load(contactProfile).apply(new RequestOptions().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
                }
           /* } catch (Exception e) {
            }*/
        }

          /*  if(request_status.equals("sent"))*/

        else if (hashMap.get("to").toString().equals(StoreData.getUserKeyFromSharedPre(context))) {
            Log.d("picprofile","dasgfdgd");
            if (request_status.equals("Accepted")) {
                Log.d("picprofile","dasdgjhn");
                holder.name_tv.setText(SENDER);
                holder.number_tv.setText("You are friends.");
                Info = "sender";
            } else {
                Log.d("picprofile", "daslkkd");
                holder.number_tv.setText("Contact request from this user.");
                hashMap.put("request_status", "Contact request from this user.");
                holder.name_tv.setText(SENDER);
            }

               // try {

                    if (hashMap.containsKey(Constant.PROFILE_PICTURE)) {
                        Log.d("picprofile","dapoosd");
                        contactProfile = hashMap.get(Constant.PROFILE_PICTURE);
                        Log.d("picprofile",contactProfile);
                    }

                    if (contactProfile != null && !contactProfile.isEmpty()) {
                        Log.d("picprofile","dasbghd");
                        Glide.with(context).load(contactProfile).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
                    } else {
                        Log.d("picprofile","dasgrgd");
                        Glide.with(context).load(R.mipmap.user).apply(new RequestOptions().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
                    }
                /*} catch (Exception e) {
                }*/


          //  }
        } else {
            holder.number_tv.setText("You are friends.");
        }

        holder.contactCheck_cb.setChecked(Boolean.parseBoolean(contacts.get(position).get("boolean")));

        holder.contactCheck_cb.setTag(contacts.get(position));

        holder.lay_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hashMaps = contacts.get(position);
                String KEY = courseKey.get(position);
                Bundle bundle=new Bundle();
                bundle.putString("KEY", KEY);
                bundle.putString("FROM", "CONTACT");
                ReplaceFragment.replace((WelcomeActivity) context,new ChatProfile(),"chat_profile",bundle);
               /* Intent mIntent = new Intent(context, ProfileActivity.class);
                mIntent.putExtra("KEY", KEY);
                mIntent.putExtra("FROM", "CONTACT");

                context.startActivity(mIntent);*/


            }
        });

        holder.contactCheck_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CheckBox cb = (CheckBox) v;
                //  HashMap<String, String> hashMap1 = (HashMap) cb.getTag();


                if (cb.isChecked()) {
                    contacts.get(position).put("boolean", Constant.TRUE_NAME);


                } else {
                    contacts.get(position).put("boolean", Constant.FALSE_NAME);

                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return contacts.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name_tv, number_tv;
        ImageView contactProfile_iv;
        CheckBox contactCheck_cb;

        LinearLayout lay_contact;

        public MyViewHolder(View itemView) {
            super(itemView);

            name_tv = (TextView) itemView.findViewById(R.id.name_tv);
            number_tv = (TextView) itemView.findViewById(R.id.number_tv);
            contactProfile_iv = (ImageView) itemView.findViewById(R.id.contactProfile_iv);
            contactCheck_cb = (CheckBox) itemView.findViewById(R.id.contactCheck_cb);
            lay_contact = (LinearLayout) itemView.findViewById(R.id.lay_contact);


            //   invite_tv.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {

            List<String> list = new ArrayList();
/*
            if (clickRecyclerInterface != null) {
                list.add(course_tv.getText().toString());
                list.add(courseKey.get(getAdapterPosition()));

                add_iv.setTag(list);
                //  course_tv.setTag(course_tv.getText().toString());


                delete_iv.setTag(courseKey.get(getAdapterPosition()));
                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }*/
        }
    }

 /*   public void addDataInList(List<HashMap<String, String>> dataList, List<String> courseKey) {
        this.dataList = dataList;
        this.courseKey = courseKey;

        Log.d("asd", courseKey + "");
        Log.d("asd", dataList + "");
    }*/


    public void addData(ArrayList<HashMap<String, String>> searchContactList) {

        this.contacts = searchContactList;

    }

    public List<HashMap<String, String>> getSelectedContactList() {

        return contacts;
    }

}
