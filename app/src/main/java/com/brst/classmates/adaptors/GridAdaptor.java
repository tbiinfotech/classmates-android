package com.brst.classmates.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brst.classmates.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 7/5/17.
 */

public class GridAdaptor extends BaseAdapter{

    String text[];

    LayoutInflater inflater;

    Context context;

    public GridAdaptor(Context context, String[] text) {

        this.context=context;
        this.text=text;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return text.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflater.inflate(R.layout.custom_grid, null);

        TextView grid_tv=(TextView)view.findViewById(R.id.grid_tv);

        grid_tv.setText(text[position]);

        return view;
    }


}
