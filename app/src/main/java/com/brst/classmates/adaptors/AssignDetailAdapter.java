package com.brst.classmates.adaptors;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.BeanClass;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.fragments.AssignmentDetail;
import com.brst.classmates.fragments.MessageReplyFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.util.MarkEnforcingInputStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 7/5/17.
 */

public class AssignDetailAdapter extends BaseExpandableListAdapter {

    private Context _context;
    //private List<HashMap<String, String>> _listDataHeader; // header titles
    // child data in format of header title, child title
   // private List<BeanClass> _listDataChild;

    ClickRecyclerInterface clickRecyclerInterface;

    ArrayList<BeanClass> commentsArrayList;


   /* public AssignDetailAdapter(Context context, List<HashMap<String, String>> listDataHeader, List<BeanClass> listDataChild) {

        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listDataChild;

        Log.d("sdsa", _listDataChild + "");
    }*/

    public AssignDetailAdapter(Context context, ArrayList<BeanClass> commentsArrayList) {

        this._context = context;
        this.commentsArrayList = commentsArrayList;
    }

    @Override
    public int getGroupCount() {

        return this.commentsArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {


        // return this._listDataChild.get(this._listDataHeader.get(groupPosition).get(Constant.USERMESS_NAME)).size();

            return commentsArrayList.get(groupPosition).getArrayList().size();



    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.commentsArrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // return this._listDataChild.get(this._listDataHeader.get(groupPosition).get(Constant.USERMESS_NAME)).get(childPosition);

        return commentsArrayList.get(groupPosition).getArrayList().get(childPosition);

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.custom_expandheader, null);
        }

        BeanClass beanClass = commentsArrayList.get(groupPosition);

        String name = beanClass.getName();
        String time = beanClass.getTime();
        String message = beanClass.getCommentReply();
        final String image = beanClass.getImage();

        final String assignKey = beanClass.getAssignmentKey();
        final String messageKey = beanClass.getMessageKey();

        String localTime = SystemTime.getInstance().getLocalTime(time);

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.header_tv);
        TextView message_tv = (TextView) convertView.findViewById(R.id.message_tv);
        TextView time_tv = (TextView) convertView.findViewById(R.id.time_tv);
        TextView reply_tv = (TextView) convertView.findViewById(R.id.reply_tv);
        ImageView comment_iv = (ImageView) convertView.findViewById(R.id.comment_iv);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        if(image!=null && !image.isEmpty())
        {
            comment_iv.setVisibility(View.VISIBLE);
            message_tv.setVisibility(View.GONE);

            Glide.with(_context).load(image).into(comment_iv);

        }
        else if(message!=null && !message.isEmpty())
        {
            comment_iv.setVisibility(View.GONE);
            message_tv.setVisibility(View.VISIBLE);
            message_tv.setText(message);
        }

        lblListHeader.setText(name);

        time_tv.setText(localTime);


        AssignmentDetail.expand_el.expandGroup(groupPosition);

        comment_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showImage(_context,image);
            }
        });


        reply_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   Toast.makeText(_context, messageKey, Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.MESSKEY_NAME, messageKey);
                bundle.putString(Constant.ASSIGNMENT_NAME, assignKey);

                MessageReplyFragment messageReplyFragment = new MessageReplyFragment();
                messageReplyFragment.setArguments(bundle);

                ReplaceFragment.replace((WelcomeActivity) _context, messageReplyFragment, "MessageReply_Fragment");
            }
        });


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        HashMap<String,String> hashMap =commentsArrayList.get(groupPosition).getArrayList().get(childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.custom_childexpand, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.expand_tv);
        TextView time_tv = (TextView) convertView.findViewById(R.id.time_tv);
        TextView msg_tv = (TextView) convertView.findViewById(R.id.msg_tv);
        ImageView commentReply_iv = (ImageView) convertView.findViewById(R.id.commentReply_iv);


        String name = hashMap.get(Constant.USERMESS_NAME);

        String time = hashMap.get(Constant.TIME_NAME);

        if(hashMap.containsKey(Constant.MESSAGE))
        {
            msg_tv.setVisibility(View.VISIBLE);
            commentReply_iv.setVisibility(View.GONE);
            String message = hashMap.get(Constant.MESSAGE);
            msg_tv.setText(message);

        }

        else if(hashMap.containsKey(Constant.IMAGE_NAME))
        {
            msg_tv.setVisibility(View.GONE);
            commentReply_iv.setVisibility(View.VISIBLE);
            final String image = hashMap.get(Constant.IMAGE_NAME);
            Glide.with(_context).load(image).into(commentReply_iv);

            commentReply_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showImage(_context,image);
                }
            });

        }

        String localTime = SystemTime.getInstance().getLocalTime(time);
        txtListChild.setText(name);
        time_tv.setText(localTime);

      //  AssignmentDetail.expand_el.smoothScrollToPosition(childPosition);


        return convertView;


    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }




    public void addData(ArrayList<BeanClass> commentsArrayList, AssignDetailAdapter assignDetailAdapter,final ExpandableListView expand_el) {
        this.commentsArrayList=commentsArrayList;

       /* expand_el.post(new Runnable() {
            @Override
            public void run() {

                expand_el.smoothScrollToPosition(getGroupCount());
            }
        });*/
    }



    private void showImage(Context context, String image) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        dialog.show();
        ImageView chat_iv = (ImageView) dialog.findViewById(R.id.chat_iv);
        ImageView cross_iv = (ImageView) dialog.findViewById(R.id.cou_bck_iv);

        Glide.with(context).load(image).into(chat_iv);

        cross_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(true);
    }
}
