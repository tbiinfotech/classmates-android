package com.brst.classmates.adaptors;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.utility.Constant;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class InviteCourseAdaptor extends RecyclerView.Adapter<InviteCourseAdaptor.MyViewHolder> {

    private ArrayList<HashMap<String, String>> dataList;


    ClickRecyclerInterface clickRecyclerInterface;

    AddAssignmentFragment addAssignmentFragment = new AddAssignmentFragment();

    public InviteCourseAdaptor(ArrayList<HashMap<String, String>> dataList) {
        this.dataList = dataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_invitecourse, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        HashMap<String, String> hashMap = dataList.get(position);

        holder.course_tv.setText(hashMap.get("subject"));

        holder.courseInvite_iv.setChecked(Boolean.parseBoolean(dataList.get(position).get(Constant.CHECKED_NAME)));

      //  holder.chk_cb.setTag(studentList.get(position));
        holder.courseInvite_iv.setTag(dataList.get(position));

        holder.courseInvite_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                HashMap<String, String> hashMap1 = (HashMap) cb.getTag();


                if (cb.isChecked()) {
                    dataList.get(position).put(Constant.CHECKED_NAME, Constant.TRUE_NAME);


                } else {
                    dataList.get(position).put(Constant.CHECKED_NAME, Constant.FALSE_NAME);

                }


            }
        });


        if (position == dataList.size() - 1) {
            holder.divider_view.setVisibility(View.GONE);


        }

    }

    public void addDataInList(ArrayList<HashMap<String, String>> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView course_tv, invite_tv;
        View divider_view;
        CheckBox courseInvite_iv;
        // ImageView addasign_iv;

        public MyViewHolder(View itemView) {
            super(itemView);

            course_tv = (TextView) itemView.findViewById(R.id.course_tv);
            // invite_tv = (TextView) itemView.findViewById(R.id.invite_tv);

            divider_view = (View) itemView.findViewById(R.id.divider_view);
            courseInvite_iv = (CheckBox) itemView.findViewById(R.id.courseInvite_iv);

            //   invite_tv.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {

            if (clickRecyclerInterface != null) {
                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }
        }
    }
}
