package com.brst.classmates.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.classmates.R;

import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public class GroupAdaptor extends RecyclerView.Adapter<GroupAdaptor.MyViewHolder> {

    List<HashMap<String, String>> groupName;
    List<HashMap<String, String>> userList = new ArrayList<>();
    List<String> msg = new ArrayList<>();
    List<String> lasttime = new ArrayList<>();

    Context context;

    ClickRecyclerInterface clickRecyclerInterface;

    public GroupAdaptor(Context context, List<HashMap<String, String>> groupName) {

        this.groupName = groupName;
        this.context = context;
        this.lasttime = lasttime;


    }

    @Override
    public GroupAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_groupscreen, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GroupAdaptor.MyViewHolder holder, final int position) {


        HashMap<String, String> hashMap = groupName.get(position);

        final String groupKey = hashMap.get(Constant.GROUP_kEY);
        String usersList = hashMap.get(Constant.USER_NAME);
        String courseKey = hashMap.get(Constant.COURSE_KEY);
        String owner = hashMap.get(Constant.OWNER);
        String text = hashMap.get(Constant.TEXT_NAME);
        String image = hashMap.get(Constant.IMAGE_NAME);
        String video = hashMap.get(Constant.VIDEO_NAME);
        String file = hashMap.get(Constant.FILE_NAME);
        String time = hashMap.get(Constant.TIME_NAME);
        String mode = hashMap.get("mode");
        Log.d("video","------"+video);
        if (hashMap.containsKey(Constant.GROUP_ICON)) {

            String groupIcon = hashMap.get(Constant.GROUP_ICON);
            Glide.with(context).load(groupIcon).apply(RequestOptions.circleCropTransform()).into(holder.group_iv);
        } else if (hashMap.containsKey(Constant.PROFILE_PICTURE)) {

            String groupIcon = hashMap.get(Constant.PROFILE_PICTURE);
            Log.d("icon",groupIcon);
            if (!groupIcon.equals("null")) {
                Glide.with(context).load(groupIcon).apply(RequestOptions.circleCropTransform()).into(holder.group_iv);
            }
            else
            {
                Glide.with(context).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.group_iv);
            }
        } else if (mode.equals("individual")) {

            Glide.with(context).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.group_iv);
        } else {

            Glide.with(context).load(R.mipmap.groupicon).apply(RequestOptions.circleCropTransform()).into(holder.group_iv);
        }
        if (time != null && !time.isEmpty()) {
            holder.time_iv.setVisibility(View.VISIBLE);

            holder.time_iv.setText(time);
        } else {
            holder.time_iv.setVisibility(View.GONE);
        }
        if (image != null && !image.isEmpty()) {
            holder.msg_tv.setText(image);
            holder.msg_tv.setVisibility(View.VISIBLE);
        } else if (text != null && !text.isEmpty()) {
            holder.msg_tv.setVisibility(View.VISIBLE);
            holder.msg_tv.setText(text);
        } else if (video != null && !video.isEmpty()) {
            Log.d("video",video);
            holder.msg_tv.setText(video);
            holder.msg_tv.setVisibility(View.VISIBLE);
        } else if (file != null && !file.isEmpty()) {
            holder.msg_tv.setText(file);
            holder.msg_tv.setVisibility(View.VISIBLE);
        } else {
            Log.d("video",video+"------");
            holder.msg_tv.setVisibility(View.GONE);
        }
        holder.groupname_tv.setText(hashMap.get(Constant.GROUP_NAME));
  /*      HashMap<String, String> userHashMap = new HashMap<>();
        userHashMap.put(Constant.GROUP_kEY, groupKey);
        userHashMap.put(Constant.USER_LIST, usersList);
        userHashMap.put(Constant.COURSE_KEY, courseKey);
        userHashMap.put(Constant.OWNER, owner);
        userHashMap.put("mode", mode);
        holder.groupname_tv.setText(hashMap.get(Constant.GROUP_NAME));

        userList.add(userHashMap);*/

 /*       final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(Constant.CHAT).child(groupKey);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                if (dataSnapshot.getValue() != null) {

                    Query lastQuery = FirebaseDatabase.getInstance().getReference(Constant.CHAT).child(groupKey).limitToLast(1);
                    lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {


                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                                if (snapshot.child(Constant.TEXT_NAME).exists()) {
                                    String lastmsg = snapshot.child(Constant.TEXT_NAME).getValue().toString();
                                    holder.msg_tv.setVisibility(View.VISIBLE);
                                    holder.msg_tv.setText(lastmsg);

                                } else if (snapshot.child(Constant.IMAGE_NAME).exists()) {

                                    holder.msg_tv.setText(Constant.IMAGE_NAME);
                                    holder.msg_tv.setVisibility(View.VISIBLE);
                                }
                                else if (snapshot.child("video").exists())
                                {
                                    holder.msg_tv.setText("video");
                                    holder.msg_tv.setVisibility(View.VISIBLE);
                                }
                                else if (snapshot.child("file").exists())
                                {
                                    holder.msg_tv.setText("file");
                                    holder.msg_tv.setVisibility(View.VISIBLE);
                                }

                                if (snapshot.child(Constant.TIME_NAME).exists()) {
                                    String time = snapshot.child(Constant.TIME_NAME).getValue().toString();

                                    String localTime = SystemTime.getInstance().getLocalTime(time);

                                    holder.time_iv.setVisibility(View.VISIBLE);

                                    holder.time_iv.setText(localTime);

                                }


                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {


                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/


        //    holder.msg_tv.setText(msg.get(position));


        //    holder.time_iv.setText(lasttime.get(position));


    }


    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public int getItemCount() {
        return groupName.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView groupname_tv, msg_tv, time_iv;
        ImageView group_iv;
        // LinearLayout course_ll;

        public MyViewHolder(View itemView) {
            super(itemView);

            groupname_tv = (TextView) itemView.findViewById(R.id.course_tv);
            msg_tv = (TextView) itemView.findViewById(R.id.msg_tv);
            time_iv = (TextView) itemView.findViewById(R.id.time_iv);
            group_iv = (ImageView) itemView.findViewById(R.id.group_iv);

        }

    }

    public void addData(List<HashMap<String, String>> groupName) {
        this.groupName = groupName;
        //  userList = new ArrayList<>();
    }


    public List<HashMap<String, String>> getUserList() {

        return groupName;
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private ClickRecyclerInterface clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickRecyclerInterface clicklistener) {

            this.clicklistener = clicklistener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recycleView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clicklistener != null) {
                        clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());

            if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {

                TextView groupname_tv = (TextView) child.findViewById(R.id.course_tv);
                child.setTag(groupname_tv.getText().toString());
                clicklistener.onRecyClick(child, rv.getChildAdapterPosition(child));

            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
