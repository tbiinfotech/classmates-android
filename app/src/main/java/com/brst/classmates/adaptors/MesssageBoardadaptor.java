package com.brst.classmates.adaptors;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;

import com.brst.classmates.classses.DownloadTask;
import com.brst.classmates.classses.FirebaseData;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.fragments.MessageBoardFragment;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.brst.classmates.fragments.OneToOneChatFragment.tempraryDirectory;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public class MesssageBoardadaptor extends RecyclerView.Adapter<MesssageBoardadaptor.MyViewHolder> {

    //   String name[];

    List<FirebaseData> chatList;

    Context context;

    String loginUserKey;
    // String tempraryDirectory = Environment.getExternalStorageDirectory() + "/classmate";

    public MesssageBoardadaptor(List<FirebaseData> chatList, Context context) {

        this.chatList = chatList;

        this.context = context;

        loginUserKey = StoreData.getUserKeyFromSharedPre(context);

    }

    @Override
    public MesssageBoardadaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_messageboard, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MesssageBoardadaptor.MyViewHolder holder, int position) {


        FirebaseData firebaseData = chatList.get(position);

        String text = firebaseData.getText();
        String userid = firebaseData.getUserid();
        final String image = firebaseData.getImage();
        String time = firebaseData.getTime();
        final String profile = firebaseData.getProfile_picture();
        final String video = firebaseData.getVideo();
        final String videoThumb = firebaseData.getVideothumb();
        final Bitmap videoBitmap = firebaseData.getVideoBitmap();
        final String originalVideoPath = firebaseData.getOriginalPath();
        final String name = firebaseData.getName();
        final String val = firebaseData.getVal();

        final String file = firebaseData.getFile();
        final String fileName = firebaseData.getFileName();
        String type = firebaseData.getType();
      //  Boolean deliver = firebaseData.getDeliver();

        if (val != null) {
            Log.d("pathvideo", val + "-----");
        }

        final String localTime = SystemTime.getInstance().getLocalTime(time);

        if (userid.equals(loginUserKey)) {


            holder.send_ll.setVisibility(View.GONE);

            holder.receive_ll.setVisibility(View.VISIBLE);
        /*    if (deliver != null) {
                if (deliver) {
                    holder.textViewDeliver.setText("deliver");
                } else {
                    holder.textViewDeliver.setText("");
                }
            }*/

            if (image != null && !image.isEmpty()) {
                holder.rightimage_ll.setVisibility(View.VISIBLE);
                holder.righttext_ll.setVisibility(View.GONE);
                holder.rightPlay_iv.setVisibility(View.GONE);

                holder.rightImageTime_tv.setText(localTime);

                Glide.with(context.getApplicationContext()).load(image).into(holder.right_iv);

                if (profile.equals("null")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.rightImageprofile_iv);

                } else {
                    Glide.with(context.getApplicationContext()).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.rightImageprofile_iv);
                }

                holder.right_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        ShowDialog.getInstance().showImage(context, image, localTime, name, video, fileName, videoBitmap);


                    }
                });
            } else if ((video != null && !video.isEmpty())) {

             /*   if (video.contains("https://firebasestorage.googleapis.com")) {
                    //  if (holder.progressRight.getVisibility()==View.VISIBLE) {

                    holder.progressRight.setVisibility(View.GONE);
                    // Log.d("pathvideo", video);
                    //  }
                } else {
                    holder.progressRight.setVisibility(View.VISIBLE);
                    //   Log.d("pathvideo", video + "-----");
                }*/
                holder.rightimage_ll.setVisibility(View.VISIBLE);
                holder.righttext_ll.setVisibility(View.GONE);
                holder.rightPlay_iv.setVisibility(View.VISIBLE);

                final String tempraryFilePath = tempraryDirectory + "/" + fileName;

                final File originalPath = new File(originalVideoPath);
                final File videoPath = new File(tempraryFilePath);

                holder.rightImageTime_tv.setText(localTime);

                if (videoBitmap != null) {

                    holder.right_iv.setImageBitmap(videoBitmap);

                } else {
                    Glide.with(context.getApplicationContext()).load(videoThumb).into(holder.right_iv);
                }

                if (profile.equals("null")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.rightImageprofile_iv);

                } else {
                    Glide.with(context.getApplicationContext()).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.rightImageprofile_iv);
                }

                holder.right_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (originalPath.exists()) {

                            ShowDialog.getInstance().showImage(context, videoThumb, localTime, name, originalVideoPath, fileName, videoBitmap);
                        } else if (videoPath.exists()) {

                            ShowDialog.getInstance().showImage(context, videoThumb, localTime, name, tempraryFilePath, fileName, videoBitmap);
                        } else {

                            ProgressDialog mProgressDialog;
                            mProgressDialog = new ProgressDialog(context);
                            mProgressDialog.setMessage(fileName);
                            mProgressDialog.setIndeterminate(true);
                            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            mProgressDialog.setCancelable(false);


                            final DownloadTask downloadTask = new DownloadTask(context, mProgressDialog, fileName);

                            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    final String tempraryFilePath = tempraryDirectory + "/" + fileName;
                                    File file1 = new File(tempraryFilePath);
                                    Log.d("qwerty", file1.exists() + "");
                                    Log.d("qwerty", DownloadTask.mProgressDialog.getProgress() + "---------------------");
                                    if (DownloadTask.mProgressDialog.getProgress() < 100) {

                                    }
                                    dialog.dismiss();
                                    downloadTask.cancel(true);
                                }
                            });

                            downloadTask.execute(video);

                            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {

                                    downloadTask.cancel(true);
                                }
                            });
                        }



                      /*  if (videoThumb.toString().contains("android.graphics.Bitmap")) {

                            ShowDialog.getInstance().showImage(context, "", localTime, MessageBoardFragment.groupName, video,fileName);
                        }
                        else {
                            ShowDialog.getInstance().showImage(context, videoThumb.toString(), localTime, MessageBoardFragment.groupName, video,fileName);
                        }
*/
                    }
                });
            } else if (file != null && !file.isEmpty()) {
                holder.rightimage_ll.setVisibility(View.VISIBLE);
                holder.righttext_ll.setVisibility(View.GONE);
                holder.rightPlay_iv.setVisibility(View.GONE);
                // holder.rightDoc_iv.setVisibility(View.VISIBLE);
                holder.rightImageTime_tv.setText(localTime);
                if (type.equals("application/pdf")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.ic_pdf).into(holder.right_iv);
                } else if (type.equals("application/msword")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.ic_doc).into(holder.right_iv);
                } else {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.ic_txt).into(holder.right_iv);
                }

                if (profile.equals("null")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.rightImageprofile_iv);

                } else {
                    Glide.with(context.getApplicationContext()).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.rightImageprofile_iv);
                }

                final String finalType = type;
                holder.right_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tempraryFilePath = tempraryDirectory + "/" + fileName;

                        File file1 = new File(tempraryFilePath);

                        if (file1.exists()) {

                            Uri uri;
                            if (Build.VERSION.SDK_INT >= 24) {
                                uri = FileProvider.getUriForFile(context, context.getPackageName() + ".my.package.name.provider", file1);
                            } else {
                                uri = Uri.fromFile(file1);
                            }
                            Intent intent = new Intent();
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setAction(Intent.ACTION_VIEW);
                            if (finalType.equals("application/pdf")) {
                                // String filetype = "application/pdf";
                                intent.setDataAndType(uri, finalType);
                            } else if (finalType.equals("application/msword")) {
                                intent.setDataAndType(uri, finalType);
                            } else {
                                intent.setDataAndType(uri, finalType);
                            }
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            PackageManager pm = context.getPackageManager();
                            List<ResolveInfo> activities = pm.queryIntentActivities(intent, 0);

                            if (activities.size() > 0) {
                                context.startActivity(intent);
                            } else {
                                Toast.makeText(context, "hi there is no pdf viewer in your system", Toast.LENGTH_SHORT).show();

                            }
                        } else {

                            ProgressDialog mProgressDialog;
                            mProgressDialog = new ProgressDialog(context);
                            mProgressDialog.setMessage(fileName);
                            mProgressDialog.setIndeterminate(true);
                            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            mProgressDialog.setCancelable(false);


                            final DownloadTask downloadTask = new DownloadTask(context, mProgressDialog, fileName);

                            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    final String tempraryFilePath = tempraryDirectory + "/" + fileName;
                                    File file1 = new File(tempraryFilePath);
                                    Log.d("qwerty", file1.exists() + "");
                                    Log.d("qwerty", DownloadTask.mProgressDialog.getProgress() + "---------------------");
                                    if (DownloadTask.mProgressDialog.getProgress() < 100) {

                                        file1.delete();
                                        if (file1.exists()) {
                                            try {
                                                file1.getCanonicalFile().delete();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            if (file1.exists()) {
                                                context.deleteFile(file1.getName());
                                            }
                                        }
                                    }
                                    dialog.dismiss();
                                    downloadTask.cancel(true);
                                }
                            });

                            downloadTask.execute(file);

                            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    downloadTask.cancel(true);
                                }
                            });
                        }

                   /*     Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(Intent.ACTION_VIEW);
                        String type = "application/pdf";
                        intent.setDataAndType(Uri.parse(tempraryFilePath), type);
                       context.startActivity(intent);*/
                    }
                });

            } else {
                holder.right_tv.setText(text);

                holder.rightTextTime_tv.setText(localTime);

                holder.righttext_ll.setVisibility(View.VISIBLE);
                holder.rightimage_ll.setVisibility(View.GONE);

                if (profile.equals("null")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.rightTextprofile_iv);

                } else {

                    Glide.with(context.getApplicationContext()).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.rightTextprofile_iv);
                }
            }


        } else {

            holder.send_ll.setVisibility(View.VISIBLE);
            holder.receive_ll.setVisibility(View.GONE);

            if (image != null && !image.isEmpty()) {
                holder.leftImage_ll.setVisibility(View.VISIBLE);
                holder.leftText_ll.setVisibility(View.GONE);
                holder.leftPlay_iv.setVisibility(View.GONE);
                holder.leftImagetime_tv.setText(localTime);

                Glide.with(context.getApplicationContext()).load(image).into(holder.leftImage_iv);

                if (profile.equals("null")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.leftimageprofile_iv);
                } else {
                    Glide.with(context.getApplicationContext()).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.leftimageprofile_iv);
                }

                holder.leftImage_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        ShowDialog.getInstance().showImage(context, image, localTime, name, video, fileName, videoBitmap);
                    }
                });


            } else if ((video != null && !video.isEmpty()) && (videoThumb != null && !videoThumb.toString().isEmpty())) {
                holder.leftImage_ll.setVisibility(View.VISIBLE);
                holder.leftText_ll.setVisibility(View.GONE);
                holder.leftPlay_iv.setVisibility(View.VISIBLE);
                holder.leftImagetime_tv.setText(localTime);

            /*    String tempraryFilePath = tempraryDirectory + "/" + fileName;
                final File file1 = new File(tempraryFilePath);*/

                final String tempraryFilePath = tempraryDirectory + "/" + fileName;

                final File originalPath = new File(originalVideoPath);
                final File videoPath = new File(tempraryFilePath);

                if (videoBitmap != null) {

                    holder.leftImage_iv.setImageBitmap(videoBitmap);

                } else {
                    Glide.with(context.getApplicationContext()).load(videoThumb).into(holder.leftImage_iv);
                }
                if (profile.equals("null")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.leftimageprofile_iv);
                } else {
                    Glide.with(context.getApplicationContext()).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.leftimageprofile_iv);
                }

                holder.leftImage_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        // ShowDialog.getInstance().showImage(context, videoThumb.toString(), localTime, MessageBoardFragment.groupName, video, fileName, videoBitmap);

                        if (originalPath.exists()) {

                            ShowDialog.getInstance().showImage(context, videoThumb, localTime, name, originalVideoPath, fileName, videoBitmap);
                        } else if (videoPath.exists()) {

                            ShowDialog.getInstance().showImage(context, videoThumb, localTime, name, tempraryFilePath, fileName, videoBitmap);
                        } else {


                            ProgressDialog mProgressDialog;
                            mProgressDialog = new ProgressDialog(context);
                            mProgressDialog.setMessage(fileName);
                            mProgressDialog.setIndeterminate(true);
                            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            mProgressDialog.setCancelable(false);


                            final DownloadTask downloadTask = new DownloadTask(context, mProgressDialog, fileName);

                            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    final String tempraryFilePath = tempraryDirectory + "/" + fileName;
                                    File file1 = new File(tempraryFilePath);
                                    Log.d("qwerty", file1.exists() + "");
                                    Log.d("qwerty", DownloadTask.mProgressDialog.getProgress() + "---------------------");
                                    if (DownloadTask.mProgressDialog.getProgress() < 100) {

                                        file1.delete();
                                        if (file1.exists()) {
                                            try {
                                                file1.getCanonicalFile().delete();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            if (file1.exists()) {
                                                context.deleteFile(file1.getName());
                                            }
                                        }
                                    }
                                    dialog.dismiss();
                                    downloadTask.cancel(true);
                                }
                            });

                            downloadTask.execute(video);

                            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    downloadTask.cancel(true);
                                }
                            });
                        }
                    }
                });

            } else if (file != null && !file.isEmpty()) {
                holder.leftImage_ll.setVisibility(View.VISIBLE);
                holder.leftText_ll.setVisibility(View.GONE);
                holder.leftPlay_iv.setVisibility(View.GONE);

                holder.leftImagetime_tv.setText(localTime);
                if (type.equals("application/pdf")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.ic_pdf).into(holder.leftImage_iv);
                } else if (type.equals("application/msword")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.ic_doc).into(holder.leftImage_iv);
                } else {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.ic_txt).into(holder.leftImage_iv);
                }

                if (profile.equals("null")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.leftimageprofile_iv);

                } else {
                    Glide.with(context.getApplicationContext()).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.leftimageprofile_iv);
                }
                final String finalType = type;
                holder.leftImage_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tempraryFilePath = tempraryDirectory + "/" + fileName;
                        Log.d("file", tempraryFilePath);
                        File file1 = new File(tempraryFilePath);
                        if (file1.exists()) {
                            Uri uri;
                            if (Build.VERSION.SDK_INT >= 24) {
                                uri = FileProvider.getUriForFile(context, context.getPackageName() + ".my.package.name.provider", file1);
                            } else {
                                uri = Uri.fromFile(file1);
                            }
                            Intent intent = new Intent();
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setAction(Intent.ACTION_VIEW);
                            if (finalType.equals("application/pdf")) {

                                intent.setDataAndType(uri, finalType);
                            } else if (finalType.equals("application/msword")) {
                                intent.setDataAndType(uri, finalType);
                            } else {
                                intent.setDataAndType(uri, finalType);
                            }
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            PackageManager pm = context.getPackageManager();
                            List<ResolveInfo> activities = pm.queryIntentActivities(intent, 0);

                            if (activities.size() > 0) {
                                context.startActivity(intent);
                            } else {
                                Toast.makeText(context, "hi there is no pdf viewer in your system", Toast.LENGTH_SHORT).show();

                            }

                        } else {

                            ProgressDialog mProgressDialog;
                            mProgressDialog = new ProgressDialog(context);
                            mProgressDialog.setMessage(fileName);
                            mProgressDialog.setIndeterminate(true);
                            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                            mProgressDialog.setCancelable(false);


                            final DownloadTask downloadTask = new DownloadTask(context, mProgressDialog, fileName);

                            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                    downloadTask.cancel(true);
                                }
                            });
                            Log.d("file", file);
                            downloadTask.execute(file);

                            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    downloadTask.cancel(true);
                                }
                            });
                        }

                    }
                });
            } else {
                holder.leftText_ll.setVisibility(View.VISIBLE);
                holder.leftText_tv.setText(text);

                holder.leftTexttime_tv.setText(localTime);
                holder.leftImage_ll.setVisibility(View.GONE);


                if (profile.equals("null")) {
                    Glide.with(context.getApplicationContext()).load(R.mipmap.user).apply(RequestOptions.circleCropTransform()).into(holder.leftTextprofile_iv);
                } else {
                    Glide.with(context.getApplicationContext()).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.leftTextprofile_iv);
                }
            }
        }


    }

  /*  private void showImage(final Context context,String image,String time) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        dialog.show();
        ImageView chat_iv = (ImageView) dialog.findViewById(R.id.chat_iv);
        final ImageView cou_bck_iv = (ImageView) dialog.findViewById(R.id.cou_bck_iv);
        TextView groupName_tv=(TextView)dialog.findViewById(R.id.groupName_tv);
        TextView time_tv=(TextView)dialog.findViewById(R.id.time_tv);

        final LinearLayout header_ll=(LinearLayout)dialog.findViewById(R.id.header_ll);
        final LinearLayout Linear_ll=(LinearLayout)dialog.findViewById(R.id.Linear_ll);
        FrameLayout frame_fl=(FrameLayout) dialog.findViewById(R.id.frame_fl);

        groupName_tv.setText(MessageBoardFragment.groupName);
        time_tv.setText(time);

        Glide.with(context).load(image).into(chat_iv);

        cou_bck_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        frame_fl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Linear_ll.getVisibility()==View.VISIBLE)
                {

                    Linear_ll.setVisibility(View.GONE);
                    cou_bck_iv.setVisibility(View.GONE);


                }

                else
                {


                    Linear_ll.setVisibility(View.VISIBLE);
                    cou_bck_iv.setVisibility(View.VISIBLE);



                }
            }
        });

        dialog.setCanceledOnTouchOutside(true);
    }*/

    @Override
    public int getItemCount() {

        return chatList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView leftText_tv, right_tv, leftTexttime_tv, leftImagetime_tv, rightTextTime_tv, rightImageTime_tv;

        ImageView leftImage_iv, right_iv, leftTextprofile_iv, leftimageprofile_iv, rightImageprofile_iv, rightTextprofile_iv, rightPlay_iv, leftPlay_iv, rightDoc_iv;

        LinearLayout leftText_ll, leftImage_ll, rightimage_ll, righttext_ll, send_ll, receive_ll;
        ProgressBar progressRight, progressLeft;
        TextView textViewDeliver;


        public MyViewHolder(View itemView) {
            super(itemView);

            //  time_tv = (TextView) itemView.findViewById(R.id.time_tv);
            leftText_tv = (TextView) itemView.findViewById(R.id.leftText_tv);
            right_tv = (TextView) itemView.findViewById(R.id.right_tv);
             //  textViewDeliver = (TextView) itemView.findViewById(R.id.textViewDeliver);

            progressLeft = (ProgressBar) itemView.findViewById(R.id.progressLeft);
            progressRight = (ProgressBar) itemView.findViewById(R.id.progressRight);

            leftImage_iv = (ImageView) itemView.findViewById(R.id.leftImage_iv);
            right_iv = (ImageView) itemView.findViewById(R.id.right_iv);
            leftTextprofile_iv = (ImageView) itemView.findViewById(R.id.leftTextprofile_iv);
            leftimageprofile_iv = (ImageView) itemView.findViewById(R.id.leftimageprofile_iv);
            rightImageprofile_iv = (ImageView) itemView.findViewById(R.id.rightImageprofile_iv);
            rightTextprofile_iv = (ImageView) itemView.findViewById(R.id.rightTextprofile_iv);
            rightPlay_iv = (ImageView) itemView.findViewById(R.id.rightPlay_iv);
            leftPlay_iv = (ImageView) itemView.findViewById(R.id.leftPlay_iv);
            //  rightDoc_iv = (ImageView) itemView.findViewById(R.id.rightDoc_iv);


            leftTexttime_tv = (TextView) itemView.findViewById(R.id.leftTexttime_tv);
            leftImagetime_tv = (TextView) itemView.findViewById(R.id.leftImagetime_tv);
            rightTextTime_tv = (TextView) itemView.findViewById(R.id.rightTextTime_tv);
            rightImageTime_tv = (TextView) itemView.findViewById(R.id.rightImageTime_tv);

            leftImage_ll = (LinearLayout) itemView.findViewById(R.id.leftImage_ll);
            leftText_ll = (LinearLayout) itemView.findViewById(R.id.leftText_ll);
            rightimage_ll = (LinearLayout) itemView.findViewById(R.id.rightimage_ll);
            righttext_ll = (LinearLayout) itemView.findViewById(R.id.righttext_ll);
            send_ll = (LinearLayout) itemView.findViewById(R.id.send_ll);
            receive_ll = (LinearLayout) itemView.findViewById(R.id.receive_ll);
        }
    }

    public void addData(ArrayList<FirebaseData> chatList, final RecyclerView message_rv, final MesssageBoardadaptor messsageBoardadaptor) {

        Log.d("size", this.chatList.size() + "-------" + chatList.size());
        int index = this.chatList.size();

      /*  for (int i = index; i < chatList.size(); i++) {
            this.chatList.add(i, chatList.get(i));

        }*/
this.chatList=chatList;
        ///  this.chatList=chatList;
        notifyDataSetChanged();

        message_rv.post(new Runnable() {
            @Override
            public void run() {
                // Call smooth scroll
                message_rv.smoothScrollToPosition(messsageBoardadaptor.getItemCount());
            }
        });
    }
}
