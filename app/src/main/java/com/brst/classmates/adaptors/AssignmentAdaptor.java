package com.brst.classmates.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.classmates.R;

import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.fragments.AssignmentDetail;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.utility.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public class AssignmentAdaptor extends RecyclerViewEmptySupport.Adapter<AssignmentAdaptor.MyViewHolder> {

    List<HashMap<String, String>> assignment_list;
    ClickRecyclerInterface clickRecyclerInterface;
    Context context;

    public AssignmentAdaptor(List<HashMap<String, String>> assignment_list, Context context) {

        this.assignment_list = assignment_list;
        this.context = context;
    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public AssignmentAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_assignment, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AssignmentAdaptor.MyViewHolder holder, int position) {

        HashMap<String, String> hashMap = assignment_list.get(position);
        final String assignName = hashMap.get(Constant.ASSIGN_NAME);
        final String assignType = hashMap.get(Constant.ASSIGNTYPE_NAME);
        final String assignStatus = hashMap.get(Constant.STATUS_NAME);
        final String assignDate = hashMap.get(Constant.ASSIGNDATE_NAME);
        final String submitNum = hashMap.get(Constant.SUBMITNUM_NAME);
        final String progressNum = hashMap.get(Constant.PROGRESSNUM_NAME);
        final String adminName = hashMap.get(Constant.ASSIGNADMIN_NAME);
        final String courseName = hashMap.get(Constant.COURSE_NAME);
        final String userKeyInvited = hashMap.get(Constant.STUDENT_LIST);
        // final String courseKey = hashMap.get(Constant.COURSE_KEY);

        String dateArray[] = assignDate.split("@");
        String date = Arrays.toString(dateArray);
        String courseKey = hashMap.get(Constant.COURSE_KEY);
        holder.assign_tv.setText(assignName);
        holder.degree_tv.setText(assignType);
        holder.status_tv.setText(assignStatus);
        holder.date_tv.setText(assignDate);
        holder.course_tv.setText(courseName);

        final String assignmentKey = hashMap.get(Constant.KEY_NAME);

        List<String> list = new ArrayList<>();
        list.add(assignmentKey);
        list.add(assignName);
        list.add(assignType);
        list.add(assignStatus);
        list.add(assignDate);
        list.add(courseKey);
        list.add(courseName);
        list.add(userKeyInvited);
        holder.assign_ll.setTag(assignmentKey + ";" + assignName + ";" + assignType + ";" + assignStatus + ";" + assignDate + ";" + courseKey + ";" + courseName+";"+adminName+";"+userKeyInvited);

      //  holder.assign_ll.setTag(list);

      //  holder.assign_tv.setTag(assignmentKey + "," + adminName);

    }

    @Override
    public int getItemCount() {
        return assignment_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView assign_tv, status_tv, degree_tv, date_tv, course_tv;
        LinearLayout assign_ll;

        public MyViewHolder(View itemView) {
            super(itemView);

            assign_tv = (TextView) itemView.findViewById(R.id.assign_tv);
            status_tv = (TextView) itemView.findViewById(R.id.status_tv);
            degree_tv = (TextView) itemView.findViewById(R.id.degree_tv);
            date_tv = (TextView) itemView.findViewById(R.id.date_tv);
            course_tv = (TextView) itemView.findViewById(R.id.course_tv);
            assign_ll = (LinearLayout) itemView.findViewById(R.id.assign_ll);

            assign_ll.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (clickRecyclerInterface != null) {
                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }
        }
    }


    public void addData(List<HashMap<String, String>> assignmentList)

    {
        this.assignment_list = assignmentList;
    }


    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private ClickRecyclerInterface clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickRecyclerInterface clicklistener) {

            this.clicklistener = clicklistener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recycleView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clicklistener != null) {
                        LinearLayout assign_ll = (LinearLayout) child.findViewById(R.id.assign_ll);
                        child.setTag(assign_ll.getTag());
                        clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());

            if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {

                LinearLayout assign_ll = (LinearLayout) child.findViewById(R.id.assign_ll);
                child.setTag(assign_ll.getTag());
                clicklistener.onRecyClick(child, rv.getChildAdapterPosition(child));

            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
