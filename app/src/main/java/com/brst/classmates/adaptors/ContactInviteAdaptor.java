package com.brst.classmates.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class ContactInviteAdaptor extends RecyclerView.Adapter<ContactInviteAdaptor.MyViewHolder> {

   ArrayList<HashMap<String,String>> contacts;
    Context context;

    AddAssignmentFragment addAssignmentFragment = new AddAssignmentFragment();


    int visibility;

    public ContactInviteAdaptor(ArrayList<HashMap<String, String>> contacts, Context context) {

        this.contacts=contacts;
        this.context=context;


        Log.d("dsds",contacts+"");
    }


    @Override
    public ContactInviteAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contactinvite, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactInviteAdaptor.MyViewHolder holder,final int position) {

    HashMap<String,String> hashMap=contacts.get(position);
     String contactName=hashMap.get("ContactName");
     String contactNumber=hashMap.get("ContactNumber");
        String contactProfile="";
        if(hashMap.containsKey("ContactProfile")) {

            contactProfile = hashMap.get("ContactProfile");
        }

        if(contactProfile!=null && !contactProfile.isEmpty()) {

            Glide.with(context).load(contactProfile).apply(RequestOptions.circleCropTransform()).into(holder.contactProfile_iv);
        }


        else
        {
            Glide.with(context).load(contactProfile).apply(new RequestOptions().placeholder(R.mipmap.user)).into(holder.contactProfile_iv);
        }

        holder.name_tv.setText(contactName);
        holder.number_tv.setText(contactNumber);

        holder.contactCheck_cb.setChecked(Boolean.parseBoolean(contacts.get(position).get("boolean")));

        //  holder.chk_cb.setTag(studentList.get(position));
        holder.contactCheck_cb.setTag(contacts.get(position));

        holder.contactCheck_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CheckBox cb = (CheckBox) v;
              //  HashMap<String, String> hashMap1 = (HashMap) cb.getTag();


                if (cb.isChecked()) {
                    contacts.get(position).put("boolean", Constant.TRUE_NAME);


                } else {
                    contacts.get(position).put("boolean", Constant.FALSE_NAME);

                }
            }
        });


    }



    @Override
    public int getItemCount() {
        return contacts.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name_tv,number_tv;
        ImageView contactProfile_iv;
        CheckBox contactCheck_cb;


        public MyViewHolder(View itemView) {
            super(itemView);

            name_tv = (TextView) itemView.findViewById(R.id.name_tv);
            number_tv = (TextView) itemView.findViewById(R.id.number_tv);
            contactProfile_iv = (ImageView) itemView.findViewById(R.id.contactProfile_iv);
            contactCheck_cb = (CheckBox) itemView.findViewById(R.id.contactCheck_cb);


            //   invite_tv.setOnClickListener(this);


        }




        @Override
        public void onClick(View v) {

            List<String> list = new ArrayList();
/*
            if (clickRecyclerInterface != null) {
                list.add(course_tv.getText().toString());
                list.add(courseKey.get(getAdapterPosition()));

                add_iv.setTag(list);
                //  course_tv.setTag(course_tv.getText().toString());


                delete_iv.setTag(courseKey.get(getAdapterPosition()));
                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }*/
        }
    }

 /*   public void addDataInList(List<HashMap<String, String>> dataList, List<String> courseKey) {
        this.dataList = dataList;
        this.courseKey = courseKey;

        Log.d("asd", courseKey + "");
        Log.d("asd", dataList + "");
    }*/



    public void addData(ArrayList<HashMap<String, String>> searchContactList) {

        this.contacts=searchContactList;

    }

    public List<HashMap<String, String>> getSelectedContactList() {

        return contacts;
    }

}
