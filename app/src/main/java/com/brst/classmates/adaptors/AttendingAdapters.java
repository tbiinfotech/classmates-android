package com.brst.classmates.adaptors;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.ProfileActivity;
import com.brst.classmates.beanclass.EventAttendingCandidates;
import com.brst.classmates.classses.NetworkCheck;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.fragments.EventListFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.brst.classmates.activity.CropImageActivity.image;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class AttendingAdapters extends RecyclerView.Adapter<AttendingAdapters.MyViewHolder> {

    ArrayList<EventAttendingCandidates>contacts;
    Context context;
    List<String> courseKey;
    List<String> courseKey_invite;
    AddAssignmentFragment addAssignmentFragment = new AddAssignmentFragment();
    public  HashMap<String, String> hashMaps=null;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    int visibility;
     int Position;
    public AttendingAdapters(ArrayList<EventAttendingCandidates> contacts, Context context, int Position) {

        this.contacts = contacts;
        this.context = context;
        this.courseKey=courseKey;
        this.Position=Position;
        Log.d("dsds", contacts + "");
    }


    @Override
    public AttendingAdapters.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_attending_adapter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AttendingAdapters.MyViewHolder holder, final int position) {
if(Position==1) {
if(contacts.get(position).getAtteding().equals("Attending")) {
    holder.name_tv.setText(contacts.get(position).getName());

    holder.status_tv.setText(contacts.get(position).getAtteding());
    // holder.name_tv_wait.setText(contacts.get(position).getName());

    //   holder.status_tv_wait.setText(hashMaps.get("WAITING"));
    String contactProfile = "";
    try {


        contactProfile = contacts.get(position).getProfile_pic();


        if (contactProfile != null && !contactProfile.isEmpty()) {

            Glide.with(context).load(contactProfile).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
        } else {
            Glide.with(context).load(contactProfile).apply(new RequestOptions().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
        }
    } catch (Exception e) {
    }
} else{
    holder.lay_contact.setVisibility(View.GONE);
}
}
        if(Position==3) {
    if(contacts.get(position).getAtteding().equals("May be")){

            holder.name_tv.setText(contacts.get(position).getName());

            holder.status_tv.setText(contacts.get(position).getAtteding());
            // holder.name_tv_wait.setText(contacts.get(position).getName());

            //   holder.status_tv_wait.setText(hashMaps.get("WAITING"));
            String contactProfile = "";
            try {


                contactProfile = contacts.get(position).getProfile_pic();


                if (contactProfile != null && !contactProfile.isEmpty()) {

                    Glide.with(context).load(contactProfile).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
                } else {
                    Glide.with(context).load(contactProfile).apply(new RequestOptions().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
                }
            } catch (Exception e) {
            }}

            else{
        holder.lay_contact.setVisibility(View.GONE);
    }

        }
        if(Position==2) {

    if(contacts.get(position).getAtteding().equals("")){
        ShowDialog.resultdialog(context,"No User!");

            /*holder.name_tv.setText(contacts.get(position).getName());

            holder.status_tv.setText(contacts.get(position).getAtteding());
            // holder.name_tv_wait.setText(contacts.get(position).getName());

            //   holder.status_tv_wait.setText(hashMaps.get("WAITING"));
            String contactProfile = "";
            try {


                contactProfile = contacts.get(position).getProfile_pic();


                if (contactProfile != null && !contactProfile.isEmpty()) {

                    Glide.with(context).load(contactProfile).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
                } else {
                    Glide.with(context).load(contactProfile).apply(new RequestOptions().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
                }
            } catch (Exception e) {
            }*/}

        }


    }


    @Override
    public int getItemCount() {
        return contacts.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name_tv, status_tv,name_tv_wait, status_tv_wait;
        ImageView contactProfile_iv,contactProfile_iv_wait;
        CheckBox contactCheck_cb;

        LinearLayout lay_contact;
        public MyViewHolder(View itemView) {
            super(itemView);

            name_tv = (TextView) itemView.findViewById(R.id.name_tv);
            status_tv = (TextView) itemView.findViewById(R.id.status_tv);
            name_tv_wait = (TextView) itemView.findViewById(R.id.name_tv_wait);
            status_tv_wait = (TextView) itemView.findViewById(R.id.status_tv_wait);
            contactProfile_iv = (ImageView) itemView.findViewById(R.id.contactProfile_iv);
            contactProfile_iv_wait = (ImageView) itemView.findViewById(R.id.contactProfile_iv_wait);
            contactCheck_cb = (CheckBox) itemView.findViewById(R.id.contactCheck_cb);
            lay_contact = (LinearLayout) itemView.findViewById(R.id.lay_contact);


            //   invite_tv.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {

            List<String> list = new ArrayList();
/*
            if (clickRecyclerInterface != null) {
                list.add(course_tv.getText().toString());
                list.add(courseKey.get(getAdapterPosition()));

                add_iv.setTag(list);
                //  course_tv.setTag(course_tv.getText().toString());


                delete_iv.setTag(courseKey.get(getAdapterPosition()));
                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }*/
        }
    }

 /*   public void addDataInList(List<HashMap<String, String>> dataList, List<String> courseKey) {
        this.dataList = dataList;
        this.courseKey = courseKey;

        Log.d("asd", courseKey + "");
        Log.d("asd", dataList + "");
    }*/





}
