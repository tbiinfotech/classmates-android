package com.brst.classmates.adaptors;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.beanclass.EventAttendingCandidates;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.EventListFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 7/4/17.
 */

public class EventAdaptor extends RecyclerView.Adapter<EventAdaptor.MyViewHolder> {

    ArrayList<HashMap>data_list = new ArrayList<>();;
    Context context;
    Object videoThumb = null;

    ClickRecyclerInterface clickRecyclerInterface;
    public static List<ArrayList<EventAttendingCandidates>> invited_user;
    public EventAdaptor( ArrayList<HashMap> data_list, Context context ) {

        this.data_list = data_list;
        this.context = context;
    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_eventlist, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        HashMap hashMap = data_list.get(position);

      final  String date = (String) hashMap.get("date");
        final String time = (String) hashMap.get("time");
        String location = (String) hashMap.get("location");

        String price = (String) hashMap.get("price");

        final String title = (String) hashMap.get("title");
        String desc = (String) hashMap.get("desc");
        String image_url = (String) hashMap.get("image_url");

        String admin = (String) hashMap.get("admin");
        String eventKey = (String) hashMap.get("event_key");
        String type = (String) hashMap.get("type");

        String attending = (String) hashMap.get("attending");
        String nonattending = (String) hashMap.get("nonattending");
        String tentative = (String) hashMap.get("mayBe");
        HashMap<String, String> invitedStudent = (HashMap) hashMap.get(Constant.INVITED_STUDENTS);

        List<String> studentKeyList = new ArrayList<>();

        for (String key : invitedStudent.keySet()) {

            studentKeyList.add(key);
        }

        String image_url1 = "";
        String image_url2 = "";
        String image_url3 = "";

        if (image_url != null && !image_url.isEmpty()) {

            holder.event_ll.setVisibility(View.VISIBLE);
            String images[] = image_url.split(",");


            if (images.length == 3) {
                image_url1 = images[0];
                image_url2 = images[1];
                image_url3 = images[2];

            }

            else if (images.length == 2) {
                image_url1 = images[0];
                image_url2 = images[1];

            }

            else if(images.length==1) {
                image_url1 = images[0];

                Log.d("sds", image_url1 + "-----" + image_url2);

            }
        } else {
            holder.event_ll.setVisibility(View.GONE);
        }


        if (admin.equals(StoreData.getUserKeyFromSharedPre(context))) {

            String listString = "";

            for (String s : studentKeyList) {
                listString += s + ",";
            }

            List<String> list = new ArrayList<>();
            list.add(eventKey);
            list.add(type);
            list.add(date);
            list.add(time);
            list.add(location);
            list.add(price);
            list.add(title);
            list.add(desc);
            list.add(image_url1);
            list.add(image_url2);
            list.add(image_url3);
            list.add(admin);
            list.add(listString);


            holder.editevent_tv.setVisibility(View.VISIBLE);

            holder.editevent_tv.setTag(list);

            holder.attending_ll.setVisibility(View.VISIBLE);
            holder.nonattending_ll.setVisibility(View.VISIBLE);
            holder.tentative_ll.setVisibility(View.VISIBLE);
            holder.admin_view.setVisibility(View.VISIBLE);

            holder.attending_tv.setText(attending);
            holder.nonAttending_tv.setText(nonattending);
            holder.tentative_tv.setText(tentative);
        } else {
            holder.editevent_tv.setVisibility(View.GONE);
            holder.admin_view.setVisibility(View.GONE);

            holder.attending_ll.setVisibility(View.GONE);
            holder.nonattending_ll.setVisibility(View.GONE);
            holder.tentative_ll.setVisibility(View.GONE);
        }

        if (date != null && !date.isEmpty() && time != null && !time.isEmpty()) {

            holder.dateAndTimeTV.setVisibility(View.VISIBLE);
            holder.dateAndTimeTV.setText(date + " " + time);


            holder.dateAndTimeTV.setVisibility(View.VISIBLE);
        } else {
            holder.dateAndTimeTV.setVisibility(View.GONE);

        }

        if (location != null && !location.isEmpty()) {
            holder.location_ll.setVisibility(View.VISIBLE);
            holder.location_view.setVisibility(View.VISIBLE);
            holder.locationTV.setText(location);
        } else {
            holder.location_ll.setVisibility(View.GONE);
            holder.location_view.setVisibility(View.GONE);

        }
        if (desc != null && !desc.isEmpty()) {
            holder.descTV.setVisibility(View.VISIBLE);
            holder.desc_View.setVisibility(View.VISIBLE);
            holder.descTV.setText(desc);
        } else {
            holder.descTV.setVisibility(View.GONE);
            holder.desc_View.setVisibility(View.GONE);

        }
        if (price != null && !price.isEmpty()) {
            holder.priceTV.setVisibility(View.VISIBLE);
            holder.priceTV.setText(price);
        } else {
            holder.priceTV.setVisibility(View.GONE);

        }
        if (title != null && !title.isEmpty()) {
            holder.titleTV.setVisibility(View.VISIBLE);
            holder.titleTV.setText(title);
        } else {
            holder.titleTV.setVisibility(View.GONE);

        }
        if (image_url1 != null && !image_url1.isEmpty()) {

            if(holder.progress.getVisibility()==View.GONE)
            {
                holder.progress.setVisibility(View.VISIBLE);
            }
          //  Glide.with(context).load(image_url1).apply(RequestOptions.placeholderOf(R.mipmap.picture)).into(holder.pictureIV);

            Glide.with(context).load(image_url1).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.progress.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {


                    holder.progress.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.pictureIV);


            final String image=image_url1;

            holder.pictureIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    ShowDialog.getInstance().showImage(context,image,date+" "+time,title, "", "", (Bitmap) videoThumb);
                }
            });
        }


        if (image_url2 != null && !image_url2.isEmpty()) {

            if(holder.progress1.getVisibility()==View.GONE)
            {
                holder.progress1.setVisibility(View.VISIBLE);
            }

          //  Glide.with(context).load(image_url2).apply(RequestOptions.placeholderOf(R.mipmap.picture)).into(holder.picture1IV);

            Glide.with(context).load(image_url2).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.progress1.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    holder.progress1.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.picture1IV);

            final String image=image_url2;

            holder.picture1IV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShowDialog.getInstance().showImage(context,image,date+" "+time,title, "", "", (Bitmap) videoThumb);
                }
            });
        }



        else
        {
           Glide.with(context).load(image_url2).into(holder.picture1IV);

            holder.progress1.setVisibility(View.GONE);
        }

        if (image_url3 != null && !image_url3.isEmpty()) {


            if(holder.progress2.getVisibility()==View.GONE)
            {
                holder.progress2.setVisibility(View.VISIBLE);
            }

          //  Glide.with(context).load(image_url3).apply(RequestOptions.placeholderOf(R.mipmap.picture)).into(holder.picture2IV);

            Glide.with(context).load(image_url3).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.progress2.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    Log.d("sdf","dfd");
                    holder.progress2.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.picture2IV);

         final String image=image_url3;

            holder.picture2IV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShowDialog.getInstance().showImage(context,image,date+" "+time,title, "", "", (Bitmap) videoThumb);
                }
            });

        }
        else
        {
            Glide.with(context).load(image_url3).into(holder.picture2IV);
            holder.progress2.setVisibility(View.GONE);
        }
        holder.attending_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getInvitedUser(1,position,"Attending");
            }
        });


        holder.nonattending_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getInvitedUser(2,position,"Not Attending");
            }
        });

        holder.tentative_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getInvitedUser(3,position,"May be");
            }
        });
    }

    public void addList(ArrayList<HashMap> data_list) {
        this.data_list = data_list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return data_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView priceTV, titleTV, locationTV, dateAndTimeTV, descTV, attending_tv, nonAttending_tv, tentative_tv;
        ImageView pictureIV, picture1IV, picture2IV;
        ImageView editevent_tv;
        LinearLayout attending_ll, nonattending_ll, event_ll,tentative_ll,location_ll,lay_invited_user;
        ProgressBar progress,progress1,progress2;
        View location_view,admin_view,desc_View;

        public MyViewHolder(View itemView) {
            super(itemView);
            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            priceTV = (TextView) itemView.findViewById(R.id.priceTV);
            locationTV = (TextView) itemView.findViewById(R.id.locationTV);
            attending_tv = (TextView) itemView.findViewById(R.id.attending_tv);
            nonAttending_tv = (TextView) itemView.findViewById(R.id.nonAttending_tv);
            tentative_tv = (TextView) itemView.findViewById(R.id.tentative_tv);

            dateAndTimeTV = (TextView) itemView.findViewById(R.id.dateAndTimeTV);
            descTV = (TextView) itemView.findViewById(R.id.descTV);
            pictureIV = (ImageView) itemView.findViewById(R.id.pictureIV);
            picture1IV = (ImageView) itemView.findViewById(R.id.picture1IV);
            picture2IV = (ImageView) itemView.findViewById(R.id.picture2IV);
            lay_invited_user=(LinearLayout)itemView.findViewById(R.id.lay_invited_user);
            editevent_tv = (ImageView) itemView.findViewById(R.id.editevent_tv);
            attending_ll = (LinearLayout) itemView.findViewById(R.id.attending_ll);
            nonattending_ll = (LinearLayout) itemView.findViewById(R.id.nonattending_ll);
            event_ll = (LinearLayout) itemView.findViewById(R.id.event_ll);
            tentative_ll = (LinearLayout) itemView.findViewById(R.id.tentative_ll);
            location_ll = (LinearLayout) itemView.findViewById(R.id.location_ll);

            progress = (ProgressBar) itemView.findViewById(R.id.progress);
            progress1 = (ProgressBar) itemView.findViewById(R.id.progress1);
            progress2 = (ProgressBar) itemView.findViewById(R.id.progress2);

            location_view = (View) itemView.findViewById(R.id.location_view);
            admin_view = (View) itemView.findViewById(R.id.admin_view);
            desc_View = (View) itemView.findViewById(R.id.desc_View);

            editevent_tv.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (clickRecyclerInterface != null) {
                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }

        }
    }





    public  void resultdialog(final Context context, String msg,String title) {
        final Dialog login_dialog = new Dialog(context);
        final TextView tv_ok, tv_dialogtext,tv_deltitle;

        login_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        login_dialog.setContentView(R.layout.dialog_validation);

        WindowManager.LayoutParams lp = login_dialog.getWindow().getAttributes();
        lp.dimAmount = 0.8f;
        login_dialog.getWindow().setAttributes(lp);

        login_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        tv_ok = (TextView) login_dialog.findViewById(R.id.tv_ok);
        tv_dialogtext = (TextView) login_dialog.findViewById(R.id.tv_dialogtext);
        tv_deltitle = (TextView) login_dialog.findViewById(R.id.tv_deltitle);
        tv_deltitle.setText(title);
        tv_dialogtext.setText(msg);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login_dialog.dismiss();


            }
        });
        login_dialog.show();
    }



    public void getInvitedUser( int position_selected,int position,String msg) {

        if(position_selected==2){   resultdialog(context,"No User!","Not Attending");}

        else{
        final Dialog dialog_search = new Dialog(context);
        final TextView tv_ok, tv_deltitle, tv_location, name;
          RecyclerView rv_attending;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog_search.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog_search.getWindow().setAttributes(lp);
        dialog_search.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_search.setContentView(R.layout.layout_attending);
              AttendingAdapters mAdapter;


        dialog_search.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        rv_attending= (RecyclerView) dialog_search.findViewById(R.id.rv_attending);
            tv_deltitle= (TextView) dialog_search.findViewById(R.id.tv_deltitle);
            tv_deltitle.setText(msg);
 tv_ok= (TextView) dialog_search.findViewById(R.id.tv_ok);

            tv_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_search.dismiss();
                }
            });
        if(position_selected==1){

             ArrayList<EventAttendingCandidates>invite_attendent;
            ArrayList<EventAttendingCandidates>invite_attendent_final=new ArrayList<>();
            invite_attendent=new ArrayList<>();


                invite_attendent=EventListFragment.invited_user.get(position);
                EventAttendingCandidates obj_attending=new EventAttendingCandidates();


                for(int j=0;j<invite_attendent.size();j++){
                    obj_attending=invite_attendent.get(j);
                if(obj_attending.getAtteding().equals("Attending")){
                    invite_attendent_final.add(invite_attendent.get(j));
                }}


            mAdapter=new AttendingAdapters(invite_attendent_final,context,position_selected);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            rv_attending.setLayoutManager(mLayoutManager);
            rv_attending.setItemAnimator(new DefaultItemAnimator());
            rv_attending.setAdapter(mAdapter);
        }
            if(position_selected==3){

                ArrayList<EventAttendingCandidates>invite_attendent;
                ArrayList<EventAttendingCandidates>invite_attendent_final=new ArrayList<>();
                invite_attendent=new ArrayList<>();


                invite_attendent=EventListFragment.invited_user.get(position);
                EventAttendingCandidates obj_attending=new EventAttendingCandidates();


                for(int j=0;j<invite_attendent.size();j++){
                    obj_attending=invite_attendent.get(j);
                    if(obj_attending.getAtteding().equals("May be")){
                        invite_attendent_final.add(invite_attendent.get(j));
                    }}


                mAdapter=new AttendingAdapters(invite_attendent_final,context,position_selected);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                rv_attending.setLayoutManager(mLayoutManager);
                rv_attending.setItemAnimator(new DefaultItemAnimator());
                rv_attending.setAdapter(mAdapter);
            }


        dialog_search.show();}
    }


}
