package com.brst.classmates.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 8/2/17.
 */

public class CourseUserAdaptor extends RecyclerViewEmptySupport.Adapter<CourseUserAdaptor.MyViewHolder> {

    List<HashMap<String, String>> studentCourseList;
    Context context;

    public CourseUserAdaptor(Context context, List<HashMap<String, String>> studentCourseList) {

        this.context = context;
        this.studentCourseList = studentCourseList;
        Log.d("ds",studentCourseList.size()+"");
    }

    @Override
    public CourseUserAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_student, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CourseUserAdaptor.MyViewHolder holder, final int position) {

        HashMap<String, String> hashMap = studentCourseList.get(position);
        String fullName = hashMap.get(Constant.FULL_NAME);
        String userKey = hashMap.get(Constant.KEY_NAME);
        String checked = hashMap.get(Constant.CHECKED_NAME);
        String profilePicture="";
        if (hashMap.containsKey(Constant.PROFILE_PICTURE)) {

             profilePicture = hashMap.get(Constant.PROFILE_PICTURE);
        }
        holder.chk_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(holder.chk_cb.isChecked())
                {
                    studentCourseList.get(position).put(Constant.CHECKED_NAME,Constant.TRUE_NAME);
                }
                else
                {
                    studentCourseList.get(position).put(Constant.CHECKED_NAME,Constant.FALSE_NAME);
                }
            }
        });

        holder.student_tv.setText(fullName);

        Glide.with(context).load(profilePicture).apply(RequestOptions.circleCropTransform()).into(holder.userprofile_iv);

    }

    @Override
    public int getItemCount() {

        return studentCourseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView student_tv;
        ImageView userprofile_iv;
        CheckBox chk_cb;


        public MyViewHolder(View itemView) {
            super(itemView);

            student_tv = (TextView) itemView.findViewById(R.id.student_tv);
            userprofile_iv = (ImageView) itemView.findViewById(R.id.userprofile_iv);
            chk_cb = (CheckBox) itemView.findViewById(R.id.chk_cb);
        }
    }

    public void addList(List<HashMap<String, String>> studentCourseList)

    {
        this.studentCourseList = studentCourseList;
    }

    public List<HashMap<String, String>> getStudentist() {

        return studentCourseList;
    }
}
