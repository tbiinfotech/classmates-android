package com.brst.classmates.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class CourseAdaptor extends RecyclerView.Adapter<CourseAdaptor.MyViewHolder> {

    String name[];
    List<HashMap<String, String>> dataList;
    List<String> courseKey;

    ClickRecyclerInterface clickRecyclerInterface;
    Context context;

    AddAssignmentFragment addAssignmentFragment = new AddAssignmentFragment();


    int visibility;

    public CourseAdaptor(List<HashMap<String, String>> dataList, List<String> courseKey, boolean visible, Context context) {
        this.dataList = dataList;
        this.courseKey = courseKey;
        this.context = context;
        visibility = visible ? View.GONE : View.VISIBLE;
        //  addVisibility = visible ? View.VISIBLE :View.GONE;

        Log.d("visible", visibility + "");
    }

    @Override
    public CourseAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_recyclerlayout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CourseAdaptor.MyViewHolder holder, int position) {

        HashMap hashMap = dataList.get(position);
        String courseName = hashMap.get(Constant.COURSE_NAME).toString();
        String key = hashMap.get(Constant.KEY_NAME).toString();
        if (visibility == 8) {
            if (key.equals(StoreData.getUserKeyFromSharedPre(context))) {
                holder.delete_iv.setVisibility(View.VISIBLE);
            } else {
                holder.delete_iv.setVisibility(View.GONE);
            }
        }

            holder.course_tv.setText(courseName);
            if (position == dataList.size() - 1) {
                holder.divider_view.setVisibility(View.GONE);
            }


    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView course_tv;
        View divider_view;
        ImageView delete_iv, add_iv;
        LinearLayout course_ll;

        public MyViewHolder(View itemView) {
            super(itemView);

            course_tv = (TextView) itemView.findViewById(R.id.course_tv);
            // invite_tv = (TextView) itemView.findViewById(R.id.invite_tv);

            divider_view = (View) itemView.findViewById(R.id.divider_view);
            delete_iv = (ImageView) itemView.findViewById(R.id.delete_iv);
            add_iv = (ImageView) itemView.findViewById(R.id.add_iv);
            course_ll = (LinearLayout) itemView.findViewById(R.id.course_ll);

            add_iv.setVisibility(visibility);
            delete_iv.setVisibility(visibility == 0 ? View.GONE : View.VISIBLE);
            course_tv.setOnClickListener(this);
            delete_iv.setOnClickListener(this);
            add_iv.setOnClickListener(this);

            //   invite_tv.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {

            List<String> list = new ArrayList();

            if (clickRecyclerInterface != null) {
                list.add(course_tv.getText().toString());
                list.add(courseKey.get(getAdapterPosition()));

                add_iv.setTag(list);
                //  course_tv.setTag(course_tv.getText().toString());


                delete_iv.setTag(courseKey.get(getAdapterPosition()));
                course_tv.setTag(courseKey.get(getAdapterPosition()));
                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }
        }
    }

    public void addDataInList(List<HashMap<String, String>> dataList, List<String> courseKey) {
        this.dataList = dataList;
        this.courseKey = courseKey;

        Log.d("asd", courseKey + "");
        Log.d("asd", dataList + "");
    }
}
