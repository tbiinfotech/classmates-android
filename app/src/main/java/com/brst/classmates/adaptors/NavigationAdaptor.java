package com.brst.classmates.adaptors;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.brst.classmates.R;

import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.fragments.MessageBoardFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.SharedPreference;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public class NavigationAdaptor extends RecyclerView.Adapter<NavigationAdaptor.MyViewHolder> {

    //   String name[];

    String list[];

    Context context;

    ClickRecyclerInterface clickRecyclerInterface;

    public NavigationAdaptor(String list[], Context context) {

        this.list = list;
        this.context = context;

    }


    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public NavigationAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_menulist, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NavigationAdaptor.MyViewHolder holder, int position) {


        if (position == 4) {
            holder.switch1.setVisibility(View.VISIBLE);
            holder.text_tv.setText(list[position]);

        } else {
            holder.switch1.setVisibility(View.GONE);
            holder.text_tv.setText(list[position]);
        }

        if (StoreData.getNotificationFromSharedPre(context).equals("true")) {
            holder.switch1.setChecked(true);
        } else {
            holder.switch1.setChecked(false);

        }

        holder.switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (holder.switch1.isChecked()) {
                    Log.d("xdf", "chk");
                    //  SharedPreference.getInstance().storeData(context, Constant.SOUND, Constant.UNMUTE);
                    FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).child(StoreData.getUserKeyFromSharedPre(context)).child(Constant.NOTIFICATION_KEY).setValue("true");
                    SharedPreference.getInstance().storeData(context, Constant.NOTIFICATION_KEY, "true");
                } else {
                    Log.d("xdf", "unchk");
                    FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).child(StoreData.getUserKeyFromSharedPre(context)).child(Constant.NOTIFICATION_KEY).setValue("false");
                    SharedPreference.getInstance().storeData(context, Constant.NOTIFICATION_KEY, "false");
                }
            }
        });


    }


    @Override
    public int getItemCount() {

        return list.length;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView text_tv;
        Switch switch1;

        public MyViewHolder(View itemView) {
            super(itemView);

            //  time_tv = (TextView) itemView.findViewById(R.id.time_tv);
            text_tv = (TextView) itemView.findViewById(R.id.text_tv);
            switch1 = (Switch) itemView.findViewById(R.id.switch1);

            text_tv.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {


            if (clickRecyclerInterface != null) {

                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }
        }
    }
}



