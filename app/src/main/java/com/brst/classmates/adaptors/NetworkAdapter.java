package com.brst.classmates.adaptors;

import android.content.Context;
import android.content.Intent;
import android.graphics.Path;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.ProfileActivity;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.fragments.ChatProfile;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class NetworkAdapter extends RecyclerView.Adapter<NetworkAdapter.MyViewHolder> {

    List<HashMap<String, String>> contacts;
    Context context;
    List<String> courseKey;
    AddAssignmentFragment addAssignmentFragment = new AddAssignmentFragment();
    public static HashMap<String, String> hashMaps = null;
    List<String> courseKey_invite;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    int visibility;
    String option;

    public NetworkAdapter(List<HashMap<String, String>> contacts, Context context, List<String> courseKey, String option) {

        this.contacts = contacts;
        this.context = context;
        this.courseKey = courseKey;
        this.option = option;
        getInvites();

    }


    @Override
    public NetworkAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_contact, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NetworkAdapter.MyViewHolder holder, final int position) {

        HashMap<String, String> hashMap = contacts.get(position);
        String firstName = hashMap.get(Constant.FIRST_NAME);
        String lastName = hashMap.get(Constant.LAST_NAME);
        String collegeName = hashMap.get(Constant.COLLEGE_NAME);
        String location = hashMap.get(Constant.COUNTRY_NAME);
        String university = hashMap.get(Constant.UNIVERSITY_NAME);
        String contactProfile = "";
        String contactName = firstName + " " + lastName;

        // Reference to an image file in Cloud Storage
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("myimage");

        holder.number_tv.setVisibility(View.VISIBLE);
        if (hashMap.containsKey(Constant.PROFILE_PICTURE)) {

            contactProfile = hashMap.get(Constant.PROFILE_PICTURE);
        }

        if (contactProfile != null && !contactProfile.isEmpty()) {

            Glide.with(context).load(contactProfile).apply(RequestOptions.circleCropTransform().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
        } else {
            Glide.with(context).load(contactProfile).apply(new RequestOptions().placeholder(R.mipmap.user).error(R.mipmap.user)).into(holder.contactProfile_iv);
        }


        if (option.equals("college")) {
            holder.name_tv.setText(contactName);
            holder.number_tv.setVisibility(View.GONE);
            // holder.number_tv.setText(location);

        } else if (option.equals("location")) {
            holder.name_tv.setText(contactName);
            holder.number_tv.setVisibility(View.GONE);
            // holder.number_tv.setVisibility(View.GONE);
            // holder.number_tv.setText(location);

        } else if (option.equals("university")) {
            holder.name_tv.setText(contactName);
            // holder.number_tv.setText(contactName);
            holder.number_tv.setVisibility(View.GONE);

        } else {
            holder.name_tv.setText(contactName);
            holder.number_tv.setVisibility(View.GONE);
            // holder.number_tv.setText(location);

        }
        holder.tv_view_profile.setText("View profile");
        holder.contactCheck_cb.setChecked(Boolean.parseBoolean(contacts.get(position).get("boolean")));

        //  holder.chk_cb.setTag(studentList.get(position));
        holder.contactCheck_cb.setTag(contacts.get(position));

        holder.lay_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hashMaps = contacts.get(position);
                Boolean already_invited = false;
                String KEY = hashMaps.get("KEY").toString();
                for (int i = 0; i < dataList.size(); i++) {
                    if (dataList.get(i).get("to").equals(KEY) && dataList.get(i).get("from").equals(StoreData.getUserKeyFromSharedPre(context))) {
                        already_invited = true;
                    } /*else {


                    }*/
                }

                if (already_invited) {
                    ShowDialog.resultdialog(context, "User already exists in your contacts.");

                } else {

                   /* Intent mIntent = new Intent(context, ProfileActivity.class);
                    mIntent.putExtra("KEY", KEY);
                    mIntent.putExtra("FROM", "NETWORK");
                    context.startActivity(mIntent);*/

                    Bundle bundle=new Bundle();
                    bundle.putString("KEY", KEY);
                    bundle.putString("FROM", "NETWORK");
                    ReplaceFragment.replace((WelcomeActivity) context,new ChatProfile(),"chat_profile",bundle);

                }
            }
        });

        holder.contactCheck_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CheckBox cb = (CheckBox) v;
                //  HashMap<String, String> hashMap1 = (HashMap) cb.getTag();


                if (cb.isChecked()) {
                    contacts.get(position).put("boolean", Constant.TRUE_NAME);


                } else {
                    contacts.get(position).put("boolean", Constant.FALSE_NAME);

                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return contacts.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name_tv, number_tv, tv_view_profile;
        ImageView contactProfile_iv;
        CheckBox contactCheck_cb;

        LinearLayout lay_contact;

        public MyViewHolder(View itemView) {
            super(itemView);

            name_tv = (TextView) itemView.findViewById(R.id.name_tv);
            number_tv = (TextView) itemView.findViewById(R.id.number_tv);
            tv_view_profile = (TextView) itemView.findViewById(R.id.tv_view_profile);
            contactProfile_iv = (ImageView) itemView.findViewById(R.id.contactProfile_iv);
            contactCheck_cb = (CheckBox) itemView.findViewById(R.id.contactCheck_cb);
            lay_contact = (LinearLayout) itemView.findViewById(R.id.lay_contact);


            //   invite_tv.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {

            List<String> list = new ArrayList();
/*
            if (clickRecyclerInterface != null) {
                list.add(course_tv.getText().toString());
                list.add(courseKey.get(getAdapterPosition()));

                add_iv.setTag(list);
                //  course_tv.setTag(course_tv.getText().toString());


                delete_iv.setTag(courseKey.get(getAdapterPosition()));
                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }*/
        }
    }

 /*   public void addDataInList(List<HashMap<String, String>> dataList, List<String> courseKey) {
        this.dataList = dataList;
        this.courseKey = courseKey;

        Log.d("asd", courseKey + "");
        Log.d("asd", dataList + "");
    }*/


    public void addData(ArrayList<HashMap<String, String>> searchContactList) {

        this.contacts = searchContactList;

    }

    public List<HashMap<String, String>> getSelectedContactList() {

        return contacts;
    }

    private void getInvites() {

        FirebaseStorage firebaseStorage;
        FirebaseDatabase firebaseDatabase;
        DatabaseReference databaseReference;
        StorageReference storageReference;
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(Constant.INVITED_USER);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                dataList = new ArrayList<HashMap<String, String>>();
                courseKey_invite = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                    HashMap hashMap = (HashMap) dataSnapshot1.getValue();
                    try {
                        HashMap hashMap1 = new HashMap();
                        HashMap hashMap2 = new HashMap();

                        hashMap1.put("to", hashMap.get("to").toString());
                        hashMap1.put("request_status", hashMap.get("request_status").toString());
                        hashMap1.put("SENDER", hashMap.get("SENDER").toString());
                        hashMap1.put("from", hashMap.get("from").toString());
                        hashMap1.put("RECEIVER", hashMap.get("RECEIVER").toString());

                        hashMap1.put(Constant.COLLEGE_NAME, hashMap.get(Constant.COLLEGE_NAME).toString());
                        hashMap1.put(Constant.UNIVERSITY_NAME, hashMap.get(Constant.UNIVERSITY_NAME).toString());
                        hashMap1.put(Constant.COUNTRY_NAME, hashMap.get(Constant.COUNTRY_NAME).toString());


                        dataList.add(hashMap1);


                        courseKey_invite.add(dataSnapshot1.getKey());

                    } catch (Exception e) {

                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                // if(NetworkCheck.isNetworkAvailable(getContext())) {
                if (ShowDialog.dialog != null) {
                    if (ShowDialog.dialog.isShowing()) {
                        ShowDialog.hideDialog();
                    }
                }

            }
        });

    }
}
