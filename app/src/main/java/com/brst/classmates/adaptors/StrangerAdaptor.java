package com.brst.classmates.adaptors;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.fragments.ChatProfile;
import com.brst.classmates.fragments.ProfileFragment;
import com.brst.classmates.fragments.UserFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public class StrangerAdaptor extends RecyclerView.Adapter<StrangerAdaptor.MyViewHolder> {

    List<String> name;
    List<HashMap> userPic;
    List<String> userKey;

    String loginUserKey;

    ClickRecyclerInterface clickRecyclerInterface;

    Context context;

    public StrangerAdaptor(Context context, List<String> name, List<HashMap> userPic, List<String> userKey) {

        this.name = name;
        this.context = context;
        this.userPic = userPic;
        this.userKey = userKey;

        loginUserKey = StoreData.getUserKeyFromSharedPre(context);

    }

    @Override
    public StrangerAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_popup, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final StrangerAdaptor.MyViewHolder holder, final int position) {

        holder.user_tv.setText(name.get(position));

        final String key = userKey.get(position);

        Log.d("dsf", userKey + "------" + StoreData.getUserKeyFromSharedPre(context));

        getStrangerUser(key, holder);

        if (userKey.get(position).equals(loginUserKey)) {
            holder.report_tv.setText("You");
            holder.report_tv.setClickable(false);
        } else {


            holder.report_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (clickRecyclerInterface != null) {
                        //  course_ll.setTag(course_tv.getText().toString());
                        clickRecyclerInterface.onRecyClick(v, position);


                    }
                }
            });

        }


        if (!userPic.isEmpty() && !userPic.equals("null")) {

            HashMap hashMap = userPic.get(position);
            final String profile = hashMap.get(Constant.PROFILE_PICTURE).toString();
            final String college = hashMap.get(Constant.COLLEGE_NAME).toString();
            final String university = hashMap.get(Constant.UNIVERSITY_NAME).toString();
            final String country = hashMap.get(Constant.COUNTRY_NAME).toString();
            final String degree = hashMap.get(Constant.DEGREE_NAME).toString();
            final String year = hashMap.get(Constant.YEAR_NAME).toString();
            Log.d("data", profile + "--" + college + "---" + university + "---" + degree + "---" + country + "---" + year);
            //  Glide.with(context).load(userPic.get(position)).apply(RequestOptions.circleCropTransform()).into(holder.user_iv);
            Glide.with(context).load(profile).apply(RequestOptions.circleCropTransform()).into(holder.user_iv);

            holder.user_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putString("imagePath", profile);
                    bundle.putString(Constant.COLLEGE_NAME, college);
                    bundle.putString(Constant.UNIVERSITY_NAME, university);
                    bundle.putString(Constant.COUNTRY_NAME, country);
                    bundle.putString(Constant.DEGREE_NAME, degree);
                    bundle.putString(Constant.YEAR_NAME, year);
                    bundle.putString("userName", holder.user_tv.getText().toString());

                    ReplaceFragment.popBackStack((WelcomeActivity) context);
                    ReplaceFragment.replace((WelcomeActivity) context, new ChatProfile(), "chat_fragmant", bundle);
                }
            });
        }

    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    public void getStrangerUser(final String key, final MyViewHolder holder) {
        FirebaseDatabase.getInstance().getReference(Constant.REGISTERED_NAME).child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d("dsd", key);
                String stranger = dataSnapshot.child("stranger").getValue().toString();
                List stringList = new ArrayList<String>(Arrays.asList(stranger.split(",")));

                if (stringList.contains(loginUserKey)) {
                    holder.report_tv.setText("Reported");
                } else if (key.equals(loginUserKey)) {
                    holder.report_tv.setText("You");
                } else {
                    holder.report_tv.setText("Report");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    @Override
    public int getItemCount() {
        return name.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView user_tv, report_tv;
        ImageView user_iv;

        public MyViewHolder(View itemView) {
            super(itemView);

            user_tv = (TextView) itemView.findViewById(R.id.user_tv);
            report_tv = (TextView) itemView.findViewById(R.id.report_tv);
            user_iv = (ImageView) itemView.findViewById(R.id.user_iv);


            //report_tv.setOnClickListener(this);
        }

    }

    public void addData(List<HashMap> userPic) {
        this.userPic = userPic;
    }
}
