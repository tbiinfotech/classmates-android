package com.brst.classmates.adaptors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.classses.ShowDialog;
import com.brst.classmates.interfaces.OnClickEvent;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by brst-pc89 on 7/4/17.
 */

public class InviteEventAdaptor extends RecyclerView.Adapter<InviteEventAdaptor.MyViewHolder> {

    ArrayList<HashMap> data_list;
    Context context;
    OnClickEvent clickRecyclerInterface;
    Object videoThumb = null;

    public InviteEventAdaptor(ArrayList<HashMap> data_list, Context context) {

        this.data_list = data_list;
        this.context = context;
    }

    public void setClickListener(OnClickEvent clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_invite, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        HashMap hashMap = data_list.get(position);

        final String date = (String) hashMap.get("date");
        final String time = (String) hashMap.get("time");
        String location = (String) hashMap.get("location");
        String price = (String) hashMap.get("price");
        final String title = (String) hashMap.get("title");
        String desc = (String) hashMap.get("desc");
        String image_url = (String) hashMap.get("image_url");
        String status = (String) hashMap.get("status");

        holder.accept_tv.setTag(title+";"+location+";"+date+";"+time);



        String image_url1 = "", image_url2 = "", image_url3 = "";

        if (image_url != null && !image_url.isEmpty()) {

            holder.invite_ll.setVisibility(View.VISIBLE);
            holder.image_view.setVisibility(View.VISIBLE);
            String images[] = image_url.split(",");


            if (images.length == 3) {
                image_url1 = images[0];
                image_url2 = images[1];
                image_url3 = images[2];
            } else if (images.length == 2) {
                image_url1 = images[0];
                image_url2 = images[1];
                image_url3 = "";
            } else {
                image_url1 = images[0];

            }
        }

        else
        {
            holder.invite_ll.setVisibility(View.GONE);
            holder.image_view.setVisibility(View.GONE);
        }

        if(status.equals(Constant.ACCEPTED_NAME))
        {
            holder.accepted_tv.setVisibility(View.VISIBLE);
            holder.accepted_tv.setText(Constant.ACCEPTED_NAME);
            holder.accepted_tv.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));


            holder.linear_ll.setVisibility(View.GONE);

        }

        else if(status.equals(Constant.REJECTED_NAME))
        {
            holder.accepted_tv.setVisibility(View.VISIBLE);
            holder.accepted_tv.setText(Constant.REJECTED_NAME);
            holder.linear_ll.setVisibility(View.GONE);
            holder.accepted_tv.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
        }

        else
        {
            holder.accepted_tv.setVisibility(View.GONE);
            holder.linear_ll.setVisibility(View.VISIBLE);
            holder.accepted_tv.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }

        if (date != null && !date.isEmpty() && time != null && !time.isEmpty()) {

            holder.dateAndTimeTV.setVisibility(View.VISIBLE);
            holder.dateAndTimeTV.setText(date + " " + time);


            holder.dateAndTimeTV.setVisibility(View.VISIBLE);
        } else {
            holder.dateAndTimeTV.setVisibility(View.GONE);

        }

        if (location != null && !location.isEmpty()) {
            holder.location_ll.setVisibility(View.VISIBLE);
            holder.location_view.setVisibility(View.VISIBLE);
            holder.locationTV.setText(location);
        } else {
            holder.location_ll.setVisibility(View.GONE);
            holder.location_view.setVisibility(View.GONE);

        }
        if (desc != null && !desc.isEmpty()) {
            holder.descTV.setVisibility(View.VISIBLE);
            holder.desc_view.setVisibility(View.VISIBLE);
            holder.descTV.setText(desc);
        } else {
            holder.descTV.setVisibility(View.GONE);
            holder.desc_view.setVisibility(View.GONE);

        }
        if (price != null && !price.isEmpty()) {
            holder.priceTV.setVisibility(View.VISIBLE);
            holder.priceTV.setText(price);
        } else {
            holder.priceTV.setVisibility(View.GONE);

        }
        if (title != null && !title.isEmpty()) {
            holder.titleTV.setVisibility(View.VISIBLE);
            holder.titleTV.setText(title);
        } else {
            holder.titleTV.setVisibility(View.GONE);

        } if (image_url1 != null && !image_url1.isEmpty()) {

            if(holder.progress.getVisibility()==View.GONE)
            {
                holder.progress.setVisibility(View.VISIBLE);
            }

          //  Glide.with(context).load(image_url1).apply(RequestOptions.placeholderOf(R.mipmap.picture)).into(holder.pictureIV);

            Glide.with(context).load(image_url1).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.progress.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {


                    holder.progress.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.pictureIV);


            final String image=image_url1;

            holder.pictureIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShowDialog.getInstance().showImage(context,image,date+" "+time,title, "", "", (Bitmap) videoThumb);
                }
            });


        }

        if (image_url2 != null && !image_url2.isEmpty()) {

            if(holder.progress1.getVisibility()==View.GONE)
            {
                holder.progress1.setVisibility(View.VISIBLE);
            }


         //   Glide.with(context).load(image_url2).apply(RequestOptions.placeholderOf(R.mipmap.picture)).into(holder.picture1IV);

            Glide.with(context).load(image_url2).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.progress1.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    holder.progress1.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.picture1IV);

            final String image=image_url2;

            holder.picture1IV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShowDialog.getInstance().showImage(context,image,date+" "+time,title, "", "", (Bitmap) videoThumb);
                }
            });



        }

        else
        {
            Glide.with(context).load(image_url2).into(holder.picture1IV);
            holder.progress1.setVisibility(View.GONE);
        }
        if (image_url3 != null && !image_url3.isEmpty()) {

            if(holder.progress2.getVisibility()==View.GONE)
            {
                holder.progress2.setVisibility(View.VISIBLE);
            }

          //  Glide.with(context).load(image_url3).apply(RequestOptions.placeholderOf(R.mipmap.picture)).into(holder.picture2IV);


            Glide.with(context).load(image_url3).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.progress2.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    Log.d("sdf","dfd");
                    holder.progress2.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.picture2IV);

            final String image=image_url3;

            holder.picture2IV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ShowDialog.getInstance().showImage(context,image,date+" "+time,title, "", "", (Bitmap) videoThumb);
                }
            });


        }
        else
        {
            Glide.with(context).load(image_url3).into(holder.picture2IV);
            holder.progress2.setVisibility(View.GONE);
        }




    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView priceTV, titleTV, locationTV, dateAndTimeTV, descTV, accept_tv, reject_tv, maybe_tv,accepted_tv;
        ImageView pictureIV,picture1IV,picture2IV;
        LinearLayout linear_ll,invite_ll,location_ll;
        View location_view,desc_view,image_view;
        ProgressBar progress,progress1,progress2;


        public MyViewHolder(View itemView) {
            super(itemView);

            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            priceTV = (TextView) itemView.findViewById(R.id.priceTV);
            locationTV = (TextView) itemView.findViewById(R.id.locationTV);
            accepted_tv = (TextView) itemView.findViewById(R.id.accepted_tv);

            dateAndTimeTV = (TextView) itemView.findViewById(R.id.dateAndTimeTV);
            descTV = (TextView) itemView.findViewById(R.id.descTV);
            pictureIV = (ImageView) itemView.findViewById(R.id.pictureIV);
            picture1IV = (ImageView) itemView.findViewById(R.id.picture1IV);
            picture2IV = (ImageView) itemView.findViewById(R.id.picture2IV);


            accept_tv = (TextView) itemView.findViewById(R.id.accept_tv);
            reject_tv = (TextView) itemView.findViewById(R.id.reject_tv);
            maybe_tv = (TextView) itemView.findViewById(R.id.maybe_tv);
            linear_ll = (LinearLayout) itemView.findViewById(R.id.linear_ll);
            invite_ll = (LinearLayout) itemView.findViewById(R.id.invite_ll);
            location_ll = (LinearLayout) itemView.findViewById(R.id.location_ll);

            location_view = (View) itemView.findViewById(R.id.location_view);
            desc_view = (View) itemView.findViewById(R.id.desc_view);
            image_view = (View) itemView.findViewById(R.id.image_view);

            progress = (ProgressBar) itemView.findViewById(R.id.progress);
            progress1 = (ProgressBar) itemView.findViewById(R.id.progress1);
            progress2 = (ProgressBar) itemView.findViewById(R.id.progress2);


            accept_tv.setOnClickListener(this);
            reject_tv.setOnClickListener(this);
            maybe_tv.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            if (clickRecyclerInterface != null) {
                HashMap hashMap=data_list.get(getAdapterPosition());
               String type= (String) hashMap.get("type");
               String event_key= (String) hashMap.get("event_key");
                data_list.remove(getAdapterPosition());
                clickRecyclerInterface.onClickEventListener(v, getAdapterPosition(),type,event_key);
            }

        }
    }

    public void addList(ArrayList<HashMap> data_list)
    {
        this.data_list=data_list;
        notifyDataSetChanged();
    }
}
