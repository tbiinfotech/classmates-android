package com.brst.classmates.adaptors;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.classses.RecyclerViewEmptySupport;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.fragments.AddAssignmentFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.sharedpreference.StoreData;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc89 on 6/28/17.
 */

public class CommentReplyAdaptor extends RecyclerView.Adapter<CommentReplyAdaptor.MyViewHolder> {


    List<HashMap<String, String>> replyList;


    Context context;

    AddAssignmentFragment addAssignmentFragment = new AddAssignmentFragment();


    public CommentReplyAdaptor(List<HashMap<String, String>> replyList, Context context) {

        this.context = context;
        this.replyList = replyList;

    }

    @Override
    public CommentReplyAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_expandheader, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentReplyAdaptor.MyViewHolder holder, int position) {

        HashMap hashMap = replyList.get(position);
        String name = hashMap.get(Constant.USERMESS_NAME).toString();
        String time = hashMap.get(Constant.TIME_NAME).toString();

        if(hashMap.containsKey(Constant.IMAGE_NAME))
        {
            final String image = hashMap.get(Constant.IMAGE_NAME).toString();

            holder.message_tv.setVisibility(View.GONE);
            holder.comment_iv.setVisibility(View.VISIBLE);

            Glide.with(context).load(image).into(holder.comment_iv);

            holder.comment_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showImage(context,image);
                }
            });
        }
        else if(hashMap.containsKey(Constant.MESSAGE))
        {
            String message = hashMap.get(Constant.MESSAGE).toString();

            holder.message_tv.setVisibility(View.VISIBLE);
            holder.comment_iv.setVisibility(View.GONE);
            holder.message_tv.setText(message);
        }
        String localTime = SystemTime.getInstance().getLocalTime(time);


        holder.lblListHeader.setText(name);

        holder.time_tv.setText(localTime);

        holder.reply_tv.setVisibility(View.GONE);



    }


    @Override
    public int getItemCount() {
        return replyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView lblListHeader, message_tv, time_tv, reply_tv;
        View divider_view;
        ImageView comment_iv;
        LinearLayout course_ll;

        public MyViewHolder(View itemView) {
            super(itemView);

            lblListHeader = (TextView) itemView.findViewById(R.id.header_tv);
            message_tv = (TextView) itemView.findViewById(R.id.message_tv);
            time_tv = (TextView) itemView.findViewById(R.id.time_tv);
            reply_tv = (TextView) itemView.findViewById(R.id.reply_tv);
            comment_iv = (ImageView) itemView.findViewById(R.id.comment_iv);


        }

    }

    public void addData(List<HashMap<String, String>> replyList, CommentReplyAdaptor commentReplyAdaptor, RecyclerViewEmptySupport messageReply_rv) {

        this.replyList = replyList;

        messageReply_rv.smoothScrollToPosition(commentReplyAdaptor.getItemCount());
    }

    private void showImage(Context context, String image) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        dialog.show();
        ImageView chat_iv = (ImageView) dialog.findViewById(R.id.chat_iv);
        ImageView cross_iv = (ImageView) dialog.findViewById(R.id.cou_bck_iv);

        Glide.with(context).load(image).into(chat_iv);

        cross_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(true);
    }
}
