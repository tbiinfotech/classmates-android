package com.brst.classmates.adaptors;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.activity.WelcomeActivity;
import com.brst.classmates.classses.ReplaceFragment;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.brst.classmates.activity.WelcomeActivity.context;

/**
 * Created by brst-pc80 on 10/16/17.
 */

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<String> al;

    ClickRecyclerInterface clickRecyclerInterface;

    public DocumentAdapter(Context context,ArrayList<String> al)
    {
        this.context=context;
        this.al=al;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.document_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final String fileName=al.get(position);
        String file[]=fileName.split("/");
        holder.textViewFile.setText(file[file.length-1]);

        String extension=fileName.substring(fileName.length()-3);


        if (extension.equals("pdf"))
        {
            Glide.with(context).load(R.mipmap.ic_pdf).apply(new RequestOptions().centerCrop()).into(holder.imageViewFile);
        }
        else if (extension.equals("txt"))
        {
            Glide.with(context).load(R.mipmap.ic_txt).apply(new RequestOptions().centerCrop()).into(holder.imageViewFile);
        }
        else
        {
            Glide.with(context).load(R.mipmap.ic_doc).apply(new RequestOptions().centerCrop()).into(holder.imageViewFile);
        }



    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }


    @Override
    public int getItemCount() {
        return al.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView textViewFile;
        ImageView imageViewFile;
        LinearLayout linearLayoutFile;
        public MyViewHolder(View v) {
            super(v);
            textViewFile=(TextView)v.findViewById(R.id.textViewFile);
            imageViewFile=(ImageView) v.findViewById(R.id.imageViewFile);
            linearLayoutFile=(LinearLayout) v.findViewById(R.id.linearLayoutFile);

            linearLayoutFile.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {


            if (clickRecyclerInterface != null) {

                linearLayoutFile.setTag(al.get(getAdapterPosition()));
                clickRecyclerInterface.onRecyClick(view, getAdapterPosition());
            }
        }
    }
}
