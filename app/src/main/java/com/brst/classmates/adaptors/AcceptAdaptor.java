package com.brst.classmates.adaptors;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brst.classmates.R;
import com.brst.classmates.classses.AlarmReceiver;
import com.brst.classmates.classses.NotificationReceiver;
import com.brst.classmates.classses.SystemTime;
import com.brst.classmates.interfaces.ClickRecyclerInterface;
import com.brst.classmates.utility.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public class AcceptAdaptor extends RecyclerView.Adapter<AcceptAdaptor.MyViewHolder> {

    String name[];
    List<HashMap<String, String>> assignmentList;
    Context context;

    ClickRecyclerInterface clickRecyclerInterface;

    public AcceptAdaptor(Context context, List<HashMap<String, String>> assignmentList) {

        this.assignmentList = assignmentList;
        this.context = context;
    }

    @Override
    public AcceptAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_accept, parent, false);
        return new MyViewHolder(view);
    }

    public void setClickListener(ClickRecyclerInterface clickListener) {
        this.clickRecyclerInterface = clickListener;
    }

    @Override
    public void onBindViewHolder(AcceptAdaptor.MyViewHolder holder, int position) {

        HashMap<String, String> hashMap = assignmentList.get(position);
        final String assignName = hashMap.get(Constant.ASSIGN_NAME);
        final String assignType = hashMap.get(Constant.ASSIGNTYPE_NAME);
        final String assignStatus = hashMap.get(Constant.STATUS_NAME);
        final String assignDate = hashMap.get(Constant.ASSIGNDATE_NAME);
        final String adminName = hashMap.get(Constant.ASSIGNADMIN_NAME);
        final String adminFullName = hashMap.get(Constant.ASSIGNOWNERNAME_NAME);
        final String adminProfile = hashMap.get(Constant.ASSIOWNERPROFILE_NAME);
        final String accept = hashMap.get(Constant.ACCEPT_NAME);
        final String assignKey = hashMap.get(Constant.KEY_NAME);
        final String courseKey = hashMap.get(Constant.COURSE_KEY);



    /*    String[] dateFormat = assignDate.split(" ");

        String date = dateFormat[0];
        String month = dateFormat[1];
        String year = dateFormat[2];

        String yearformat[] = year.split("@");
        String yearName = yearformat[0];
        String time = yearformat[1];

        String timeformat[] = time.split(":");
        String hour = timeformat[0];
        String minute = timeformat[1];


        if (month.endsWith(",")) {
            month = month.substring(0, month.length() - 1);
        }
        int monthInt = 0;
        Calendar cal = Calendar.getInstance();

        try {
            cal.setTime(new SimpleDateFormat("MMM").parse(month));
            monthInt = cal.get(Calendar.MONTH);


        } catch (ParseException e) {
            e.printStackTrace();

        }*/

        holder.assign_tv.setText(assignName);
        holder.degree_tv.setText(assignType);
        holder.date_tv.setText(assignDate);
        holder.sender_tv.setText(adminFullName);

        holder.acceptassign_tv.setTag(assignKey+";"+assignDate+";"+assignName+";"+courseKey);
        holder.rejectassign_tv.setTag(assignKey+","+courseKey);

        Glide.with(context).load(adminProfile).apply(RequestOptions.circleCropTransform()).into(holder.sender_iv);

        if (accept.equals(Constant.FALSE_NAME)) {
            holder.accept_ll.setVisibility(View.VISIBLE);
            holder.accept_tv.setVisibility(View.GONE);
        } else {
            holder.accept_tv.setVisibility(View.VISIBLE);
            holder.accept_ll.setVisibility(View.GONE);

            if (accept.equals(Constant.ACCEPTED_NAME)) {
                holder.accept_tv.setText(Constant.ACCEPTED_NAME);


            } else {
                holder.accept_tv.setText(Constant.REJECTED_NAME);
            }
        }


    }


    @Override
    public int getItemCount() {
       // if(assignmentList.size()>0) {

            return assignmentList.size();
       // }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView assign_tv, degree_tv, date_tv, sender_tv, accept_tv, acceptassign_tv, rejectassign_tv;
        ImageView sender_iv;

        LinearLayout accept_ll;

        public MyViewHolder(View itemView) {
            super(itemView);

            assign_tv = (TextView) itemView.findViewById(R.id.assign_tv);
            degree_tv = (TextView) itemView.findViewById(R.id.degree_tv);
            date_tv = (TextView) itemView.findViewById(R.id.date_tv);
            sender_tv = (TextView) itemView.findViewById(R.id.sender_tv);
            sender_iv = (ImageView) itemView.findViewById(R.id.sender_iv);
            accept_tv = (TextView) itemView.findViewById(R.id.accept_tv);
            accept_ll = (LinearLayout) itemView.findViewById(R.id.accept_ll);
            acceptassign_tv = (TextView) itemView.findViewById(R.id.acceptassign_tv);
            rejectassign_tv = (TextView) itemView.findViewById(R.id.rejectassign_tv);

            acceptassign_tv.setOnClickListener(this);
            rejectassign_tv.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            if (clickRecyclerInterface != null) {

                clickRecyclerInterface.onRecyClick(v, getAdapterPosition());
            }
        }
    }

    public void addData(List<HashMap<String, String>> assignmentList) {
        this.assignmentList = assignmentList;
    }
}
